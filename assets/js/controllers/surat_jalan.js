var SuratJalan = {
 module: function () {
  return 'surat_jalan';
 },

 add: function () {
  window.location.href = url.base_url(SuratJalan.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(SuratJalan.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(SuratJalan.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(SuratJalan.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'invoice': $('#invoice').val(),
   'tanggal': $('#tanggal').val(),
   'total': $('#total').text(),
  };

  return data;
 },

 simpan: function (id) {
  var data = SuratJalan.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(SuratJalan.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(SuratJalan.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(SuratJalan.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(SuratJalan.module()) + "detail/" + id;
 },

 setThousandSparator: function () {
  $('#jumlah').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(SuratJalan.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(SuratJalan.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },
 
 setDate: function () {
  $('input#tanggal').datepicker({
   dateFormat: 'yy-mm-dd',
   todayHighlight: true,
  });
 },
 
 setSelect2: function () {
  $("#invoice").select2();
 },
 
 cetak: function (id) {
  window.open(url.base_url(SuratJalan.module()) + "printFaktur/" + id);
 },
 
 getFakturDetail: function (elm) {
  var faktur = $(elm).val();

  $.ajax({
   type: 'POST',
   data: {
    faktur: faktur
   },
   dataType: 'json',
   async: false,
   url: url.base_url(SuratJalan.module()) + "getFakturDetail",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();
    $('div.form-item').html(resp.view_item);
    $('label#total').text(resp.total);
   }
  });
 },
};

$(function () {
 SuratJalan.setDate();
 SuratJalan.setSelect2();
 SuratJalan.setThousandSparator();
});