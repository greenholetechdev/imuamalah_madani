var PegawaiKontrak = {
 module: function () {
  return 'pegawai_kontrak';
 },

 add: function () {
  window.location.href = url.base_url(PegawaiKontrak.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(PegawaiKontrak.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(PegawaiKontrak.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(PegawaiKontrak.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'pegawai': $('#pegawai').val(),
   'tanggal_awal': $('#tanggal_awal').val(),
   'tanggal_akhir': $('#tanggal_akhir').val(),
  };

  return data;
 },

 simpan: function (id) {
  var data = PegawaiKontrak.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(PegawaiKontrak.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(PegawaiKontrak.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(PegawaiKontrak.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(PegawaiKontrak.module()) + "detail/" + id;
 },

 setThousandSparator: function () {
  $('#jumlah').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(PegawaiKontrak.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(PegawaiKontrak.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },
 
 setDate: function () {
  $('input#tanggal_awal').datepicker({
   dateFormat: 'yy-mm-dd',
   todayHighlight: true,
   orientation:'bottom left'
  });
  $('input#tanggal_akhir').datepicker({
   dateFormat: 'yy-mm-dd',
   todayHighlight: true,
   orientation:'bottom left'
  });
 },
 
 setSelect2: function () {
  $("#pegawai").select2();
 },
 
 cetak: function (id) {
  window.open(url.base_url(PegawaiKontrak.module()) + "printFaktur/" + id);
 },
};

$(function () {
 PegawaiKontrak.setDate();
 PegawaiKontrak.setSelect2();
 PegawaiKontrak.setThousandSparator();
});