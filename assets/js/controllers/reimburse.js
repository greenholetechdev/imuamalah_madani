var Reimburse = {
 module: function () {
  return 'reimburse';
 },

 add: function () {
  window.location.href = url.base_url(Reimburse.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Reimburse.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Reimburse.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Reimburse.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'pegawai': $('#pegawai').val(),
   'tanggal': $('#tanggal').val(),
   'jumlah': $('#jumlah').val(),
   'keterangan': $('#keterangan').val(),
  };

  return data;
 },

 simpan: function (id) {
  var data = Reimburse.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Reimburse.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Reimburse.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Reimburse.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Reimburse.module()) + "detail/" + id;
 },

 setThousandSparator: function () {
  $('#jumlah').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Reimburse.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Reimburse.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },
 
 setDate: function () {
  $('input#tanggal').datepicker({
   dateFormat: 'yy-mm-dd',
   todayHighlight: true,
  });
 },
 
 setSelect2: function () {
  $("#pegawai").select2();
 },
 
 cetak: function (id) {
  window.open(url.base_url(Reimburse.module()) + "printFaktur/" + id);
 },
};

$(function () {
 Reimburse.setDate();
 Reimburse.setSelect2();
 Reimburse.setThousandSparator();
});