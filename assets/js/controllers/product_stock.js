var ProductStock = {
 module: function () {
  return 'product_stock';
 },

 add: function () {
  window.location.href = url.base_url(ProductStock.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(ProductStock.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(ProductStock.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(ProductStock.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'product': $('#product').val(),
   'stock': $('#stock').val(),
   'product_satuan': $('#product_satuan').val(),
   'gudang': $('#gudang').val(),
   'rak': $('#rak').val(),
  };

  return data;
 },

 simpan: function (id) {
  var data = ProductStock.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(ProductStock.module()) + "simpan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(ProductStock.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(ProductStock.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(ProductStock.module()) + "detail/" + id;
 },

 setThousandSparator: function () {
  $('#jumlah').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(ProductStock.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(ProductStock.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },
 
 setSelect2: function(){
  $("#product").select2();
  $("#product_satuan").select2();
  $("#gudang").select2();
  $("#rak").select2();
 },
 
 getSatuanProduk: function(elm){
  var product = $(elm).val();
  $.ajax({
   type: 'POST',
   data: {
    product: product
   },
   dataType: 'html',
   async: false,
   url: url.base_url(ProductStock.module()) + "getSatuanProduk",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },
   
   beforeSend: function () {
    message.loadingProses("Proses Retrieving Satuan...");
   },
   
   success: function (resp) {
    message.closeLoading();
    $('div#content-satuan').html(resp);
   }
  });
 }
};

$(function () {
 ProductStock.setSelect2();
 ProductStock.setThousandSparator();
});