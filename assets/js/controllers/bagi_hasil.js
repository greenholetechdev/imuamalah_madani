var BagiHasil = {
 module: function () {
  return 'bagi_hasil';
 },

 add: function () {
  window.location.href = url.base_url(BagiHasil.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(BagiHasil.module()) + "index";
 },

 search: function (elm, e) {
  var keyWord = $.trim($('input#date').val());
  $.ajax({
   type: 'POST',
   data: {
    keyword: keyWord
   },
   dataType: 'html',
   async: false,
   url: url.base_url(BagiHasil.module()) + "getData",
   success: function (resp) {
    $('div#data_detail').html(resp);
   }
  });
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'nama': $('#nama').val(),
   'no_hp': $('#no_hp').val()
  };

  return data;
 },

 simpan: function (id) {
  var data = BagiHasil.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(BagiHasil.module()) + "simpan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(BagiHasil.module()) + "detail" + '/' + resp.tipe_rumah;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(BagiHasil.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(BagiHasil.module()) + "detail/" + id;
 },

 setDate: function () {
  
  $('input#date').daterangepicker();
 }
};

$(function () {
 BagiHasil.setDate();
});