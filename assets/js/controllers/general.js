var General = {
 module: function () {
  return 'general';
 },

 add: function () {
  window.location.href = url.base_url(General.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(General.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(General.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(General.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'title': $('#title').val(),
   'alamat': $('#alamat').val(),
   'file_str': $('#file_str').val(),
  };

  return data;
 },

 simpan: function (id) {
  var data = General.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);
  formData.append('file', $('input#file').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(General.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(General.module()) + "detail" + '/' + resp.pengguna;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error(resp.message);
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(General.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(General.module()) + "detail/" + id;
 },

 checkAksesAktif: function (elm) {
  var akses = $(elm).val();
  if (akses != 1) {
   $.each($(elm).find('option'), function () {
    if ($(this).is(':selected')) {
     $(this).removeAttr('selected');
    }
   });

   $(elm).find('option:eq(0)').prop("selected");
   var msg = "<p>Silakan Upgrade Sistem Anda..</p>";
   msg += "<p>Mohon segera kontak <b><a href='http://www.support@greenholetech.com'>support@greenholetech.com</a></b></p>";
   bootbox.dialog({
    message: msg,
    size: 'large'
   });
  }
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(General.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(General.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 checkFile: function (elm) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'png' || type_file == 'jpg' || type_file == 'jpeg') {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat Png, Jpg, Jpeg');
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showLogo: function (elm, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    foto: $.trim($(elm).attr('file'))
   },
   dataType: 'html',
   async: false,
   url: url.base_url(General.module()) + "showLogo",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
//     size: 'large'
    });
   }
  });
 },
 
 gantiFile: function (elm, e) {
  e.preventDefault();
  var file_input = $('div#file_input');
  file_input.removeClass('hidden');
  
  $('div#detail_file').addClass('hidden');
 }
};