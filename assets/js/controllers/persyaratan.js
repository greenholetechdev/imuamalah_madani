var Persyaratan = {
 module: function () {
  return 'persyaratan';
 },

 add: function () {
  window.location.href = url.base_url(Persyaratan.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Persyaratan.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Persyaratan.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Persyaratan.module()) + "index";
   }
  }
 },

 getDetailBerkas: function () {
  var data = [];
  var tr = $('table#list_berkas').find('tbody').find('tr');
  $.each(tr, function () {
   var is_check = $(this).find('input#check').is(':checked');
   var nama_berkas = $(this).find('input#jenis_berkas').val();
   var status = 'N';
   
   if(is_check){
    status = 'Y';
   }
   
   data.push({
    'id' : $(this).attr('id') ? $(this).attr('id') : '',
    'nama_berkas': nama_berkas,
    'berkas_lama': $(this).find('label#berkas_lama').text(),
    'status' : status
   });
  });
  
  return data;
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'syarat': $('#syarat').val(),
   'keterangan': $('#keterangan').val(),
   'kategori_akad': $('#kategori_akad').val(),
   'jenis_akad': $('#jenis_akad').val(),
   'detail': Persyaratan.getDetailBerkas()
  };

  return data;
 },

 simpan: function (id) {
  var data = Persyaratan.getPostData();
  console.log(data);
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);
  
//  var inputBerkas = $('input[type="file"]');
//  var i = 1;
//  $.each(inputBerkas, function () {   
//   formData.append('berkas'+i, $(this).prop('files')[0]);
//   i+=1;
//  });

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Persyaratan.module()) + "simpan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Persyaratan.module()) + "detail" + '/' + resp.syarat;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Persyaratan.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Persyaratan.module()) + "detail/" + id;
 },

 removeBerkas: function (elm) {
  $(elm).closest('tr').remove();
 },

 addBerkas: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('td:eq(2)').html('<i class="mdi mdi-minus mdi-24px hover" onclick="Persyaratan.removeBerkas(this)"></i>');
  newTr.find('input#jenis_berkas').val('');
  tr.after(newTr);
 },

 checkDataBerkas: function (elm) {
  var check = $(elm).is(':checked');
  var tr = $(elm).closest('tr');
  if (check) {
   tr.find('label#status_berkas').addClass('hide');
   tr.find('input#berkas').removeClass('hide');
   tr.find('label#berkas_lama').removeClass('hide');
  } else {
   tr.find('label#status_berkas').removeClass('hide');
   tr.find('input#berkas').addClass('hide');
   tr.find('label#berkas_lama').addClass('hide');
  }
 },
 
 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Persyaratan.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Persyaratan.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 }
};