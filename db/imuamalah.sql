-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 12, 2019 at 02:52 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `imuamalah`
--

-- --------------------------------------------------------

--
-- Table structure for table `ansuran`
--

CREATE TABLE `ansuran` (
  `id` int(11) NOT NULL,
  `periode_tahun` varchar(50) DEFAULT NULL,
  `ansuran` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `akun` varchar(255) DEFAULT NULL,
  `nama_bank` varchar(255) DEFAULT NULL,
  `no_rekening` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `akun`, `nama_bank`, `no_rekening`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Tejo Kurniawan', 'Mandiri', '14132434', '2019-10-10', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `coa`
--

CREATE TABLE `coa` (
  `id` int(11) NOT NULL,
  `code` varchar(100) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coa`
--

INSERT INTO `coa` (`id`, `code`, `parent`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, '01', NULL, 'Income / Pemasukan', '2019-10-19', 1, '2019-10-22 02:55:00', 1, 0),
(2, '02', NULL, 'Cost of Revenue / Pengeluaran', '2019-10-22', 1, NULL, NULL, 0),
(3, '03', NULL, 'Expenses / Beban Biaya', '2019-10-22', 1, NULL, NULL, 0),
(4, '04', NULL, 'Payable / Utang usaha', '2019-10-22', 1, '2019-10-22 08:17:41', NULL, 0),
(5, '05', NULL, 'Equity / Ekuitas', '2019-10-22', 1, NULL, NULL, 0),
(6, '06', NULL, 'Aset Tidak Lancar / Aset Tetap', '2019-10-22', 1, NULL, NULL, 0),
(7, '07', NULL, 'Aset Lancar', '2019-10-22', 1, '2019-10-22 03:04:44', 1, 0),
(8, '08', NULL, 'Bank & Kas', '2019-10-22', 1, NULL, NULL, 0),
(9, '09', NULL, 'Liabilitas', '2019-10-22', 1, NULL, NULL, 0),
(10, '10', NULL, 'Receivable / Piutang usaha', '2019-10-22', NULL, '2019-10-22 08:18:05', NULL, 0),
(11, '11', NULL, 'Kasbon', '2019-10-24', 1, '2019-10-24 04:39:31', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `coa_type`
--

CREATE TABLE `coa_type` (
  `id` int(11) NOT NULL,
  `jenis` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coa_type`
--

INSERT INTO `coa_type` (`id`, `jenis`) VALUES
(1, 'KREDIT'),
(2, 'DEBIT');

-- --------------------------------------------------------

--
-- Table structure for table `data_notifikasi_tagihan`
--

CREATE TABLE `data_notifikasi_tagihan` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(155) DEFAULT NULL,
  `message` varchar(150) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `jenis` varchar(100) DEFAULT NULL COMMENT 'FAKTUR\nRETUR\nPENGADAAN',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `document_pengiriman`
--

CREATE TABLE `document_pengiriman` (
  `id` int(11) NOT NULL,
  `no_dokumen` varchar(155) DEFAULT NULL,
  `tgl_pengiriman` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dokumen_pengiriman_status`
--

CREATE TABLE `dokumen_pengiriman_status` (
  `id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'SENT\nBROKE\nRECEIVED',
  `document_pengiriman` int(11) NOT NULL,
  `keterangan` varchar(155) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `feature`
--

CREATE TABLE `feature` (
  `id` int(11) NOT NULL,
  `nama` varchar(155) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feature`
--

INSERT INTO `feature` (`id`, `nama`) VALUES
(1, 'Dashboard'),
(2, 'Pelanggan'),
(3, 'Produk'),
(4, 'Surat Jalan'),
(5, 'Retur Penjualan'),
(6, 'Rute Salesman'),
(7, 'Faktur Bersyarat'),
(8, 'Faktur'),
(9, 'Syarat Pembayaran'),
(10, 'Metode Pembayaran'),
(11, 'Vendor'),
(12, 'Pengadaan'),
(13, 'Retur Pembelian'),
(14, 'Tagihan'),
(15, 'Biaya Umum'),
(16, 'Gudang'),
(17, 'Rak'),
(18, 'Stok'),
(19, 'Bagan Akun'),
(20, 'Bank Akun'),
(21, 'Kas Bank'),
(22, 'Mata Uang'),
(23, 'Gaji'),
(24, 'Kategori Gaji'),
(25, 'Periode'),
(26, 'Kasbon'),
(27, 'Karyawan'),
(28, 'Kontrak'),
(29, 'E-Faktur'),
(30, 'Zakat Usaha'),
(31, 'Pembayaran'),
(32, 'Kasbon Bayar'),
(33, 'Reimburse'),
(34, 'Order');

-- --------------------------------------------------------

--
-- Table structure for table `general`
--

CREATE TABLE `general` (
  `id` int(11) NOT NULL,
  `title` varchar(150) DEFAULT NULL,
  `logo` text,
  `alamat` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general`
--

INSERT INTO `general` (`id`, `title`, `logo`, `alamat`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Perusahaan', 'blank.png', 'Surabaya', NULL, NULL, '2019-10-24 09:22:21', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `gudang`
--

CREATE TABLE `gudang` (
  `id` int(11) NOT NULL,
  `kode` varchar(155) DEFAULT NULL,
  `nama_gudang` varchar(255) DEFAULT NULL,
  `panjang` int(11) DEFAULT NULL,
  `lebar` int(11) DEFAULT NULL,
  `tinggi` int(11) DEFAULT NULL,
  `max_tonase` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gudang`
--

INSERT INTO `gudang` (`id`, `kode`, `nama_gudang`, `panjang`, `lebar`, `tinggi`, `max_tonase`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, '001', 'UMUM', 12, 12, 12, 100, NULL, NULL, '2019-11-26 02:54:31', 1, 0),
(2, 'GU001', 'Gudang Barang Retur', 12, 12, 12, 1000, '2019-12-10', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hari`
--

CREATE TABLE `hari` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hari`
--

INSERT INTO `hari` (`id`, `nama`) VALUES
(1, 'Senin'),
(2, 'Selasa'),
(3, 'Rabu'),
(4, 'Kamis'),
(5, 'Jumat'),
(6, 'Sabtu'),
(7, 'Minggu');

-- --------------------------------------------------------

--
-- Table structure for table `indetitas`
--

CREATE TABLE `indetitas` (
  `id` int(11) NOT NULL,
  `identitas` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `investor`
--

CREATE TABLE `investor` (
  `id` int(11) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `no_hp` varchar(45) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `alamat` text,
  `keterangan` varchar(150) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL,
  `jenis_pembayaran` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `potongan` int(11) NOT NULL,
  `ref` int(11) DEFAULT NULL COMMENT 'oder table',
  `pot_faktur` int(11) DEFAULT NULL,
  `no_faktur` varchar(155) DEFAULT NULL,
  `tanggal_faktur` datetime DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `jenis_pembayaran`, `pembeli`, `potongan`, `ref`, `pot_faktur`, `no_faktur`, `tanggal_faktur`, `tanggal_bayar`, `total`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(3, 2, 4, 1, 3, NULL, 'INV19DEC001', '2019-12-01 00:00:00', '2019-12-19', 53866, '2019-12-01', 1, '2019-12-03 16:00:52', 1, 0),
(4, 1, 5, 1, 5, NULL, 'INV19DEC002', '2019-12-01 00:00:00', '2019-12-01', 19063, '2019-12-01', 1, '2019-12-01 15:36:40', 1, 0),
(5, 1, 4, 1, 6, NULL, 'INV19DEC003', '2019-12-20 00:00:00', '2019-12-01', 210464, '2019-12-01', 1, NULL, NULL, 0),
(7, 1, 4, 1, NULL, NULL, 'INV19DEC004', '2019-12-17 00:00:00', '2019-12-12', 53866, '2019-12-02', 1, NULL, NULL, 0),
(8, 1, 4, 1, 17, NULL, 'INV19DEC005', '2019-12-26 00:00:00', '2019-12-03', 106182, '2019-12-03', 1, NULL, NULL, 0),
(9, 1, 6, 1, 16, NULL, 'INV19DEC006', '2019-12-10 00:00:00', '2019-12-03', 824833, '2019-12-03', 1, '2019-12-04 04:14:46', 1, 0),
(10, 1, 4, 1, NULL, 40000, 'INV19DEC007', '2019-12-25 00:00:00', '2019-12-25', 498660, '2019-12-04', 1, NULL, NULL, 0),
(11, 1, 5, 2, NULL, 10, 'INV19DEC008', '2019-12-04 00:00:00', '2019-12-19', 48479, '2019-12-04', 1, NULL, NULL, 0),
(12, 1, 4, 1, NULL, NULL, 'INV19DEC009', '2019-12-04 00:00:00', '2019-12-20', 360000, '2019-12-04', 1, NULL, NULL, 0),
(13, 1, 5, 1, NULL, NULL, 'INV19DEC010', '2019-12-05 00:00:00', '2019-12-05', 269330, '2019-12-04', 1, NULL, NULL, 0),
(15, 1, 4, 1, NULL, NULL, 'INV19DEC011', '2019-12-06 00:00:00', '2019-12-06', 969586, '2019-12-06', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_pot_product`
--

CREATE TABLE `invoice_pot_product` (
  `id` int(11) NOT NULL,
  `invoice_product` int(11) NOT NULL,
  `nilai` int(11) DEFAULT NULL,
  `potongan` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_pot_product`
--

INSERT INTO `invoice_pot_product` (`id`, `invoice_product`, `nilai`, `potongan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 5, 5000, 1, '2019-12-01', 1, NULL, NULL, 0),
(2, 10, 10000, 1, '2019-12-04', 1, NULL, NULL, 0),
(3, 11, 7000, 1, '2019-12-04', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_print`
--

CREATE TABLE `invoice_print` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `cetak` int(11) DEFAULT NULL,
  `approve` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_product`
--

CREATE TABLE `invoice_product` (
  `id` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `product_satuan` int(11) NOT NULL,
  `metode_bayar` int(11) NOT NULL,
  `bank` int(11) DEFAULT '0',
  `pajak` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_product`
--

INSERT INTO `invoice_product` (`id`, `invoice`, `product_satuan`, `metode_bayar`, `bank`, `pajak`, `qty`, `sub_total`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(3, 3, 5371, 1, 0, 1, 1, 53866, '2019-12-01', 1, NULL, NULL, 0),
(4, 4, 5395, 1, 0, 1, 1, 19063, '2019-12-01', 1, NULL, NULL, 0),
(5, 5, 5371, 1, 0, 1, 4, 210464, '2019-12-01', 1, NULL, NULL, 0),
(7, 7, 5371, 1, 0, 1, 1, 53866, '2019-12-02', 1, NULL, NULL, 0),
(8, 8, 5428, 1, 0, 1, 6, 106182, '2019-12-03', 1, NULL, NULL, 0),
(9, 9, 5407, 1, 0, 1, 3, 680235, '2019-12-03', 1, NULL, NULL, 0),
(10, 9, 5371, 1, 0, 1, 1, 43866, '2019-12-04', 1, NULL, NULL, 0),
(11, 9, 5371, 1, 0, 1, 2, 100732, '2019-12-04', 1, NULL, NULL, 0),
(12, 10, 5371, 1, 0, 1, 10, 538660, '2019-12-04', 1, NULL, NULL, 0),
(13, 11, 5371, 1, 0, 1, 1, 53866, '2019-12-04', 1, NULL, NULL, 0),
(14, 12, 5859, 1, 0, 1, 20, 360000, '2019-12-04', 1, NULL, NULL, 0),
(15, 13, 5371, 1, 0, 1, 5, 269330, '2019-12-04', 1, NULL, NULL, 0),
(17, 15, 5372, 1, 0, 1, 1, 969586, '2019-12-06', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_sisa`
--

CREATE TABLE `invoice_sisa` (
  `id` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_sisa`
--

INSERT INTO `invoice_sisa` (`id`, `invoice`, `jumlah`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(3, 3, 53866, '2019-12-01', 1, NULL, NULL, 0),
(4, 4, 19063, '2019-12-01', 1, NULL, NULL, 0),
(5, 5, 210464, '2019-12-01', 1, NULL, NULL, 0),
(7, 7, 53866, '2019-12-02', 1, NULL, NULL, 0),
(8, 8, 106182, '2019-12-03', 1, NULL, NULL, 0),
(9, 9, 824833, '2019-12-03', 1, '2019-12-04 04:14:46', 1, 0),
(10, 10, 498660, '2019-12-04', 1, NULL, NULL, 0),
(11, 11, 48479, '2019-12-04', 1, NULL, NULL, 0),
(12, 12, 360000, '2019-12-04', 1, NULL, NULL, 0),
(13, 13, 269330, '2019-12-04', 1, NULL, NULL, 0),
(15, 15, 969586, '2019-12-06', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_status`
--

CREATE TABLE `invoice_status` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'PAID\nDRAFT\nVALID',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_status`
--

INSERT INTO `invoice_status` (`id`, `user`, `invoice`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(3, 1, 3, 'DRAFT', '2019-12-01', 1, NULL, NULL, 0),
(4, 1, 4, 'DRAFT', '2019-12-01', 1, NULL, NULL, 0),
(5, 1, 5, 'DRAFT', '2019-12-01', 1, NULL, NULL, 0),
(7, 1, 7, 'DRAFT', '2019-12-02', 1, NULL, NULL, 0),
(8, 1, 8, 'DRAFT', '2019-12-03', 1, NULL, NULL, 0),
(9, 1, 9, 'DRAFT', '2019-12-03', 1, NULL, NULL, 0),
(10, 1, 10, 'DRAFT', '2019-12-04', 1, NULL, NULL, 0),
(11, 1, 11, 'DRAFT', '2019-12-04', 1, NULL, NULL, 0),
(12, 1, 12, 'DRAFT', '2019-12-04', 1, NULL, NULL, 0),
(13, 1, 13, 'DRAFT', '2019-12-04', 1, NULL, NULL, 0),
(15, 1, 15, 'DRAFT', '2019-12-06', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_akad`
--

CREATE TABLE `jenis_akad` (
  `id` int(11) NOT NULL,
  `jenis` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_akad`
--

INSERT INTO `jenis_akad` (`id`, `jenis`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Jenis Akad 1', '2019-10-11', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_notif_pengiriman`
--

CREATE TABLE `jenis_notif_pengiriman` (
  `id` int(11) NOT NULL,
  `jenis` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_notif_pengiriman`
--

INSERT INTO `jenis_notif_pengiriman` (`id`, `jenis`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`) VALUES
(1, 'Email', 1, NULL, NULL, NULL, NULL),
(2, 'Sms', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_pembayaran`
--

CREATE TABLE `jenis_pembayaran` (
  `id` int(11) NOT NULL,
  `jenis` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_pembayaran`
--

INSERT INTO `jenis_pembayaran` (`id`, `jenis`) VALUES
(1, 'TUNAI'),
(2, 'KREDIT');

-- --------------------------------------------------------

--
-- Table structure for table `jurnal`
--

CREATE TABLE `jurnal` (
  `id` int(11) NOT NULL,
  `no_jurnal` varchar(155) DEFAULT NULL,
  `ref` varchar(155) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurnal`
--

INSERT INTO `jurnal` (`id`, `no_jurnal`, `ref`, `tanggal`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(3, 'JURNAL19DEC001', 'INV19DEC001', '2019-12-01', '2019-12-01', 1, NULL, NULL, 0),
(4, 'JURNAL19DEC002', 'PROC19DEC001', '2019-12-01', '2019-12-01', 1, NULL, NULL, 0),
(5, 'JURNAL19DEC003', 'PROC19DEC002', '2019-12-01', '2019-12-01', 1, NULL, NULL, 0),
(6, 'JURNAL19DEC004', 'INV19DEC002', '2019-12-01', '2019-12-01', 1, NULL, NULL, 0),
(7, 'JURNAL19DEC005', 'INV19DEC003', '2019-12-01', '2019-12-01', 1, NULL, NULL, 0),
(9, 'JURNAL19DEC006', 'INV19DEC004', '2019-12-02', '2019-12-02', 1, NULL, NULL, 0),
(10, 'JURNAL19DEC007', 'INV19DEC005', '2019-12-03', '2019-12-03', 1, NULL, NULL, 0),
(11, 'JURNAL19DEC008', 'INV19DEC006', '2019-12-03', '2019-12-03', 1, NULL, NULL, 0),
(12, 'JURNAL19DEC009', 'INV19DEC007', '2019-12-04', '2019-12-04', 1, NULL, NULL, 0),
(13, 'JURNAL19DEC010', 'INV19DEC008', '2019-12-04', '2019-12-04', 1, NULL, NULL, 0),
(14, 'JURNAL19DEC011', 'INV19DEC009', '2019-12-04', '2019-12-04', 1, NULL, NULL, 0),
(15, 'JURNAL19DEC012', 'INV19DEC010', '2019-12-04', '2019-12-04', 1, NULL, NULL, 0),
(17, 'JURNAL19DEC013', 'INV19DEC011', '2019-12-06', '2019-12-06', 1, NULL, NULL, 0),
(18, 'JURNAL19DEC014', 'PROC19DEC003', '2019-12-10', '2019-12-10', 1, NULL, NULL, 0),
(19, 'JURNAL19DEC015', 'REPROC19DEC001', '2019-12-10', '2019-12-10', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_detail`
--

CREATE TABLE `jurnal_detail` (
  `id` int(11) NOT NULL,
  `jurnal` int(11) NOT NULL,
  `jurnal_struktur` int(11) NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurnal_detail`
--

INSERT INTO `jurnal_detail` (`id`, `jurnal`, `jurnal_struktur`, `jumlah`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(5, 3, 1, 53866, '2019-12-01', 1, NULL, NULL, 0),
(6, 3, 2, 53866, '2019-12-01', 1, NULL, NULL, 0),
(7, 4, 11, 2000000, '2019-12-01', 1, NULL, NULL, 0),
(8, 4, 12, 2000000, '2019-12-01', 1, NULL, NULL, 0),
(9, 5, 11, 2000000, '2019-12-01', 1, NULL, NULL, 0),
(10, 5, 12, 2000000, '2019-12-01', 1, NULL, NULL, 0),
(11, 6, 1, 19063, '2019-12-01', 1, NULL, NULL, 0),
(12, 6, 2, 19063, '2019-12-01', 1, NULL, NULL, 0),
(13, 7, 1, 210464, '2019-12-01', 1, NULL, NULL, 0),
(14, 7, 2, 210464, '2019-12-01', 1, NULL, NULL, 0),
(17, 9, 1, 53866, '2019-12-02', 1, NULL, NULL, 0),
(18, 9, 2, 53866, '2019-12-02', 1, NULL, NULL, 0),
(19, 10, 1, 106182, '2019-12-03', 1, NULL, NULL, 0),
(20, 10, 2, 106182, '2019-12-03', 1, NULL, NULL, 0),
(21, 11, 1, 824833, '2019-12-03', 1, '2019-12-04 04:14:46', 1, 0),
(22, 11, 2, 824833, '2019-12-03', 1, '2019-12-04 04:14:46', 1, 0),
(23, 12, 1, 498660, '2019-12-04', 1, NULL, NULL, 0),
(24, 12, 2, 498660, '2019-12-04', 1, NULL, NULL, 0),
(25, 13, 1, 48479, '2019-12-04', 1, NULL, NULL, 0),
(26, 13, 2, 48479, '2019-12-04', 1, NULL, NULL, 0),
(27, 14, 1, 360000, '2019-12-04', 1, NULL, NULL, 0),
(28, 14, 2, 360000, '2019-12-04', 1, NULL, NULL, 0),
(29, 15, 1, 269330, '2019-12-04', 1, NULL, NULL, 0),
(30, 15, 2, 269330, '2019-12-04', 1, NULL, NULL, 0),
(33, 17, 1, 969586, '2019-12-06', 1, NULL, NULL, 0),
(34, 17, 2, 969586, '2019-12-06', 1, NULL, NULL, 0),
(35, 18, 11, 150000, '2019-12-10', 1, NULL, NULL, 0),
(36, 18, 12, 150000, '2019-12-10', 1, NULL, NULL, 0),
(37, 19, 13, 30000, '2019-12-10', 1, NULL, NULL, 0),
(38, 19, 14, 30000, '2019-12-10', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_struktur`
--

CREATE TABLE `jurnal_struktur` (
  `id` int(11) NOT NULL,
  `feature` int(11) NOT NULL,
  `coa` int(11) NOT NULL,
  `coa_type` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurnal_struktur`
--

INSERT INTO `jurnal_struktur` (`id`, `feature`, `coa`, `coa_type`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 8, 10, 2, '2019-10-22', NULL, '2019-10-22 08:18:37', NULL, 0),
(2, 8, 1, 1, '2019-10-22', NULL, NULL, NULL, 0),
(3, 31, 10, 1, '2019-10-22', 1, '2019-10-22 16:24:37', 1, 0),
(4, 31, 8, 2, '2019-10-22', 1, NULL, NULL, 0),
(5, 23, 3, 2, '2019-10-23', 1, NULL, NULL, 0),
(6, 23, 8, 1, '2019-10-23', 1, NULL, NULL, 0),
(7, 26, 8, 1, '2019-10-24', 1, NULL, NULL, 0),
(8, 26, 10, 2, '2019-10-24', 1, NULL, NULL, 0),
(9, 32, 8, 2, '2019-10-24', 1, NULL, NULL, 0),
(10, 32, 10, 1, '2019-10-24', 1, NULL, NULL, 0),
(11, 12, 2, 2, '2019-10-25', 1, NULL, NULL, 0),
(12, 12, 8, 1, '2019-10-25', 1, NULL, NULL, 0),
(13, 13, 2, 1, '2019-10-26', 1, NULL, NULL, 0),
(14, 13, 8, 2, '2019-10-26', 1, NULL, NULL, 0),
(15, 5, 8, 1, '2019-10-29', 1, NULL, NULL, 0),
(16, 5, 10, 2, '2019-10-29', 1, NULL, NULL, 0),
(17, 33, 8, 1, '2019-10-30', 1, NULL, NULL, 0),
(18, 33, 2, 2, '2019-10-30', 1, NULL, NULL, 0),
(19, 15, 2, 2, '2019-11-01', 1, NULL, NULL, 0),
(20, 15, 8, 1, '2019-11-01', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kas`
--

CREATE TABLE `kas` (
  `id` int(11) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kasbon`
--

CREATE TABLE `kasbon` (
  `id` int(11) NOT NULL,
  `no_kasbon` varchar(155) DEFAULT NULL,
  `pegawai` int(11) NOT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kasbon_paid`
--

CREATE TABLE `kasbon_paid` (
  `id` int(11) NOT NULL,
  `payroll` int(11) NOT NULL,
  `kasbon` int(11) NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kasbon_payment`
--

CREATE TABLE `kasbon_payment` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(155) DEFAULT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kasbon_payment_item`
--

CREATE TABLE `kasbon_payment_item` (
  `id` int(11) NOT NULL,
  `kasbon_payment` int(11) NOT NULL,
  `kasbon` int(11) NOT NULL,
  `jumlah_bayar` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kasbon_sisa`
--

CREATE TABLE `kasbon_sisa` (
  `id` int(11) NOT NULL,
  `kasbon` int(11) NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kasbon_status`
--

CREATE TABLE `kasbon_status` (
  `id` int(11) NOT NULL,
  `kasbon` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'DRAFT\nPAID',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kategori_akad`
--

CREATE TABLE `kategori_akad` (
  `id` int(11) NOT NULL,
  `kategori` varchar(100) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kategori_product`
--

CREATE TABLE `kategori_product` (
  `id` int(11) NOT NULL,
  `kategori` varchar(100) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_product`
--

INSERT INTO `kategori_product` (`id`, `kategori`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'UMUM', '2019-10-09', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kerja_sama_internal`
--

CREATE TABLE `kerja_sama_internal` (
  `id` int(11) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `presentase` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_user_position`
--

CREATE TABLE `log_user_position` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_user_position`
--

INSERT INTO `log_user_position` (`id`, `user`, `lat`, `lng`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 3, -7.250445, 112.768845, NULL, NULL, NULL, NULL, 0),
(2, 3, -6.2, 106.816666, NULL, NULL, NULL, NULL, 0),
(3, 3, -7.161367, 113.482498, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `metode_bayar`
--

CREATE TABLE `metode_bayar` (
  `id` int(11) NOT NULL,
  `metode` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `metode_bayar`
--

INSERT INTO `metode_bayar` (`id`, `metode`) VALUES
(1, 'CASH'),
(2, 'TRANSFER');

-- --------------------------------------------------------

--
-- Table structure for table `metode_pembayaran`
--

CREATE TABLE `metode_pembayaran` (
  `id` int(11) NOT NULL,
  `metode` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `metode_pembayaran`
--

INSERT INTO `metode_pembayaran` (`id`, `metode`) VALUES
(1, 'Tagihan'),
(2, 'Vendor'),
(3, 'Lain - lain');

-- --------------------------------------------------------

--
-- Table structure for table `minggu`
--

CREATE TABLE `minggu` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `minggu`
--

INSERT INTO `minggu` (`id`, `nama`) VALUES
(1, 'Minggu 1'),
(2, 'Minggu 2'),
(3, 'Minggu 3'),
(4, 'Minggu 4');

-- --------------------------------------------------------

--
-- Table structure for table `notif_has_jenis_pengiriman`
--

CREATE TABLE `notif_has_jenis_pengiriman` (
  `id` int(11) NOT NULL,
  `notif_jatuh_tempo` int(11) NOT NULL,
  `jenis_notif_pengiriman` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notif_has_jenis_pengiriman`
--

INSERT INTO `notif_has_jenis_pengiriman` (`id`, `notif_jatuh_tempo`, `jenis_notif_pengiriman`, `createddate`, `createdby`, `updateddate`, `updatedby`) VALUES
(1, 1, 1, '2019-10-24', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `notif_jatuh_tempo`
--

CREATE TABLE `notif_jatuh_tempo` (
  `id` int(11) NOT NULL,
  `nama_notif` varchar(150) DEFAULT NULL,
  `jadwal_notif` int(11) DEFAULT NULL COMMENT 'Jumlah Hari',
  `jenis_notif` varchar(1) DEFAULT NULL COMMENT '+ = Lebih\n- = Kurang',
  `status` int(11) DEFAULT '0' COMMENT '0 : Tidak Aktif\n1 : Aktif',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notif_jatuh_tempo`
--

INSERT INTO `notif_jatuh_tempo` (`id`, `nama_notif`, `jadwal_notif`, `jenis_notif`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Notif Jatuh Tempo', 12, '+', 0, '2019-10-24', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `no_order` varchar(155) DEFAULT NULL,
  `potongan` int(11) NOT NULL,
  `pot_faktur` int(11) DEFAULT NULL,
  `tanggal_faktur` datetime DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `pembeli`, `no_order`, `potongan`, `pot_faktur`, `tanggal_faktur`, `total`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 4, 'OR19DEC001', 1, NULL, '2019-12-11 00:00:00', 0, '2019-12-01', 1, '2019-12-01 02:48:45', 1, 0),
(2, 4, 'OR19DEC002', 1, NULL, '2019-12-01 00:00:00', 0, '2019-12-01', 1, '2019-12-01 03:01:18', 1, 0),
(3, 4, 'OR19DEC003', 1, NULL, '2019-12-01 00:00:00', 53866, '2019-12-01', 1, NULL, NULL, 0),
(4, 4, 'OR19DEC004', 1, NULL, '2019-12-09 00:00:00', 478705, '2019-12-01', 1, NULL, NULL, 0),
(5, 5, 'OR19DEC005', 1, NULL, '2019-12-01 00:00:00', 19063, '2019-12-01', 1, NULL, NULL, 0),
(6, 4, 'OR19DEC006', 1, NULL, '2019-12-20 00:00:00', 210464, '2019-12-01', 1, NULL, NULL, 0),
(7, 4, 'OR19DEC007', 1, NULL, '2019-12-26 00:00:00', 629348, '2019-12-01', 1, NULL, NULL, 0),
(8, 4, 'OR19DEC008', 1, NULL, '2019-12-26 00:00:00', 2586770, '2019-12-01', 1, NULL, NULL, 0),
(9, 6, 'OR19DEC009', 1, NULL, '2019-12-18 00:00:00', 176, '2019-12-01', 1, '2019-12-01 23:56:11', 1, 0),
(10, 4, 'OR19DEC010', 1, 0, '2019-12-18 00:00:00', 1360342, '2019-12-01', 1, '2019-12-01 23:57:45', 1, 0),
(11, 4, 'OR19DEC011', 1, NULL, '2019-12-11 00:00:00', 0, '2019-12-01', 1, '2019-12-02 00:03:10', 1, 0),
(12, 4, 'OR19DEC012', 1, NULL, '2019-12-18 00:00:00', 38214, '2019-12-02', 1, '2019-12-02 00:05:07', 1, 0),
(13, 5, 'OR19DEC013', 1, NULL, '2019-12-11 00:00:00', 67672, '2019-12-02', 1, NULL, NULL, 0),
(14, 4, 'OR19DEC014', 3, NULL, '2019-12-02 00:00:00', 53000, '2019-12-02', 4, NULL, NULL, 0),
(15, 5, 'OR19DEC015', 3, NULL, '2019-12-02 00:00:00', 50000, '2019-12-02', 4, NULL, NULL, 0),
(16, 6, 'OR19DEC016', 1, NULL, '2019-12-25 00:00:00', 680235, '2019-12-03', 1, '2019-12-03 03:16:21', 1, 0),
(17, 4, 'OR19DEC017', 1, NULL, '2019-12-26 00:00:00', 106182, '2019-12-03', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_pot_product`
--

CREATE TABLE `order_pot_product` (
  `id` int(11) NOT NULL,
  `order_product` int(11) NOT NULL,
  `nilai` int(11) DEFAULT NULL,
  `potongan` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_pot_product`
--

INSERT INTO `order_pot_product` (`id`, `order_product`, `nilai`, `potongan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 7, 5000, 1, '2019-12-01', 1, NULL, NULL, 0),
(2, 10, 50000, 1, '2019-12-01', 1, NULL, NULL, 0),
(3, 11, 100000, 1, '2019-12-01', 1, NULL, NULL, 0),
(6, 14, 1000, 1, '2019-12-01', 1, NULL, NULL, 0),
(11, 19, 10, 2, '2019-12-02', 1, NULL, NULL, 0),
(12, 21, 866, 1, '2019-12-02', NULL, NULL, NULL, 0),
(13, 22, 3866, 1, '2019-12-02', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `id` int(11) NOT NULL,
  `product_satuan` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`id`, `product_satuan`, `order`, `qty`, `sub_total`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(3, 5371, 3, 1, 53866, '2019-12-01', 1, NULL, NULL, 0),
(4, 5391, 4, 1, 194674, '2019-12-01', 1, NULL, NULL, 0),
(5, 5411, 4, 1, 284031, '2019-12-01', 1, NULL, NULL, 0),
(6, 5395, 5, 1, 19063, '2019-12-01', 1, NULL, NULL, 0),
(7, 5371, 6, 4, 210464, '2019-12-01', 1, NULL, NULL, 0),
(8, 5391, 7, 2, 389348, '2019-12-01', 1, NULL, NULL, 0),
(9, 5956, 7, 4, 240000, '2019-12-01', 1, NULL, NULL, 0),
(10, 5998, 8, 50, 702250, '2019-12-01', 1, NULL, NULL, 0),
(11, 5422, 8, 40, 1884520, '2019-12-01', 1, NULL, NULL, 0),
(14, 5374, 10, 4, 1358600, '2019-12-01', 1, NULL, NULL, 0),
(19, 6114, 12, 3, 31474, '2019-12-02', 1, NULL, NULL, 0),
(20, 5424, 13, 4, 67672, '2019-12-02', 1, NULL, NULL, 0),
(21, 5371, 14, 1, 53866, '2019-12-02', NULL, NULL, NULL, 0),
(22, 5371, 15, 1, 50000, '2019-12-02', NULL, NULL, NULL, 0),
(23, 5407, 16, 3, 680235, '2019-12-03', 1, '2019-12-03 03:16:21', 1, 0),
(24, 5428, 17, 6, 106182, '2019-12-03', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE `order_status` (
  `id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`id`, `order`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 'DRAFT', '2019-12-01', 1, NULL, NULL, 0),
(2, 1, 'CONFIRM', '2019-12-01', 1, NULL, NULL, 0),
(4, 1, 'CANCEL', '2019-12-01', 1, NULL, NULL, 0),
(5, 2, 'DRAFT', '2019-12-01', 1, NULL, NULL, 0),
(6, 2, 'CONFIRM', '2019-12-01', 1, NULL, NULL, 0),
(8, 2, 'CANCEL', '2019-12-01', 1, NULL, NULL, 0),
(9, 3, 'DRAFT', '2019-12-01', 1, NULL, NULL, 0),
(10, 3, 'CONFIRM', '2019-12-01', 1, NULL, NULL, 0),
(11, 3, 'VALIDATE', '2019-12-01', 1, NULL, NULL, 0),
(12, 4, 'DRAFT', '2019-12-01', 1, NULL, NULL, 0),
(13, 5, 'DRAFT', '2019-12-01', 1, NULL, NULL, 0),
(14, 5, 'CONFIRM', '2019-12-01', 1, NULL, NULL, 0),
(15, 5, 'VALIDATE', '2019-12-01', 1, NULL, NULL, 0),
(16, 6, 'DRAFT', '2019-12-01', 1, NULL, NULL, 0),
(17, 6, 'CONFIRM', '2019-12-01', 1, NULL, NULL, 0),
(18, 6, 'VALIDATE', '2019-12-01', 1, NULL, NULL, 0),
(19, 7, 'DRAFT', '2019-12-01', 1, NULL, NULL, 0),
(20, 8, 'DRAFT', '2019-12-01', 1, NULL, NULL, 0),
(21, 9, 'DRAFT', '2019-12-01', 1, NULL, NULL, 0),
(22, 10, 'DRAFT', '2019-12-01', 1, NULL, NULL, 0),
(23, 11, 'DRAFT', '2019-12-01', 1, NULL, NULL, 0),
(24, 12, 'DRAFT', '2019-12-02', 1, NULL, NULL, 0),
(25, 12, 'CONFIRM', '2019-12-02', 1, NULL, NULL, 0),
(26, 13, 'DRAFT', '2019-12-02', 1, NULL, NULL, 0),
(27, 14, 'DRAFT', '2019-12-02', NULL, NULL, NULL, 0),
(28, 15, 'DRAFT', '2019-12-02', NULL, NULL, NULL, 0),
(29, 16, 'DRAFT', '2019-12-03', 1, NULL, NULL, 0),
(30, 17, 'DRAFT', '2019-12-03', 1, NULL, NULL, 0),
(31, 17, 'CONFIRM', '2019-12-03', 1, NULL, NULL, 0),
(32, 17, 'VALIDATE', '2019-12-03', 1, NULL, NULL, 0),
(33, 16, 'CONFIRM', '2019-12-03', 1, NULL, NULL, 0),
(34, 16, 'VALIDATE', '2019-12-03', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pajak`
--

CREATE TABLE `pajak` (
  `id` int(11) NOT NULL,
  `jenis` varchar(155) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `persentase` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pajak`
--

INSERT INTO `pajak` (`id`, `jenis`, `keterangan`, `persentase`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Bebas Pajak', 'Bebas tidak kena pajak', 0, NULL, NULL, NULL, NULL, 0),
(2, ' PPH 10 %', 'Kena Pajak 10 Persen', 10, '2019-10-10', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `partial_payment`
--

CREATE TABLE `partial_payment` (
  `id` int(11) NOT NULL,
  `no_faktur_bayar` varchar(150) DEFAULT NULL,
  `invoice` int(11) NOT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `partial_payment_status`
--

CREATE TABLE `partial_payment_status` (
  `id` int(11) NOT NULL,
  `partial_payment` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'LUNAS\nKREDIT',
  `sisa_hutang` int(11) DEFAULT '0',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `no_faktur_bayar` varchar(155) DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_item`
--

CREATE TABLE `payment_item` (
  `id` int(11) NOT NULL,
  `payment` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `jumlah_bayar` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payroll`
--

CREATE TABLE `payroll` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(155) DEFAULT NULL,
  `periode` int(11) NOT NULL,
  `pegawai` int(11) NOT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `keterangan` text,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payroll_category`
--

CREATE TABLE `payroll_category` (
  `id` int(11) NOT NULL,
  `jenis` varchar(45) DEFAULT NULL COMMENT 'POTONGAN\nTUNJANGAN\nDLL',
  `action` varchar(12) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payroll_category`
--

INSERT INTO `payroll_category` (`id`, `jenis`, `action`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Potongan BPJS', '-', NULL, NULL, '2019-10-13 05:29:56', 1, 0),
(2, 'Potongan Pajak', '-', '2019-10-13', 1, '2019-10-13 05:29:48', 1, 0),
(3, 'Gaji Pokok', '+', '2019-10-13', 1, '2019-10-13 05:28:33', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `payroll_item`
--

CREATE TABLE `payroll_item` (
  `id` int(11) NOT NULL,
  `payroll` int(11) NOT NULL,
  `payroll_category` int(11) NOT NULL,
  `keterangan` text,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payroll_workdays`
--

CREATE TABLE `payroll_workdays` (
  `id` int(11) NOT NULL,
  `payroll` int(11) NOT NULL,
  `keterangan` text,
  `qty_hari` int(11) DEFAULT NULL,
  `qty_jam` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `no_hp` varchar(45) DEFAULT NULL,
  `email` text,
  `alamat` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id`, `nama`, `no_hp`, `email`, `alamat`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Dodik Rismawan', '-', '-', '-', '2019-12-02', 1, NULL, NULL, 0),
(2, 'Dwi', '-', '-', '-', '2019-12-02', 1, NULL, NULL, 0),
(3, 'Risky', '-', '-', '-', '2019-12-02', 1, NULL, NULL, 0),
(4, 'Adit', '-', '-', '-', '2019-12-02', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai_kontrak`
--

CREATE TABLE `pegawai_kontrak` (
  `id` int(11) NOT NULL,
  `pegawai` int(11) NOT NULL,
  `tanggal_awal` date DEFAULT NULL,
  `tanggal_akhir` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_has_angsuran`
--

CREATE TABLE `pembayaran_has_angsuran` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `pembayaran_product` int(11) NOT NULL,
  `total_bayar` int(11) DEFAULT NULL,
  `tgl_angsuran` date DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_has_cash`
--

CREATE TABLE `pembayaran_has_cash` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `pembayaran_product` int(11) NOT NULL,
  `tgl_bayar` date DEFAULT NULL,
  `total_bayar` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_has_product`
--

CREATE TABLE `pembayaran_has_product` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `pembayaran_product` int(11) NOT NULL,
  `total_bayar` int(11) DEFAULT NULL,
  `tgl_angsuran` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_lain_lain`
--

CREATE TABLE `pembayaran_lain_lain` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(45) DEFAULT NULL,
  `jenis_pembayaran` int(11) NOT NULL,
  `jenis` varchar(150) DEFAULT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `file` text,
  `tgl_bayar` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_product`
--

CREATE TABLE `pembayaran_product` (
  `id` int(11) NOT NULL,
  `pembeli_has_product` int(11) NOT NULL,
  `status_pembayaran` int(11) NOT NULL,
  `tgl_bayar` datetime DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_tagihan`
--

CREATE TABLE `pembayaran_tagihan` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `tagihan` int(11) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `jenis_pembayaran` int(11) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `file` text,
  `tgl_bayar` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_vendor`
--

CREATE TABLE `pembayaran_vendor` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `vendor` int(11) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `jenis_pembayaran` int(11) NOT NULL,
  `file` text,
  `tgl_bayar` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli`
--

CREATE TABLE `pembeli` (
  `id` int(11) NOT NULL,
  `pembeli_kategori` int(11) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `alamat` varchar(150) DEFAULT NULL,
  `email` text,
  `no_hp` varchar(45) DEFAULT NULL,
  `foto` text,
  `kelurahan` varchar(255) DEFAULT NULL,
  `kecamatan` varchar(255) DEFAULT NULL,
  `kota` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembeli`
--

INSERT INTO `pembeli` (`id`, `pembeli_kategori`, `nama`, `alamat`, `email`, `no_hp`, `foto`, `kelurahan`, `kecamatan`, `kota`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(4, 2, 'SUMAR Bu', 'Jatim, Kab. Pacitan, PASAR PUNUNG', '-', '-', NULL, 'Tes', 'Tes', 'Tes', '2019-11-09', NULL, '2019-12-12 14:42:46', 1, 0),
(5, 1, 'TAMARA TK', 'Jatim, Kab. Pacitan, JL. RAYA PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(6, 1, 'DEWI SRI TK', 'Jatim, Kab. Pacitan, JL. RAYA PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(7, 1, 'WAWAN Mas', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(8, 1, 'YUNI Mbak', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(9, 1, 'PRAPTI Mbak', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(10, 1, 'PRIH Bu', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(11, 1, 'SUTINO Pak', 'Jatim, Kab. Pacitan, SUKODONO DONOROJO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(12, 1, 'SLAMET Pak', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(13, 1, 'NINIK Bu', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(14, 1, 'NURYANTO Mas', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(15, 1, 'TARJO Bu', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(16, 1, 'ANIK Bu', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(17, 1, 'YENI Mbak', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(18, 1, 'AMIN Bu', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(19, 1, 'WIDODO Pak', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(20, 1, 'GALAXY TK', 'Jatim, Kab. Pacitan, CANGKRING', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(21, 1, 'TEPENG Pak', 'Jatim, Kab. Pacitan, MBARAK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(22, 1, 'WAHYU Pak', 'Jatim, Kab. Pacitan, MBARAK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(23, 1, 'EKA PUTRA TK', 'Jatim, Kab. Pacitan, LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(24, 1, 'BAMBANG Mas TJM', 'Jatim, Kab. Pacitan, LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(25, 1, 'CINDY MART', 'Jatim, Kab. Pacitan, HADILUWIH', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(26, 1, 'GANDHI TK', 'Jatim, Kab. Pacitan, PAGEREJO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(27, 1, 'SAPODO TK', 'Jatim, Kab. Pacitan, PASAR LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(28, 1, 'US Mbak', 'Jatim, Kab. Pacitan, JL. RAYA LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(29, 1, 'PUTRA LANGGENG 1', 'Jatim, Kab. Pacitan, JL. RAYA LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(30, 1, 'CAHYO TK', 'Jatim, Kab. Pacitan, TAWANG SIDOMULYO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(31, 1, 'WARDI Bu', 'Jatim, Kab. Pacitan, PASAR LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(32, 1, 'JAYA MANDIRI', 'Jatim, Kab. Pacitan, PASAR LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(33, 1, 'JAYA MANDIRI 1', 'Jatim, Kab. Pacitan, ( TENGAH ) / PASAR LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(34, 1, 'JAYA MANDIRI PUTRA', 'Jatim, Kab. Pacitan, PASAR LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(35, 1, 'TUPANI TK', 'Jatim, Kab. Pacitan, JL. RAYA LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(36, 1, 'ETA Mbak Kios Daging', 'Jatim, Kab. Pacitan, PAGEREJO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(37, 1, 'WASONO Pak', 'Jatim, Kab. Pacitan, PASAR TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(38, 1, 'TONI Pak', 'Jatim, Kab. Pacitan, BUNGUR', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(39, 1, 'WINATUN Bu', 'Jatim, Kab. Pacitan, JATIGUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(40, 1, 'SUMBER MANIS TK', 'Jatim, Kab. Pacitan, JATIGUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(41, 1, 'SARTUMI Bu', 'Jatim, Kab. Pacitan, JATIGUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(42, 1, 'BONANZA TK', 'Jatim, Kab. Pacitan, KETRO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(43, 1, 'SUYATI Bu', 'Jatim, Kab. Pacitan, KETRO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(44, 1, 'PANTES TK', 'Jatim, Kab. Pacitan, KETRO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(45, 1, 'TUNAS HARAPAN TK', 'Jatim, Kab. Pacitan, KETRO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(46, 1, 'MITRA MULYA 1', 'Jatim, Kab. Pacitan, KETRO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(47, 1, 'REJEKI INDAH TK', 'Jatim, Kab. Pacitan, KETRO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(48, 1, 'SABAR MART', 'Jatim, Kab. Pacitan, PASAR TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(49, 1, 'SETYA JAYA MM', 'Jatim, Kab. Pacitan, PASAR TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(50, 1, 'MITRA MULYA 2', 'Jatim, Kab. Pacitan, PASAR TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(51, 1, 'SUMBER MANIS TK', 'Jatim, Kab. Pacitan, PASAR TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(52, 1, 'INTAN TK', 'Jatim, Kab. Pacitan, PASAR TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(53, 1, 'K MART', 'Jatim, Kab. Pacitan, BUNGUR', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(54, 1, 'SETYA JAYA GROSIR', 'Jatim, Kab. Pacitan, PASAR TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(55, 1, 'CANDRA TK', 'Jatim, Kab. Pacitan, KETRO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(56, 1, 'TITIK Bu', 'Jatim, Kab. Pacitan, JATIGUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(57, 1, 'LANGGENG TK', 'Jatim, Kab. Pacitan, SALAK TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(58, 1, 'PANIKEM Bu', 'Jatim, Kab. Pacitan, PASAR TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(59, 1, 'MAMIK Bu', 'Jatim, Kab. Pacitan, PASAR TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(60, 1, 'ANUGRAH JAYA TK', 'Jatim, Kab. Pacitan, BUNGUR', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(61, 1, 'MULYATI Bu', 'Jatim, Kab. Pacitan, PASAR ARJOWINANGUN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(62, 1, 'BOWO Pak', 'Jatim, Kab. Pacitan, PASAR ARJOWINANGUN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(63, 1, 'LASTRI Bu DALAM', 'Jatim, Kab. Pacitan, PASAR ARJOWINANGUN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(64, 1, 'PANCA JAYA 2', 'Jatim, Kab. Pacitan, PASAR ARJOWINANGUN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(65, 1, 'TOMIKO MART', 'Jatim, Kab. Pacitan, JL. RAYA KEBONAGUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(66, 1, 'UMI Mbak', 'Jatim, Kab. Pacitan, PASAR ARJOWINANGUN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(67, 1, 'SRI DAWET Bu', 'Jatim, Kab. Pacitan, PASAR ARJOWINANGUN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(68, 1, 'YUYUN DELIMA Bu', 'Jatim, Kab. Pacitan, PASAR ARJOWINANGUN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(69, 1, 'MUL Mbak', 'Jatim, Kab. Pacitan, PASAR ARJOWINANGUN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(70, 1, 'TUNJUNG Bu', 'Jatim, Kab. Pacitan, BELAKANG PASAR ARJOWINANGUN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(71, 1, 'TENANG TK', 'Jatim, Kab. Pacitan, JL. MAGHRIBI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(72, 1, 'PADI MURNI TK', 'Jatim, Kab. Pacitan, ARJOWINANGUN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(73, 1, 'SUMBER BARU TK', 'Jatim, Kab. Pacitan, PASAR ARJOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(74, 1, 'SURANI Mas', 'Jatim, Kab. Pacitan, JL. RY PACITAN PONOROGO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(75, 1, 'DENEZA TK', 'Jatim, Kab. Pacitan, JL. RY PACITAN PONOROGO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(76, 1, 'MERPATI TK', 'Jatim, Kab. Pacitan, PASAR ARJOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(77, 1, 'BENTI Mbak', 'Jatim, Kab. Pacitan, PASAR ARJOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(78, 1, 'FAZA JAYA TK', 'Jatim, Kab. Pacitan, SEDAYU', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(79, 1, 'TOMIKO MART', 'Jatim, Kab. Pacitan, JL. RY PACITAN PONOROGO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(80, 1, 'ENDRO Pak', 'Jatim, Kab. Pacitan, DEPAN POLSEK TELENG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(81, 1, 'TOYO Bu', 'Jatim, Kab. Pacitan, JL. PANCER DOOR', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(82, 1, 'SITI Bu', 'Jatim, Kab. Pacitan, PASAR MINULYO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(83, 1, 'MITRA SWALAYAN', 'Jatim, Kab. Pacitan, PUCANGSEWU', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(84, 1, 'POJOK DURO TK', 'Jatim, Kab. Pacitan, DEPAN POLSEK TELENG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(85, 1, 'FAREL TK', 'Jatim, Kab. Pacitan, BLIMBING', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(86, 1, 'ABIMAT', 'Jatim, Kab. Pacitan, JL. KARTINI No. 24', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(87, 1, 'ENGGAL SATU', 'Jatim, Kab. Pacitan, JL. JEND SUDIRMAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(88, 1, 'ENGGAL DUA', 'Jatim, Kab. Pacitan, JL. JEND SUDIRMAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(89, 1, 'PERMATA MM', 'Jatim, Kab. Pacitan, JL. GATOT SUBROTO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(90, 1, 'SUSILO ALIP TK', 'Jatim, Kab. Pacitan, GANTUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(91, 1, 'BUDI ASIH TK', 'Jatim, Kab. Pacitan, PACITAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(92, 1, 'PAWIT MULYA TK', 'Jatim, Kab. Pacitan, PLOSO PACITAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(93, 1, 'MORO SENENG', 'Jatim, Kab. Pacitan, PACITAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(94, 1, 'MANSYUR SUMBER BARU', 'Jatim, Kab. Pacitan, GANTUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(95, 1, 'AN NUR TK', 'Jatim, Kab. Pacitan, TREMAS', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(96, 1, 'AL MUBAROK TK', 'Jatim, Kab. Pacitan, TREMAS', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(97, 1, 'MENEER NY', 'Jatim, Kab. Pacitan, SAMPING PASAR ARJOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(98, 1, 'HUDA PAK', 'Jatim, Kab. Pacitan, PASAR ARJOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(99, 1, 'ASSALAM TK', 'Jatim, Kab. Pacitan, TREMAS', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(100, 1, 'MUSRIFAH BU', 'Jatim, Kab. Pacitan, TREMAS', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(101, 1, 'MADYA TK', 'Jatim, Kab. Pacitan, LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(102, 1, 'SUKIRMAN PAK', 'Jatim, Kab. Pacitan, JL RY LOROK SUDIMORO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(103, 1, 'BERO BU', 'Jatim, Kab. Pacitan, DEPAN PASAR LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(104, 1, 'CBSA', 'Jatim, Kab. Pacitan, JL RY LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(105, 1, 'SRI BU', 'Jatim, Kab. Pacitan, PASAR KETRO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(106, 1, 'REJEKI MULYA TK', 'Jatim, Kab. Pacitan, KETRO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(107, 1, 'RUSMINAH BU', 'Jatim, Kab. Pacitan, PASAR TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(108, 1, 'DIDIT PAK', 'Jatim, Kab. Pacitan, PASAR TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(109, 1, 'DIRGAHAYU Tk', 'Jatim, Kab. Pacitan, MENADI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(110, 1, 'GARUDA SEJAHTERA UD', 'Jatim, Kab. Pacitan, JL RY PACITAN TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(111, 1, 'POJOK Tk', 'Jatim, Kab. Pacitan, PERTIGAAN PASAR ARJOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(112, 1, 'GOTRI Bu', 'Jatim, Kab. Pacitan, ARJOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(113, 1, 'SUMBER MAKMUR UD', 'Jatim, Kab. Pacitan, SEMO ARJOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(114, 1, 'EMI Bu', 'Jatim, Kab. Pacitan, ARJOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(115, 1, 'MUSLIHAH Bu', 'Jatim, Kab. Pacitan, JATIMALANG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(116, 1, 'FAUZI Bu', 'Jatim, Kab. Pacitan, SEDAYU ARJOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(117, 1, 'ISTIQOMAH Bu', 'Jatim, Kab. Pacitan, DEPAN PASAR ARJOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(118, 1, 'KANTOR (                       )', 'Jatim, Kab. Pacitan, New Costumer', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(119, 1, 'NIKMAH Bu', 'Jatim, Kab. Pacitan, ARJOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(120, 1, 'YAYUK Bu', 'Jatim, Kab. Pacitan, ARJOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(121, 1, 'DEPI Mbak', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(122, 1, 'ATIN Bu', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(123, 1, 'SUPRIHATIN Bu', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(124, 1, 'MIN Pak', 'Jatim, Kab. Pacitan, DEPAN STADION ARJOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(125, 1, 'GARUDA TK', 'Jatim, Kab. Pacitan, SAMPING PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(126, 1, 'SIS Bu', 'Jatim, Kab. Pacitan, TANGKLUK DONOROJO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(127, 1, 'NARTI Bu', 'Jatim, Kab. Pacitan, BELAKANG PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(128, 1, 'NUR Mbak', 'Jatim, Kab. Pacitan, TANGKLUK DONOROJO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(129, 1, 'SAFAAT Tk', 'Jatim, Kab. Pacitan, BUBAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(130, 1, 'RENI Bu', 'Jatim, Kab. Pacitan, TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(131, 1, 'PANTES MART', 'Jatim, Kab. Pacitan, PASAR KETRO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(132, 1, 'INDAH Mbak', 'Jatim, Kab. Pacitan, JL RY LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(133, 1, 'RINI Bu', 'Jatim, Kab. Pacitan, JL RY LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(134, 1, 'TIN Bu', 'Jatim, Kab. Pacitan, Dalam Pasar Lorok', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(135, 1, 'UT Bu', 'Jatim, Kab. Pacitan, Dalam Pasar Lorok', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(136, 1, 'YULI Mbak', 'Jatim, Kab. Pacitan, Pasar Sayur Arjowinangun', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(137, 1, 'SRI YULI Mbak', 'Jatim, Kab. Pacitan, Pasar Sayur Arjowinangun', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(138, 1, 'KUSUMA Tk', 'Jatim, Kab. Pacitan, JL RY KAYEN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(139, 1, 'SEJATI Tk', 'Jatim, Kab. Pacitan, JL RY KEBONAGUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(140, 1, 'SRI LANGGENG Tk', 'Jatim, Kab. Pacitan, Pasar Arjowinangun', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(141, 1, 'SABAR Tk / IIN Mbak', 'Jatim, Kab. Pacitan, Pasar Arjowinangun', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(142, 1, 'ANDI Mas', 'Jatim, Kab. Pacitan, Pasar Arjowinangun', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(143, 1, 'KASMIN Tk', 'Jatim, Kab. Pacitan, Pasar Arjowinangun', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(144, 1, 'RENI Mbak', 'Jatim, Kab. Pacitan, Pasar Arjowinangun', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(145, 1, 'PONO Bu', 'Jatim, Kab. Pacitan, Pasar Arjowinangun', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(146, 1, 'MURNI Bu', 'Jatim, Kab. Pacitan, Pasar Arjowinangun', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(147, 1, 'SITI BUMBON', 'Jatim, Kab. Pacitan, Pasar Arjowinangun', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(148, 1, 'KURNIA Tk', 'Jatim, Kab. Pacitan, Pasar Arjowinangun', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(149, 1, 'UUN Bu / ANEKA Tk', 'Jatim, Kab. Pacitan, Pasar Arjowinangun', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(150, 1, 'SLAMET Bu', 'Jatim, Kab. Pacitan, Pasar Arjosari', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(151, 1, 'SURYANI Bu', 'Jatim, Kab. Pacitan, Pasar Arjosari', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(152, 1, 'Pur Bu', 'Jatim, Kab. Pacitan, Dalam Pasar Arjosari', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(153, 1, 'PUJI Bu', 'Jatim, Kab. Pacitan, Pasar Arjosari', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(154, 1, 'PUTRA GAMPIL Tk', 'Jatim, Kab. Pacitan, JL RY PRINGKUKU - PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(155, 1, 'ANIK Bu', 'Jatim, Kab. Pacitan, SEDENG PRINGKUKU', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(156, 1, 'ASRA Pak', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(157, 1, 'INDAH Mbak', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(158, 1, 'RINI Bu', 'Jatim, Kab. Pacitan, GONDOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(159, 1, 'RUMINI', 'Jatim, Kab. Pacitan, GONDOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(160, 1, 'WIDODO Pak', 'Jatim, Kab. Pacitan, PASAR DONOROJO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(161, 1, 'SRI RAHAYU', 'Jatim, Kab. Pacitan, GONDOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(162, 1, 'BERKAH MULYA Tk', 'Jatim, Kab. Pacitan, GONDOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(163, 1, 'MINIARTO Tk', 'Jatim, Kab. Pacitan, PRINGKUKU', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(164, 1, 'PALUPI Bu', 'Jatim, Kab. Pacitan, PASAR DONOROJO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(165, 1, 'WIJI Bu', 'Jatim, Kab. Pacitan, PASAR MINULYO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(166, 1, 'NUR Bu', 'Jatim, Kab. Pacitan, PASAR MINULYO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(167, 1, 'MITRA MULYA 3', 'Jatim, Kab. Pacitan, WONOANTI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(168, 1, 'MUSRINI Bu', 'Jatim, Kab. Pacitan, KETRO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(169, 1, 'SURYA MART', 'Jatim, Kab. Pacitan, JL RY LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(170, 1, 'WID Bu', 'Jatim, Kab. Pacitan, JL Ry LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(171, 1, 'SRI Bu', 'Jatim, Kab. Pacitan, LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(172, 1, 'GUSMAT Pak', 'Jatim, Kab. Pacitan, Jl Agus Salim, Tanjung Sari Rt/Rw : 001/001', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(173, 1, 'RINA Bu', 'Jatim, Kab. Pacitan, DALAM PASAR ARJOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(174, 1, 'RINA Mbak SM', 'Jatim, Kab. Pacitan, PASAR ARJOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(175, 1, 'PRATIWI Tk', 'Jatim, Kab. Pacitan, GEGERAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(176, 1, 'TANTI Mbak', 'Jatim, Kab. Pacitan, KEBONDALEM', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(177, 1, 'MANUNGGAL JAYA Tk', 'Jatim, Kab. Pacitan, SAMPING PASAR KEBONDALEM', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(178, 1, 'ARI Mbak', 'Jatim, Kab. Pacitan, PRINGKUKU', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(179, 1, 'MAKMUR MURNI Tk', 'Jatim, Kab. Pacitan, PASAR MINULYO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(180, 1, 'SINAR PAHALA Tk', 'Jatim, Kab. Pacitan, DEPAN PASAR PRINGKUKU', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(181, 1, 'PUTRA MAPAN Tk', 'Jatim, Kab. Pacitan, PRINGKUKU', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(182, 1, 'SUMINI Bu', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(183, 1, 'YUNI Mbak', 'Jatim, Kab. Pacitan, DONOROJO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(184, 1, 'WATIK Bu', 'Jatim, Kab. Pacitan, NGADIREJAN PRINGKUKU', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(185, 1, 'INTI Tk / Bu Musani', 'Jatim, Kab. Pacitan, Jl Cut Mutia Krajan Lor Ploso', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(186, 1, 'NUR Bu', 'Jatim, Kab. Pacitan, Jl Cut Mutia Ploso', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(187, 1, 'TRENDY Tk', 'Jatim, Kab. Pacitan, Jl Wahid Hasim Ploso', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(188, 1, 'SITI Bu', 'Jatim, Kab. Pacitan, BAREHAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(189, 1, 'MAMIK Bu', 'Jatim, Kab. Pacitan, PASAR MINULYO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(190, 1, 'TURINI Bu', 'Jatim, Kab. Pacitan, PASAR MINULYO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(191, 1, 'TATIK Bu', 'Jatim, Kab. Pacitan, PASAR MINULYO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(192, 1, 'ENI Bu', 'Jatim, Kab. Pacitan, TANJUNGSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(193, 1, 'KUSUMA Tk', 'Jatim, Kab. Pacitan, SUMBERHARJO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(194, 1, 'KARIM Tk', 'Jatim, Kab. Pacitan, BANGUNSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(195, 1, 'PURWO WIDODO Tk', 'Jatim, Kab. Pacitan, PAGEREJO LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(196, 1, 'WAHYONO', 'Jatim, Kab. Pacitan, SIRNOBOYO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(197, 1, 'YANTO Bu', 'Jatim, Kab. Pacitan, TEGALOMBO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(198, 1, 'TOINEM Bu', 'Jatim, Kab. Pacitan, TEGALOMBO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(199, 1, 'DAMAS Pak', 'Jatim, Kab. Pacitan, PASAR TEGALOMBO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(200, 1, 'WIN Bu', 'Jatim, Kab. Pacitan, PASAR TEGALOMBO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(201, 1, 'WATIK Bu', 'Jatim, Kab. Pacitan, NGRECO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(202, 1, 'SUNARMI Bu', 'Jatim, Kab. Pacitan, NGRECO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(203, 1, 'RATMI Bu Lurah', 'Jatim, Kab. Pacitan, TEGALOMBO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(204, 1, 'BIMA PUTRA Tk', 'Jatim, Kab. Pacitan, GEMAHARJO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(205, 1, 'BARBERICE ABUTEKE RESTO', 'Jatim, Kab. Pacitan, JL. Ikan Nila No 21 Rt 02/Rw 07 Ngampel', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(206, 1, 'HARYATI Bu', 'Jatim, Kab. Pacitan, PRINGKUKU', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(207, 1, 'SUNAR Pak / Tk CILIK', 'Jatim, Kab. Pacitan, PRINGKUKU', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(208, 1, 'WATIK Bu', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(209, 1, 'UDIN Pak', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(210, 1, 'DEWI Bu', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(211, 1, 'KENCANA Tk', 'Jatim, Kab. Pacitan, GONDOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(212, 1, 'MITRA MULIA / Dua M', 'Jatim, Kab. Pacitan, GONDOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(213, 1, 'ERJE Tk', 'Jatim, Kab. Pacitan, GONDOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(214, 1, 'MAS DONA', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(215, 1, 'MANDO Tk', 'Jatim, Kab. Pacitan, JL RY TELENG RIA', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(216, 1, 'TARI Bu', 'Jatim, Kab. Pacitan, PASAR MINULYO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(217, 1, 'MUKIN Bu', 'Jatim, Kab. Pacitan, PASAR MINULYO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(218, 1, 'SEMI Bu', 'Jatim, Kab. Pacitan, LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(219, 1, 'PUTRA SAPODO Tk', 'Jatim, Kab. Pacitan, DEPAN PASAR LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(220, 1, 'WIWIT Mbak', 'Jatim, Kab. Pacitan, JATIMALANG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(221, 1, 'YUNI Mbak', 'Jatim, Kab. Pacitan, GEDANGAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(222, 1, 'BERKAH ABADI', 'Jatim, Kab. Pacitan, BANJARSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(223, 1, 'NOVITA Tk', 'Jatim, Kab. Pacitan, GONDOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(224, 1, 'TARNO Pak', 'Jatim, Kab. Pacitan, DALAM PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(225, 1, 'PANDU JAYA Tk', 'Jatim, Kab. Pacitan, PLOSO PACITAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(226, 1, 'YANI Bu', 'Jatim, Kab. Pacitan, PASAR MINULYO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(227, 1, 'FIRMAN Pak', 'Jatim, Kab. Pacitan, PASAR MINULYO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(228, 1, 'WARSITO Bu', 'Jatim, Kab. Pacitan, PASAR MINULYO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(229, 1, 'MARTI Bu', 'Jatim, Kab. Pacitan, PASAR MINULYO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(230, 1, 'MUHAMMAD Pak', 'Jatim, Kab. Pacitan, PASAR MINULYO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(231, 1, 'LASTRI Bu', 'Jatim, Kab. Pacitan, JATIGUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(232, 1, 'ASSIFA Tk', 'Jatim, Kab. Pacitan, BUBAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(233, 1, 'TEMON Bu', 'Jatim, Kab. Pacitan, TERMINAL LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(234, 1, 'SENTRA FC', 'Jatim, Kab. Pacitan, KEBONAGUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(235, 1, 'NANIK Bu', 'Jatim, Kab. Pacitan, DEPAN PASAR ARJOWINANGUN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(236, 1, 'MAKMUR AJW Tk', 'Jatim, Kab. Pacitan, PASAR ARJOWINANGUN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(237, 1, 'YANA Mbak', 'Jatim, Kab. Pacitan, JATI MALANG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(238, 1, 'DWI Mbak', 'Jatim, Kab. Pacitan, JATIMALANG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(239, 1, 'MUR Mbak', 'Jatim, Kab. Pacitan, SEDENG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(240, 1, 'OPICK CELL', 'Jatim, Kab. Pacitan, DONOROJO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(241, 1, 'LESTARI Tk', 'Jatim, Kab. Pacitan, DONOROJO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(242, 1, 'JELITA Tk', 'Jatim, Kab. Pacitan, PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(243, 1, 'ANIS WARUNG Mbak', 'Jatim, Kab. Pacitan, KALIPUCUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(244, 1, 'SUGENG Bu', 'Jatim, Kab. Pacitan, BANGUNSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(245, 1, 'PINI Mbak', 'Jatim, Kab. Pacitan, MBLUMBANG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(246, 1, 'RAGIL Tk', 'Jatim, Kab. Pacitan, BAREHAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(247, 1, 'PUR Bu', 'Jatim, Kab. Pacitan, MINULYO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(248, 1, 'SIJU Bu', 'Jatim, Kab. Pacitan, BAREHAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(249, 1, 'RINA Mbak', 'Jatim, Kab. Pacitan, TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(250, 1, 'SARIPAH Bu', 'Jatim, Kab. Pacitan, PRADAH', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(251, 1, 'SUM Bu', 'Jatim, Kab. Pacitan, PRADAH', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(252, 1, 'TRUBUS Tk', 'Jatim, Kab. Pacitan, PRADAH', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(253, 1, 'NADILA Tk', 'Jatim, Kab. Pacitan, PRADAH', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(254, 1, 'TITIK Bu', 'Jatim, Kab. Pacitan, BORANG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(255, 1, 'IKA Mbak', 'Jatim, Kab. Pacitan, BORANG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(256, 1, 'CITRA TUNGGAL Tk', 'Jatim, Kab. Pacitan, BORANG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(257, 1, 'PANCA JAYA', 'Jatim, Kab. Pacitan, ARJOWINANGUN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(258, 1, 'PUJI Bu', 'Jatim, Kab. Pacitan, ARJOWINANGUN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(259, 1, 'ANIK Mbak', 'Jatim, Kab. Pacitan, GANTUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(260, 1, 'TITIK Mbak', 'Jatim, Kab. Pacitan, PACITAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(261, 1, 'EKO Bu', 'Jatim, Kab. Pacitan, PLOSO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(262, 1, 'INDAH JAYA Tk', 'Jatim, Kab. Pacitan, PLOSO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(263, 1, 'KOP PRAJAMUKTI', 'Jatim, Kab. Pacitan, Barat Pendopo Kabupaten', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(264, 1, 'TRIMANUNGGAL FC', 'Jatim, Kab. Pacitan, DEPAN TERMINAL BUS', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(265, 1, 'VERA Mbak', 'Jatim, Kab. Pacitan, TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(266, 1, 'DWI KARYA Tk', 'Jatim, Kab. Pacitan, TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(267, 1, 'TULUS Mas', 'Jatim, Kab. Pacitan, LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(268, 1, 'Umi Mbak', 'Jatim, Kab. Pacitan, Semanten', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(269, 1, 'SUMBER ASIH Tk / Mb SIH', 'Jatim, Kab. Pacitan, Pasar Arjowinangun', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(270, 1, 'NUR Mbak', 'Jatim, Kab. Pacitan, MENADI ( depan SD )', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(271, 1, 'KOP HIKMAH', 'Jatim, Kab. Pacitan, PACITAN ( SAMPING RSUD )', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(272, 1, 'SURATI Bu', 'Jatim, Kab. Pacitan, CANDI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(273, 1, 'DWI Mbak', 'Jatim, Kab. Pacitan, KELIP', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(274, 1, 'SARTI Bu', 'Jatim, Kab. Pacitan, ARJOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(275, 1, 'JADI JAYA TK', 'Jatim, Kab. Pacitan, DEPAN PASAR ARJOWINANGUN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(276, 1, 'ABADI TK', 'Jatim, Kab. Pacitan, ARJOWINANGUN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(277, 1, 'SANTOSO Pak', 'Jatim, Kab. Pacitan, PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(278, 1, 'GURIT Bu', 'Jatim, Kab. Pacitan, TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(279, 1, 'WIJI BAKSO', 'Jatim, Kab. Pacitan, PASAR TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(280, 1, 'AYAS Tk', 'Jatim, Kab. Pacitan, LOROG NGADIROJO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(281, 1, 'BERKAH Tk', 'Jatim, Kab. Pacitan, BELAKANG PASAR LOROG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(282, 1, 'LILIK Mbak', 'Jatim, Kab. Pacitan, LOROG NGADIROJO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(283, 1, 'TINI SAYUR Bu', 'Jatim, Kab. Pacitan, PASAR ARJOWINANGUN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(284, 1, 'HIDAYAH MULYA Tk', 'Jatim, Kab. Pacitan, GONDOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(285, 1, 'MANDALA FOTO COPY', 'Jatim, Kab. Pacitan, PRINGKUKU', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(286, 1, 'MITRA TANI TK', 'Jatim, Kab. Pacitan, Jl. Cut Nyal Dien', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(287, 1, 'ENDAH Mbak', 'Jatim, Kab. Pacitan, BUBAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(288, 1, 'ALYA FOTO COPY', 'Jatim, Kab. Pacitan, KETRO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(289, 1, 'PUTRA HARSA FC', 'Jatim, Kab. Pacitan, TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(290, 1, 'SINAR AGUNG FC', 'Jatim, Kab. Pacitan, TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(291, 1, 'JONI Bu', 'Jatim, Kab. Pacitan, TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(292, 1, 'PUTRI KEMBAR TK', 'Jatim, Kab. Pacitan, SEDENG PRINGKUKU', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(293, 1, 'YUNI Mbak', 'Jatim, Kab. Pacitan, PUCANGSEWU', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(294, 1, 'SURYA BARU Tk', 'Jatim, Kab. Pacitan, PACITAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(295, 1, 'ARMY FC / PAK BEDI', 'Jatim, Kab. Pacitan, MENTORO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(296, 1, 'SIRU Mbah', 'Jatim, Kab. Pacitan, PASAR MINULYO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(297, 1, 'BAYU PUTRA Tk', 'Jatim, Kab. Pacitan, TELENG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(298, 1, 'SOGIYEM Bu', 'Jatim, Kab. Pacitan, PASAR ARJOWINANGUN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(299, 1, 'LAMBANG Tk', 'Jatim, Kab. Pacitan, TANGKLUK DONOROJO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(300, 1, 'INDAH Mbak', 'Jatim, Kab. Pacitan, PERUMNAS BANGUNSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(301, 1, 'APOTEK TULAKAN', 'Jatim, Kab. Pacitan, TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(302, 1, 'JUMIRAN Pak', 'Jatim, Kab. Pacitan, TEGALOMBO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(303, 1, 'LUMAYAN JUNIOR Tk', 'Jatim, Kab. Pacitan, DEPAN PASAR TEGALOMBO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(304, 1, 'SMKN 2 PACITAN', 'Jatim, Kab. Pacitan, Jl. Walanda Maramis', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(305, 1, 'SUNDARI Bu', 'Jatim, Kab. Pacitan, SEDENG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(306, 1, 'WULAN Mbak', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(307, 1, 'FELDA Tk', 'Jatim, Kab. Pacitan, BANGUNSARI ( DPN ARMOT )', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(308, 1, 'AKMAL Tk', 'Jatim, Kab. Pacitan, PACITAN ( SEBELUM RSUD )', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(309, 1, 'AL Mbak', 'Jatim, Kab. Pacitan, DEPAN LAPANGAN MANGGALA', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(310, 1, 'BAPANGAN SEHAT APOTEK', 'Jatim, Kab. Pacitan, BAPANGAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(311, 1, 'GIYO Bu', 'Jatim, Kab. Pacitan, KETRO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(312, 1, 'TUKIYAH Bu', 'Jatim, Kab. Pacitan, KETRO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(313, 1, 'TEGAR JAYA', 'Jatim, Kab. Pacitan, Samping Terminal Pacitan', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(314, 1, 'BABY SHOP LOROK', 'Jatim, Kab. Pacitan, Jl Ry NGADIROJO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(315, 1, 'APOTEK MEYLA FARMA', 'Jatim, Kab. Pacitan, Jl. Ry NGADIROJO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(316, 1, 'APOTEK ARJOSARI', 'Jatim, Kab. Pacitan, Arjosari', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(317, 1, 'DANANG Bu', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(318, 1, 'DIPO Tk', 'Jatim, Kab. Pacitan, PASAR PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(319, 1, 'ZABRAN 2', 'Jatim, Kab. Pacitan, DEPAN MAKAM KUCUR', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(320, 1, 'TAKIM Pak', 'Jatim, Kab. Pacitan, PLOSO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(321, 1, 'KOPERASI RSUD (AMONG HUSADA)', 'Jatim, Kab. Pacitan, PACITAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(322, 1, 'AMAN JAYA Tk', 'Jatim, Kab. Pacitan, SEDENG, PURI INDAH', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(323, 1, 'LUWES MM', 'Jatim, Kab. Pacitan, JL.P.SUDIRMAN NO.11 ARJOWINANGUN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(324, 1, 'NANJAK TK / ANIS', 'Jatim, Kab. Pacitan, PRINGKUKU', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(325, 1, 'ASTUTI', 'Jatim, Kab. Pacitan, Jl.P.TENDEAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(326, 1, 'APOTEK CUEK FARMA', 'Jatim, Kab. Pacitan, PEREMPATAN CUEK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(327, 1, 'GUNARDI', 'Jatim, Kab. Pacitan, KARANG GEDE', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(328, 1, 'ENDANG', 'Jatim, Kab. Pacitan, TEMON', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(329, 1, 'SULATIN', 'Jatim, Kab. Pacitan, GONDANG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(330, 1, 'GAVIN', 'Jatim, Kab. Pacitan, GONDANG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(331, 1, 'MISGIATI', 'Jatim, Kab. Pacitan, GONDANG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(332, 1, 'DELITA TARA', 'Jatim, Kab. Pacitan, GONDANG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(333, 1, 'SUMBER REJEKI', 'Jatim, Kab. Pacitan, NAWANGAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(334, 1, 'PANDU JAYA', 'Jatim, Kab. Pacitan, NAWANGAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(335, 1, 'PANDU JAYA NW', 'Jatim, Kab. Pacitan, NAWANGAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(336, 1, 'WILLY MANDIRI', 'Jatim, Kab. Pacitan, NAWNGAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(337, 1, 'KANCIL BU', 'Jatim, Kab. Pacitan, NAWANGAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(338, 1, 'SULASIH', 'Jatim, Kab. Pacitan, PAGER GUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(339, 1, 'DUA BERSAUDARA', 'Jatim, Kab. Pacitan, PAGER GUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(340, 1, 'JALI CELL', 'Jatim, Kab. Pacitan, NAWANGAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(341, 1, 'KARYA BAROKAH NW', 'Jatim, Kab. Pacitan, NAWANGAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(342, 1, 'BAROKAH NW', 'Jatim, Kab. Pacitan, NAWANGAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(343, 1, 'NUSANTARA INDAH /ANNA', 'Jatim, Kab. Pacitan, NAWANGAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(344, 1, 'ARDIN', 'Jatim, Kab. Pacitan, MUJING NAWANGAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(345, 1, 'ARMINI', 'Jatim, Kab. Pacitan, NAWANGAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(346, 1, 'SIFA TK', 'Jatim, Kab. Pacitan, NAWANGAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(347, 1, 'RAHAYU', 'Jatim, Kab. Pacitan, NAWANGAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(348, 1, 'ASHA CELL', 'Jatim, Kab. Pacitan, PACITAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(349, 1, 'ASHA CELL', 'Jatim, Kab. Pacitan, PACITAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(350, 1, 'BAGAS MANDIRI', 'Jatim, Kab. Pacitan, NGRECO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(351, 1, 'YITNO PAK', 'Jatim, Kab. Pacitan, NGUNUT BANDAR', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(352, 1, 'RIKO MOTOR', 'Jatim, Kab. Pacitan, BANDAR', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(353, 1, 'WATIK BU', 'Jatim, Kab. Pacitan, BANDAR { DEPAN UPT TK & SD}', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(354, 1, 'PARAMITHA TK', 'Jatim, Kab. Pacitan, BANDAR', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(355, 1, 'AZIZA MART', 'Jatim, Kab. Pacitan, LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(356, 1, 'PUTRA LANGGENG 2', 'Jatim, Kab. Pacitan, JL.RAYA LOROK', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(357, 1, 'JENY', 'Jatim, Kab. Pacitan, DEPAN STADION ARJOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(358, 1, 'RIVAN SEJATI / MAS ANTOK', 'Jatim, Kab. Pacitan, PACITAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(359, 1, 'SARMUN TK', 'Jatim, Kab. Pacitan, GEMAHARJO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(360, 1, 'LI GONDANG', 'Jatim, Kab. Pacitan, NAWANGAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(361, 1, 'TRI JAYA', 'Jatim, Kab. Pacitan, PLOSO PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(362, 1, 'SARI JAYA', 'Jatim, Kab. Pacitan, PLOSO PUNUNG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(363, 1, 'SUPARTI', 'Jatim, Kab. Pacitan, GONDOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(364, 1, 'BAROKAH (G)', 'Jatim, Kab. Pacitan, GONDOSARI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(365, 1, 'PUR/ SURATI', 'Jatim, Kab. Pacitan, BELAKANG PASAR NGADIREJAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(366, 1, 'HARYONO', 'Jatim, Kab. Pacitan, GLINGGANGAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(367, 1, 'RINTA TK', 'Jatim, Kab. Pacitan, GLINGGANGAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(368, 1, 'ARI SETYAWATI BU', 'Jatim, Kab. Pacitan, GLINGGANGAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(369, 1, 'PUTRI TK', 'Jatim, Kab. Pacitan, NGADIREJAN ( DPN LAP NGADIREJAN)', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(370, 1, 'DWI CANDI', 'Jatim, Kab. Pacitan, CANDI PRINGKUKU', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(371, 1, 'DIAN', 'Jatim, Kab. Pacitan, TELENG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(372, 1, 'KARTINI 2', 'Jatim, Kab. Pacitan, TELENG', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(373, 1, 'SAYUTI (C)', 'Jatim, Kab. Pacitan, CANDI PRINGKUKU', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(374, 1, 'APOTEK BAPANGAN', 'Jatim, Kab. Pacitan, PACITAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(375, 1, 'ARKA DIATER', 'Jatim, Kab. Pacitan, SIRNOBOYO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(376, 1, 'KUD MITRA', 'Jatim, Kab. Pacitan, TULAKAN', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(377, 1, 'DWI SURYO', 'Jatim, Kab. Pacitan, SUDIMORO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(378, 1, 'NUSANTARA MART', 'Jatim, Kab. Pacitan, SUDIMORO', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0),
(379, 1, 'PRAPTO', 'Jatim, Kab. Pacitan, MELATI', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `jatuh_tempo` int(11) DEFAULT NULL COMMENT 'Hari',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli_has_angsuran`
--

CREATE TABLE `pembeli_has_angsuran` (
  `id` int(11) NOT NULL,
  `pembeli_has_product` int(11) NOT NULL,
  `product_has_harga_angsuran` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli_has_identitas`
--

CREATE TABLE `pembeli_has_identitas` (
  `id` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `indetitas` int(11) NOT NULL,
  `no_identitas` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli_has_persyaratan`
--

CREATE TABLE `pembeli_has_persyaratan` (
  `id` int(11) NOT NULL,
  `pembelian` int(11) NOT NULL,
  `persyaratan_has_berkas` int(11) NOT NULL,
  `berkas` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli_has_product`
--

CREATE TABLE `pembeli_has_product` (
  `id` int(11) NOT NULL,
  `pembelian` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `pembeli` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `status_pembelian` int(11) NOT NULL,
  `tgl_beli` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli_kategori`
--

CREATE TABLE `pembeli_kategori` (
  `id` int(11) NOT NULL,
  `kategori` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembeli_kategori`
--

INSERT INTO `pembeli_kategori` (`id`, `kategori`) VALUES
(1, 'KONSUMEN I'),
(2, 'KONSUMEN II');

-- --------------------------------------------------------

--
-- Table structure for table `pengiriman`
--

CREATE TABLE `pengiriman` (
  `id` int(11) NOT NULL,
  `no_pengiriman` varchar(150) DEFAULT NULL,
  `tanggal_pengiriman` date DEFAULT NULL,
  `tanggal_awal_order` date DEFAULT NULL,
  `tanggal_akhir_order` date DEFAULT NULL,
  `sopir` varchar(255) DEFAULT NULL,
  `no_polisi` varchar(45) DEFAULT NULL,
  `filter_by` varchar(45) DEFAULT NULL COMMENT 'SALES\nCUSTOMER',
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengiriman`
--

INSERT INTO `pengiriman` (`id`, `no_pengiriman`, `tanggal_pengiriman`, `tanggal_awal_order`, `tanggal_akhir_order`, `sopir`, `no_polisi`, `filter_by`, `total`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2, 'SEND19DEC001', '2019-12-01', NULL, NULL, 's', 'asd', 'pelanggan', 53866, '2019-12-01', 1, NULL, NULL, 0),
(3, 'SEND19DEC002', '2019-12-18', NULL, NULL, 'SUGENG', 'AG66181', 'pelanggan', 210464, '2019-12-02', 1, '2019-12-02 09:00:40', 1, 1),
(4, 'SEND19DEC003', '2019-12-10', '2019-12-02', '2019-12-02', 'askdjlsad', 'jhasdasd', 'pelanggan', 53866, '2019-12-02', 1, NULL, NULL, 0),
(5, 'SEND19DEC004', '2019-12-25', '2019-12-04', '2019-12-04', 'TES', '211', 'pelanggan', 898660, '2019-12-04', 1, NULL, NULL, 0),
(6, 'SEND19DEC005', '2019-12-19', '2019-12-01', '2019-12-27', 'DA', '12', 'pelanggan', 1273274, '2019-12-04', 1, NULL, NULL, 0),
(7, 'SEND19DEC006', '2019-12-05', '2019-12-01', '2019-12-20', 'sadads', 'w23', 'pelanggan', 0, '2019-12-04', 1, NULL, NULL, 0),
(8, 'SEND19DEC007', '2019-12-05', '2019-12-01', '2020-01-08', 'sdsdf', 'sdsdf', 'pelanggan', 2490130, '2019-12-05', 1, NULL, NULL, 0),
(9, 'SEND19DEC008', '2019-12-06', '2019-12-01', '2020-01-14', 'tes', '13123', 'pelanggan', 3731476, '2019-12-06', 1, NULL, NULL, 0),
(10, 'SEND19DEC009', '2019-12-06', '2019-12-01', '2020-01-27', 'sdf', 'sdsd', 'pelanggan', 3481716, '2019-12-06', 1, NULL, NULL, 0),
(11, 'SEND19DEC010', '2019-12-06', '2019-12-01', '2020-01-27', 'sadsad', 'sadsad', 'pelanggan', 3481716, '2019-12-06', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pengiriman_detail`
--

CREATE TABLE `pengiriman_detail` (
  `id` int(11) NOT NULL,
  `pengiriman` int(11) NOT NULL,
  `invoice_product` int(11) NOT NULL,
  `product_satuan` int(11) DEFAULT NULL,
  `qty` varchar(50) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updatedate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengiriman_detail`
--

INSERT INTO `pengiriman_detail` (`id`, `pengiriman`, `invoice_product`, `product_satuan`, `qty`, `createddate`, `createdby`, `updatedate`, `updatedby`, `deleted`) VALUES
(2, 2, 3, NULL, NULL, '2019-12-01', 1, NULL, NULL, 0),
(3, 3, 5, NULL, NULL, '2019-12-02', 1, NULL, NULL, 0),
(4, 4, 7, NULL, NULL, '2019-12-02', 1, NULL, NULL, 0),
(5, 5, 12, NULL, NULL, '2019-12-04', 1, NULL, NULL, 0),
(6, 5, 14, NULL, NULL, '2019-12-04', 1, NULL, NULL, 0),
(7, 6, 4, NULL, NULL, '2019-12-04', 1, NULL, NULL, 0),
(8, 6, 8, NULL, NULL, '2019-12-04', 1, NULL, NULL, 0),
(9, 6, 9, NULL, NULL, '2019-12-04', 1, NULL, NULL, 0),
(10, 6, 10, NULL, NULL, '2019-12-04', 1, NULL, NULL, 0),
(11, 7, 11, 5371, '8', '2019-12-04', 1, NULL, NULL, 0),
(12, 8, 3, 5371, '25', '2019-12-05', 1, NULL, NULL, 0),
(13, 8, 4, 5395, '1', '2019-12-05', 1, NULL, NULL, 0),
(14, 8, 8, 5428, '6', '2019-12-05', 1, NULL, NULL, 0),
(15, 8, 9, 5407, '3', '2019-12-05', 1, NULL, NULL, 0),
(16, 8, 14, 5859, '20', '2019-12-05', 1, NULL, NULL, 0),
(17, 9, 3, 5371, '26', '2019-12-06', 1, NULL, NULL, 0),
(18, 9, 4, 5395, '2', '2019-12-06', 1, NULL, NULL, 0),
(19, 9, 8, 5428, '12', '2019-12-06', 1, NULL, NULL, 0),
(20, 9, 9, 5407, '6', '2019-12-06', 1, NULL, NULL, 0),
(21, 9, 14, 5859, '40', '2019-12-06', 1, NULL, NULL, 0),
(22, 10, 17, 5372, '1/25', '2019-12-06', 1, NULL, NULL, 0),
(23, 10, 9, 5407, '3', '2019-12-06', 1, NULL, NULL, 0),
(24, 10, 4, 5395, '1', '2019-12-06', 1, NULL, NULL, 0),
(25, 10, 8, 5428, '6', '2019-12-06', 1, NULL, NULL, 0),
(26, 10, 14, 5859, '20', '2019-12-06', 1, NULL, NULL, 0),
(27, 11, 17, 5372, '1/25', '2019-12-06', 1, NULL, NULL, 0),
(28, 11, 9, 5407, '3', '2019-12-06', 1, NULL, NULL, 0),
(29, 11, 4, 5395, '1', '2019-12-06', 1, NULL, NULL, 0),
(30, 11, 8, 5428, '6', '2019-12-06', 1, NULL, NULL, 0),
(31, 11, 14, 5859, '20', '2019-12-06', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pengiriman_has_customer`
--

CREATE TABLE `pengiriman_has_customer` (
  `id` int(11) NOT NULL,
  `pengiriman` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengiriman_has_customer`
--

INSERT INTO `pengiriman_has_customer` (`id`, `pengiriman`, `pembeli`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2, 2, 4, '2019-12-01', 1, NULL, NULL, 0),
(3, 3, 4, '2019-12-02', 1, NULL, NULL, 0),
(4, 4, 4, '2019-12-02', 1, NULL, NULL, 0),
(5, 5, 4, '2019-12-04', 1, NULL, NULL, 0),
(6, 6, 4, '2019-12-04', 1, NULL, NULL, 0),
(7, 6, 5, '2019-12-04', 1, NULL, NULL, 0),
(8, 6, 6, '2019-12-04', 1, NULL, NULL, 0),
(9, 7, 4, '2019-12-04', 1, NULL, NULL, 0),
(10, 7, 5, '2019-12-04', 1, NULL, NULL, 0),
(11, 7, 6, '2019-12-04', 1, NULL, NULL, 0),
(12, 8, 4, '2019-12-05', 1, NULL, NULL, 0),
(13, 8, 5, '2019-12-05', 1, NULL, NULL, 0),
(14, 8, 6, '2019-12-05', 1, NULL, NULL, 0),
(15, 9, 4, '2019-12-06', 1, NULL, NULL, 0),
(16, 9, 5, '2019-12-06', 1, NULL, NULL, 0),
(17, 9, 6, '2019-12-06', 1, NULL, NULL, 0),
(18, 10, 4, '2019-12-06', 1, NULL, NULL, 0),
(19, 10, 5, '2019-12-06', 1, NULL, NULL, 0),
(20, 10, 6, '2019-12-06', 1, NULL, NULL, 0),
(21, 11, 4, '2019-12-06', 1, NULL, NULL, 0),
(22, 11, 5, '2019-12-06', 1, NULL, NULL, 0),
(23, 11, 6, '2019-12-06', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pengiriman_has_sales`
--

CREATE TABLE `pengiriman_has_sales` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `pengiriman` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengiriman_status`
--

CREATE TABLE `pengiriman_status` (
  `id` int(11) NOT NULL,
  `pengiriman` int(11) NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengiriman_status`
--

INSERT INTO `pengiriman_status` (`id`, `pengiriman`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2, 2, 'DRAFT', '2019-12-01', 1, NULL, NULL, 0),
(3, 3, 'DRAFT', '2019-12-02', 1, NULL, NULL, 0),
(4, 4, 'DRAFT', '2019-12-02', 1, NULL, NULL, 0),
(5, 5, 'DRAFT', '2019-12-04', 1, NULL, NULL, 0),
(6, 6, 'DRAFT', '2019-12-04', 1, NULL, NULL, 0),
(7, 7, 'DRAFT', '2019-12-04', 1, NULL, NULL, 0),
(8, 8, 'DRAFT', '2019-12-05', 1, NULL, NULL, 0),
(9, 9, 'DRAFT', '2019-12-06', 1, NULL, NULL, 0),
(10, 10, 'DRAFT', '2019-12-06', 1, NULL, NULL, 0),
(11, 11, 'DRAFT', '2019-12-06', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `periode`
--

CREATE TABLE `periode` (
  `id` int(11) NOT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updatedate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `periode`
--

INSERT INTO `periode` (`id`, `month`, `year`, `createddate`, `createdby`, `updatedate`, `updatedby`, `deleted`) VALUES
(1, 10, 2019, '2019-10-13', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `persyaratan`
--

CREATE TABLE `persyaratan` (
  `id` int(11) NOT NULL,
  `syarat` varchar(150) DEFAULT NULL,
  `kategori_akad` int(11) NOT NULL,
  `jenis_akad` int(11) NOT NULL,
  `lampirkan_berkas` int(11) DEFAULT NULL COMMENT '0 : Tidak\n1: YA',
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `persyaratan_has_berkas`
--

CREATE TABLE `persyaratan_has_berkas` (
  `id` int(11) NOT NULL,
  `persyaratan` int(11) NOT NULL,
  `nama_berkas` varchar(150) DEFAULT NULL COMMENT 'Nama Berkas Misal (KTP, SIM, dLL)',
  `status` varchar(150) DEFAULT NULL COMMENT 'Status Tanpa Upload Berkas atau Tidak\nN : Tanpa Berkas\nY : Dengan Berkas',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `potongan`
--

CREATE TABLE `potongan` (
  `id` int(11) NOT NULL,
  `potongan` varchar(45) DEFAULT NULL COMMENT 'Nominal\nPersentase'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `potongan`
--

INSERT INTO `potongan` (`id`, `potongan`) VALUES
(1, 'Nominal'),
(2, 'Persentase'),
(3, 'Tidak ada potongan');

-- --------------------------------------------------------

--
-- Table structure for table `priveledge`
--

CREATE TABLE `priveledge` (
  `id` int(11) NOT NULL,
  `hak_akses` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `priveledge`
--

INSERT INTO `priveledge` (`id`, `hak_akses`) VALUES
(1, 'superadmin'),
(2, 'company'),
(3, 'sales'),
(4, 'operational'),
(5, 'manajemen hrd'),
(6, 'manajemen accounting'),
(7, 'direktur');

-- --------------------------------------------------------

--
-- Table structure for table `procurement`
--

CREATE TABLE `procurement` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(150) DEFAULT NULL,
  `vendor` int(11) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `procurement`
--

INSERT INTO `procurement` (`id`, `no_faktur`, `vendor`, `total`, `tanggal`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(8, 'PROC19DEC001', 1, 2000000, '2019-12-01', '-', '2019-12-01', 1, NULL, NULL, 0),
(9, 'PROC19DEC002', 1, 2000000, '2019-12-01', '-', '2019-12-01', 1, NULL, NULL, 0),
(10, 'PROC19DEC003', 1, 150000, '2019-12-10', 'pengadaan lifre', '2019-12-10', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `procurement_item`
--

CREATE TABLE `procurement_item` (
  `id` int(11) NOT NULL,
  `procurement` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `satuan` int(11) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `procurement_item`
--

INSERT INTO `procurement_item` (`id`, `procurement`, `product`, `satuan`, `harga`, `qty`, `sub_total`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(8, 8, 2841, 893, 40000, 50, 2000000, NULL, '2019-12-01', 1, NULL, NULL, 0),
(9, 9, 2841, 893, 40000, 50, 2000000, NULL, '2019-12-01', 1, NULL, NULL, 0),
(10, 10, 2840, 893, 15000, 10, 150000, NULL, '2019-12-10', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `procurement_retur`
--

CREATE TABLE `procurement_retur` (
  `id` int(11) NOT NULL,
  `procurement` int(11) NOT NULL,
  `no_faktur` varchar(155) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `procurement_retur`
--

INSERT INTO `procurement_retur` (`id`, `procurement`, `no_faktur`, `tanggal`, `keterangan`, `total`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 10, 'REPROC19DEC001', '2019-12-10', 'barang rusak', 30000, '2019-12-10', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `procurement_retur_item`
--

CREATE TABLE `procurement_retur_item` (
  `id` int(11) NOT NULL,
  `procurement_item` int(11) NOT NULL,
  `procurement_retur` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `procurement_retur_item`
--

INSERT INTO `procurement_retur_item` (`id`, `procurement_item`, `procurement_retur`, `qty`, `sub_total`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 10, 1, 2, 30000, '2019-12-10', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `procurement_status`
--

CREATE TABLE `procurement_status` (
  `id` int(11) NOT NULL,
  `procurement` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'DRAFT\nPAID',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `procurement_status`
--

INSERT INTO `procurement_status` (`id`, `procurement`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 8, 'DRAFT', '2019-12-01', 1, NULL, NULL, 0),
(2, 9, 'DRAFT', '2019-12-01', 1, NULL, NULL, 0),
(3, 10, 'DRAFT', '2019-12-10', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `product` varchar(150) DEFAULT NULL,
  `kode_product` varchar(50) DEFAULT NULL,
  `tipe_product` int(11) NOT NULL,
  `kategori_product` int(11) NOT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product`, `kode_product`, `tipe_product`, `kategori_product`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2840, 'Lifree Lapisan Penyerap 18P', '1914103', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2841, 'MamyPoko Pants X-tra Kering Slim M30', '2813303', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2842, 'MamyPoko Pants X-tra Kering Slim L28', '2813403', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2843, 'MamyPoko Pants X-tra Kering S11', '2814211', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2844, 'MamyPoko Pants X-tra Kering S22', '2814212', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2845, 'MamyPoko Pants X-tra Kering S1', '2814224', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2846, 'MamyPoko Pants X-tra Kering S40', '2814227', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2847, 'MamyPoko Pants X-tra Kering S58', '2814229', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2848, 'MamyPoko Pants X-tra Kering M8', '2814309', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2849, 'MamyPoko Pants X-tra Kering M9', '2814316', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2850, 'MamyPoko Pants X-tra Kering M20', '2814317', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2851, 'MamyPoko Pants X-tra Kering M1', '2814331', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2852, 'MamyPoko Pants X-tra Kering M34', '2814337', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2853, 'MamyPoko Pants X-tra Kering L8', '2814417', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2854, 'MamyPoko Pants X-tra Kering L20', '2814418', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2855, 'MamyPoko Pants X-tra Kering L1', '2814430', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2856, 'MamyPoko Pants X-tra Kering L30', '2814437', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2857, 'MamyPoko Pants X-tra Kering XL7', '2814516', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2858, 'MamyPoko Pants X-tra Kering XL20', '2814517', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2859, 'MamyPoko Pants X-tra Kering XL1', '2814529', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2860, 'MamyPoko Pants X-tra Kering XL26', '2814535', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2861, 'MamyPoko Pants X-tra Kering XXL1', '2814607', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2862, 'MamyPoko Pants X-tra Kering XXL6', '2814609', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2863, 'MamyPoko Pants X-tra Kering XXL18', '2814610', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2864, 'MamyPoko X-tra Kering S12', '2843202', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2865, 'MamyPoko X-tra Kering S44', '2843205', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2866, 'MamyPoko X-tra Kering M10', '2843301', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2867, 'MamyPoko X-tra Kering M22', '2843302', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2868, 'MamyPoko X-tra Kering M40', '2843304', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2869, 'Charm Safe Night 42cm Gather 8P', '5393726', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2870, 'Charm Cooling Fresh Wing 24P (23cm)', '6801312', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2871, 'Charm Cooling Fresh Non Wing 24P (23cm)', '6801332', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2872, 'Charm Cooling Fresh Wing Long 20P (26cm)', '6801412', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2873, 'CBF Extra Maxi Wing 10P', '6811317', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2874, 'CBF Extra Maxi Non Wing 8P', '6811361', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2875, 'CBF Extra Maxi Non Wing 20P', '6811366', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2876, 'CBF Extra Maxi Non Wing 30F', '6811367', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2877, 'CBF Extra Maxi Wing 20F', '6811371', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2878, 'CBF Extra Maxi Wing 30F', '6811372', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2879, 'Charm Extra Comfort Maxi Wing 18P (23cm)', '6841312', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2880, 'Charm Extra Comfort Maxi Wing 8P (26cm)', '6841411', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2881, 'Charm Extra Comfort Maxi Wing 16P (26cm)', '6841412', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2882, 'Charm Extra Comfort Maxi Non Wing 16P (26cm)', '6841432', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2883, 'Charm Safe Night 29cm Wing 10P', '6893518', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2884, 'Charm Safe Night 29cm Wing 20P', '6893519', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2885, 'Charm Safe Night 35cm Wing 6P', '6893617', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2886, 'Charm Safe Night 35cm Wing 12P', '6893618', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2887, 'Charm Safe Night 35cm Gather 6P', '6893624', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2888, 'Charm Safe Night 35cm Gather 12G', '6893656', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2889, 'Lifree Popok Celana Light XL3', '9811152', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2890, 'Lifree Popok Celana Extra M5 Small Pack', '9811232', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2891, 'Lifree Popok Celana Extra M20 Jumbo Pack', '9811234', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2892, 'Lifree Popok Celana Extra L16 Jumbo Pack', '9811244', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2893, 'Lifree Popok Celana Extra XL3 Small Pack', '9811252', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2894, 'Lifree Popok Perekat M1', '9812331', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2895, 'Lifree Popok Perekat M9', '9812333', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2896, 'Lifree Popok Perekat L1', '9813141', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2897, 'BISCUIT CB BAL COCONUT 10', 'BBALC', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2898, 'BISCUIT CB BAL DURIAN 10', 'BBALD', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2899, 'BISCUIT CB BONBON 21 COKLAT 3X7', 'BBB21C', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2900, 'BISCUIT CB BONBON 21 LEMON 3X7', 'BBB21L', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2901, 'BISCUIT CB BONBON 21 STRAW 3X7', 'BBB21S', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2902, 'BISCUIT CB BONBON 21 VANILA 3X7', 'BBB21V', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2903, 'BISCUIT CB BUTTER PACK COCONUT 24', 'BBPCO', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2904, 'BISCUIT CB BUTTER PACK LEMON 24', 'BBPL', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2905, 'BISCUIT CB BLACK SAND COKLAT 6X20', 'BBSC', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2906, 'BISCUIT CB BLACK SAND VANILLA 6X20', 'BBSV', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2907, 'BISCUIT CB BUTTER VANILLA 18', 'BBV', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2908, 'BISCUIT CB KUKIS BUTTER 6X20', 'BCOB', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2909, 'BISCUIT CB KUKIS COCONUT 6X20', 'BCOCO', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2910, 'BISCUIT CB KUKIS WHITE COFFEE 6X20', 'BCOWC', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2911, 'BISCUIT CB COATING COKLAT VANILLA 6X20', 'BCT', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2912, 'BISCUIT CB COATING 50GR CHOCOVANILA 3X10', 'BCT50', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2913, 'BISCUIT CB DOS COCONUT 3X10', 'BDOSC', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2914, 'BISCUIT CB DOS DURIAN 3X10', 'BDOSD', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2915, 'BISC CB GARUDAYANA AR', 'BGAR', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2916, 'BISC CB GARUDAYANA AR BBQ', 'BGARB', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2917, 'BIHUN JAGUNG SENDOK 10Pcs X 290Gr', 'BJSAO', 31, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2918, 'BISCUIT CB MINI ROL COCONUT 9X10', 'BMRC', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2919, 'BISCUIT CB MINI ROL DURIAN 9X10', 'BMRD', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2920, 'BISCUIT CB MARIE SUSU PACK 10', 'BMSP', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2921, 'BISCUIT CB MARIE SUSU RENTENG 12X10', 'BMSR', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2922, 'BISCUIT CB ROSE CREAM DURIAN 21', 'BRCD21', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2923, 'BISCUIT CB ROSE CREAM LEMON 21', 'BRCL21', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2924, 'BISCUIT CB ROSE CREAM PINEAPPLE 21', 'BRCP21', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2925, 'BISCUIT CB ROSE CREAM STRAW 21', 'BRCS21', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2926, 'BISCUIT CB ROSE CREAM VANILLA 21', 'BRCV21', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2927, 'BIHUN URAI SUPERIOR ISI 10 EKONOMIS', 'BSAOE', 31, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2928, 'BIHUN PUTRI JAGUNG 12Pcs X 335Gr', 'BSBAB', 31, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2929, 'Alligator Big Scissors Black Grip', 'BSBL', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2930, 'Alligator Big Scissors Blue Grip', 'BSBU', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2931, 'BICKUIT HOT SPICY CHESEE 3X10', 'BSH', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2932, 'Alligator Cutter AL - 77 Red', 'CALR', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2933, 'Alligator Cutter AL - 77 Yellow', 'CALY', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2934, 'KN CHEW2BALL MOOMOO CHO 12BULK 2RCG 10SCH', 'CBMC', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2935, 'KN CHEW2BALL MOOMOO FRT 12BULK 2RCG 10SCH', 'CBMF', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2936, 'KN CHEW2BALL 20BULK 10TRNGL 3PCS ANGGUR', 'CBTAG', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2937, 'KN CHEW2BALL 20BULK 10TRGLE 3PCS MANGGA', 'CBTMG', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2938, 'KN CHEW2BALL 20BULK 10TRGLE 3PCS ORANGE', 'CBTOR', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2939, 'KINO CHEW2BALL 20BAG 10TRGLE ORG MGO', 'CBTORM', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2940, 'KN CHEW2BALL 20BULK 10TRGLE 3PCS STRWBRY', 'CBTST', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2941, 'KN CHEW2BALL 20BAG 10TRGLE STRAW GRAPE', 'CBTSTG', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2942, 'KINO CHEWEY 24BAGS 50PCS 2.5G DRN BKK', 'CDB2', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2943, 'KINO CHEWEY 24BAGS 50PCS 2.5G MGG ARUM', 'CMA2', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2944, 'KINO CANDY OPLOZZ 10BULK 2RCG', 'CO10', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2945, 'Kino Candy Oplozz 6Bag 200Pcs', 'CO6', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2946, 'KINO CANDY OPLOZZ TOPLES 12TPLS', 'COT', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2947, 'Chizku Roll Keju 6x24x8gr', 'CRK', 34, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2948, '\"Alligator cloth  tape Black ( 2\"\" )\"', 'CTBL', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2949, '\"Alligator cloth tape Black 36mm (1,5\"\")\"', 'CTBL36', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2950, 'KINO CHEWEY 6 TPLS DURIAN BANGKOK', 'CTDB', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2951, 'KINO CHEWEY 6 TPLS MANGGA ARUM', 'CTMA', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2952, 'CUP Q CHOCOLATE', 'CUP', 35, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2953, 'Djeng Ayu Minyak Kemiri 75ml', 'DAM75', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2954, 'Djeng Ayu SK Original 100ml  Hijau', 'DAO100', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2955, 'Djeng Ayu SK Rose 100ml  Pink', 'DAR100', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2956, 'DJ. BIG DONALD JELLY STICK 4 S', 'DBJS', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2957, 'DJ.BIGSTICK PUDDING PACK', 'DBPP', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2958, 'DJ.BIGSTICK PUDDING TOPLES OVAL', 'DBPT', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2959, 'DJ.BIG STICK ISI 120 PACK', 'DBS', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2960, 'DJ.BIG STICK ISI TOPLES', 'DBST', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2961, 'DJ.PUDING TOPLES MINI', 'DBTM', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2962, 'DJ.BIGSTICK TOPLES OVAL', 'DBTO', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2963, 'DJ.BIG STICK TAPPERWARE', 'DBTW', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2964, 'COCO DRINK 135 ML', 'DCD', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2965, 'Domoo Corn Stick  Coklat  6 x 20 x 10gr', 'DCSC1', 34, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2966, 'Domoo Corn Stick Jagung Bakar 6 x 20 x 10gr', 'DCSJB1', 34, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2967, 'Domoo Corn Stick Kelapa Bakar 6 x 20 x 10gr', 'DCSKB1', 34, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2968, 'DJ-ES LILIN 150 PCS', 'DEL', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2969, 'DJ.ES SALJU', 'DESP', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2970, 'DJ.ES SALJU TOPLES', 'DEST', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2971, 'DONALD ES TELER POLOS 150ML', 'DET', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2972, 'DJ.FRUITY CUP', 'DFC', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2973, 'DJ.DRINK FRUITY JALA', 'DFJ', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2974, 'DJ.DRINK FRUITY PUDING JALA', 'DFP', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2975, 'DJ.FRUITY CUP TOPLES TISSUE', 'DFTT', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2976, 'DJ. ES TUNG-TUNG', 'DJET', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2977, 'DJ.JELLY GUM', 'DJG', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2978, 'DJ.JELLY GUM BLUE', 'DJGB', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2979, 'DJ. Jelly Gum Pouch', 'DJGP', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2980, 'DJ. JELLY TABUNG TOPLES 180', 'DJTT', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2981, 'Domoo Long Stick Jagung Coklat 6 bag @ 20 pcs', 'DLCK1', 34, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2982, 'Domoo Long Stick Jagung Coffee 6 bag @ 20 pcs', 'DLCOF1', 34, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2983, 'DJ.PUDING CUP 60 ML', 'DPC', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2984, 'DJ.PUDING ES TUNG-TUNG', 'DPET', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2985, 'DJ.PUDING TOPLES JUMBO', 'DPTJ', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2986, 'DJ. SUPER JELL', 'DSJ', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2987, 'Double Tape 12mmx12', 'DT12', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2988, 'Double Tape 24mmx12', 'DT24', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2989, 'DJ. TOPCOCO NATA DECOCO', 'DTC', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2990, 'SS FRENTA 10PACK @60SCT BLACKCURRENT', 'FBC', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2991, 'SS FRENTA 10PACK @60SCT BUBBLE GUM', 'FBG', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2992, 'SS FRENTA 10PACK @60SCT COTTON CANDY', 'FCC', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2993, 'SS FRENTA 10PACK @60SCT COLA', 'FCO', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2994, 'SS FRENTA 10PACK @60SCT FRUITPUNCH', 'FFP', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2995, 'SS FRENTA 10PACK @60SCT LYCHEE', 'FLC', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2996, 'SS FRENTA 10PACK @60SCT LEMON', 'FLE', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2997, 'SS FRENTA 10PACK @60SCT LOLLYPOP', 'FLP', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2998, 'SS FRENTA 10PACK @60SCT MANGGA', 'FMG', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(2999, 'SS FRENTA 10PACK @60SCT ORANGE', 'FOR', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3000, 'SS FRENTA 10PACK @60SCT STRAWBERRY', 'FOS', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3001, 'SS FRENTA 10PACK @60SCT PELANGI', 'FPL', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3002, 'Jahe Coklat Renteng', 'JCR', 35, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3003, 'Jahe Kopi Susu Renteng 12 X 10', 'JKS', 35, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3004, 'Jahe Susu Renteng 12 X 10', 'JSUR', 35, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3005, 'Jahe Wangi Metalized Renteng 40 X 10', 'JWR', 35, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3006, 'Kino Frenta 24Bags 35Pcs Green Apple', 'KFGA', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3007, 'Kino Frenta 24Bags 35Pcs Strawberry', 'KFST', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3008, 'Kacang Kancil Putih 200gr @20', 'KKP200', 38, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3009, 'Alligator Liquid Glue 70 ml', 'LG70', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3010, 'Morris Aqua 100ml Biru', 'MAQ100', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3011, 'Morris Army 100ml Hijau tua', 'MAR100', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3012, 'Morris Bm Confidence 100ml Hijau', 'MBC100', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3013, 'Morris BM Confidence 50ml Hijau', 'MBC50', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3014, 'Morris BM Dream 100ml Orange', 'MBD100', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3015, 'Morris BM Dream 50ml Orange', 'MBD50', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3016, 'Morris Bunga Diva 60ml Kuning', 'MBD60', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3017, 'Morris Bm Eternal 100ml Kuning', 'MBE100', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3018, 'Morris BM Eternal 50ml Kuning', 'MBE50', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3019, 'Morris BM Flower 100ml Ungu', 'MBFL100', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3020, 'Morris BM Flower 50ml Ungu', 'MBFL50', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3021, 'Morris BM Freedom 100ml Biru', 'MBFR100', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3022, 'Morris BM Freedom 50ml Biru', 'MBFR50', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3023, 'Morris Bunga Goddess 60ml Ungu', 'MBG60', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3024, 'Morris BM Heart 100ml Pink', 'MBH100', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3025, 'Morris BM Heart 50ml Pink', 'MBH50', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3026, 'MEICO BUBBLE POLOS 150 ML', 'MBP', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3027, 'Morris BM Power 100ml Hitam', 'MBP100', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3028, 'Morris BM Power 50ml Hitam', 'MBP50', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3029, 'Morris Bunga Princess 60ml Pink', 'MBP60', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3030, 'Morris Bm Spirit 100ml Merah', 'MBS100', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3031, 'Morris BM Spirit 50ml Merah', 'MBS50', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3032, 'Morris Bunga Venus 60ml Ungu', 'MBV60', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3033, 'Morris Emotion 100ml Kuning', 'MEM100', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3034, 'Mrs EDP Trop Floral 110ml Merah', 'METFL', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3035, 'Mrs EDP Trop Fruity 110ml Kuning', 'METFR', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3036, 'Mrs EDP Trop Marine 110ml Biru', 'METM', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3037, 'Mrs EDP Trop Nature 110ml Hijau', 'METN', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3038, 'Mrs EDP Trop Paradise 110ml Ungu', 'METP', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3039, 'Mrs EDP Trop Romance 110ml Pink', 'METR', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3040, 'Morris Extreme 100ml Hitam', 'MEX100', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3041, 'Morris Fresh 100ml Hijau muda', 'MFR100', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3042, 'MoMoGi Cappucinno 8X20', 'MGCAP', 38, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3043, 'MoMoGi Chocolate 8X20', 'MGCOK', 38, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3044, 'MoMoGi Jagung Bakar 8X20', 'MGJB', 38, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3045, 'MoMoGi Keju 8X20', 'MGJU', 38, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3046, 'MoMoGi Love Heart Cheese 25Gr', 'MGLH25', 38, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3047, 'MoMoGi Mini Stick Roasted Corn 25Gr', 'MGMS25', 38, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3048, 'MoMoGi Star Chocolate 25Gr', 'MGSC25', 38, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3049, 'MoMoGi Tutti Fruti 8X20', 'MGTF', 38, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3050, 'MoMoGi Mini Twist Roasted Corn 25Gr', 'MGTW25', 38, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3051, 'MEICCO KUBUS POLOS 150 ML', 'MKP', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3052, 'MIE KERING SUPERIOR TALI UNGU', 'MLBG', 31, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3053, 'SUPERIOR MIE BAKSO', 'MSLDD', 31, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3054, 'Morris Sport 100ml Orange', 'MSP100', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3055, 'MIE KERING SPIDER TALI ORANGE', 'MSPLBDB', 31, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3056, 'Morris Teen 50ml Cute Ungu', 'MTCU', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3057, 'Morris Teen 50ml Elegant Merah', 'MTEM', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3058, 'Morris Teen 50ml Fresh Hijau', 'MTFH', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3059, 'Morris Teen 50ml Fashion Orange', 'MTFO', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3060, 'Morris Teen 50ml Princess Pink', 'MTPP', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3061, 'Morris Teen 50ml Trendy Biru', 'MTTB', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3062, 'Nitchi Pasta Coklat 12 x 24 x 10gr', 'NPC24', 34, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3063, 'Nitchi Pasta Susu 12 x 24 x 10gr', 'NPS24', 34, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3064, 'Nitchi Rice Assorted Sprinkle 4 x 30 x 8gr', 'NRA', 34, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3065, 'Nitchi Rice Coklat Sprinkle 4 x 30 x 8gr', 'NRC', 34, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3066, 'MINYAK GORENG OILKU 1 LITER', 'OIL1', 31, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3067, 'MINYAK GORENG OILKU 2 LITER', 'OIL2', 31, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3068, 'Alligator OPP Tape Brown Local', 'OTBRL', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3069, 'Alligator OPP Tape Clear Local', 'OTCL', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3070, 'Ballpoint Pen Alligator7 Star Black Ink', 'P7SB', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3071, 'Ballpoint Pen AlligatorD7 Black Ink', 'PD7BL', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3072, 'Ballpoint Alligator R7 Retech black ink', 'PR7RB', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3073, 'SN7 Alligator Ice Color Black Ink Box12', 'PSN7B', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3074, 'SN7 Alligator Translucent Blue Box12', 'PSN7TBU', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3075, 'SN7 Alligator Translucent Red Box12', 'PSN7TR', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3076, 'Alligator Refill Cutter5 PC', 'RC5', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3077, 'Morris Robot 60ml Futuristik Putih', 'RF60', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3078, 'Morris Robot 60ml Navy Biru', 'RN60', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3079, 'Morris Robot 60ml Touring  Merah', 'RT60', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3080, 'Morris Robot 60ml VIP  Hitam', 'RV60', 36, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3081, 'SS SUSU SODA 4PACK @50SCT ANGGUR', 'SAG', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3082, 'Alligator Small Cutter AL - 75 Red', 'SCALR', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3083, 'Alligator Small Cutter AL - 75 Yellow', 'SCALY', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3084, 'SS SUSU SODA 4PACK @50SCT CCPNDN', 'SCP', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3085, 'SEGARSARI 10PACK 60SCT 7G APLE FUJI', 'SGAF', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3086, 'SEGARSARI 10PACK 60SCT 7G ANGGUR NCP', 'SGAG', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3087, 'SEGARSARI 10PACK 60SCT 7G GUAVA NCP', 'SGGU', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3088, 'SEGARSARI 10PACK 60SCT 7G JERMAN NCP', 'SGJM', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3089, 'SEGARSARI 10PACK 60SCT 7G JERUK PERAS', 'SGJP', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3090, 'SEGARSARI 10PACK 60SCT 7G KELAPA MUDA', 'SGKM', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3091, 'SEGARSARI 10PACK 60SCT 7G MANGGA', 'SGMG', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3092, 'SEGARSARI 10PACK 60SCT 7G AMRCN SWT ORGE', 'SGSO', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3093, 'SEGARSARI 2BULK 40SCH 20G TARIK EXTR TOPG', 'SGTRE', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3094, 'SEGARSARI 2BULK 40SCH 20G THAI EXTR TOPG', 'SGTTE', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3095, 'Segarsari Thai Tea 8Rtg 12 Sch ( Shake )', 'SGTTS', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3096, 'SARI KACANG HIJAU 200ML', 'SKH', 38, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3097, 'SEKAR 10PACK @10SCH @15GR K.HIJAU', 'SKHJ', 28, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3098, 'SEKAR 10PACK @10SCH @15GR K.HITAM', 'SKHT', 28, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3099, 'SS SUSU SODA 4PACK @50SCT MELON', 'SML', 28, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3100, 'Stapler Pin No 10 Mini Box Allligator', 'SP10', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3101, 'Alligator Small Scissors Black Grip', 'SSBL', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3102, 'Alligator Small Scissors Blue Grip', 'SSBU', 32, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3103, 'SS SUSU SODA 4PACK @50SCT STRAWBERRY', 'SST', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3104, 'Santri 120 ml', 'ST12', 39, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3105, 'Santri 1.500 ml', 'ST150', 39, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3106, 'Santri 240 ml', 'ST24', 39, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3107, 'Santri 330 ml', 'ST33', 39, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3108, 'Santri 600 ml', 'ST60', 39, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3109, 'SS SUSU SODA 4PACK @50SCT TARO', 'STA', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3110, 'S-TEH GULA BATU 10BULK 6RCG 11SACHET 7GR', 'TGB1', 33, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3111, 'TANAM JAGUNG 60Pcs X 60Gr', 'TJ1SS', 31, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3112, 'TANAM JAGUNG 12Pcs X 300Gr', 'TJ4SS', 31, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3113, 'TRICKS BISK KENTANG BBQ 24X10X20Gr', 'TRBKB', 34, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3114, 'TRICKS BISK KENTANG KIMCHI 24X10X20Gr', 'TRBKK1', 34, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3115, 'TRICKS BISK KENTANG ORI 24X10X20Gr', 'TRBKO', 34, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3116, 'VITCOOL GRAPE 180 ML', 'VITGRAP', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3117, 'VITCOOL ORANGE 180 ML', 'VITOR', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3118, 'VITCOOL SIRSAK 180 ML', 'VITSIR', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3119, 'VITCOOL STRAWBERRY 180 ML', 'VITSTRO', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3120, 'Wasuka Choku Chocolate Flavour 6 @ 24 pc', 'WCCF1', 34, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3121, 'WAFER CB 155gr 6X5', 'WF155', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3122, 'WAFER CB 35gr 8X10', 'WF35', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3123, 'WAFER CB 50gr 3X10', 'WF50', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3124, 'WAFER CB COKLAT 10gr 8X20', 'WFC10', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3125, 'WAFER CB GARUDAYANA AR', 'WGAR', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3126, 'WAFER CB GARUDAYANA AR VANILLA', 'WGARV', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3127, 'WAFER CB KERO JUMBO COKLAT STRAWBERRY 12X10', 'WKJCS', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3128, 'WAFER CB KERO JUMBO COKLAT VANILLA 12X10', 'WKJCV', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3129, 'Wasuka Mini Coklat 6 x 20', 'WMC1', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3130, 'Wasuka Mini Stawbery 6 x 20', 'WMS1', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3131, 'WAFER CB PREMIER COKLAT 10X10', 'WPC', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3132, 'WAFER CB PREMIER STRAWBERRY 10X10', 'WPS', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3133, 'WAFER STICK CB GARUDAYANA AR', 'WSGAR', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3134, 'WAFER STICK CB GARUDAYANA AR VANILA', 'WSGARV', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3135, 'WAFER STICK KERO MIX ROLL BAG 30', 'WSKMB', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3136, 'WAFER STICK KERO MIX ROLL 6X10', 'WSKMR', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3137, 'WAFER STICK KERO MIX ROLL 10X10', 'WSKMR10', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3138, 'WAFER STICK KERO PACK COKLAT 5X10', 'WSKPC', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3139, 'WAFER STICK KERO PACK STRAWBERRY 5X10', 'WSKPS', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3140, 'WAFER STICK KERO TOPLES 30 10X3', 'WSKT', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3141, 'WAFER STICK CB PACK COKLAT 45gr 24', 'WSPC', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3142, 'WAFER STICK CB PACK VANILLA 45gr 24', 'WSPV', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3143, 'WAFER CB WHITE COFFEE 30', 'WWC', 30, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3144, 'YOU C 1.000 LEMON WATER', 'YCLMN1000', 38, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3145, 'YOU C 1.000 ORANGE WATER', 'YCORG1000', 38, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3146, 'YOCOOL STRAWBERY', 'YCOS', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3147, 'YOCOOL BLACKCURRENT', 'YOBC', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3148, 'YOCOOL MANGGO', 'YOMG', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3149, 'YOCOOL ORANGE', 'YOOR', 37, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3150, 'MamyPoko Pants X-tra Kering XXL24', '2814620', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3151, 'MamyPoko Open Standart S24', '2843203', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3152, 'Mamypoko Wipes ANTISEPTIK P2X48F', '2852117', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3153, 'Mamypoko Wipes Reguler P2X52G', '2852220', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3154, 'Mamypoko Wipes ANTISEPTIK NP2X48F', '2852224', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3155, 'CBF Extra Maxi Non Wing 10P', '6811337', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3156, 'CBF EMW 30E+CSN 350 MM 1G', '6851341', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0),
(3157, 'CBF EMNW 30E+CSN 350 MM 1G', '6851362', 29, 1, NULL, '2019-11-30', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_distriburst`
--

CREATE TABLE `product_distriburst` (
  `id` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `tanggal_kirim` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_has_foto`
--

CREATE TABLE `product_has_foto` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `foto` longtext,
  `status` varchar(45) DEFAULT NULL COMMENT 'updated : File Diganti dengan file lain',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_has_harga_angsuran`
--

CREATE TABLE `product_has_harga_angsuran` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `ansuran` int(11) NOT NULL,
  `period_start` date DEFAULT NULL,
  `period_end` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `harga_total` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_has_harga_jual_pokok`
--

CREATE TABLE `product_has_harga_jual_pokok` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `period_start` date DEFAULT NULL,
  `period_end` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_has_harga_jual_tunai`
--

CREATE TABLE `product_has_harga_jual_tunai` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `period_start` date DEFAULT NULL,
  `period_end` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_kirim`
--

CREATE TABLE `product_kirim` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `document_pengiriman` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_kirim_status`
--

CREATE TABLE `product_kirim_status` (
  `id` int(11) NOT NULL,
  `product_kirim` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'SENT\nRECEIVED\nBROKE',
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_log_stock`
--

CREATE TABLE `product_log_stock` (
  `id` int(11) NOT NULL,
  `product_satuan` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'OUT',
  `qty` int(11) DEFAULT NULL,
  `stok_kategori` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL COMMENT 'PEMBELIAN\nPENGIRIMAN',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_log_stock`
--

INSERT INTO `product_log_stock` (`id`, `product_satuan`, `status`, `qty`, `stok_kategori`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(3, 5371, 'OUT', 1, NULL, 'Faktur Pelanggan', '2019-12-01', 1, NULL, NULL, 0),
(4, 5373, 'IN', 50, 1, 'Pengadaan', '2019-12-01', 1, NULL, NULL, 0),
(5, 5373, 'IN', 50, 1, 'Pengadaan', '2019-12-01', 1, NULL, NULL, 0),
(6, 5395, 'OUT', 1, NULL, 'Faktur Pelanggan', '2019-12-01', 1, NULL, NULL, 0),
(7, 5371, 'OUT', 4, NULL, 'Faktur Pelanggan', '2019-12-01', 1, NULL, NULL, 0),
(9, 5371, 'OUT', 1, 1, 'Faktur Pelanggan', '2019-12-02', 1, NULL, NULL, 0),
(10, 5428, 'OUT', 6, NULL, 'Faktur Pelanggan', '2019-12-03', 1, NULL, NULL, 0),
(11, 5407, 'OUT', 3, NULL, 'Faktur Pelanggan', '2019-12-03', 1, NULL, NULL, 0),
(12, 5371, 'OUT', 1, 1, 'Faktur Pelanggan', '2019-12-04', 1, NULL, NULL, 0),
(13, 5371, 'OUT', 2, 1, 'Faktur Pelanggan', '2019-12-04', 1, NULL, NULL, 0),
(14, 5371, 'OUT', 10, 1, 'Faktur Pelanggan', '2019-12-04', 1, NULL, NULL, 0),
(15, 5371, 'OUT', 1, 1, 'Faktur Pelanggan', '2019-12-04', 1, NULL, NULL, 0),
(16, 5859, 'OUT', 20, 1, 'Faktur Pelanggan', '2019-12-04', 1, NULL, NULL, 0),
(17, 5371, 'OUT', 5, 1, 'Faktur Pelanggan', '2019-12-04', 1, NULL, NULL, 0),
(19, 5372, 'OUT', 1, 1, 'Faktur Pelanggan', '2019-12-06', 1, NULL, NULL, 0),
(20, 5372, 'RETUR', 1, 1, 'Retur Barang Customer', '2019-12-10', 1, NULL, NULL, 0),
(21, 5372, 'RETUR', 1, 1, 'Retur Barang Customer', '2019-12-10', 1, NULL, NULL, 0),
(22, 5372, 'RETUR', 1, 1, 'Retur Barang Customer', '2019-12-10', 1, NULL, NULL, 0),
(23, 5371, 'IN', 10, 1, 'Pengadaan', '2019-12-10', 1, NULL, NULL, 0),
(24, 5371, 'OUT', 2, 1, 'Retur Pengadaan', '2019-12-10', 1, NULL, NULL, 0),
(25, 5371, 'RETUR', 1, 1, 'Retur Barang Customer', '2019-12-12', NULL, NULL, NULL, 0),
(26, 5371, 'RETUR', 12, 1, 'Retur Barang Customer', '2019-12-12', NULL, NULL, NULL, 0),
(27, 5371, 'RETUR', 2, 1, 'Retur Barang Customer', '2019-12-12', NULL, NULL, NULL, 0),
(28, 5371, 'RETUR', 2, 1, 'Retur Barang Customer', '2019-12-12', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_satuan`
--

CREATE TABLE `product_satuan` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `satuan` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL COMMENT 'Isi Dari Satuan',
  `harga` int(11) DEFAULT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_satuan`
--

INSERT INTO `product_satuan` (`id`, `product`, `satuan`, `qty`, `harga`, `harga_beli`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(5371, 2840, 893, 1, 53866, 51684, '2019-11-30', 1, NULL, NULL, 0),
(5372, 2840, 892, 18, 969586, 930312, '2019-11-30', 1, NULL, NULL, 0),
(5373, 2841, 893, 1, 56650, 54355, '2019-11-30', 1, NULL, NULL, 0),
(5374, 2841, 892, 6, 339900, 326130, '2019-11-30', 1, NULL, NULL, 0),
(5375, 2842, 893, 1, 56650, 54355, '2019-11-30', 1, NULL, NULL, 0),
(5376, 2842, 892, 6, 339900, 326130, '2019-11-30', 1, NULL, NULL, 0),
(5377, 2843, 893, 1, 16223, 15574, '2019-11-30', 1, NULL, NULL, 0),
(5378, 2843, 892, 12, 194674, 186888, '2019-11-30', 1, NULL, NULL, 0),
(5379, 2844, 893, 1, 30450, 29232, '2019-11-30', 1, NULL, NULL, 0),
(5380, 2844, 892, 8, 243602, 233856, '2019-11-30', 1, NULL, NULL, 0),
(5381, 2845, 895, 1, 1742, 1671, '2019-11-30', 1, NULL, NULL, 0),
(5382, 2845, 894, 10, 17424, 16710, '2019-11-30', 1, NULL, NULL, 0),
(5383, 2845, 892, 120, 209088, 200520, '2019-11-30', 1, NULL, NULL, 0),
(5384, 2846, 893, 1, 56686, 54390, '2019-11-30', 1, NULL, NULL, 0),
(5385, 2846, 892, 4, 226745, 217560, '2019-11-30', 1, NULL, NULL, 0),
(5386, 2847, 893, 1, 80903, 77626, '2019-11-30', 1, NULL, NULL, 0),
(5387, 2847, 892, 4, 323611, 310504, '2019-11-30', 1, NULL, NULL, 0),
(5388, 2848, 893, 1, 14500, 13800, '2019-11-30', 1, NULL, NULL, 0),
(5389, 2848, 892, 12, 174000, 165600, '2019-11-30', 1, NULL, NULL, 0),
(5390, 2849, 893, 1, 16223, 15574, '2019-11-30', 1, NULL, NULL, 0),
(5391, 2849, 892, 12, 194674, 186888, '2019-11-30', 1, NULL, NULL, 0),
(5392, 2850, 893, 1, 35690, 34261, '2019-11-30', 1, NULL, NULL, 0),
(5393, 2850, 892, 6, 214137, 205566, '2019-11-30', 1, NULL, NULL, 0),
(5394, 2851, 895, 1, 1906, 1829, '2019-11-30', 1, NULL, NULL, 0),
(5395, 2851, 894, 10, 19063, 18290, '2019-11-30', 1, NULL, NULL, 0),
(5396, 2851, 892, 120, 228756, 219480, '2019-11-30', 1, NULL, NULL, 0),
(5397, 2852, 893, 1, 56686, 54390, '2019-11-30', 1, NULL, NULL, 0),
(5398, 2852, 892, 4, 226745, 217560, '2019-11-30', 1, NULL, NULL, 0),
(5399, 2853, 893, 1, 16223, 15574, '2019-11-30', 1, NULL, NULL, 0),
(5400, 2853, 892, 12, 194674, 186888, '2019-11-30', 1, NULL, NULL, 0),
(5401, 2854, 893, 1, 39626, 38041, '2019-11-30', 1, NULL, NULL, 0),
(5402, 2854, 892, 6, 237758, 228246, '2019-11-30', 1, NULL, NULL, 0),
(5403, 2855, 895, 1, 1906, 1829, '2019-11-30', 1, NULL, NULL, 0),
(5404, 2855, 894, 10, 19063, 18290, '2019-11-30', 1, NULL, NULL, 0),
(5405, 2855, 892, 120, 228756, 219480, '2019-11-30', 1, NULL, NULL, 0),
(5406, 2856, 893, 1, 56686, 54390, '2019-11-30', 1, NULL, NULL, 0),
(5407, 2856, 892, 4, 226745, 217560, '2019-11-30', 1, NULL, NULL, 0),
(5408, 2857, 893, 1, 16223, 15565, '2019-11-30', 1, NULL, NULL, 0),
(5409, 2857, 892, 12, 194674, 186780, '2019-11-30', 1, NULL, NULL, 0),
(5410, 2858, 893, 1, 47339, 45445, '2019-11-30', 1, NULL, NULL, 0),
(5411, 2858, 892, 6, 284031, 272670, '2019-11-30', 1, NULL, NULL, 0),
(5412, 2859, 895, 1, 2398, 2300, '2019-11-30', 1, NULL, NULL, 0),
(5413, 2859, 894, 10, 23980, 23000, '2019-11-30', 1, NULL, NULL, 0),
(5414, 2859, 892, 120, 287760, 276000, '2019-11-30', 1, NULL, NULL, 0),
(5415, 2860, 893, 1, 59641, 57225, '2019-11-30', 1, NULL, NULL, 0),
(5416, 2860, 892, 4, 238564, 228900, '2019-11-30', 1, NULL, NULL, 0),
(5417, 2861, 895, 1, 2800, 2686, '2019-11-30', 1, NULL, NULL, 0),
(5418, 2861, 894, 12, 33594, 32232, '2019-11-30', 1, NULL, NULL, 0),
(5419, 2861, 892, 120, 403128, 386784, '2019-11-30', 1, NULL, NULL, 0),
(5420, 2862, 893, 1, 16852, 16169, '2019-11-30', 1, NULL, NULL, 0),
(5421, 2862, 892, 12, 202224, 194028, '2019-11-30', 1, NULL, NULL, 0),
(5422, 2863, 893, 1, 49613, 47628, '2019-11-30', 1, NULL, NULL, 0),
(5423, 2863, 892, 6, 297680, 285768, '2019-11-30', 1, NULL, NULL, 0),
(5424, 2864, 893, 1, 16918, 16232, '2019-11-30', 1, NULL, NULL, 0),
(5425, 2864, 892, 12, 203016, 194784, '2019-11-30', 1, NULL, NULL, 0),
(5426, 2865, 893, 1, 57157, 54842, '2019-11-30', 1, NULL, NULL, 0),
(5427, 2865, 892, 4, 228628, 219368, '2019-11-30', 1, NULL, NULL, 0),
(5428, 2866, 893, 1, 17697, 16980, '2019-11-30', 1, NULL, NULL, 0),
(5429, 2866, 892, 12, 212362, 203760, '2019-11-30', 1, NULL, NULL, 0),
(5430, 2867, 893, 1, 35171, 33746, '2019-11-30', 1, NULL, NULL, 0),
(5431, 2867, 892, 6, 211028, 202476, '2019-11-30', 1, NULL, NULL, 0),
(5432, 2868, 893, 1, 61493, 59002, '2019-11-30', 1, NULL, NULL, 0),
(5433, 2868, 892, 4, 245973, 236008, '2019-11-30', 1, NULL, NULL, 0),
(5434, 2869, 893, 1, 18900, 18134, '2019-11-30', 1, NULL, NULL, 0),
(5435, 2869, 892, 30, 567006, 544020, '2019-11-30', 1, NULL, NULL, 0),
(5436, 2870, 893, 1, 19600, 18806, '2019-11-30', 1, NULL, NULL, 0),
(5437, 2870, 892, 15, 293997, 282090, '2019-11-30', 1, NULL, NULL, 0),
(5438, 2871, 893, 1, 15700, 15064, '2019-11-30', 1, NULL, NULL, 0),
(5439, 2871, 892, 15, 235505, 225960, '2019-11-30', 1, NULL, NULL, 0),
(5440, 2872, 893, 1, 19600, 18806, '2019-11-30', 1, NULL, NULL, 0),
(5441, 2872, 892, 15, 293997, 282090, '2019-11-30', 1, NULL, NULL, 0),
(5442, 2873, 893, 1, 6300, 6044, '2019-11-30', 1, NULL, NULL, 0),
(5443, 2873, 892, 48, 302386, 290112, '2019-11-30', 1, NULL, NULL, 0),
(5444, 2874, 893, 1, 3400, 3264, '2019-11-30', 1, NULL, NULL, 0),
(5445, 2874, 892, 60, 204006, 195840, '2019-11-30', 1, NULL, NULL, 0),
(5446, 2875, 893, 1, 8201, 7868, '2019-11-30', 1, NULL, NULL, 0),
(5447, 2875, 892, 24, 196812, 188832, '2019-11-30', 1, NULL, NULL, 0),
(5448, 2876, 893, 1, 11600, 11129, '2019-11-30', 1, NULL, NULL, 0),
(5449, 2876, 892, 24, 278388, 267096, '2019-11-30', 1, NULL, NULL, 0),
(5450, 2877, 893, 1, 12000, 11513, '2019-11-30', 1, NULL, NULL, 0),
(5451, 2877, 892, 24, 288000, 276312, '2019-11-30', 1, NULL, NULL, 0),
(5452, 2878, 893, 1, 16250, 15543, '2019-11-30', 1, NULL, NULL, 0),
(5453, 2878, 927, 24, 390000, 373032, '2019-11-30', 1, NULL, NULL, 0),
(5454, 2879, 893, 1, 13200, 12672, '2019-11-30', 1, NULL, NULL, 0),
(5455, 2879, 892, 24, 316800, 304128, '2019-11-30', 1, NULL, NULL, 0),
(5456, 2880, 893, 1, 7500, 7200, '2019-11-30', 1, NULL, NULL, 0),
(5457, 2880, 927, 48, 359990, 345600, '2019-11-30', 1, NULL, NULL, 0),
(5458, 2881, 893, 1, 14600, 14016, '2019-11-30', 1, NULL, NULL, 0),
(5459, 2881, 892, 24, 350407, 336384, '2019-11-30', 1, NULL, NULL, 0),
(5460, 2882, 893, 1, 11700, 11232, '2019-11-30', 1, NULL, NULL, 0),
(5461, 2882, 892, 24, 280790, 269568, '2019-11-30', 1, NULL, NULL, 0),
(5462, 2883, 893, 1, 7900, 7584, '2019-11-30', 1, NULL, NULL, 0),
(5463, 2883, 892, 48, 379210, 364032, '2019-11-30', 1, NULL, NULL, 0),
(5464, 2884, 893, 1, 15400, 14784, '2019-11-30', 1, NULL, NULL, 0),
(5465, 2884, 892, 24, 369600, 354816, '2019-11-30', 1, NULL, NULL, 0),
(5466, 2885, 893, 1, 7101, 6816, '2019-11-30', 1, NULL, NULL, 0),
(5467, 2885, 892, 40, 284020, 272640, '2019-11-30', 1, NULL, NULL, 0),
(5468, 2886, 893, 1, 13900, 13344, '2019-11-30', 1, NULL, NULL, 0),
(5469, 2886, 892, 24, 333600, 320256, '2019-11-30', 1, NULL, NULL, 0),
(5470, 2887, 893, 1, 8700, 8352, '2019-11-30', 1, NULL, NULL, 0),
(5471, 2887, 892, 40, 347996, 334080, '2019-11-30', 1, NULL, NULL, 0),
(5472, 2888, 893, 1, 16900, 16224, '2019-11-30', 1, NULL, NULL, 0),
(5473, 2888, 892, 24, 405600, 389376, '2019-11-30', 1, NULL, NULL, 0),
(5474, 2889, 893, 1, 41727, 40057, '2019-11-30', 1, NULL, NULL, 0),
(5475, 2889, 892, 10, 417274, 400570, '2019-11-30', 1, NULL, NULL, 0),
(5476, 2890, 893, 1, 43582, 41816, '2019-11-30', 1, NULL, NULL, 0),
(5477, 2890, 892, 10, 435820, 418160, '2019-11-30', 1, NULL, NULL, 0),
(5478, 2891, 893, 1, 172200, 165225, '2019-11-30', 1, NULL, NULL, 0),
(5479, 2891, 892, 3, 516599, 495675, '2019-11-30', 1, NULL, NULL, 0),
(5480, 2892, 893, 1, 172200, 165225, '2019-11-30', 1, NULL, NULL, 0),
(5481, 2892, 892, 3, 516599, 495675, '2019-11-30', 1, NULL, NULL, 0),
(5482, 2893, 893, 1, 43582, 41816, '2019-11-30', 1, NULL, NULL, 0),
(5483, 2893, 892, 10, 435820, 418160, '2019-11-30', 1, NULL, NULL, 0),
(5484, 2894, 895, 1, 5254, 5043, '2019-11-30', 1, NULL, NULL, 0),
(5485, 2894, 894, 4, 21014, 20172, '2019-11-30', 1, NULL, NULL, 0),
(5486, 2894, 892, 48, 252173, 242064, '2019-11-30', 1, NULL, NULL, 0),
(5487, 2895, 893, 1, 53673, 51526, '2019-11-30', 1, NULL, NULL, 0),
(5488, 2895, 892, 12, 644081, 618312, '2019-11-30', 1, NULL, NULL, 0),
(5489, 2896, 895, 1, 6129, 5883, '2019-11-30', 1, NULL, NULL, 0),
(5490, 2896, 894, 4, 24517, 23532, '2019-11-30', 1, NULL, NULL, 0),
(5491, 2896, 892, 48, 294202, 282384, '2019-11-30', 1, NULL, NULL, 0),
(5492, 2897, 897, 1, 2108, 1963, '2019-11-30', 1, NULL, NULL, 0),
(5493, 2897, 898, 10, 21080, 19634, '2019-11-30', 1, NULL, NULL, 0),
(5494, 2898, 897, 1, 2108, 1963, '2019-11-30', 1, NULL, NULL, 0),
(5495, 2898, 898, 10, 21080, 19634, '2019-11-30', 1, NULL, NULL, 0),
(5496, 2899, 899, 1, 3740, 3483, '2019-11-30', 1, NULL, NULL, 0),
(5497, 2899, 896, 7, 26180, 24383, '2019-11-30', 1, NULL, NULL, 0),
(5498, 2899, 892, 21, 78540, 73150, '2019-11-30', 1, NULL, NULL, 0),
(5499, 2900, 899, 1, 3740, 3483, '2019-11-30', 1, NULL, NULL, 0),
(5500, 2900, 896, 7, 26180, 24383, '2019-11-30', 1, NULL, NULL, 0),
(5501, 2900, 892, 21, 78540, 73150, '2019-11-30', 1, NULL, NULL, 0),
(5502, 2901, 899, 1, 3740, 3483, '2019-11-30', 1, NULL, NULL, 0),
(5503, 2901, 896, 7, 26180, 24383, '2019-11-30', 1, NULL, NULL, 0),
(5504, 2901, 892, 21, 78540, 73150, '2019-11-30', 1, NULL, NULL, 0),
(5505, 2902, 899, 1, 3740, 3483, '2019-11-30', 1, NULL, NULL, 0),
(5506, 2902, 896, 7, 26180, 24383, '2019-11-30', 1, NULL, NULL, 0),
(5507, 2902, 892, 21, 78540, 73150, '2019-11-30', 1, NULL, NULL, 0),
(5508, 2903, 893, 1, 1615, 1504, '2019-11-30', 1, NULL, NULL, 0),
(5509, 2903, 892, 24, 38760, 36100, '2019-11-30', 1, NULL, NULL, 0),
(5510, 2904, 893, 1, 1615, 1504, '2019-11-30', 1, NULL, NULL, 0),
(5511, 2904, 892, 24, 38760, 36100, '2019-11-30', 1, NULL, NULL, 0),
(5512, 2905, 899, 1, 413, 384, '2019-11-30', 1, NULL, NULL, 0),
(5513, 2905, 896, 20, 8254, 7687, '2019-11-30', 1, NULL, NULL, 0),
(5514, 2905, 892, 120, 49521, 46123, '2019-11-30', 1, NULL, NULL, 0),
(5515, 2906, 899, 1, 413, 384, '2019-11-30', 1, NULL, NULL, 0),
(5516, 2906, 896, 20, 8254, 7687, '2019-11-30', 1, NULL, NULL, 0),
(5517, 2906, 892, 120, 49521, 46123, '2019-11-30', 1, NULL, NULL, 0),
(5518, 2907, 899, 1, 5893, 5489, '2019-11-30', 1, NULL, NULL, 0),
(5519, 2907, 896, 6, 35360, 32933, '2019-11-30', 1, NULL, NULL, 0),
(5520, 2907, 892, 18, 106080, 98800, '2019-11-30', 1, NULL, NULL, 0),
(5521, 2908, 899, 1, 391, 364, '2019-11-30', 1, NULL, NULL, 0),
(5522, 2908, 896, 20, 7820, 7283, '2019-11-30', 1, NULL, NULL, 0),
(5523, 2908, 892, 120, 46920, 43700, '2019-11-30', 1, NULL, NULL, 0),
(5524, 2909, 899, 1, 391, 364, '2019-11-30', 1, NULL, NULL, 0),
(5525, 2909, 896, 20, 7820, 7283, '2019-11-30', 1, NULL, NULL, 0),
(5526, 2909, 892, 120, 46920, 43700, '2019-11-30', 1, NULL, NULL, 0),
(5527, 2910, 896, 1, 8000, 7283, '2019-11-30', 1, NULL, NULL, 0),
(5528, 2910, 892, 6, 48000, 43700, '2019-11-30', 1, NULL, NULL, 0),
(5529, 2911, 899, 1, 408, 380, '2019-11-30', 1, NULL, NULL, 0),
(5530, 2911, 896, 20, 8160, 7600, '2019-11-30', 1, NULL, NULL, 0),
(5531, 2911, 892, 120, 48960, 45600, '2019-11-30', 1, NULL, NULL, 0),
(5532, 2912, 899, 1, 1890, 1742, '2019-11-30', 1, NULL, NULL, 0),
(5533, 2912, 896, 10, 18900, 17417, '2019-11-30', 1, NULL, NULL, 0),
(5534, 2912, 892, 30, 56500, 52248, '2019-11-30', 1, NULL, NULL, 0),
(5535, 2913, 899, 1, 2108, 1963, '2019-11-30', 1, NULL, NULL, 0),
(5536, 2913, 896, 10, 21080, 19633, '2019-11-30', 1, NULL, NULL, 0),
(5537, 2913, 892, 30, 63240, 58900, '2019-11-30', 1, NULL, NULL, 0),
(5538, 2914, 899, 1, 2108, 1963, '2019-11-30', 1, NULL, NULL, 0),
(5539, 2914, 896, 10, 21080, 19633, '2019-11-30', 1, NULL, NULL, 0),
(5540, 2914, 892, 30, 63240, 58900, '2019-11-30', 1, NULL, NULL, 0),
(5541, 2915, 899, 1, 391, 364, '2019-11-30', 1, NULL, NULL, 0),
(5542, 2915, 896, 20, 7820, 7283, '2019-11-30', 1, NULL, NULL, 0),
(5543, 2915, 892, 120, 46920, 43700, '2019-11-30', 1, NULL, NULL, 0),
(5544, 2916, 899, 1, 391, 364, '2019-11-30', 1, NULL, NULL, 0),
(5545, 2916, 896, 20, 7820, 7283, '2019-11-30', 1, NULL, NULL, 0),
(5546, 2916, 892, 120, 46920, 43700, '2019-11-30', 1, NULL, NULL, 0),
(5547, 2917, 897, 1, 5100, 4597, '2019-11-30', 1, NULL, NULL, 0),
(5548, 2917, 898, 10, 51001, 45968, '2019-11-30', 1, NULL, NULL, 0),
(5549, 2918, 899, 1, 408, 380, '2019-11-30', 1, NULL, NULL, 0),
(5550, 2918, 896, 10, 4080, 3800, '2019-11-30', 1, NULL, NULL, 0),
(5551, 2918, 892, 90, 36720, 34200, '2019-11-30', 1, NULL, NULL, 0),
(5552, 2919, 899, 1, 408, 380, '2019-11-30', 1, NULL, NULL, 0),
(5553, 2919, 896, 10, 4080, 3800, '2019-11-30', 1, NULL, NULL, 0),
(5554, 2919, 892, 90, 36720, 34200, '2019-11-30', 1, NULL, NULL, 0),
(5555, 2920, 893, 1, 3162, 2945, '2019-11-30', 1, NULL, NULL, 0),
(5556, 2920, 892, 10, 31620, 29450, '2019-11-30', 1, NULL, NULL, 0),
(5557, 2921, 895, 1, 391, 364, '2019-11-30', 1, NULL, NULL, 0),
(5558, 2921, 894, 10, 3910, 3642, '2019-11-30', 1, NULL, NULL, 0),
(5559, 2921, 892, 120, 46920, 43700, '2019-11-30', 1, NULL, NULL, 0),
(5560, 2922, 899, 1, 3740, 3483, '2019-11-30', 1, NULL, NULL, 0),
(5561, 2922, 896, 7, 26180, 24383, '2019-11-30', 1, NULL, NULL, 0),
(5562, 2922, 892, 21, 78540, 73150, '2019-11-30', 1, NULL, NULL, 0),
(5563, 2923, 899, 1, 3740, 3483, '2019-11-30', 1, NULL, NULL, 0),
(5564, 2923, 896, 7, 26180, 24383, '2019-11-30', 1, NULL, NULL, 0),
(5565, 2923, 892, 21, 78540, 73150, '2019-11-30', 1, NULL, NULL, 0),
(5566, 2924, 899, 1, 3740, 3483, '2019-11-30', 1, NULL, NULL, 0),
(5567, 2924, 896, 7, 26180, 24383, '2019-11-30', 1, NULL, NULL, 0),
(5568, 2924, 892, 21, 78540, 73150, '2019-11-30', 1, NULL, NULL, 0),
(5569, 2925, 899, 1, 3740, 3483, '2019-11-30', 1, NULL, NULL, 0),
(5570, 2925, 896, 7, 26180, 24383, '2019-11-30', 1, NULL, NULL, 0),
(5571, 2925, 892, 21, 78540, 73150, '2019-11-30', 1, NULL, NULL, 0),
(5572, 2926, 899, 1, 3740, 3483, '2019-11-30', 1, NULL, NULL, 0),
(5573, 2926, 896, 7, 26180, 24383, '2019-11-30', 1, NULL, NULL, 0),
(5574, 2926, 892, 21, 78540, 73150, '2019-11-30', 1, NULL, NULL, 0),
(5575, 2927, 897, 1, 5100, 4553, '2019-11-30', 1, NULL, NULL, 0),
(5576, 2927, 898, 10, 51000, 45531, '2019-11-30', 1, NULL, NULL, 0),
(5577, 2928, 897, 1, 5542, 4840, '2019-11-30', 1, NULL, NULL, 0),
(5578, 2928, 898, 12, 66500, 58080, '2019-11-30', 1, NULL, NULL, 0),
(5579, 2929, 893, 1, 11000, 9360, '2019-11-30', 1, NULL, NULL, 0),
(5580, 2930, 899, 1, 11000, 9360, '2019-11-30', 1, NULL, NULL, 0),
(5581, 2930, 896, 12, 132000, 112320, '2019-11-30', 1, NULL, NULL, 0),
(5582, 2931, 899, 1, 1600, 1456, '2019-11-30', 1, NULL, NULL, 0),
(5583, 2931, 896, 10, 16000, 14566, '2019-11-30', 1, NULL, NULL, 0),
(5584, 2931, 892, 30, 48000, 43700, '2019-11-30', 1, NULL, NULL, 0),
(5585, 2932, 899, 1, 9000, 7560, '2019-11-30', 1, NULL, NULL, 0),
(5586, 2932, 896, 12, 108000, 90720, '2019-11-30', 1, NULL, NULL, 0),
(5587, 2933, 899, 1, 9000, 7560, '2019-11-30', 1, NULL, NULL, 0),
(5588, 2933, 896, 12, 108000, 90720, '2019-11-30', 1, NULL, NULL, 0),
(5589, 2934, 895, 1, 360, 342, '2019-11-30', 1, NULL, NULL, 0),
(5590, 2934, 894, 20, 7200, 6840, '2019-11-30', 1, NULL, NULL, 0),
(5591, 2934, 892, 240, 86400, 82080, '2019-11-30', 1, NULL, NULL, 0),
(5592, 2935, 895, 1, 360, 342, '2019-11-30', 1, NULL, NULL, 0),
(5593, 2935, 894, 20, 7200, 6840, '2019-11-30', 1, NULL, NULL, 0),
(5594, 2935, 892, 240, 86400, 82080, '2019-11-30', 1, NULL, NULL, 0),
(5595, 2936, 895, 1, 360, 342, '2019-11-30', 1, NULL, NULL, 0),
(5596, 2936, 894, 10, 3600, 3420, '2019-11-30', 1, NULL, NULL, 0),
(5597, 2936, 892, 200, 72000, 68400, '2019-11-30', 1, NULL, NULL, 0),
(5598, 2937, 895, 1, 360, 342, '2019-11-30', 1, NULL, NULL, 0),
(5599, 2937, 894, 10, 3600, 3420, '2019-11-30', 1, NULL, NULL, 0),
(5600, 2937, 892, 200, 72000, 68400, '2019-11-30', 1, NULL, NULL, 0),
(5601, 2938, 895, 1, 360, 342, '2019-11-30', 1, NULL, NULL, 0),
(5602, 2938, 894, 10, 3600, 3420, '2019-11-30', 1, NULL, NULL, 0),
(5603, 2938, 892, 200, 72000, 68400, '2019-11-30', 1, NULL, NULL, 0),
(5604, 2939, 900, 1, 4500, 4180, '2019-11-30', 1, NULL, NULL, 0),
(5605, 2939, 892, 20, 90000, 83600, '2019-11-30', 1, NULL, NULL, 0),
(5606, 2940, 895, 1, 360, 342, '2019-11-30', 1, NULL, NULL, 0),
(5607, 2940, 894, 10, 3600, 3420, '2019-11-30', 1, NULL, NULL, 0),
(5608, 2940, 892, 200, 72000, 68400, '2019-11-30', 1, NULL, NULL, 0),
(5609, 2941, 893, 1, 4500, 4180, '2019-11-30', 1, NULL, NULL, 0),
(5610, 2941, 892, 20, 90000, 83600, '2019-11-30', 1, NULL, NULL, 0),
(5611, 2942, 900, 1, 4000, 3800, '2019-11-30', 1, NULL, NULL, 0),
(5612, 2942, 892, 24, 96000, 91200, '2019-11-30', 1, NULL, NULL, 0),
(5613, 2943, 900, 1, 4000, 3800, '2019-11-30', 1, NULL, NULL, 0),
(5614, 2943, 892, 24, 96000, 91200, '2019-11-30', 1, NULL, NULL, 0),
(5615, 2944, 894, 1, 7200, 6840, '2019-11-30', 1, NULL, NULL, 0),
(5616, 2944, 892, 10, 72000, 68400, '2019-11-30', 1, NULL, NULL, 0),
(5617, 2945, 900, 1, 16000, 15200, '2019-11-30', 1, NULL, NULL, 0),
(5618, 2945, 892, 6, 96000, 91200, '2019-11-30', 1, NULL, NULL, 0),
(5619, 2946, 901, 1, 11250, 10450, '2019-11-30', 1, NULL, NULL, 0),
(5620, 2946, 892, 12, 135000, 125400, '2019-11-30', 1, NULL, NULL, 0),
(5621, 2947, 899, 1, 400, 372, '2019-11-30', 1, NULL, NULL, 0),
(5622, 2947, 896, 24, 9600, 8928, '2019-11-30', 1, NULL, NULL, 0),
(5623, 2947, 892, 144, 57600, 53568, '2019-11-30', 1, NULL, NULL, 0),
(5624, 2948, 893, 1, 13500, 11520, '2019-11-30', 1, NULL, NULL, 0),
(5625, 2949, 893, 1, 11000, 9360, '2019-11-30', 1, NULL, NULL, 0),
(5626, 2950, 901, 1, 10800, 9975, '2019-11-30', 1, NULL, NULL, 0),
(5627, 2950, 892, 10, 64800, 59850, '2019-11-30', 1, NULL, NULL, 0),
(5628, 2951, 901, 1, 10800, 9975, '2019-11-30', 1, NULL, NULL, 0),
(5629, 2951, 892, 10, 64800, 59850, '2019-11-30', 1, NULL, NULL, 0),
(5630, 2952, 895, 1, 1325, 1257, '2019-11-30', 1, NULL, NULL, 0),
(5631, 2952, 894, 10, 13250, 12569, '2019-11-30', 1, NULL, NULL, 0),
(5632, 2952, 892, 120, 159000, 150822, '2019-11-30', 1, NULL, NULL, 0),
(5633, 2953, 893, 1, 7000, 5863, '2019-11-30', 1, NULL, NULL, 0),
(5634, 2954, 893, 1, 7000, 5863, '2019-11-30', 1, NULL, NULL, 0),
(5635, 2955, 893, 1, 7000, 5863, '2019-11-30', 1, NULL, NULL, 0),
(5636, 2956, 899, 1, 344, 323, '2019-11-30', 1, NULL, NULL, 0),
(5637, 2956, 896, 40, 13750, 12925, '2019-11-30', 1, NULL, NULL, 0),
(5638, 2956, 892, 160, 55000, 51700, '2019-11-30', 1, NULL, NULL, 0),
(5639, 2957, 899, 1, 342, 321, '2019-11-30', 1, NULL, NULL, 0),
(5640, 2957, 896, 10, 3417, 3212, '2019-11-30', 1, NULL, NULL, 0),
(5641, 2957, 892, 120, 41000, 38540, '2019-11-30', 1, NULL, NULL, 0),
(5642, 2958, 899, 1, 390, 367, '2019-11-30', 1, NULL, NULL, 0),
(5643, 2958, 896, 40, 15600, 14664, '2019-11-30', 1, NULL, NULL, 0),
(5644, 2958, 892, 120, 46800, 43992, '2019-11-30', 1, NULL, NULL, 0),
(5645, 2959, 899, 1, 338, 317, '2019-11-30', 1, NULL, NULL, 0),
(5646, 2959, 896, 12, 4050, 3807, '2019-11-30', 1, NULL, NULL, 0),
(5647, 2959, 892, 120, 40500, 38070, '2019-11-30', 1, NULL, NULL, 0),
(5648, 2960, 899, 1, 390, 367, '2019-11-30', 1, NULL, NULL, 0),
(5649, 2960, 896, 40, 15600, 14664, '2019-11-30', 1, NULL, NULL, 0),
(5650, 2960, 892, 120, 46800, 43992, '2019-11-30', 1, NULL, NULL, 0),
(5651, 2961, 893, 1, 350, 329, '2019-11-30', 1, NULL, NULL, 0),
(5652, 2961, 933, 30, 10500, 9870, '2019-11-30', 1, NULL, NULL, 0),
(5653, 2961, 932, 90, 31500, 29610, '2019-11-30', 1, NULL, NULL, 0),
(5654, 2962, 899, 1, 390, 367, '2019-11-30', 1, NULL, NULL, 0),
(5655, 2962, 896, 40, 15600, 14664, '2019-11-30', 1, NULL, NULL, 0),
(5656, 2962, 892, 120, 46800, 43992, '2019-11-30', 1, NULL, NULL, 0),
(5657, 2963, 899, 1, 390, 367, '2019-11-30', 1, NULL, NULL, 0),
(5658, 2963, 896, 40, 15600, 14664, '2019-11-30', 1, NULL, NULL, 0),
(5659, 2963, 892, 120, 46800, 43992, '2019-11-30', 1, NULL, NULL, 0),
(5660, 2964, 893, 1, 750, 705, '2019-11-30', 1, NULL, NULL, 0),
(5661, 2964, 892, 24, 18000, 16920, '2019-11-30', 1, NULL, NULL, 0),
(5662, 2965, 899, 1, 400, 372, '2019-11-30', 1, NULL, NULL, 0),
(5663, 2965, 896, 20, 8000, 7440, '2019-11-30', 1, NULL, NULL, 0),
(5664, 2965, 892, 120, 48000, 44640, '2019-11-30', 1, NULL, NULL, 0),
(5665, 2966, 899, 1, 400, 372, '2019-11-30', 1, NULL, NULL, 0),
(5666, 2966, 896, 20, 8000, 7440, '2019-11-30', 1, NULL, NULL, 0),
(5667, 2966, 892, 120, 48000, 44640, '2019-11-30', 1, NULL, NULL, 0),
(5668, 2967, 899, 1, 400, 372, '2019-11-30', 1, NULL, NULL, 0),
(5669, 2967, 896, 20, 8000, 7440, '2019-11-30', 1, NULL, NULL, 0),
(5670, 2967, 892, 120, 48000, 44640, '2019-11-30', 1, NULL, NULL, 0),
(5671, 2968, 899, 1, 390, 367, '2019-11-30', 1, NULL, NULL, 0),
(5672, 2968, 896, 10, 3900, 3666, '2019-11-30', 1, NULL, NULL, 0),
(5673, 2968, 892, 150, 58500, 54990, '2019-11-30', 1, NULL, NULL, 0),
(5674, 2969, 899, 1, 667, 627, '2019-11-30', 1, NULL, NULL, 0),
(5675, 2969, 896, 6, 4000, 3760, '2019-11-30', 1, NULL, NULL, 0),
(5676, 2969, 892, 60, 40000, 37600, '2019-11-30', 1, NULL, NULL, 0),
(5677, 2970, 899, 1, 722, 679, '2019-11-30', 1, NULL, NULL, 0),
(5678, 2970, 896, 12, 8667, 8147, '2019-11-30', 1, NULL, NULL, 0),
(5679, 2970, 892, 72, 52000, 48880, '2019-11-30', 1, NULL, NULL, 0),
(5680, 2971, 893, 1, 750, 705, '2019-11-30', 1, NULL, NULL, 0),
(5681, 2971, 892, 24, 18000, 16920, '2019-11-30', 1, NULL, NULL, 0),
(5682, 2972, 899, 1, 367, 345, '2019-11-30', 1, NULL, NULL, 0),
(5683, 2972, 896, 10, 3667, 3447, '2019-11-30', 1, NULL, NULL, 0),
(5684, 2972, 892, 120, 44000, 41360, '2019-11-30', 1, NULL, NULL, 0),
(5685, 2973, 899, 1, 380, 357, '2019-11-30', 1, NULL, NULL, 0),
(5686, 2973, 896, 10, 3800, 3572, '2019-11-30', 1, NULL, NULL, 0),
(5687, 2973, 892, 100, 38000, 35720, '2019-11-30', 1, NULL, NULL, 0),
(5688, 2974, 899, 1, 380, 357, '2019-11-30', 1, NULL, NULL, 0),
(5689, 2974, 896, 10, 3800, 3572, '2019-11-30', 1, NULL, NULL, 0),
(5690, 2974, 892, 100, 38000, 35720, '2019-11-30', 1, NULL, NULL, 0),
(5691, 2975, 899, 1, 383, 360, '2019-11-30', 1, NULL, NULL, 0),
(5692, 2975, 896, 50, 19167, 18017, '2019-11-30', 1, NULL, NULL, 0),
(5693, 2975, 892, 150, 57500, 54050, '2019-11-30', 1, NULL, NULL, 0),
(5694, 2976, 893, 1, 361, 339, '2019-11-30', 1, NULL, NULL, 0),
(5695, 2976, 934, 18, 6500, 6110, '2019-11-30', 1, NULL, NULL, 0),
(5696, 2976, 935, 72, 26000, 24440, '2019-11-30', 1, NULL, NULL, 0),
(5697, 2977, 899, 1, 358, 336, '2019-11-30', 1, NULL, NULL, 0),
(5698, 2977, 896, 24, 8583, 8068, '2019-11-30', 1, NULL, NULL, 0),
(5699, 2977, 892, 144, 51500, 48410, '2019-11-30', 1, NULL, NULL, 0),
(5700, 2978, 899, 1, 358, 336, '2019-11-30', 1, NULL, NULL, 0),
(5701, 2978, 896, 24, 8583, 8068, '2019-11-30', 1, NULL, NULL, 0),
(5702, 2978, 892, 144, 51500, 48410, '2019-11-30', 1, NULL, NULL, 0),
(5703, 2979, 893, 1, 4100, 3823, '2019-11-30', 1, NULL, NULL, 0),
(5704, 2979, 892, 15, 61500, 57340, '2019-11-30', 1, NULL, NULL, 0),
(5705, 2980, 893, 1, 361, 332, '2019-11-30', 1, NULL, NULL, 0),
(5706, 2980, 933, 60, 21667, 19897, '2019-11-30', 1, NULL, NULL, 0),
(5707, 2980, 932, 180, 65000, 59690, '2019-11-30', 1, NULL, NULL, 0),
(5708, 2981, 899, 1, 400, 372, '2019-11-30', 1, NULL, NULL, 0),
(5709, 2981, 896, 20, 8000, 7440, '2019-11-30', 1, NULL, NULL, 0),
(5710, 2981, 892, 120, 48000, 44640, '2019-11-30', 1, NULL, NULL, 0),
(5711, 2982, 899, 1, 400, 372, '2019-11-30', 1, NULL, NULL, 0),
(5712, 2982, 896, 20, 8000, 7440, '2019-11-30', 1, NULL, NULL, 0),
(5713, 2982, 892, 120, 48000, 44640, '2019-11-30', 1, NULL, NULL, 0),
(5714, 2983, 893, 1, 396, 372, '2019-11-30', 1, NULL, NULL, 0),
(5715, 2983, 892, 48, 19000, 17860, '2019-11-30', 1, NULL, NULL, 0),
(5716, 2984, 893, 1, 361, 339, '2019-11-30', 1, NULL, NULL, 0),
(5717, 2984, 934, 18, 6500, 6110, '2019-11-30', 1, NULL, NULL, 0),
(5718, 2984, 935, 72, 26000, 24440, '2019-11-30', 1, NULL, NULL, 0),
(5719, 2985, 893, 1, 381, 358, '2019-11-30', 1, NULL, NULL, 0),
(5720, 2985, 933, 60, 22833, 21463, '2019-11-30', 1, NULL, NULL, 0),
(5721, 2985, 932, 180, 68500, 64390, '2019-11-30', 1, NULL, NULL, 0),
(5722, 2986, 893, 1, 719, 676, '2019-11-30', 1, NULL, NULL, 0),
(5723, 2986, 892, 48, 34500, 32430, '2019-11-30', 1, NULL, NULL, 0),
(5724, 2987, 893, 1, 0, 2232, '2019-11-30', 1, NULL, NULL, 0),
(5725, 2988, 893, 1, 5500, 4320, '2019-11-30', 1, NULL, NULL, 0),
(5726, 2989, 894, 1, 3800, 3572, '2019-11-30', 1, NULL, NULL, 0),
(5727, 2989, 892, 15, 57000, 53580, '2019-11-30', 1, NULL, NULL, 0),
(5728, 2990, 947, 1, 276, 262, '2019-11-30', 1, NULL, NULL, 0),
(5729, 2990, 904, 10, 2758, 2620, '2019-11-30', 1, NULL, NULL, 0),
(5730, 2990, 896, 60, 16550, 15723, '2019-11-30', 1, NULL, NULL, 0),
(5731, 2990, 892, 600, 165500, 157225, '2019-11-30', 1, NULL, NULL, 0),
(5732, 2991, 947, 1, 276, 262, '2019-11-30', 1, NULL, NULL, 0),
(5733, 2991, 904, 10, 2758, 2620, '2019-11-30', 1, NULL, NULL, 0),
(5734, 2991, 896, 60, 16550, 15723, '2019-11-30', 1, NULL, NULL, 0),
(5735, 2991, 892, 600, 165500, 157225, '2019-11-30', 1, NULL, NULL, 0),
(5736, 2992, 947, 1, 276, 262, '2019-11-30', 1, NULL, NULL, 0),
(5737, 2992, 904, 10, 2758, 2620, '2019-11-30', 1, NULL, NULL, 0),
(5738, 2992, 896, 60, 16550, 15723, '2019-11-30', 1, NULL, NULL, 0),
(5739, 2992, 892, 600, 165500, 157225, '2019-11-30', 1, NULL, NULL, 0),
(5740, 2993, 947, 1, 276, 262, '2019-11-30', 1, NULL, NULL, 0),
(5741, 2993, 904, 10, 2758, 2620, '2019-11-30', 1, NULL, NULL, 0),
(5742, 2993, 896, 60, 16550, 15723, '2019-11-30', 1, NULL, NULL, 0),
(5743, 2993, 892, 600, 165500, 157225, '2019-11-30', 1, NULL, NULL, 0),
(5744, 2994, 947, 1, 276, 262, '2019-11-30', 1, NULL, NULL, 0),
(5745, 2994, 904, 10, 2758, 2620, '2019-11-30', 1, NULL, NULL, 0),
(5746, 2994, 896, 60, 16550, 15723, '2019-11-30', 1, NULL, NULL, 0),
(5747, 2994, 892, 600, 165500, 157225, '2019-11-30', 1, NULL, NULL, 0),
(5748, 2995, 947, 1, 276, 262, '2019-11-30', 1, NULL, NULL, 0),
(5749, 2995, 904, 10, 2758, 2620, '2019-11-30', 1, NULL, NULL, 0),
(5750, 2995, 896, 60, 16550, 15723, '2019-11-30', 1, NULL, NULL, 0),
(5751, 2995, 892, 600, 165500, 157225, '2019-11-30', 1, NULL, NULL, 0),
(5752, 2996, 947, 1, 276, 262, '2019-11-30', 1, NULL, NULL, 0),
(5753, 2996, 904, 10, 2758, 2620, '2019-11-30', 1, NULL, NULL, 0),
(5754, 2996, 896, 60, 16550, 15723, '2019-11-30', 1, NULL, NULL, 0),
(5755, 2996, 892, 600, 165500, 157225, '2019-11-30', 1, NULL, NULL, 0),
(5756, 2997, 947, 1, 276, 262, '2019-11-30', 1, NULL, NULL, 0),
(5757, 2997, 904, 10, 2758, 2620, '2019-11-30', 1, NULL, NULL, 0),
(5758, 2997, 896, 60, 16550, 15723, '2019-11-30', 1, NULL, NULL, 0),
(5759, 2997, 892, 600, 165500, 157225, '2019-11-30', 1, NULL, NULL, 0),
(5760, 2998, 947, 1, 276, 262, '2019-11-30', 1, NULL, NULL, 0),
(5761, 2998, 904, 10, 2758, 2620, '2019-11-30', 1, NULL, NULL, 0),
(5762, 2998, 896, 60, 16550, 15723, '2019-11-30', 1, NULL, NULL, 0),
(5763, 2998, 892, 600, 165500, 157225, '2019-11-30', 1, NULL, NULL, 0),
(5764, 2999, 947, 1, 276, 262, '2019-11-30', 1, NULL, NULL, 0),
(5765, 2999, 904, 10, 2758, 2620, '2019-11-30', 1, NULL, NULL, 0),
(5766, 2999, 896, 60, 16550, 15723, '2019-11-30', 1, NULL, NULL, 0),
(5767, 2999, 892, 600, 165500, 157225, '2019-11-30', 1, NULL, NULL, 0),
(5768, 3000, 947, 1, 276, 262, '2019-11-30', 1, NULL, NULL, 0),
(5769, 3000, 904, 10, 2758, 2620, '2019-11-30', 1, NULL, NULL, 0),
(5770, 3000, 896, 60, 16550, 15723, '2019-11-30', 1, NULL, NULL, 0),
(5771, 3000, 892, 600, 165500, 157225, '2019-11-30', 1, NULL, NULL, 0),
(5772, 3001, 947, 1, 276, 262, '2019-11-30', 1, NULL, NULL, 0),
(5773, 3001, 904, 10, 2758, 2620, '2019-11-30', 1, NULL, NULL, 0),
(5774, 3001, 896, 60, 16550, 15723, '2019-11-30', 1, NULL, NULL, 0),
(5775, 3001, 892, 600, 165500, 157225, '2019-11-30', 1, NULL, NULL, 0),
(5776, 3002, 895, 1, 900, 852, '2019-11-30', 1, NULL, NULL, 0),
(5777, 3002, 894, 10, 9000, 8516, '2019-11-30', 1, NULL, NULL, 0),
(5778, 3002, 892, 120, 108000, 102190, '2019-11-30', 1, NULL, NULL, 0),
(5779, 3003, 895, 1, 900, 852, '2019-11-30', 1, NULL, NULL, 0),
(5780, 3003, 894, 10, 9000, 8516, '2019-11-30', 1, NULL, NULL, 0),
(5781, 3003, 892, 120, 108000, 102190, '2019-11-30', 1, NULL, NULL, 0),
(5782, 3004, 895, 1, 950, 900, '2019-11-30', 1, NULL, NULL, 0),
(5783, 3004, 894, 10, 9500, 9005, '2019-11-30', 1, NULL, NULL, 0),
(5784, 3004, 892, 120, 114000, 108054, '2019-11-30', 1, NULL, NULL, 0),
(5785, 3005, 895, 1, 925, 815, '2019-11-30', 1, NULL, NULL, 0),
(5786, 3005, 894, 10, 9250, 8148, '2019-11-30', 1, NULL, NULL, 0),
(5787, 3005, 892, 400, 370000, 325923, '2019-11-30', 1, NULL, NULL, 0),
(5788, 3006, 900, 1, 5750, 5463, '2019-11-30', 1, NULL, NULL, 0),
(5789, 3006, 892, 24, 138000, 131100, '2019-11-30', 1, NULL, NULL, 0),
(5790, 3007, 900, 1, 5750, 5463, '2019-11-30', 1, NULL, NULL, 0),
(5791, 3007, 892, 24, 138000, 131100, '2019-11-30', 1, NULL, NULL, 0),
(5792, 3008, 897, 1, 5900, 5700, '2019-11-30', 1, NULL, NULL, 0),
(5793, 3008, 898, 20, 118000, 114000, '2019-11-30', 1, NULL, NULL, 0),
(5794, 3009, 899, 1, 6500, 4352, '2019-11-30', 1, NULL, NULL, 0),
(5795, 3009, 896, 12, 78000, 52229, '2019-11-30', 1, NULL, NULL, 0),
(5796, 3009, 892, 216, 1404000, 940118, '2019-11-30', 1, NULL, NULL, 0),
(5797, 3010, 893, 1, 21000, 17448, '2019-11-30', 1, NULL, NULL, 0),
(5798, 3011, 893, 1, 21000, 17448, '2019-11-30', 1, NULL, NULL, 0),
(5799, 3012, 893, 1, 14000, 11641, '2019-11-30', 1, NULL, NULL, 0),
(5800, 3013, 893, 1, 8500, 7119, '2019-11-30', 1, NULL, NULL, 0),
(5801, 3014, 893, 1, 14000, 11641, '2019-11-30', 1, NULL, NULL, 0),
(5802, 3015, 893, 1, 8500, 7119, '2019-11-30', 1, NULL, NULL, 0),
(5803, 3016, 893, 1, 23500, 19681, '2019-11-30', 1, NULL, NULL, 0),
(5804, 3017, 893, 1, 14000, 11641, '2019-11-30', 1, NULL, NULL, 0),
(5805, 3018, 893, 1, 8500, 7119, '2019-11-30', 1, NULL, NULL, 0),
(5806, 3019, 893, 1, 14000, 11641, '2019-11-30', 1, NULL, NULL, 0),
(5807, 3020, 893, 1, 8500, 7119, '2019-11-30', 1, NULL, NULL, 0),
(5808, 3021, 893, 1, 14000, 11641, '2019-11-30', 1, NULL, NULL, 0),
(5809, 3022, 893, 1, 8500, 7119, '2019-11-30', 1, NULL, NULL, 0),
(5810, 3023, 893, 1, 23500, 19681, '2019-11-30', 1, NULL, NULL, 0),
(5811, 3024, 893, 1, 14000, 11641, '2019-11-30', 1, NULL, NULL, 0),
(5812, 3025, 893, 1, 8500, 7119, '2019-11-30', 1, NULL, NULL, 0),
(5813, 3026, 893, 1, 750, 705, '2019-11-30', 1, NULL, NULL, 0),
(5814, 3026, 892, 24, 18000, 16920, '2019-11-30', 1, NULL, NULL, 0),
(5815, 3027, 893, 1, 14000, 11641, '2019-11-30', 1, NULL, NULL, 0),
(5816, 3028, 893, 1, 8500, 7119, '2019-11-30', 1, NULL, NULL, 0),
(5817, 3029, 893, 1, 23500, 19681, '2019-11-30', 1, NULL, NULL, 0),
(5818, 3030, 893, 1, 14000, 11641, '2019-11-30', 1, NULL, NULL, 0),
(5819, 3031, 893, 1, 8500, 7119, '2019-11-30', 1, NULL, NULL, 0),
(5820, 3032, 893, 1, 23500, 19681, '2019-11-30', 1, NULL, NULL, 0),
(5821, 3033, 893, 1, 21000, 17448, '2019-11-30', 1, NULL, NULL, 0),
(5822, 3034, 893, 1, 25000, 20940, '2019-11-30', 1, NULL, NULL, 0),
(5823, 3035, 893, 1, 25000, 20940, '2019-11-30', 1, NULL, NULL, 0),
(5824, 3036, 893, 1, 25000, 20940, '2019-11-30', 1, NULL, NULL, 0),
(5825, 3037, 893, 1, 25000, 20940, '2019-11-30', 1, NULL, NULL, 0),
(5826, 3038, 893, 1, 25000, 20940, '2019-11-30', 1, NULL, NULL, 0),
(5827, 3039, 893, 1, 25000, 20940, '2019-11-30', 1, NULL, NULL, 0),
(5828, 3040, 893, 1, 21000, 17448, '2019-11-30', 1, NULL, NULL, 0),
(5829, 3041, 893, 1, 21000, 17448, '2019-11-30', 1, NULL, NULL, 0),
(5830, 3042, 896, 1, 8250, 7688, '2019-11-30', 1, NULL, NULL, 0),
(5831, 3042, 892, 8, 66000, 61500, '2019-11-30', 1, NULL, NULL, 0),
(5832, 3043, 896, 1, 8250, 7688, '2019-11-30', 1, NULL, NULL, 0),
(5833, 3043, 892, 8, 66000, 61500, '2019-11-30', 1, NULL, NULL, 0),
(5834, 3044, 896, 1, 8250, 7688, '2019-11-30', 1, NULL, NULL, 0),
(5835, 3044, 892, 8, 66000, 61500, '2019-11-30', 1, NULL, NULL, 0),
(5836, 3045, 896, 1, 8250, 7688, '2019-11-30', 1, NULL, NULL, 0),
(5837, 3045, 892, 8, 66000, 61500, '2019-11-30', 1, NULL, NULL, 0),
(5838, 3046, 899, 1, 2500, 1667, '2019-11-30', 1, NULL, NULL, 0),
(5839, 3046, 896, 5, 12500, 8333, '2019-11-30', 1, NULL, NULL, 0),
(5840, 3046, 892, 30, 75000, 50000, '2019-11-30', 1, NULL, NULL, 0),
(5841, 3047, 899, 1, 2500, 1667, '2019-11-30', 1, NULL, NULL, 0),
(5842, 3047, 896, 5, 12500, 8333, '2019-11-30', 1, NULL, NULL, 0),
(5843, 3047, 892, 30, 75000, 50000, '2019-11-30', 1, NULL, NULL, 0),
(5844, 3048, 899, 1, 2500, 1667, '2019-11-30', 1, NULL, NULL, 0),
(5845, 3048, 896, 5, 12500, 8333, '2019-11-30', 1, NULL, NULL, 0),
(5846, 3048, 892, 30, 75000, 50000, '2019-11-30', 1, NULL, NULL, 0),
(5847, 3049, 896, 1, 8250, 7688, '2019-11-30', 1, NULL, NULL, 0),
(5848, 3049, 892, 8, 66000, 61500, '2019-11-30', 1, NULL, NULL, 0),
(5849, 3050, 899, 1, 2500, 1667, '2019-11-30', 1, NULL, NULL, 0),
(5850, 3050, 896, 5, 12500, 8333, '2019-11-30', 1, NULL, NULL, 0),
(5851, 3050, 892, 30, 75000, 50000, '2019-11-30', 1, NULL, NULL, 0),
(5852, 3051, 893, 1, 708, 666, '2019-11-30', 1, NULL, NULL, 0),
(5853, 3051, 892, 24, 17000, 15980, '2019-11-30', 1, NULL, NULL, 0),
(5854, 3052, 897, 1, 4900, 4274, '2019-11-30', 1, NULL, NULL, 0),
(5855, 3052, 898, 5, 24500, 21370, '2019-11-30', 1, NULL, NULL, 0),
(5856, 3053, 893, 1, 26750, 23117, '2019-11-30', 1, NULL, NULL, 0),
(5857, 3054, 893, 1, 21000, 17448, '2019-11-30', 1, NULL, NULL, 0),
(5858, 3055, 897, 5, 3600, 3370, '2019-11-30', 1, '2019-12-04 09:36:17', 1, 0),
(5859, 3055, 898, 1, 18000, 16851, '2019-11-30', 1, '2019-12-04 09:36:04', 1, 0),
(5860, 3056, 893, 1, 13500, 11306, '2019-11-30', 1, NULL, NULL, 0),
(5861, 3057, 893, 1, 13500, 11306, '2019-11-30', 1, NULL, NULL, 0),
(5862, 3058, 893, 1, 13500, 11306, '2019-11-30', 1, NULL, NULL, 0),
(5863, 3059, 893, 1, 13500, 11306, '2019-11-30', 1, NULL, NULL, 0),
(5864, 3060, 893, 1, 13500, 11306, '2019-11-30', 1, NULL, NULL, 0),
(5865, 3061, 893, 1, 13500, 11306, '2019-11-30', 1, NULL, NULL, 0),
(5866, 3062, 899, 1, 400, 372, '2019-11-30', 1, NULL, NULL, 0),
(5867, 3062, 896, 24, 9600, 8928, '2019-11-30', 1, NULL, NULL, 0),
(5868, 3062, 892, 288, 115200, 107136, '2019-11-30', 1, NULL, NULL, 0),
(5869, 3063, 899, 1, 400, 372, '2019-11-30', 1, NULL, NULL, 0),
(5870, 3063, 896, 24, 9600, 8928, '2019-11-30', 1, NULL, NULL, 0),
(5871, 3063, 892, 288, 115200, 107136, '2019-11-30', 1, NULL, NULL, 0),
(5872, 3064, 899, 1, 400, 372, '2019-11-30', 1, NULL, NULL, 0),
(5873, 3064, 896, 30, 12000, 11160, '2019-11-30', 1, NULL, NULL, 0),
(5874, 3064, 892, 120, 48000, 44640, '2019-11-30', 1, NULL, NULL, 0),
(5875, 3065, 899, 1, 400, 372, '2019-11-30', 1, NULL, NULL, 0),
(5876, 3065, 896, 30, 12000, 11160, '2019-11-30', 1, NULL, NULL, 0),
(5877, 3065, 892, 120, 48000, 44640, '2019-11-30', 1, NULL, NULL, 0),
(5878, 3066, 893, 1, 10000, 9763, '2019-11-30', 1, NULL, NULL, 0),
(5879, 3066, 892, 12, 120000, 117160, '2019-11-30', 1, NULL, NULL, 0),
(5880, 3067, 893, 1, 20000, 19527, '2019-11-30', 1, NULL, NULL, 0),
(5881, 3067, 892, 6, 120000, 117160, '2019-11-30', 1, NULL, NULL, 0),
(5882, 3068, 893, 1, 8000, 7182, '2019-11-30', 1, NULL, NULL, 0),
(5883, 3068, 937, 6, 48000, 43092, '2019-11-30', 1, NULL, NULL, 0),
(5884, 3068, 938, 72, 576000, 517104, '2019-11-30', 1, NULL, NULL, 0),
(5885, 3069, 893, 1, 8000, 7182, '2019-11-30', 1, NULL, NULL, 0),
(5886, 3069, 937, 6, 48000, 43092, '2019-11-30', 1, NULL, NULL, 0),
(5887, 3069, 938, 72, 576002, 517104, '2019-11-30', 1, NULL, NULL, 0),
(5888, 3070, 899, 1, 1250, 1080, '2019-11-30', 1, NULL, NULL, 0),
(5889, 3070, 896, 12, 15000, 12960, '2019-11-30', 1, NULL, NULL, 0),
(5890, 3070, 892, 72, 90000, 77760, '2019-11-30', 1, NULL, NULL, 0),
(5891, 3071, 896, 1, 10500, 9072, '2019-11-30', 1, NULL, NULL, 0),
(5892, 3072, 945, 1, 1417, 1260, '2019-11-30', 1, NULL, NULL, 0),
(5893, 3072, 939, 12, 17000, 15120, '2019-11-30', 1, NULL, NULL, 0),
(5894, 3072, 943, 72, 102000, 90720, '2019-11-30', 1, NULL, NULL, 0),
(5895, 3073, 946, 1, 1250, 1116, '2019-11-30', 1, NULL, NULL, 0),
(5896, 3073, 944, 12, 15000, 13392, '2019-11-30', 1, NULL, NULL, 0),
(5897, 3073, 892, 72, 90000, 80352, '2019-11-30', 1, NULL, NULL, 0),
(5898, 3074, 946, 1, 1250, 1116, '2019-11-30', 1, NULL, NULL, 0),
(5899, 3074, 944, 12, 15000, 13392, '2019-11-30', 1, NULL, NULL, 0),
(5900, 3074, 892, 72, 90000, 80352, '2019-11-30', 1, NULL, NULL, 0),
(5901, 3075, 946, 1, 1250, 1116, '2019-11-30', 1, NULL, NULL, 0),
(5902, 3075, 944, 12, 15000, 13392, '2019-11-30', 1, NULL, NULL, 0),
(5903, 3075, 892, 72, 90000, 80352, '2019-11-30', 1, NULL, NULL, 0),
(5904, 3076, 899, 1, 4000, 3600, '2019-11-30', 1, NULL, NULL, 0),
(5905, 3076, 896, 10, 40000, 36000, '2019-11-30', 1, NULL, NULL, 0),
(5906, 3077, 893, 1, 23500, 19681, '2019-11-30', 1, NULL, NULL, 0),
(5907, 3078, 893, 1, 23500, 19681, '2019-11-30', 1, NULL, NULL, 0),
(5908, 3079, 893, 1, 23500, 19681, '2019-11-30', 1, NULL, NULL, 0),
(5909, 3080, 893, 1, 23500, 19681, '2019-11-30', 1, NULL, NULL, 0),
(5910, 3081, 947, 1, 875, 831, '2019-11-30', 1, NULL, NULL, 0),
(5911, 3081, 904, 10, 8750, 8313, '2019-11-30', 1, NULL, NULL, 0),
(5912, 3081, 896, 50, 43750, 41563, '2019-11-30', 1, NULL, NULL, 0),
(5913, 3081, 892, 200, 175000, 166250, '2019-11-30', 1, NULL, NULL, 0),
(5914, 3082, 899, 1, 5500, 5040, '2019-11-30', 1, NULL, NULL, 0),
(5915, 3082, 896, 24, 132000, 120960, '2019-11-30', 1, NULL, NULL, 0),
(5916, 3083, 899, 1, 5500, 5040, '2019-11-30', 1, NULL, NULL, 0),
(5917, 3083, 896, 24, 132000, 120960, '2019-11-30', 1, NULL, NULL, 0),
(5918, 3084, 947, 1, 875, 831, '2019-11-30', 1, NULL, NULL, 0),
(5919, 3084, 904, 10, 8750, 8313, '2019-11-30', 1, NULL, NULL, 0),
(5920, 3084, 896, 50, 43750, 41563, '2019-11-30', 1, NULL, NULL, 0),
(5921, 3084, 892, 200, 175000, 166250, '2019-11-30', 1, NULL, NULL, 0),
(5922, 3085, 947, 1, 276, 262, '2019-11-30', 1, NULL, NULL, 0),
(5923, 3085, 904, 10, 2758, 2620, '2019-11-30', 1, NULL, NULL, 0),
(5924, 3085, 896, 60, 16550, 15723, '2019-11-30', 1, NULL, NULL, 0),
(5925, 3085, 892, 600, 165500, 157225, '2019-11-30', 1, NULL, NULL, 0),
(5926, 3086, 947, 1, 276, 262, '2019-11-30', 1, NULL, NULL, 0),
(5927, 3086, 904, 10, 2758, 2620, '2019-11-30', 1, NULL, NULL, 0),
(5928, 3086, 896, 60, 16550, 15723, '2019-11-30', 1, NULL, NULL, 0),
(5929, 3086, 892, 600, 165500, 157225, '2019-11-30', 1, NULL, NULL, 0),
(5930, 3087, 947, 1, 276, 262, '2019-11-30', 1, NULL, NULL, 0),
(5931, 3087, 904, 10, 2758, 2620, '2019-11-30', 1, NULL, NULL, 0),
(5932, 3087, 896, 60, 16550, 15723, '2019-11-30', 1, NULL, NULL, 0),
(5933, 3087, 892, 600, 165500, 157225, '2019-11-30', 1, NULL, NULL, 0),
(5934, 3088, 947, 1, 276, 262, '2019-11-30', 1, NULL, NULL, 0),
(5935, 3088, 904, 10, 2758, 2620, '2019-11-30', 1, NULL, NULL, 0),
(5936, 3088, 896, 60, 16550, 15723, '2019-11-30', 1, NULL, NULL, 0),
(5937, 3088, 892, 600, 165500, 157225, '2019-11-30', 1, NULL, NULL, 0),
(5938, 3089, 947, 1, 276, 262, '2019-11-30', 1, NULL, NULL, 0),
(5939, 3089, 904, 10, 2758, 2620, '2019-11-30', 1, NULL, NULL, 0),
(5940, 3089, 896, 60, 16550, 15723, '2019-11-30', 1, NULL, NULL, 0),
(5941, 3089, 892, 600, 165500, 157225, '2019-11-30', 1, NULL, NULL, 0),
(5942, 3090, 947, 1, 276, 262, '2019-11-30', 1, NULL, NULL, 0),
(5943, 3090, 904, 10, 2758, 2620, '2019-11-30', 1, NULL, NULL, 0),
(5944, 3090, 896, 60, 16550, 15723, '2019-11-30', 1, NULL, NULL, 0),
(5945, 3090, 892, 600, 165500, 157225, '2019-11-30', 1, NULL, NULL, 0),
(5946, 3091, 947, 1, 276, 262, '2019-11-30', 1, NULL, NULL, 0),
(5947, 3091, 904, 10, 2758, 2620, '2019-11-30', 1, NULL, NULL, 0),
(5948, 3091, 896, 60, 16550, 15723, '2019-11-30', 1, NULL, NULL, 0),
(5949, 3091, 892, 600, 165500, 157225, '2019-11-30', 1, NULL, NULL, 0),
(5950, 3092, 947, 1, 276, 262, '2019-11-30', 1, NULL, NULL, 0),
(5951, 3092, 904, 10, 2758, 2620, '2019-11-30', 1, NULL, NULL, 0),
(5952, 3092, 896, 60, 16550, 15723, '2019-11-30', 1, NULL, NULL, 0),
(5953, 3092, 892, 600, 165500, 157225, '2019-11-30', 1, NULL, NULL, 0),
(5954, 3093, 947, 1, 1500, 1425, '2019-11-30', 1, NULL, NULL, 0),
(5955, 3093, 904, 10, 15000, 14250, '2019-11-30', 1, NULL, NULL, 0),
(5956, 3093, 896, 40, 60000, 57000, '2019-11-30', 1, NULL, NULL, 0),
(5957, 3093, 892, 80, 120000, 114000, '2019-11-30', 1, NULL, NULL, 0),
(5958, 3094, 947, 1, 1500, 1425, '2019-11-30', 1, NULL, NULL, 0),
(5959, 3094, 904, 10, 15000, 14250, '2019-11-30', 1, NULL, NULL, 0),
(5960, 3094, 896, 40, 60000, 57000, '2019-11-30', 1, NULL, NULL, 0),
(5961, 3094, 892, 80, 120000, 114000, '2019-11-30', 1, NULL, NULL, 0),
(5962, 3095, 894, 1, 15000, 14250, '2019-11-30', 1, NULL, NULL, 0),
(5963, 3095, 892, 8, 120000, 114000, '2019-11-30', 1, NULL, NULL, 0),
(5964, 3096, 893, 1, 2500, 2167, '2019-11-30', 1, NULL, NULL, 0),
(5965, 3096, 892, 24, 60000, 52000, '2019-11-30', 1, NULL, NULL, 0),
(5966, 3097, 895, 1, 350, 333, '2019-11-30', 1, NULL, NULL, 0),
(5967, 3097, 894, 10, 3500, 3325, '2019-11-30', 1, NULL, NULL, 0),
(5968, 3097, 892, 100, 35000, 33250, '2019-11-30', 1, NULL, NULL, 0),
(5969, 3098, 895, 1, 350, 333, '2019-11-30', 1, NULL, NULL, 0),
(5970, 3098, 894, 10, 3500, 3325, '2019-11-30', 1, NULL, NULL, 0),
(5971, 3098, 892, 100, 35000, 33250, '2019-11-30', 1, NULL, NULL, 0),
(5972, 3099, 947, 1, 875, 831, '2019-11-30', 1, NULL, NULL, 0),
(5973, 3099, 904, 10, 8750, 8313, '2019-11-30', 1, NULL, NULL, 0),
(5974, 3099, 896, 50, 43750, 41563, '2019-11-30', 1, NULL, NULL, 0),
(5975, 3099, 892, 200, 175000, 166250, '2019-11-30', 1, NULL, NULL, 0),
(5976, 3100, 899, 1, 1250, 1080, '2019-11-30', 1, NULL, NULL, 0),
(5977, 3100, 896, 10, 12500, 10800, '2019-11-30', 1, NULL, NULL, 0),
(5978, 3100, 892, 1000, 1250000, 1080000, '2019-11-30', 1, NULL, NULL, 0),
(5979, 3101, 899, 1, 7500, 6480, '2019-11-30', 1, NULL, NULL, 0),
(5980, 3101, 896, 12, 90000, 77760, '2019-11-30', 1, NULL, NULL, 0),
(5981, 3102, 899, 1, 7500, 6480, '2019-11-30', 1, NULL, NULL, 0),
(5982, 3102, 896, 12, 90000, 77760, '2019-11-30', 1, NULL, NULL, 0),
(5983, 3103, 947, 1, 875, 831, '2019-11-30', 1, NULL, NULL, 0),
(5984, 3103, 904, 10, 8750, 8313, '2019-11-30', 1, NULL, NULL, 0),
(5985, 3103, 896, 50, 43750, 41563, '2019-11-30', 1, NULL, NULL, 0),
(5986, 3103, 892, 200, 175000, 166250, '2019-11-30', 1, NULL, NULL, 0),
(5987, 3104, 892, 1, 19500, 15443, '2019-11-30', 1, NULL, NULL, 0),
(5988, 3105, 892, 1, 37000, 27443, '2019-11-30', 1, NULL, NULL, 0),
(5989, 3106, 892, 1, 22000, 17743, '2019-11-30', 1, NULL, NULL, 0),
(5990, 3107, 892, 1, 36000, 26943, '2019-11-30', 1, NULL, NULL, 0),
(5991, 3108, 892, 1, 38000, 29443, '2019-11-30', 1, NULL, NULL, 0),
(5992, 3109, 947, 1, 875, 831, '2019-11-30', 1, NULL, NULL, 0),
(5993, 3109, 904, 10, 8750, 8313, '2019-11-30', 1, NULL, NULL, 0),
(5994, 3109, 896, 50, 43750, 41563, '2019-11-30', 1, NULL, NULL, 0),
(5995, 3109, 892, 200, 175000, 166250, '2019-11-30', 1, NULL, NULL, 0),
(5996, 3110, 947, 1, 251, 238, '2019-11-30', 1, NULL, NULL, 0),
(5997, 3110, 904, 10, 2508, 2382, '2019-11-30', 1, NULL, NULL, 0),
(5998, 3110, 896, 60, 15045, 14293, '2019-11-30', 1, NULL, NULL, 0),
(5999, 3110, 892, 660, 165500, 157225, '2019-11-30', 1, NULL, NULL, 0),
(6000, 3111, 948, 1, 980, 810, '2019-11-30', 1, NULL, NULL, 0),
(6001, 3111, 905, 6, 5880, 4860, '2019-11-30', 1, NULL, NULL, 0),
(6002, 3111, 897, 60, 58800, 48600, '2019-11-30', 1, NULL, NULL, 0),
(6003, 3112, 897, 1, 4638, 4093, '2019-11-30', 1, NULL, NULL, 0),
(6004, 3112, 898, 12, 55656, 49116, '2019-11-30', 1, NULL, NULL, 0),
(6005, 3113, 899, 1, 800, 760, '2019-11-30', 1, NULL, NULL, 0),
(6006, 3113, 896, 10, 8000, 7605, '2019-11-30', 1, NULL, NULL, 0),
(6007, 3113, 892, 240, 192000, 182515, '2019-11-30', 1, NULL, NULL, 0),
(6008, 3114, 899, 1, 800, 760, '2019-11-30', 1, NULL, NULL, 0),
(6009, 3114, 896, 10, 8000, 7605, '2019-11-30', 1, NULL, NULL, 0),
(6010, 3114, 892, 240, 192000, 182515, '2019-11-30', 1, NULL, NULL, 0),
(6011, 3115, 899, 1, 800, 760, '2019-11-30', 1, NULL, NULL, 0),
(6012, 3115, 896, 10, 8000, 7605, '2019-11-30', 1, NULL, NULL, 0),
(6013, 3115, 892, 24, 192000, 182515, '2019-11-30', 1, NULL, NULL, 0),
(6014, 3116, 893, 1, 750, 705, '2019-11-30', 1, NULL, NULL, 0),
(6015, 3116, 892, 24, 18000, 16920, '2019-11-30', 1, NULL, NULL, 0),
(6016, 3117, 893, 1, 750, 705, '2019-11-30', 1, NULL, NULL, 0),
(6017, 3117, 892, 24, 18000, 16920, '2019-11-30', 1, NULL, NULL, 0),
(6018, 3118, 893, 1, 750, 705, '2019-11-30', 1, NULL, NULL, 0),
(6019, 3118, 892, 24, 18000, 16920, '2019-11-30', 1, NULL, NULL, 0),
(6020, 3119, 893, 1, 750, 705, '2019-11-30', 1, NULL, NULL, 0),
(6021, 3119, 892, 24, 18000, 16920, '2019-11-30', 1, NULL, NULL, 0),
(6022, 3120, 899, 1, 403, 372, '2019-11-30', 1, NULL, NULL, 0),
(6023, 3120, 896, 24, 9667, 8928, '2019-11-30', 1, NULL, NULL, 0),
(6024, 3120, 892, 144, 58003, 53568, '2019-11-30', 1, NULL, NULL, 0),
(6025, 3121, 899, 1, 3944, 3673, '2019-11-30', 1, NULL, NULL, 0),
(6026, 3121, 896, 5, 19720, 18367, '2019-11-30', 1, NULL, NULL, 0),
(6027, 3121, 892, 30, 118320, 110200, '2019-11-30', 1, NULL, NULL, 0),
(6028, 3122, 899, 1, 740, 689, '2019-11-30', 1, NULL, NULL, 0),
(6029, 3122, 896, 10, 7395, 6888, '2019-11-30', 1, NULL, NULL, 0),
(6030, 3122, 892, 80, 59160, 55100, '2019-11-30', 1, NULL, NULL, 0),
(6031, 3123, 899, 1, 1564, 1457, '2019-11-30', 1, NULL, NULL, 0),
(6032, 3123, 896, 10, 15640, 14567, '2019-11-30', 1, NULL, NULL, 0),
(6033, 3123, 892, 30, 46920, 43700, '2019-11-30', 1, NULL, NULL, 0),
(6034, 3124, 899, 1, 395, 368, '2019-11-30', 1, NULL, NULL, 0),
(6035, 3124, 896, 20, 7905, 7363, '2019-11-30', 1, NULL, NULL, 0),
(6036, 3124, 892, 160, 63240, 58900, '2019-11-30', 1, NULL, NULL, 0),
(6037, 3125, 899, 1, 391, 364, '2019-11-30', 1, NULL, NULL, 0),
(6038, 3125, 896, 20, 7820, 7283, '2019-11-30', 1, NULL, NULL, 0),
(6039, 3125, 892, 120, 46920, 43700, '2019-11-30', 1, NULL, NULL, 0),
(6040, 3126, 899, 1, 391, 364, '2019-11-30', 1, NULL, NULL, 0),
(6041, 3126, 896, 20, 7820, 7283, '2019-11-30', 1, NULL, NULL, 0),
(6042, 3126, 892, 120, 46920, 43700, '2019-11-30', 1, NULL, NULL, 0),
(6043, 3127, 899, 1, 408, 380, '2019-11-30', 1, NULL, NULL, 0),
(6044, 3127, 896, 10, 4080, 3800, '2019-11-30', 1, NULL, NULL, 0),
(6045, 3127, 892, 120, 48960, 45600, '2019-11-30', 1, NULL, NULL, 0),
(6046, 3128, 899, 1, 408, 380, '2019-11-30', 1, NULL, NULL, 0),
(6047, 3128, 896, 10, 4080, 3800, '2019-11-30', 1, NULL, NULL, 0),
(6048, 3128, 892, 120, 48960, 45600, '2019-11-30', 1, NULL, NULL, 0),
(6049, 3129, 899, 1, 400, 372, '2019-11-30', 1, NULL, NULL, 0),
(6050, 3129, 896, 20, 8000, 7440, '2019-11-30', 1, NULL, NULL, 0),
(6051, 3129, 892, 120, 48000, 44640, '2019-11-30', 1, NULL, NULL, 0),
(6052, 3130, 899, 1, 400, 372, '2019-11-30', 1, NULL, NULL, 0),
(6053, 3130, 896, 20, 8000, 7440, '2019-11-30', 1, NULL, NULL, 0),
(6054, 3130, 892, 120, 48000, 44640, '2019-11-30', 1, NULL, NULL, 0),
(6055, 3131, 896, 1, 3925, 3658, '2019-11-30', 1, NULL, NULL, 0),
(6056, 3131, 892, 10, 39250, 36575, '2019-11-30', 1, NULL, NULL, 0),
(6057, 3132, 896, 1, 3925, 3658, '2019-11-30', 1, NULL, NULL, 0),
(6058, 3132, 892, 10, 39250, 36575, '2019-11-30', 1, NULL, NULL, 0),
(6059, 3133, 899, 1, 391, 364, '2019-11-30', 1, NULL, NULL, 0),
(6060, 3133, 896, 20, 7820, 7283, '2019-11-30', 1, NULL, NULL, 0),
(6061, 3133, 892, 120, 46920, 43700, '2019-11-30', 1, NULL, NULL, 0),
(6062, 3134, 899, 1, 391, 364, '2019-11-30', 1, NULL, NULL, 0),
(6063, 3134, 896, 20, 7820, 7283, '2019-11-30', 1, NULL, NULL, 0),
(6064, 3134, 892, 120, 46920, 43700, '2019-11-30', 1, NULL, NULL, 0),
(6065, 3135, 893, 1, 1564, 1457, '2019-11-30', 1, NULL, NULL, 0),
(6066, 3135, 892, 30, 46920, 43700, '2019-11-30', 1, NULL, NULL, 0),
(6067, 3136, 899, 1, 400, 372, '2019-11-30', 1, NULL, NULL, 0),
(6068, 3136, 896, 6, 2397, 2233, '2019-11-30', 1, NULL, NULL, 0),
(6069, 3136, 892, 60, 23970, 22325, '2019-11-30', 1, NULL, NULL, 0),
(6070, 3137, 899, 1, 393, 366, '2019-11-30', 1, NULL, NULL, 0),
(6071, 3137, 896, 10, 3927, 3658, '2019-11-30', 1, NULL, NULL, 0),
(6072, 3137, 892, 100, 39270, 36575, '2019-11-30', 1, NULL, NULL, 0),
(6073, 3138, 899, 1, 388, 361, '2019-11-30', 1, NULL, NULL, 0),
(6074, 3138, 896, 10, 3876, 3610, '2019-11-30', 1, NULL, NULL, 0),
(6075, 3138, 892, 50, 19380, 18050, '2019-11-30', 1, NULL, NULL, 0),
(6076, 3139, 899, 1, 388, 361, '2019-11-30', 1, NULL, NULL, 0),
(6077, 3139, 896, 10, 3876, 3610, '2019-11-30', 1, NULL, NULL, 0),
(6078, 3139, 892, 50, 19380, 18050, '2019-11-30', 1, NULL, NULL, 0),
(6079, 3140, 893, 1, 1734, 1615, '2019-11-30', 1, NULL, NULL, 0),
(6080, 3140, 892, 30, 52020, 48450, '2019-11-30', 1, NULL, NULL, 0),
(6081, 3141, 893, 1, 1615, 1504, '2019-11-30', 1, NULL, NULL, 0),
(6082, 3141, 892, 24, 38760, 36100, '2019-11-30', 1, NULL, NULL, 0),
(6083, 3142, 893, 1, 1615, 1504, '2019-11-30', 1, NULL, NULL, 0),
(6084, 3142, 892, 24, 38760, 36100, '2019-11-30', 1, NULL, NULL, 0),
(6085, 3143, 899, 1, 2006, 1868, '2019-11-30', 1, NULL, NULL, 0),
(6086, 3143, 896, 10, 20060, 18683, '2019-11-30', 1, NULL, NULL, 0),
(6087, 3143, 892, 30, 60180, 56050, '2019-11-30', 1, NULL, NULL, 0),
(6088, 3144, 893, 1, 5500, 5133, '2019-11-30', 1, NULL, NULL, 0),
(6089, 3144, 892, 30, 165000, 154000, '2019-11-30', 1, NULL, NULL, 0),
(6090, 3145, 893, 1, 5500, 5167, '2019-11-30', 1, NULL, NULL, 0),
(6091, 3145, 892, 30, 165000, 155000, '2019-11-30', 1, NULL, NULL, 0),
(6092, 3146, 893, 1, 771, 725, '2019-11-30', 1, NULL, NULL, 0),
(6093, 3146, 892, 24, 18500, 17389, '2019-11-30', 1, NULL, NULL, 0),
(6094, 3147, 893, 1, 771, 725, '2019-11-30', 1, NULL, NULL, 0),
(6095, 3147, 892, 24, 18500, 17389, '2019-11-30', 1, NULL, NULL, 0),
(6096, 3148, 893, 1, 771, 725, '2019-11-30', 1, NULL, NULL, 0),
(6097, 3148, 892, 24, 18500, 17389, '2019-11-30', 1, NULL, NULL, 0),
(6098, 3149, 893, 1, 771, 725, '2019-11-30', 1, NULL, NULL, 0),
(6099, 3149, 892, 24, 18500, 17389, '2019-11-30', 1, NULL, NULL, 0),
(6100, 3150, 893, 1, 62211, 59491, '2019-11-30', 1, NULL, NULL, 0),
(6101, 3150, 892, 4, 248844, 237964, '2019-11-30', 1, NULL, NULL, 0),
(6102, 3151, 893, 1, 33458, 31946, '2019-11-30', 1, NULL, NULL, 0),
(6103, 3151, 892, 6, 200747, 191678, '2019-11-30', 1, NULL, NULL, 0),
(6104, 3152, 896, 1, 16920, 16071, '2019-11-30', 1, NULL, NULL, 0),
(6105, 3152, 892, 12, 203035, 192852, '2019-11-30', 1, NULL, NULL, 0),
(6106, 3153, 896, 1, 16920, 16071, '2019-11-30', 1, NULL, NULL, 0),
(6107, 3153, 892, 12, 203035, 192852, '2019-11-30', 1, NULL, NULL, 0),
(6108, 3154, 896, 1, 16920, 16071, '2019-11-30', 1, NULL, NULL, 0),
(6109, 3154, 892, 12, 203035, 192852, '2019-11-30', 1, NULL, NULL, 0),
(6110, 3155, 893, 1, 4360, 4150, '2019-11-30', 1, NULL, NULL, 0),
(6111, 3155, 892, 48, 209280, 199200, '2019-11-30', 1, NULL, NULL, 0),
(6112, 3156, 893, 1, 16253, 15545, '2019-11-30', 1, NULL, NULL, 0),
(6113, 3156, 892, 24, 390072, 373080, '2019-11-30', 1, NULL, NULL, 0),
(6114, 3157, 893, 1, 11657, 11150, '2019-11-30', 1, NULL, NULL, 0),
(6115, 3157, 892, 24, 279768, 267600, '2019-11-30', 1, NULL, NULL, 0),
(6116, 2884, 893, 1, 15500, 14784, '2019-11-30', 1, NULL, NULL, 0),
(6117, 2884, 892, 24, 372000, 354816, '2019-11-30', 1, NULL, NULL, 0),
(6118, 2886, 893, 1, 14000, 13344, '2019-11-30', 1, NULL, NULL, 0),
(6119, 2886, 892, 24, 336000, 320256, '2019-11-30', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_stock`
--

CREATE TABLE `product_stock` (
  `id` int(11) NOT NULL,
  `product_satuan` int(11) NOT NULL,
  `stock` int(11) DEFAULT NULL,
  `gudang` int(11) DEFAULT NULL,
  `rak` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_stock`
--

INSERT INTO `product_stock` (`id`, `product_satuan`, `stock`, `gudang`, `rak`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(6613, 5371, 25, 1, 1, '2019-11-30', 1, '2019-12-12 04:18:25', NULL, 0),
(6614, 5372, 6, 1, 1, '2019-11-30', 1, '2019-12-10 08:32:29', 1, 0),
(6615, 5373, 50, 1, 1, '2019-11-30', 1, '2019-12-01 05:01:12', 1, 0),
(6616, 5374, 5, 1, 1, '2019-11-30', 1, '2019-12-05 09:30:07', 1, 0),
(6617, 5375, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6618, 5376, 0, 1, 1, '2019-11-30', 1, '2019-12-04 02:40:07', 1, 0),
(6619, 5377, 0, 1, 1, '2019-11-30', 1, '2019-12-04 02:35:18', 1, 0),
(6620, 5378, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6621, 5379, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6622, 5380, 0, 1, 1, '2019-11-30', 1, '2019-12-04 02:41:08', 1, 0),
(6623, 5381, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6624, 5382, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6625, 5383, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6626, 5384, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6627, 5385, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6628, 5386, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6629, 5387, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6630, 5388, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6631, 5389, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6632, 5390, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6633, 5391, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6634, 5392, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6635, 5393, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6636, 5394, 0, 1, 1, '2019-11-30', 1, '2019-12-01 10:36:29', 1, 0),
(6637, 5395, 119, 1, 1, '2019-11-30', 1, '2019-12-01 15:36:40', 1, 0),
(6638, 5396, 0, 1, 1, '2019-11-30', 1, '2019-12-01 10:41:57', 1, 0),
(6639, 5397, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6640, 5398, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6641, 5399, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6642, 5400, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6643, 5401, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6644, 5402, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6645, 5403, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6646, 5404, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6647, 5405, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6648, 5406, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6649, 5407, 0, 1, 1, '2019-11-30', 1, '2019-12-03 03:16:58', 1, 0),
(6650, 5408, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6651, 5409, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6652, 5410, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6653, 5411, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6654, 5412, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6655, 5413, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6656, 5414, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6657, 5415, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6658, 5416, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6659, 5417, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6660, 5418, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6661, 5419, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6662, 5420, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6663, 5421, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6664, 5422, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6665, 5423, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6666, 5424, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6667, 5425, 40, 1, 1, '2019-11-30', 1, '2019-12-02 00:34:54', 1, 0),
(6668, 5426, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6669, 5427, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6670, 5428, 6, 1, 1, '2019-11-30', 1, '2019-12-03 02:26:08', 1, 0),
(6671, 5429, 3, 1, 1, '2019-11-30', 1, '2019-12-03 02:26:08', 1, 0),
(6672, 5430, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6673, 5431, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6674, 5432, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6675, 5433, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6676, 5434, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6677, 5435, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6678, 5436, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6679, 5437, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6680, 5438, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6681, 5439, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6682, 5440, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6683, 5441, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6684, 5442, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6685, 5443, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6686, 5444, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6687, 5445, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6688, 5446, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6689, 5447, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6690, 5448, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6691, 5449, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6692, 5450, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6693, 5451, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6694, 5452, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6695, 5453, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6696, 5454, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6697, 5455, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6698, 5456, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6699, 5457, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6700, 5458, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6701, 5459, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6702, 5460, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6703, 5461, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6704, 5462, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6705, 5463, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6706, 5464, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6707, 5465, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6708, 5466, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6709, 5467, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6710, 5468, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6711, 5469, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6712, 5470, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6713, 5471, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6714, 5472, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6715, 5473, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6716, 5474, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6717, 5475, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6718, 5476, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6719, 5477, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6720, 5478, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6721, 5479, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6722, 5480, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6723, 5481, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6724, 5482, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6725, 5483, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6726, 5484, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6727, 5485, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6728, 5486, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6729, 5487, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6730, 5488, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6731, 5489, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6732, 5490, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6733, 5491, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6734, 5492, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6735, 5493, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6736, 5494, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6737, 5495, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6738, 5496, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6739, 5497, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6740, 5498, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6741, 5499, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6742, 5500, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6743, 5501, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6744, 5502, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6745, 5503, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6746, 5504, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6747, 5505, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6748, 5506, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6749, 5507, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6750, 5508, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6751, 5509, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6752, 5510, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6753, 5511, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6754, 5512, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6755, 5513, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6756, 5514, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6757, 5515, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6758, 5516, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6759, 5517, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6760, 5518, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6761, 5519, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6762, 5520, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6763, 5521, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6764, 5522, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6765, 5523, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6766, 5524, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6767, 5525, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6768, 5526, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6769, 5527, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6770, 5528, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6771, 5529, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6772, 5530, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6773, 5531, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6774, 5532, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6775, 5533, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6776, 5534, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6777, 5535, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6778, 5536, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6779, 5537, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6780, 5538, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6781, 5539, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6782, 5540, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6783, 5541, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6784, 5542, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6785, 5543, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6786, 5544, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6787, 5545, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6788, 5546, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6789, 5547, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6790, 5548, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6791, 5549, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6792, 5550, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6793, 5551, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6794, 5552, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6795, 5553, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6796, 5554, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6797, 5555, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6798, 5556, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6799, 5557, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6800, 5558, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6801, 5559, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6802, 5560, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6803, 5561, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6804, 5562, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6805, 5563, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6806, 5564, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6807, 5565, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6808, 5566, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6809, 5567, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6810, 5568, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6811, 5569, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6812, 5570, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6813, 5571, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6814, 5572, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6815, 5573, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6816, 5574, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6817, 5575, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6818, 5576, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6819, 5577, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6820, 5578, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6821, 5579, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6822, 5580, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6823, 5581, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6824, 5582, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6825, 5583, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6826, 5584, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6827, 5585, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6828, 5586, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6829, 5587, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6830, 5588, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6831, 5589, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6832, 5590, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6833, 5591, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6834, 5592, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6835, 5593, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6836, 5594, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6837, 5595, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6838, 5596, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6839, 5597, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6840, 5598, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6841, 5599, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6842, 5600, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6843, 5601, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6844, 5602, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6845, 5603, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6846, 5604, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6847, 5605, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6848, 5606, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6849, 5607, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6850, 5608, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6851, 5609, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6852, 5610, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6853, 5611, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6854, 5612, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6855, 5613, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6856, 5614, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6857, 5615, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6858, 5616, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6859, 5617, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6860, 5618, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6861, 5619, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6862, 5620, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6863, 5621, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6864, 5622, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6865, 5623, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6866, 5624, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6867, 5625, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6868, 5626, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6869, 5627, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6870, 5628, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6871, 5629, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6872, 5630, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6873, 5631, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6874, 5632, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6875, 5633, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6876, 5634, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6877, 5635, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6878, 5636, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6879, 5637, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6880, 5638, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6881, 5639, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6882, 5640, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6883, 5641, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6884, 5642, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6885, 5643, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6886, 5644, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6887, 5645, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6888, 5646, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6889, 5647, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6890, 5648, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6891, 5649, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6892, 5650, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6893, 5651, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6894, 5652, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6895, 5653, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6896, 5654, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6897, 5655, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6898, 5656, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6899, 5657, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6900, 5658, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6901, 5659, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6902, 5660, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6903, 5661, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6904, 5662, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6905, 5663, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6906, 5664, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6907, 5665, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6908, 5666, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6909, 5667, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6910, 5668, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6911, 5669, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6912, 5670, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6913, 5671, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6914, 5672, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6915, 5673, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6916, 5674, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6917, 5675, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6918, 5676, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6919, 5677, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6920, 5678, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6921, 5679, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6922, 5680, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6923, 5681, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6924, 5682, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6925, 5683, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6926, 5684, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6927, 5685, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6928, 5686, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6929, 5687, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6930, 5688, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6931, 5689, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6932, 5690, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6933, 5691, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6934, 5692, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6935, 5693, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6936, 5694, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6937, 5695, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6938, 5696, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6939, 5697, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6940, 5698, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6941, 5699, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6942, 5700, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6943, 5701, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6944, 5702, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6945, 5703, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6946, 5704, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6947, 5705, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6948, 5706, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6949, 5707, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6950, 5708, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6951, 5709, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6952, 5710, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6953, 5711, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6954, 5712, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6955, 5713, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6956, 5714, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6957, 5715, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6958, 5716, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6959, 5717, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6960, 5718, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6961, 5719, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6962, 5720, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6963, 5721, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6964, 5722, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6965, 5723, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6966, 5724, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6967, 5725, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6968, 5726, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6969, 5727, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6970, 5728, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6971, 5729, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6972, 5730, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6973, 5731, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6974, 5732, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6975, 5733, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6976, 5734, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6977, 5735, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6978, 5736, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6979, 5737, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6980, 5738, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6981, 5739, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6982, 5740, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6983, 5741, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6984, 5742, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6985, 5743, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6986, 5744, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6987, 5745, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6988, 5746, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6989, 5747, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6990, 5748, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6991, 5749, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6992, 5750, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6993, 5751, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6994, 5752, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6995, 5753, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6996, 5754, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6997, 5755, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6998, 5756, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(6999, 5757, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7000, 5758, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7001, 5759, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7002, 5760, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7003, 5761, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7004, 5762, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7005, 5763, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7006, 5764, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7007, 5765, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7008, 5766, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7009, 5767, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7010, 5768, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7011, 5769, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7012, 5770, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7013, 5771, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7014, 5772, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7015, 5773, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7016, 5774, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7017, 5775, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7018, 5776, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7019, 5777, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7020, 5778, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7021, 5779, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7022, 5780, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7023, 5781, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7024, 5782, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7025, 5783, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7026, 5784, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7027, 5785, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7028, 5786, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7029, 5787, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7030, 5788, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7031, 5789, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7032, 5790, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7033, 5791, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7034, 5792, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7035, 5793, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7036, 5794, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7037, 5795, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7038, 5796, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7039, 5797, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7040, 5798, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7041, 5799, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7042, 5800, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7043, 5801, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7044, 5802, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7045, 5803, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7046, 5804, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7047, 5805, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7048, 5806, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7049, 5807, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7050, 5808, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7051, 5809, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7052, 5810, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7053, 5811, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7054, 5812, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7055, 5813, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7056, 5814, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7057, 5815, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7058, 5816, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7059, 5817, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7060, 5818, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7061, 5819, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7062, 5820, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7063, 5821, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7064, 5822, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7065, 5823, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7066, 5824, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7067, 5825, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7068, 5826, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7069, 5827, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7070, 5828, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7071, 5829, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7072, 5830, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7073, 5831, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7074, 5832, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7075, 5833, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7076, 5834, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7077, 5835, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7078, 5836, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7079, 5837, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7080, 5838, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7081, 5839, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7082, 5840, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7083, 5841, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7084, 5842, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7085, 5843, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7086, 5844, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7087, 5845, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7088, 5846, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7089, 5847, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7090, 5848, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7091, 5849, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7092, 5850, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7093, 5851, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7094, 5852, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7095, 5853, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7096, 5854, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7097, 5855, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7098, 5856, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7099, 5857, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7100, 5858, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7101, 5859, 100, 1, 1, '2019-11-30', 1, '2019-12-04 09:22:15', 1, 0),
(7102, 5860, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7103, 5861, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7104, 5862, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7105, 5863, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7106, 5864, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7107, 5865, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7108, 5866, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7109, 5867, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7110, 5868, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7111, 5869, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7112, 5870, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7113, 5871, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7114, 5872, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7115, 5873, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7116, 5874, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7117, 5875, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7118, 5876, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7119, 5877, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7120, 5878, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7121, 5879, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7122, 5880, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7123, 5881, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7124, 5882, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7125, 5883, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7126, 5884, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7127, 5885, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7128, 5886, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7129, 5887, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7130, 5888, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7131, 5889, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7132, 5890, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7133, 5891, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7134, 5892, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7135, 5893, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7136, 5894, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7137, 5895, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7138, 5896, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7139, 5897, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7140, 5898, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7141, 5899, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7142, 5900, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7143, 5901, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7144, 5902, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7145, 5903, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7146, 5904, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7147, 5905, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7148, 5906, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7149, 5907, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7150, 5908, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7151, 5909, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7152, 5910, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7153, 5911, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7154, 5912, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7155, 5913, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7156, 5914, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7157, 5915, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7158, 5916, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7159, 5917, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7160, 5918, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7161, 5919, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7162, 5920, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7163, 5921, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7164, 5922, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7165, 5923, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7166, 5924, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7167, 5925, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7168, 5926, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7169, 5927, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7170, 5928, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7171, 5929, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7172, 5930, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7173, 5931, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7174, 5932, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7175, 5933, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7176, 5934, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7177, 5935, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7178, 5936, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7179, 5937, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7180, 5938, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7181, 5939, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7182, 5940, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7183, 5941, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7184, 5942, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7185, 5943, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7186, 5944, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7187, 5945, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7188, 5946, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7189, 5947, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7190, 5948, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7191, 5949, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7192, 5950, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7193, 5951, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7194, 5952, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7195, 5953, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7196, 5954, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7197, 5955, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7198, 5956, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7199, 5957, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7200, 5958, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7201, 5959, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7202, 5960, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7203, 5961, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7204, 5962, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7205, 5963, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7206, 5964, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7207, 5965, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7208, 5966, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7209, 5967, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7210, 5968, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7211, 5969, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7212, 5970, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7213, 5971, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7214, 5972, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7215, 5973, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7216, 5974, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7217, 5975, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7218, 5976, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7219, 5977, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7220, 5978, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7221, 5979, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7222, 5980, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7223, 5981, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7224, 5982, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7225, 5983, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7226, 5984, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7227, 5985, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7228, 5986, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7229, 5987, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7230, 5988, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7231, 5989, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7232, 5990, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7233, 5991, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7234, 5992, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7235, 5993, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7236, 5994, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7237, 5995, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7238, 5996, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7239, 5997, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7240, 5998, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7241, 5999, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7242, 6000, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7243, 6001, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7244, 6002, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7245, 6003, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7246, 6004, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7247, 6005, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7248, 6006, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7249, 6007, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7250, 6008, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7251, 6009, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7252, 6010, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7253, 6011, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7254, 6012, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7255, 6013, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7256, 6014, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7257, 6015, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7258, 6016, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7259, 6017, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7260, 6018, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7261, 6019, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7262, 6020, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7263, 6021, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7264, 6022, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7265, 6023, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7266, 6024, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7267, 6025, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7268, 6026, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7269, 6027, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7270, 6028, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7271, 6029, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7272, 6030, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7273, 6031, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7274, 6032, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7275, 6033, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7276, 6034, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7277, 6035, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7278, 6036, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7279, 6037, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7280, 6038, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7281, 6039, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7282, 6040, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7283, 6041, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7284, 6042, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7285, 6043, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7286, 6044, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7287, 6045, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7288, 6046, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7289, 6047, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7290, 6048, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7291, 6049, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7292, 6050, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7293, 6051, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7294, 6052, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7295, 6053, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7296, 6054, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7297, 6055, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7298, 6056, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7299, 6057, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7300, 6058, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7301, 6059, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7302, 6060, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7303, 6061, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7304, 6062, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7305, 6063, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7306, 6064, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7307, 6065, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7308, 6066, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7309, 6067, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7310, 6068, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7311, 6069, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7312, 6070, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7313, 6071, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7314, 6072, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7315, 6073, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7316, 6074, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7317, 6075, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7318, 6076, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7319, 6077, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7320, 6078, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7321, 6079, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7322, 6080, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7323, 6081, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7324, 6082, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7325, 6083, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7326, 6084, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7327, 6085, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7328, 6086, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7329, 6087, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7330, 6088, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7331, 6089, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7332, 6090, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7333, 6091, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7334, 6092, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7335, 6093, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7336, 6094, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7337, 6095, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7338, 6096, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7339, 6097, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7340, 6098, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7341, 6099, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7342, 6100, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7343, 6101, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7344, 6102, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7345, 6103, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7346, 6104, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7347, 6105, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7348, 6106, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7349, 6107, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7350, 6108, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7351, 6109, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7352, 6110, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7353, 6111, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7354, 6112, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7355, 6113, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7356, 6114, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7357, 6115, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7358, 6116, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7359, 6117, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7360, 6118, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0),
(7361, 6119, 0, 1, 1, '2019-11-30', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_stock_minus`
--

CREATE TABLE `product_stock_minus` (
  `id` int(11) NOT NULL,
  `product_stock` int(11) NOT NULL,
  `stock` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_surat_jalan`
--

CREATE TABLE `product_surat_jalan` (
  `id` int(11) NOT NULL,
  `no_sj` varchar(155) DEFAULT NULL,
  `document_pengiriman` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_vendor`
--

CREATE TABLE `product_vendor` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `vendor` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rak`
--

CREATE TABLE `rak` (
  `id` int(11) NOT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `nama_rak` varchar(255) DEFAULT NULL,
  `panjang` int(11) DEFAULT NULL,
  `lebar` int(11) DEFAULT NULL,
  `tinggi` int(11) DEFAULT NULL,
  `max_tonase` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rak`
--

INSERT INTO `rak` (`id`, `kode`, `nama_rak`, `panjang`, `lebar`, `tinggi`, `max_tonase`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'RAK001', 'UTAMA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `reimburse`
--

CREATE TABLE `reimburse` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(155) DEFAULT NULL,
  `pegawai` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `return_penjualan`
--

CREATE TABLE `return_penjualan` (
  `id` int(11) NOT NULL,
  `no_retur` varchar(255) DEFAULT NULL,
  `invoice` int(11) NOT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `return_penjualan_item`
--

CREATE TABLE `return_penjualan_item` (
  `id` int(11) NOT NULL,
  `return_penjualan` int(11) NOT NULL,
  `invoice_product` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `stok_kategori` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `retur_barang`
--

CREATE TABLE `retur_barang` (
  `id` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `no_retur` varchar(155) DEFAULT NULL,
  `tanggal_retur` date DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `retur_barang`
--

INSERT INTO `retur_barang` (`id`, `pembeli`, `no_retur`, `tanggal_retur`, `total`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2, 4, 'RETURBR19DEC001', '2019-12-10', 969586, '2019-12-10', 1, NULL, NULL, 0),
(3, 4, 'RETURBR19DEC002', '2019-12-10', 969586, '2019-12-10', 1, NULL, NULL, 0),
(4, 4, 'RETURBR19DEC003', '2019-12-11', 969586, '2019-12-10', 1, NULL, NULL, 0),
(5, 4, 'RETURBR19DEC004', '2019-12-12', 53866, '2019-12-12', 4, NULL, NULL, 0),
(6, 4, 'RETURBR19DEC005', '2019-12-12', 646392, '2019-12-12', 4, NULL, NULL, 0),
(7, 4, 'RETURBR19DEC006', '2019-12-12', 107732, '2019-12-12', 4, NULL, NULL, 0),
(13, 4, 'RETURBR19DEC007', '2019-12-12', 107732, '2019-12-12', 4, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `retur_barang_item`
--

CREATE TABLE `retur_barang_item` (
  `id` int(11) NOT NULL,
  `product_satuan` int(11) NOT NULL,
  `retur_barang` int(11) NOT NULL,
  `stok_kategori` int(11) NOT NULL,
  `gudang` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `retur_barang_item`
--

INSERT INTO `retur_barang_item` (`id`, `product_satuan`, `retur_barang`, `stok_kategori`, `gudang`, `qty`, `sub_total`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2, 5372, 2, 1, 2, 1, 969586, '2019-12-10', 1, NULL, NULL, 0),
(3, 5372, 3, 1, 2, 1, 969586, '2019-12-10', 1, NULL, NULL, 0),
(4, 5372, 4, 1, 1, 1, 969586, '2019-12-10', 1, NULL, NULL, 0),
(5, 5371, 5, 1, 2, 1, 53866, '2019-12-12', NULL, NULL, NULL, 0),
(6, 5371, 6, 1, 2, 12, 646392, '2019-12-12', NULL, NULL, NULL, 0),
(7, 5371, 7, 1, 2, 2, 107732, '2019-12-12', NULL, NULL, NULL, 0),
(13, 5371, 13, 1, 2, 2, 107732, '2019-12-12', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `retur_barang_status`
--

CREATE TABLE `retur_barang_status` (
  `id` int(11) NOT NULL,
  `retur_barang` int(11) NOT NULL,
  `status` varchar(150) DEFAULT NULL COMMENT 'VALIDATE\nDRAFT',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `retur_barang_status`
--

INSERT INTO `retur_barang_status` (`id`, `retur_barang`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 2, 'DRAFT', '2019-12-10', 1, NULL, NULL, 0),
(2, 3, 'DRAFT', '2019-12-10', 1, NULL, NULL, 0),
(3, 4, 'DRAFT', '2019-12-10', 1, NULL, NULL, 0),
(4, 5, 'DRAFT', '2019-12-12', NULL, NULL, NULL, 0),
(5, 6, 'DRAFT', '2019-12-12', NULL, NULL, NULL, 0),
(6, 7, 'DRAFT', '2019-12-12', NULL, NULL, NULL, 0),
(7, 13, 'DRAFT', '2019-12-12', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `retur_order`
--

CREATE TABLE `retur_order` (
  `id` int(11) NOT NULL,
  `no_retur` varchar(155) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `retur_order_item`
--

CREATE TABLE `retur_order_item` (
  `id` int(11) NOT NULL,
  `retur_order` int(11) NOT NULL,
  `order_product` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `stok_kategori` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_rute`
--

CREATE TABLE `sales_rute` (
  `id` int(11) NOT NULL,
  `no_rute` varchar(155) DEFAULT NULL,
  `minggu` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_rute_customer`
--

CREATE TABLE `sales_rute_customer` (
  `id` int(11) NOT NULL,
  `sales_rute` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_rute_hari`
--

CREATE TABLE `sales_rute_hari` (
  `id` int(11) NOT NULL,
  `sales_rute` int(11) NOT NULL,
  `hari` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_rute_user`
--

CREATE TABLE `sales_rute_user` (
  `id` int(11) NOT NULL,
  `sales_rute` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE `satuan` (
  `id` int(11) NOT NULL,
  `nama_satuan` varchar(155) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`id`, `nama_satuan`, `parent`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(892, 'DUS', NULL, '2019-11-12', 1, NULL, NULL, 0),
(893, 'PCS', 892, '2019-11-12', 1, NULL, NULL, 0),
(894, 'RTG', 892, '2019-11-12', 1, NULL, NULL, 0),
(895, 'PCS', 894, '2019-11-12', 1, NULL, NULL, 0),
(896, 'PAK', 892, '2019-11-12', 1, NULL, NULL, 0),
(897, 'BAL', NULL, '2019-11-12', 1, NULL, NULL, 0),
(898, 'PCS', 897, '2019-11-12', 1, NULL, NULL, 0),
(899, 'PCS', 896, '2019-11-12', 1, NULL, NULL, 0),
(900, 'BAG', 892, '2019-11-12', 1, NULL, NULL, 0),
(901, 'TPL', 892, '2019-11-12', 1, NULL, NULL, 0),
(902, 'PCS', 901, '2019-11-12', 1, NULL, NULL, 0),
(903, 'RTG', 894, '2019-11-12', 1, NULL, NULL, 0),
(904, 'RTG', 896, '2019-11-12', 1, NULL, NULL, 0),
(905, 'RTG', 897, '2019-11-12', 1, NULL, NULL, 0),
(924, 'DUS', 893, '2019-11-30', 1, NULL, NULL, 0),
(925, 'RTG', 893, '2019-11-30', 1, NULL, NULL, 0),
(926, 'DUS', 894, '2019-11-30', 1, NULL, NULL, 0),
(927, 'PCS', 893, '2019-11-30', 1, NULL, NULL, 0),
(928, 'BAL', 893, '2019-11-30', 1, NULL, NULL, 0),
(929, 'PAK', 893, '2019-11-30', 1, NULL, NULL, 0),
(930, 'DUS', 896, '2019-11-30', 1, NULL, NULL, 0),
(931, 'DUS', 900, '2019-11-30', 1, NULL, NULL, 0),
(932, 'DUS', 901, '2019-11-30', 1, NULL, NULL, 0),
(933, 'TPL', 893, '2019-11-30', 1, NULL, NULL, 0),
(934, 'LYR', 893, '2019-11-30', 1, NULL, NULL, 0),
(935, 'DUS', 934, '2019-11-30', 1, NULL, NULL, 0),
(936, 'PAK', 894, '2019-11-30', 1, NULL, NULL, 0),
(937, 'SLP', 893, '2019-11-30', 1, NULL, NULL, 0),
(938, 'DUS', 937, '2019-11-30', 1, NULL, NULL, 0),
(939, 'BOX', 943, '2019-11-30', 1, NULL, NULL, 0),
(940, 'PAK', 939, '2019-11-30', 1, NULL, NULL, 0),
(941, 'DUS', 939, '2019-11-30', 1, NULL, NULL, 0),
(942, 'BAL', 894, '2019-11-30', 1, NULL, NULL, 0),
(943, 'PAK', NULL, NULL, NULL, NULL, NULL, 0),
(944, 'BOX', 892, NULL, NULL, NULL, NULL, 0),
(945, 'PCS', 939, NULL, NULL, NULL, NULL, 0),
(946, 'PCS', 944, NULL, NULL, NULL, NULL, 0),
(947, 'PCS', 904, NULL, NULL, NULL, NULL, 0),
(948, 'PCS', 905, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `status_pembayaran`
--

CREATE TABLE `status_pembayaran` (
  `id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status_pembelian`
--

CREATE TABLE `status_pembelian` (
  `id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_pembelian`
--

INSERT INTO `status_pembelian` (`id`, `status`) VALUES
(1, 'CASH'),
(2, 'KREDIT');

-- --------------------------------------------------------

--
-- Table structure for table `stok_kategori`
--

CREATE TABLE `stok_kategori` (
  `id` int(11) NOT NULL,
  `kategori` varchar(155) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stok_kategori`
--

INSERT INTO `stok_kategori` (`id`, `kategori`) VALUES
(1, 'GOOD STOCK'),
(2, 'BAD STOCK');

-- --------------------------------------------------------

--
-- Table structure for table `surat_jalan`
--

CREATE TABLE `surat_jalan` (
  `id` int(11) NOT NULL,
  `no_surat_jalan` varchar(155) DEFAULT NULL,
  `invoice` int(11) NOT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tagihan`
--

CREATE TABLE `tagihan` (
  `id` int(11) NOT NULL,
  `tagihan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `threshold_mas`
--

CREATE TABLE `threshold_mas` (
  `id` int(11) NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `period_start` date DEFAULT NULL,
  `period_end` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `threshold_mas`
--

INSERT INTO `threshold_mas` (`id`, `jumlah`, `harga`, `period_start`, `period_end`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 85, 695000, '2019-08-31', '2020-08-19', '2019-11-06', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tipe_product`
--

CREATE TABLE `tipe_product` (
  `id` int(11) NOT NULL,
  `tipe` varchar(45) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipe_product`
--

INSERT INTO `tipe_product` (`id`, `tipe`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(15, 'Pembalut', '2019-11-09', 1, NULL, NULL, 0),
(16, 'Popok', '2019-11-09', 1, NULL, NULL, 0),
(17, 'Tissue', '2019-11-09', 1, NULL, NULL, 0),
(18, 'Sembako', '2019-11-09', 1, NULL, NULL, 0),
(19, 'Bumbu', '2019-11-09', 1, NULL, NULL, 0),
(20, 'Snack', '2019-11-09', 1, NULL, NULL, 0),
(21, 'Seasonal', '2019-11-09', 1, NULL, NULL, 0),
(22, 'ATK', '2019-11-09', 1, NULL, NULL, 0),
(23, 'Candy', '2019-11-09', 1, NULL, NULL, 0),
(24, 'Jelly', '2019-11-09', 1, NULL, NULL, 0),
(25, 'Beverage', '2019-11-09', 1, NULL, NULL, 0),
(26, 'Parfum', '2019-11-09', 1, NULL, NULL, 0),
(28, 'BARANG', NULL, NULL, NULL, NULL, 0),
(29, 'Unicharm', NULL, NULL, NULL, NULL, 0),
(30, 'Columbia', NULL, NULL, NULL, NULL, 0),
(31, 'Superior', NULL, NULL, NULL, NULL, 0),
(32, 'Alligator', NULL, NULL, NULL, NULL, 0),
(33, 'Kino', NULL, NULL, NULL, NULL, 0),
(34, 'JSA', NULL, NULL, NULL, NULL, 0),
(35, 'Intrasari', NULL, NULL, NULL, NULL, 0),
(36, 'Morris', NULL, NULL, NULL, NULL, 0),
(37, 'MAC', NULL, NULL, NULL, NULL, 0),
(38, 'Freelance', NULL, NULL, NULL, NULL, 0),
(39, 'Sidogiri', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` text,
  `pegawai` int(11) DEFAULT NULL,
  `priveledge` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `pegawai`, `priveledge`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'administrator', '6d89013342e5133da1c45e095caa26a6', NULL, 1, NULL, NULL, '2019-10-14 15:28:49', 1, 0),
(2, 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', NULL, 1, '2019-10-14', 1, NULL, NULL, 0),
(3, 'bejo', 'd4c01b1d3471a1b41ad485918d2298cb', 1, 3, '2019-10-24', 1, '2019-12-02 09:26:14', 1, 1),
(4, 'dodik', '82b00125c2ec05d38220ed4e1774e084', 1, 3, '2019-12-02', 1, NULL, NULL, 0),
(5, 'dwi', '7aa2602c588c05a93baf10128861aeb9', 2, 3, '2019-12-02', 1, NULL, NULL, 0),
(6, 'risky', 'db5ee8e280c13e08523b28ca17db5ffb', 3, 3, '2019-12-02', 1, NULL, NULL, 0),
(7, 'adit', '486b6c6b267bc61677367eb6b6458764', 4, 3, '2019-12-02', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_sms_gateway`
--

CREATE TABLE `user_sms_gateway` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `url` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `id` int(11) NOT NULL,
  `nama_vendor` varchar(150) DEFAULT NULL,
  `vendor_category` int(11) NOT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`id`, `nama_vendor`, `vendor_category`, `no_hp`, `keterangan`, `alamat`, `email`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Vendor', 2, '085748233712', '-', 'Desa Bangsri 2 Rt2 Rw 4 Kecamatan Nglegok Kabupaten Blitar\nDesa Bangsri 2 Rt2 Rw 4 Kecamatan Nglegok Kabupaten Blitar', 'dodikitn@gmail.com', 1, '2019-11-09', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_category`
--

CREATE TABLE `vendor_category` (
  `id` int(11) NOT NULL,
  `kategori` varchar(45) DEFAULT NULL COMMENT 'COMPANY\nSUPPLIER'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_category`
--

INSERT INTO `vendor_category` (`id`, `kategori`) VALUES
(1, 'COMPANY'),
(2, 'SUPPLIER');

-- --------------------------------------------------------

--
-- Table structure for table `zakat_mal`
--

CREATE TABLE `zakat_mal` (
  `id` int(11) NOT NULL,
  `threshold_mas` int(11) NOT NULL,
  `tot_kas_bank` int(15) NOT NULL,
  `tot_inventori` int(15) NOT NULL,
  `tot_hutang` int(15) NOT NULL,
  `tot_bruto` int(15) NOT NULL,
  `tot_net` int(15) NOT NULL,
  `zakat_threshold` int(15) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zakat_mal`
--

INSERT INTO `zakat_mal` (`id`, `threshold_mas`, `tot_kas_bank`, `tot_inventori`, `tot_hutang`, `tot_bruto`, `tot_net`, `zakat_threshold`, `total`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(0, 1, 0, 23800000, 0, 23800000, 23800000, 59075000, 0, '2019-11-10', 1, NULL, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ansuran`
--
ALTER TABLE `ansuran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coa`
--
ALTER TABLE `coa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coa_type`
--
ALTER TABLE `coa_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_notifikasi_tagihan`
--
ALTER TABLE `data_notifikasi_tagihan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_pengiriman`
--
ALTER TABLE `document_pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dokumen_pengiriman_status`
--
ALTER TABLE `dokumen_pengiriman_status`
  ADD PRIMARY KEY (`id`,`document_pengiriman`),
  ADD KEY `fk_dokumen_pengiriman_status_document_pengiriman1_idx` (`document_pengiriman`);

--
-- Indexes for table `feature`
--
ALTER TABLE `feature`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general`
--
ALTER TABLE `general`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gudang`
--
ALTER TABLE `gudang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hari`
--
ALTER TABLE `hari`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `indetitas`
--
ALTER TABLE `indetitas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investor`
--
ALTER TABLE `investor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`,`jenis_pembayaran`,`pembeli`,`potongan`),
  ADD KEY `fk_invoice_pembeli1_idx` (`pembeli`),
  ADD KEY `fk_invoice_potongan1_idx` (`potongan`),
  ADD KEY `fk_invoice_jenis_pembayaran1_idx` (`jenis_pembayaran`);

--
-- Indexes for table `invoice_pot_product`
--
ALTER TABLE `invoice_pot_product`
  ADD PRIMARY KEY (`id`,`invoice_product`,`potongan`),
  ADD KEY `fk_invoice_pot_product_potongan1_idx` (`potongan`),
  ADD KEY `fk_invoice_pot_product_invoice_product1_idx` (`invoice_product`);

--
-- Indexes for table `invoice_print`
--
ALTER TABLE `invoice_print`
  ADD PRIMARY KEY (`id`,`user`,`invoice`),
  ADD KEY `fk_invoice_print_invoice1_idx` (`invoice`),
  ADD KEY `fk_invoice_print_user1_idx` (`user`);

--
-- Indexes for table `invoice_product`
--
ALTER TABLE `invoice_product`
  ADD PRIMARY KEY (`id`,`invoice`,`product_satuan`,`metode_bayar`,`pajak`),
  ADD KEY `fk_invoice_product_invoice1_idx` (`invoice`),
  ADD KEY `fk_invoice_product_product_satuan1_idx` (`product_satuan`),
  ADD KEY `fk_invoice_product_pajak1_idx` (`pajak`),
  ADD KEY `fk_invoice_product_metode_bayar1_idx` (`metode_bayar`);

--
-- Indexes for table `invoice_sisa`
--
ALTER TABLE `invoice_sisa`
  ADD PRIMARY KEY (`id`,`invoice`),
  ADD KEY `fk_invoice_sisa_invoice1_idx` (`invoice`);

--
-- Indexes for table `invoice_status`
--
ALTER TABLE `invoice_status`
  ADD PRIMARY KEY (`id`,`user`,`invoice`),
  ADD KEY `fk_invoice_status_invoice1_idx` (`invoice`),
  ADD KEY `fk_invoice_status_user1_idx` (`user`);

--
-- Indexes for table `jenis_akad`
--
ALTER TABLE `jenis_akad`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_notif_pengiriman`
--
ALTER TABLE `jenis_notif_pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_pembayaran`
--
ALTER TABLE `jenis_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurnal`
--
ALTER TABLE `jurnal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurnal_detail`
--
ALTER TABLE `jurnal_detail`
  ADD PRIMARY KEY (`id`,`jurnal`,`jurnal_struktur`),
  ADD KEY `fk_jurnal_detail_jurnal1_idx` (`jurnal`),
  ADD KEY `fk_jurnal_detail_jurnal_struktur1_idx` (`jurnal_struktur`);

--
-- Indexes for table `jurnal_struktur`
--
ALTER TABLE `jurnal_struktur`
  ADD PRIMARY KEY (`id`,`feature`,`coa`,`coa_type`),
  ADD KEY `fk_jurnal_struktur_coa_type1_idx` (`coa_type`),
  ADD KEY `fk_jurnal_struktur_coa1_idx` (`coa`),
  ADD KEY `fk_jurnal_struktur_feature1_idx` (`feature`);

--
-- Indexes for table `kas`
--
ALTER TABLE `kas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kasbon`
--
ALTER TABLE `kasbon`
  ADD PRIMARY KEY (`id`,`pegawai`),
  ADD KEY `fk_kasbon_pegawai1_idx` (`pegawai`);

--
-- Indexes for table `kasbon_paid`
--
ALTER TABLE `kasbon_paid`
  ADD PRIMARY KEY (`id`,`payroll`,`kasbon`),
  ADD KEY `fk_kasbon_paid_kasbon1_idx` (`kasbon`),
  ADD KEY `fk_kasbon_paid_payroll1_idx` (`payroll`);

--
-- Indexes for table `kasbon_payment`
--
ALTER TABLE `kasbon_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kasbon_payment_item`
--
ALTER TABLE `kasbon_payment_item`
  ADD PRIMARY KEY (`id`,`kasbon_payment`,`kasbon`),
  ADD KEY `fk_kasbon_payment_item_kasbon_payment1_idx` (`kasbon_payment`),
  ADD KEY `fk_kasbon_payment_item_kasbon1_idx` (`kasbon`);

--
-- Indexes for table `kasbon_sisa`
--
ALTER TABLE `kasbon_sisa`
  ADD PRIMARY KEY (`id`,`kasbon`),
  ADD KEY `fk_kasbon_sisa_kasbon1_idx` (`kasbon`);

--
-- Indexes for table `kasbon_status`
--
ALTER TABLE `kasbon_status`
  ADD PRIMARY KEY (`id`,`kasbon`),
  ADD KEY `fk_kasbon_status_kasbon1_idx` (`kasbon`);

--
-- Indexes for table `kategori_akad`
--
ALTER TABLE `kategori_akad`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_product`
--
ALTER TABLE `kategori_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kerja_sama_internal`
--
ALTER TABLE `kerja_sama_internal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_user_position`
--
ALTER TABLE `log_user_position`
  ADD PRIMARY KEY (`id`,`user`),
  ADD KEY `fk_log_user_position_user1_idx` (`user`);

--
-- Indexes for table `metode_bayar`
--
ALTER TABLE `metode_bayar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metode_pembayaran`
--
ALTER TABLE `metode_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `minggu`
--
ALTER TABLE `minggu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notif_has_jenis_pengiriman`
--
ALTER TABLE `notif_has_jenis_pengiriman`
  ADD PRIMARY KEY (`id`,`notif_jatuh_tempo`,`jenis_notif_pengiriman`),
  ADD KEY `fk_notif_has_jenis_pengiriman_notif_jatuh_tempo1_idx` (`notif_jatuh_tempo`),
  ADD KEY `fk_notif_has_jenis_pengiriman_jenis_notif_pengiriman1_idx` (`jenis_notif_pengiriman`);

--
-- Indexes for table `notif_jatuh_tempo`
--
ALTER TABLE `notif_jatuh_tempo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`,`pembeli`,`potongan`),
  ADD KEY `fk_order_pembeli1_idx` (`pembeli`),
  ADD KEY `fk_order_potongan1_idx` (`potongan`);

--
-- Indexes for table `order_pot_product`
--
ALTER TABLE `order_pot_product`
  ADD PRIMARY KEY (`id`,`order_product`,`potongan`),
  ADD KEY `fk_order_pot_product_order_product1_idx` (`order_product`),
  ADD KEY `fk_order_pot_product_potongan1_idx` (`potongan`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`id`,`product_satuan`,`order`),
  ADD KEY `fk_order_product_order1_idx` (`order`),
  ADD KEY `fk_order_product_product_satuan1_idx` (`product_satuan`);

--
-- Indexes for table `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`id`,`order`),
  ADD KEY `fk_order_status_order1_idx` (`order`);

--
-- Indexes for table `pajak`
--
ALTER TABLE `pajak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partial_payment`
--
ALTER TABLE `partial_payment`
  ADD PRIMARY KEY (`id`,`invoice`),
  ADD KEY `fk_partial_payment_invoice1_idx` (`invoice`);

--
-- Indexes for table `partial_payment_status`
--
ALTER TABLE `partial_payment_status`
  ADD PRIMARY KEY (`id`,`partial_payment`),
  ADD KEY `fk_partial_payment_status_partial_payment1_idx` (`partial_payment`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_item`
--
ALTER TABLE `payment_item`
  ADD PRIMARY KEY (`id`,`payment`,`invoice`),
  ADD KEY `fk_payment_item_payment1_idx` (`payment`),
  ADD KEY `fk_payment_item_invoice1_idx` (`invoice`);

--
-- Indexes for table `payroll`
--
ALTER TABLE `payroll`
  ADD PRIMARY KEY (`id`,`periode`,`pegawai`),
  ADD KEY `fk_payroll_pegawai1_idx` (`pegawai`),
  ADD KEY `fk_payroll_periode1_idx` (`periode`);

--
-- Indexes for table `payroll_category`
--
ALTER TABLE `payroll_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payroll_item`
--
ALTER TABLE `payroll_item`
  ADD PRIMARY KEY (`id`,`payroll`,`payroll_category`),
  ADD KEY `fk_payroll_item_payroll_category1_idx` (`payroll_category`),
  ADD KEY `fk_payroll_item_payroll1_idx` (`payroll`);

--
-- Indexes for table `payroll_workdays`
--
ALTER TABLE `payroll_workdays`
  ADD PRIMARY KEY (`id`,`payroll`),
  ADD KEY `fk_payroll_workdays_payroll1_idx` (`payroll`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai_kontrak`
--
ALTER TABLE `pegawai_kontrak`
  ADD PRIMARY KEY (`id`,`pegawai`),
  ADD KEY `fk_pegawai_kontrak_pegawai1_idx` (`pegawai`);

--
-- Indexes for table `pembayaran_has_angsuran`
--
ALTER TABLE `pembayaran_has_angsuran`
  ADD PRIMARY KEY (`id`,`pembayaran_product`),
  ADD KEY `fk_pembayaran_has_angsuran_pembayaran_product1_idx` (`pembayaran_product`);

--
-- Indexes for table `pembayaran_has_cash`
--
ALTER TABLE `pembayaran_has_cash`
  ADD PRIMARY KEY (`id`,`pembayaran_product`),
  ADD KEY `fk_pembayaran_has_cash_pembayaran_rumah1_idx` (`pembayaran_product`);

--
-- Indexes for table `pembayaran_has_product`
--
ALTER TABLE `pembayaran_has_product`
  ADD PRIMARY KEY (`id`,`pembayaran_product`),
  ADD KEY `fk_pembayaran_has_angsuran_pembayaran1_idx` (`pembayaran_product`);

--
-- Indexes for table `pembayaran_lain_lain`
--
ALTER TABLE `pembayaran_lain_lain`
  ADD PRIMARY KEY (`id`,`jenis_pembayaran`),
  ADD KEY `fk_pembayaran_lain_lain_jenis_pembayaran1_idx` (`jenis_pembayaran`);

--
-- Indexes for table `pembayaran_product`
--
ALTER TABLE `pembayaran_product`
  ADD PRIMARY KEY (`id`,`pembeli_has_product`,`status_pembayaran`),
  ADD KEY `fk_pembayaran_pembeli_has_rumah1_idx` (`pembeli_has_product`),
  ADD KEY `fk_pembayaran_status_pembayaran1_idx` (`status_pembayaran`);

--
-- Indexes for table `pembayaran_tagihan`
--
ALTER TABLE `pembayaran_tagihan`
  ADD PRIMARY KEY (`id`,`tagihan`,`jenis_pembayaran`),
  ADD KEY `fk_pembayaran_tagihan_tagihan1_idx` (`tagihan`),
  ADD KEY `fk_pembayaran_tagihan_jenis_pembayaran1_idx` (`jenis_pembayaran`);

--
-- Indexes for table `pembayaran_vendor`
--
ALTER TABLE `pembayaran_vendor`
  ADD PRIMARY KEY (`id`,`vendor`,`jenis_pembayaran`),
  ADD KEY `fk_pembayaran_vendor_vendor1_idx` (`vendor`),
  ADD KEY `fk_pembayaran_vendor_jenis_pembayaran1_idx` (`jenis_pembayaran`);

--
-- Indexes for table `pembeli`
--
ALTER TABLE `pembeli`
  ADD PRIMARY KEY (`id`,`pembeli_kategori`),
  ADD KEY `fk_pembeli_pembeli_kategori1_idx` (`pembeli_kategori`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembeli_has_angsuran`
--
ALTER TABLE `pembeli_has_angsuran`
  ADD PRIMARY KEY (`id`,`pembeli_has_product`,`product_has_harga_angsuran`),
  ADD KEY `fk_pembeli_has_angsuran_pembeli_has_rumah1_idx` (`pembeli_has_product`),
  ADD KEY `fk_pembeli_has_angsuran_rumah_has_harga_angsuran1_idx` (`product_has_harga_angsuran`);

--
-- Indexes for table `pembeli_has_identitas`
--
ALTER TABLE `pembeli_has_identitas`
  ADD PRIMARY KEY (`id`,`pembeli`,`indetitas`),
  ADD KEY `fk_kreditur_has_identitas_kreditur1_idx` (`pembeli`),
  ADD KEY `fk_kreditur_has_identitas_indetitas1_idx` (`indetitas`);

--
-- Indexes for table `pembeli_has_persyaratan`
--
ALTER TABLE `pembeli_has_persyaratan`
  ADD PRIMARY KEY (`id`,`pembelian`,`persyaratan_has_berkas`),
  ADD KEY `fk_pembeli_has_persyaratan_persyaratan_has_berkas1_idx` (`persyaratan_has_berkas`),
  ADD KEY `fk_pembeli_has_persyaratan_pembelian1_idx` (`pembelian`);

--
-- Indexes for table `pembeli_has_product`
--
ALTER TABLE `pembeli_has_product`
  ADD PRIMARY KEY (`id`,`pembelian`,`pembeli`,`product`,`status_pembelian`),
  ADD KEY `fk_kreditur_has_rumah_kreditur1_idx` (`pembeli`),
  ADD KEY `fk_kreditur_has_rumah_rumah1_idx` (`product`),
  ADD KEY `fk_pembeli_has_rumah_status_pembelian1_idx` (`status_pembelian`),
  ADD KEY `fk_pembeli_has_rumah_pembelian1_idx` (`pembelian`);

--
-- Indexes for table `pembeli_kategori`
--
ALTER TABLE `pembeli_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengiriman`
--
ALTER TABLE `pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengiriman_detail`
--
ALTER TABLE `pengiriman_detail`
  ADD PRIMARY KEY (`id`,`pengiriman`,`invoice_product`),
  ADD KEY `fk_pengiriman_detail_pengiriman1_idx` (`pengiriman`),
  ADD KEY `fk_pengiriman_detail_invoice_product1_idx` (`invoice_product`);

--
-- Indexes for table `pengiriman_has_customer`
--
ALTER TABLE `pengiriman_has_customer`
  ADD PRIMARY KEY (`id`,`pengiriman`,`pembeli`),
  ADD KEY `fk_pengiriman_has_customer_pembeli1_idx` (`pembeli`),
  ADD KEY `fk_pengiriman_has_customer_pengiriman1_idx` (`pengiriman`);

--
-- Indexes for table `pengiriman_has_sales`
--
ALTER TABLE `pengiriman_has_sales`
  ADD PRIMARY KEY (`id`,`user`,`pengiriman`),
  ADD KEY `fk_pengiriman_has_sales_pengiriman1_idx` (`pengiriman`),
  ADD KEY `fk_pengiriman_has_sales_user1_idx` (`user`);

--
-- Indexes for table `pengiriman_status`
--
ALTER TABLE `pengiriman_status`
  ADD PRIMARY KEY (`id`,`pengiriman`),
  ADD KEY `fk_pengiriman_status_pengiriman1_idx` (`pengiriman`);

--
-- Indexes for table `periode`
--
ALTER TABLE `periode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `persyaratan`
--
ALTER TABLE `persyaratan`
  ADD PRIMARY KEY (`id`,`kategori_akad`,`jenis_akad`),
  ADD KEY `fk_persyaratan_kategori_akad1_idx` (`kategori_akad`),
  ADD KEY `fk_persyaratan_jenis_akad1_idx` (`jenis_akad`);

--
-- Indexes for table `persyaratan_has_berkas`
--
ALTER TABLE `persyaratan_has_berkas`
  ADD PRIMARY KEY (`id`,`persyaratan`),
  ADD KEY `fk_persyaratan_has_berkas_persyaratan1_idx` (`persyaratan`);

--
-- Indexes for table `potongan`
--
ALTER TABLE `potongan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `priveledge`
--
ALTER TABLE `priveledge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procurement`
--
ALTER TABLE `procurement`
  ADD PRIMARY KEY (`id`,`vendor`),
  ADD KEY `fk_procurement_vendor1_idx` (`vendor`);

--
-- Indexes for table `procurement_item`
--
ALTER TABLE `procurement_item`
  ADD PRIMARY KEY (`id`,`procurement`,`product`,`satuan`),
  ADD KEY `fk_procurement_item_product1_idx` (`product`),
  ADD KEY `fk_procurement_item_satuan1_idx` (`satuan`),
  ADD KEY `fk_procurement_item_procurement1_idx` (`procurement`);

--
-- Indexes for table `procurement_retur`
--
ALTER TABLE `procurement_retur`
  ADD PRIMARY KEY (`id`,`procurement`),
  ADD KEY `fk_procurement_retur_procurement1_idx` (`procurement`);

--
-- Indexes for table `procurement_retur_item`
--
ALTER TABLE `procurement_retur_item`
  ADD PRIMARY KEY (`id`,`procurement_item`,`procurement_retur`),
  ADD KEY `fk_procurement_return_item_procurement_retur1_idx` (`procurement_retur`),
  ADD KEY `fk_procurement_return_item_procurement_item1_idx` (`procurement_item`);

--
-- Indexes for table `procurement_status`
--
ALTER TABLE `procurement_status`
  ADD PRIMARY KEY (`id`,`procurement`),
  ADD KEY `fk_procurement_status_procurement1_idx` (`procurement`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`,`tipe_product`,`kategori_product`),
  ADD KEY `fk_rumah_kategori_rumah1_idx` (`kategori_product`),
  ADD KEY `fk_rumah_tipe_rumah1_idx` (`tipe_product`);

--
-- Indexes for table `product_distriburst`
--
ALTER TABLE `product_distriburst`
  ADD PRIMARY KEY (`id`,`pembeli`,`product`),
  ADD KEY `fk_product_distriburst_product1_idx` (`product`),
  ADD KEY `fk_product_distriburst_pembeli1_idx` (`pembeli`);

--
-- Indexes for table `product_has_foto`
--
ALTER TABLE `product_has_foto`
  ADD PRIMARY KEY (`id`,`product`),
  ADD KEY `fk_rumah_has_foto_rumah1_idx` (`product`);

--
-- Indexes for table `product_has_harga_angsuran`
--
ALTER TABLE `product_has_harga_angsuran`
  ADD PRIMARY KEY (`id`,`product`,`ansuran`),
  ADD KEY `fk_rumah_has_harga_angsuran_rumah1_idx` (`product`),
  ADD KEY `fk_rumah_has_harga_angsuran_ansuran1_idx` (`ansuran`);

--
-- Indexes for table `product_has_harga_jual_pokok`
--
ALTER TABLE `product_has_harga_jual_pokok`
  ADD PRIMARY KEY (`id`,`product`),
  ADD KEY `fk_rumah_has_harga_jual_rumah1_idx` (`product`);

--
-- Indexes for table `product_has_harga_jual_tunai`
--
ALTER TABLE `product_has_harga_jual_tunai`
  ADD PRIMARY KEY (`id`,`product`),
  ADD KEY `fk_rumah_has_harga_jual_kredit_rumah1_idx` (`product`);

--
-- Indexes for table `product_kirim`
--
ALTER TABLE `product_kirim`
  ADD PRIMARY KEY (`id`,`product`,`document_pengiriman`),
  ADD KEY `fk_product_kirim_document_pengiriman1_idx` (`document_pengiriman`),
  ADD KEY `fk_product_kirim_product1_idx` (`product`);

--
-- Indexes for table `product_kirim_status`
--
ALTER TABLE `product_kirim_status`
  ADD PRIMARY KEY (`id`,`product_kirim`),
  ADD KEY `fk_product_kirim_status_product_kirim1_idx` (`product_kirim`);

--
-- Indexes for table `product_log_stock`
--
ALTER TABLE `product_log_stock`
  ADD PRIMARY KEY (`id`,`product_satuan`),
  ADD KEY `fk_product_log_stock_product_satuan1_idx` (`product_satuan`);

--
-- Indexes for table `product_satuan`
--
ALTER TABLE `product_satuan`
  ADD PRIMARY KEY (`id`,`product`,`satuan`),
  ADD KEY `fk_product_satuan_product1_idx` (`product`),
  ADD KEY `fk_product_satuan_satuan1_idx` (`satuan`);

--
-- Indexes for table `product_stock`
--
ALTER TABLE `product_stock`
  ADD PRIMARY KEY (`id`,`product_satuan`),
  ADD KEY `fk_product_stock_product_satuan1_idx` (`product_satuan`);

--
-- Indexes for table `product_stock_minus`
--
ALTER TABLE `product_stock_minus`
  ADD PRIMARY KEY (`id`,`product_stock`),
  ADD KEY `fk_product_stock_minus_product_stock1_idx` (`product_stock`);

--
-- Indexes for table `product_surat_jalan`
--
ALTER TABLE `product_surat_jalan`
  ADD PRIMARY KEY (`id`,`document_pengiriman`),
  ADD KEY `fk_product_surat_jalan_document_pengiriman1_idx` (`document_pengiriman`);

--
-- Indexes for table `product_vendor`
--
ALTER TABLE `product_vendor`
  ADD PRIMARY KEY (`id`,`product`,`vendor`),
  ADD KEY `fk_product_vendor_product1_idx` (`product`),
  ADD KEY `fk_product_vendor_vendor1_idx` (`vendor`);

--
-- Indexes for table `rak`
--
ALTER TABLE `rak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reimburse`
--
ALTER TABLE `reimburse`
  ADD PRIMARY KEY (`id`,`pegawai`),
  ADD KEY `fk_reimburse_pegawai1_idx` (`pegawai`);

--
-- Indexes for table `return_penjualan`
--
ALTER TABLE `return_penjualan`
  ADD PRIMARY KEY (`id`,`invoice`),
  ADD KEY `fk_return_penjualan_invoice1_idx` (`invoice`);

--
-- Indexes for table `return_penjualan_item`
--
ALTER TABLE `return_penjualan_item`
  ADD PRIMARY KEY (`id`,`return_penjualan`,`invoice_product`),
  ADD KEY `fk_return_penjualan_item_return_penjualan1_idx` (`return_penjualan`),
  ADD KEY `fk_return_penjualan_item_invoice_product1_idx` (`invoice_product`);

--
-- Indexes for table `retur_barang`
--
ALTER TABLE `retur_barang`
  ADD PRIMARY KEY (`id`,`pembeli`),
  ADD KEY `fk_retur_barang_pembeli1_idx` (`pembeli`);

--
-- Indexes for table `retur_barang_item`
--
ALTER TABLE `retur_barang_item`
  ADD PRIMARY KEY (`id`,`product_satuan`,`retur_barang`,`stok_kategori`),
  ADD KEY `fk_retur_barang_item_retur_barang1_idx` (`retur_barang`),
  ADD KEY `fk_retur_barang_item_stok_kategori1_idx` (`stok_kategori`),
  ADD KEY `fk_retur_barang_item_product_satuan1_idx` (`product_satuan`);

--
-- Indexes for table `retur_barang_status`
--
ALTER TABLE `retur_barang_status`
  ADD PRIMARY KEY (`id`,`retur_barang`),
  ADD KEY `fk_retur_barang_status_retur_barang1_idx` (`retur_barang`);

--
-- Indexes for table `retur_order`
--
ALTER TABLE `retur_order`
  ADD PRIMARY KEY (`id`,`order`),
  ADD KEY `fk_retur_order_order1_idx` (`order`);

--
-- Indexes for table `retur_order_item`
--
ALTER TABLE `retur_order_item`
  ADD PRIMARY KEY (`id`,`retur_order`,`order_product`),
  ADD KEY `fk_retur_order_item_order_product1_idx` (`order_product`),
  ADD KEY `fk_retur_order_item_retur_order1_idx` (`retur_order`);

--
-- Indexes for table `sales_rute`
--
ALTER TABLE `sales_rute`
  ADD PRIMARY KEY (`id`,`minggu`),
  ADD KEY `fk_sales_rute_minggu1_idx` (`minggu`);

--
-- Indexes for table `sales_rute_customer`
--
ALTER TABLE `sales_rute_customer`
  ADD PRIMARY KEY (`id`,`sales_rute`,`pembeli`),
  ADD KEY `fk_sales_rute_customer_pembeli1_idx` (`pembeli`),
  ADD KEY `fk_sales_rute_customer_sales_rute1_idx` (`sales_rute`);

--
-- Indexes for table `sales_rute_hari`
--
ALTER TABLE `sales_rute_hari`
  ADD PRIMARY KEY (`id`,`sales_rute`,`hari`),
  ADD KEY `fk_sales_rute_hari_sales_rute1_idx` (`sales_rute`),
  ADD KEY `fk_sales_rute_hari_hari1_idx` (`hari`);

--
-- Indexes for table `sales_rute_user`
--
ALTER TABLE `sales_rute_user`
  ADD PRIMARY KEY (`id`,`sales_rute`,`user`),
  ADD KEY `fk_sales_rute_user_user1_idx` (`user`),
  ADD KEY `fk_sales_rute_user_sales_rute1_idx` (`sales_rute`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_pembayaran`
--
ALTER TABLE `status_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_pembelian`
--
ALTER TABLE `status_pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stok_kategori`
--
ALTER TABLE `stok_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surat_jalan`
--
ALTER TABLE `surat_jalan`
  ADD PRIMARY KEY (`id`,`invoice`),
  ADD KEY `fk_surat_jalan_invoice1_idx` (`invoice`);

--
-- Indexes for table `tagihan`
--
ALTER TABLE `tagihan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `threshold_mas`
--
ALTER TABLE `threshold_mas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipe_product`
--
ALTER TABLE `tipe_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`,`priveledge`),
  ADD KEY `fk_user_priveledge_idx` (`priveledge`);

--
-- Indexes for table `user_sms_gateway`
--
ALTER TABLE `user_sms_gateway`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`id`,`vendor_category`),
  ADD KEY `fk_vendor_vendor_category1_idx` (`vendor_category`);

--
-- Indexes for table `vendor_category`
--
ALTER TABLE `vendor_category`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ansuran`
--
ALTER TABLE `ansuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `coa`
--
ALTER TABLE `coa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `coa_type`
--
ALTER TABLE `coa_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_notifikasi_tagihan`
--
ALTER TABLE `data_notifikasi_tagihan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `document_pengiriman`
--
ALTER TABLE `document_pengiriman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dokumen_pengiriman_status`
--
ALTER TABLE `dokumen_pengiriman_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feature`
--
ALTER TABLE `feature`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `general`
--
ALTER TABLE `general`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `gudang`
--
ALTER TABLE `gudang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `hari`
--
ALTER TABLE `hari`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `indetitas`
--
ALTER TABLE `indetitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `investor`
--
ALTER TABLE `investor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `invoice_pot_product`
--
ALTER TABLE `invoice_pot_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `invoice_print`
--
ALTER TABLE `invoice_print`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_product`
--
ALTER TABLE `invoice_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `invoice_sisa`
--
ALTER TABLE `invoice_sisa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `invoice_status`
--
ALTER TABLE `invoice_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `jenis_akad`
--
ALTER TABLE `jenis_akad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `jenis_notif_pengiriman`
--
ALTER TABLE `jenis_notif_pengiriman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jenis_pembayaran`
--
ALTER TABLE `jenis_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jurnal`
--
ALTER TABLE `jurnal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `jurnal_detail`
--
ALTER TABLE `jurnal_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `jurnal_struktur`
--
ALTER TABLE `jurnal_struktur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `kas`
--
ALTER TABLE `kas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kasbon`
--
ALTER TABLE `kasbon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kasbon_paid`
--
ALTER TABLE `kasbon_paid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kasbon_payment`
--
ALTER TABLE `kasbon_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kasbon_payment_item`
--
ALTER TABLE `kasbon_payment_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kasbon_sisa`
--
ALTER TABLE `kasbon_sisa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kasbon_status`
--
ALTER TABLE `kasbon_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kategori_akad`
--
ALTER TABLE `kategori_akad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kategori_product`
--
ALTER TABLE `kategori_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kerja_sama_internal`
--
ALTER TABLE `kerja_sama_internal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `log_user_position`
--
ALTER TABLE `log_user_position`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `metode_bayar`
--
ALTER TABLE `metode_bayar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `metode_pembayaran`
--
ALTER TABLE `metode_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `minggu`
--
ALTER TABLE `minggu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `notif_has_jenis_pengiriman`
--
ALTER TABLE `notif_has_jenis_pengiriman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `notif_jatuh_tempo`
--
ALTER TABLE `notif_jatuh_tempo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `order_pot_product`
--
ALTER TABLE `order_pot_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `order_status`
--
ALTER TABLE `order_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `pajak`
--
ALTER TABLE `pajak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `partial_payment`
--
ALTER TABLE `partial_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `partial_payment_status`
--
ALTER TABLE `partial_payment_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_item`
--
ALTER TABLE `payment_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payroll`
--
ALTER TABLE `payroll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payroll_category`
--
ALTER TABLE `payroll_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payroll_item`
--
ALTER TABLE `payroll_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payroll_workdays`
--
ALTER TABLE `payroll_workdays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pegawai_kontrak`
--
ALTER TABLE `pegawai_kontrak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_has_angsuran`
--
ALTER TABLE `pembayaran_has_angsuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_has_cash`
--
ALTER TABLE `pembayaran_has_cash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_has_product`
--
ALTER TABLE `pembayaran_has_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_lain_lain`
--
ALTER TABLE `pembayaran_lain_lain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_product`
--
ALTER TABLE `pembayaran_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_tagihan`
--
ALTER TABLE `pembayaran_tagihan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_vendor`
--
ALTER TABLE `pembayaran_vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembeli`
--
ALTER TABLE `pembeli`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=380;

--
-- AUTO_INCREMENT for table `pembelian`
--
ALTER TABLE `pembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembeli_has_angsuran`
--
ALTER TABLE `pembeli_has_angsuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembeli_has_identitas`
--
ALTER TABLE `pembeli_has_identitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembeli_has_persyaratan`
--
ALTER TABLE `pembeli_has_persyaratan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembeli_has_product`
--
ALTER TABLE `pembeli_has_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembeli_kategori`
--
ALTER TABLE `pembeli_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pengiriman`
--
ALTER TABLE `pengiriman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `pengiriman_detail`
--
ALTER TABLE `pengiriman_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `pengiriman_has_customer`
--
ALTER TABLE `pengiriman_has_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `pengiriman_has_sales`
--
ALTER TABLE `pengiriman_has_sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengiriman_status`
--
ALTER TABLE `pengiriman_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `periode`
--
ALTER TABLE `periode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `persyaratan`
--
ALTER TABLE `persyaratan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `persyaratan_has_berkas`
--
ALTER TABLE `persyaratan_has_berkas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `potongan`
--
ALTER TABLE `potongan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `priveledge`
--
ALTER TABLE `priveledge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `procurement`
--
ALTER TABLE `procurement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `procurement_item`
--
ALTER TABLE `procurement_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `procurement_retur`
--
ALTER TABLE `procurement_retur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `procurement_retur_item`
--
ALTER TABLE `procurement_retur_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `procurement_status`
--
ALTER TABLE `procurement_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3158;

--
-- AUTO_INCREMENT for table `product_distriburst`
--
ALTER TABLE `product_distriburst`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_has_foto`
--
ALTER TABLE `product_has_foto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_has_harga_angsuran`
--
ALTER TABLE `product_has_harga_angsuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_has_harga_jual_pokok`
--
ALTER TABLE `product_has_harga_jual_pokok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_has_harga_jual_tunai`
--
ALTER TABLE `product_has_harga_jual_tunai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_kirim`
--
ALTER TABLE `product_kirim`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_kirim_status`
--
ALTER TABLE `product_kirim_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_log_stock`
--
ALTER TABLE `product_log_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `product_satuan`
--
ALTER TABLE `product_satuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6120;

--
-- AUTO_INCREMENT for table `product_stock`
--
ALTER TABLE `product_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7362;

--
-- AUTO_INCREMENT for table `product_stock_minus`
--
ALTER TABLE `product_stock_minus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_surat_jalan`
--
ALTER TABLE `product_surat_jalan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_vendor`
--
ALTER TABLE `product_vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rak`
--
ALTER TABLE `rak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reimburse`
--
ALTER TABLE `reimburse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `return_penjualan`
--
ALTER TABLE `return_penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `return_penjualan_item`
--
ALTER TABLE `return_penjualan_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `retur_barang`
--
ALTER TABLE `retur_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `retur_barang_item`
--
ALTER TABLE `retur_barang_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `retur_barang_status`
--
ALTER TABLE `retur_barang_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `retur_order`
--
ALTER TABLE `retur_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `retur_order_item`
--
ALTER TABLE `retur_order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_rute`
--
ALTER TABLE `sales_rute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_rute_customer`
--
ALTER TABLE `sales_rute_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_rute_hari`
--
ALTER TABLE `sales_rute_hari`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_rute_user`
--
ALTER TABLE `sales_rute_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=949;

--
-- AUTO_INCREMENT for table `status_pembayaran`
--
ALTER TABLE `status_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `status_pembelian`
--
ALTER TABLE `status_pembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stok_kategori`
--
ALTER TABLE `stok_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `surat_jalan`
--
ALTER TABLE `surat_jalan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tagihan`
--
ALTER TABLE `tagihan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `threshold_mas`
--
ALTER TABLE `threshold_mas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tipe_product`
--
ALTER TABLE `tipe_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_sms_gateway`
--
ALTER TABLE `user_sms_gateway`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vendor_category`
--
ALTER TABLE `vendor_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dokumen_pengiriman_status`
--
ALTER TABLE `dokumen_pengiriman_status`
  ADD CONSTRAINT `fk_dokumen_pengiriman_status_document_pengiriman1` FOREIGN KEY (`document_pengiriman`) REFERENCES `document_pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `fk_invoice_jenis_pembayaran1` FOREIGN KEY (`jenis_pembayaran`) REFERENCES `jenis_pembayaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_pembeli1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_potongan1` FOREIGN KEY (`potongan`) REFERENCES `potongan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice_pot_product`
--
ALTER TABLE `invoice_pot_product`
  ADD CONSTRAINT `fk_invoice_pot_product_invoice_product1` FOREIGN KEY (`invoice_product`) REFERENCES `invoice_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_pot_product_potongan1` FOREIGN KEY (`potongan`) REFERENCES `potongan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice_print`
--
ALTER TABLE `invoice_print`
  ADD CONSTRAINT `fk_invoice_print_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_print_user1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice_product`
--
ALTER TABLE `invoice_product`
  ADD CONSTRAINT `fk_invoice_product_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_product_metode_bayar1` FOREIGN KEY (`metode_bayar`) REFERENCES `metode_bayar` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_product_pajak1` FOREIGN KEY (`pajak`) REFERENCES `pajak` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_product_product_satuan1` FOREIGN KEY (`product_satuan`) REFERENCES `product_satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice_sisa`
--
ALTER TABLE `invoice_sisa`
  ADD CONSTRAINT `fk_invoice_sisa_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice_status`
--
ALTER TABLE `invoice_status`
  ADD CONSTRAINT `fk_invoice_status_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_status_user1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `jurnal_detail`
--
ALTER TABLE `jurnal_detail`
  ADD CONSTRAINT `fk_jurnal_detail_jurnal1` FOREIGN KEY (`jurnal`) REFERENCES `jurnal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_jurnal_detail_jurnal_struktur1` FOREIGN KEY (`jurnal_struktur`) REFERENCES `jurnal_struktur` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `jurnal_struktur`
--
ALTER TABLE `jurnal_struktur`
  ADD CONSTRAINT `fk_jurnal_struktur_coa1` FOREIGN KEY (`coa`) REFERENCES `coa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_jurnal_struktur_coa_type1` FOREIGN KEY (`coa_type`) REFERENCES `coa_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_jurnal_struktur_feature1` FOREIGN KEY (`feature`) REFERENCES `feature` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kasbon`
--
ALTER TABLE `kasbon`
  ADD CONSTRAINT `fk_kasbon_pegawai1` FOREIGN KEY (`pegawai`) REFERENCES `pegawai` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kasbon_paid`
--
ALTER TABLE `kasbon_paid`
  ADD CONSTRAINT `fk_kasbon_paid_kasbon1` FOREIGN KEY (`kasbon`) REFERENCES `kasbon` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_kasbon_paid_payroll1` FOREIGN KEY (`payroll`) REFERENCES `payroll` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kasbon_payment_item`
--
ALTER TABLE `kasbon_payment_item`
  ADD CONSTRAINT `fk_kasbon_payment_item_kasbon1` FOREIGN KEY (`kasbon`) REFERENCES `kasbon` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_kasbon_payment_item_kasbon_payment1` FOREIGN KEY (`kasbon_payment`) REFERENCES `kasbon_payment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kasbon_sisa`
--
ALTER TABLE `kasbon_sisa`
  ADD CONSTRAINT `fk_kasbon_sisa_kasbon1` FOREIGN KEY (`kasbon`) REFERENCES `kasbon` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kasbon_status`
--
ALTER TABLE `kasbon_status`
  ADD CONSTRAINT `fk_kasbon_status_kasbon1` FOREIGN KEY (`kasbon`) REFERENCES `kasbon` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `log_user_position`
--
ALTER TABLE `log_user_position`
  ADD CONSTRAINT `fk_log_user_position_user1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `notif_has_jenis_pengiriman`
--
ALTER TABLE `notif_has_jenis_pengiriman`
  ADD CONSTRAINT `fk_notif_has_jenis_pengiriman_jenis_notif_pengiriman1` FOREIGN KEY (`jenis_notif_pengiriman`) REFERENCES `jenis_notif_pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_notif_has_jenis_pengiriman_notif_jatuh_tempo1` FOREIGN KEY (`notif_jatuh_tempo`) REFERENCES `notif_jatuh_tempo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `fk_order_pembeli1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_potongan1` FOREIGN KEY (`potongan`) REFERENCES `potongan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order_pot_product`
--
ALTER TABLE `order_pot_product`
  ADD CONSTRAINT `fk_order_pot_product_order_product1` FOREIGN KEY (`order_product`) REFERENCES `order_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_pot_product_potongan1` FOREIGN KEY (`potongan`) REFERENCES `potongan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order_product`
--
ALTER TABLE `order_product`
  ADD CONSTRAINT `fk_order_product_order1` FOREIGN KEY (`order`) REFERENCES `order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_product_product_satuan1` FOREIGN KEY (`product_satuan`) REFERENCES `product_satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order_status`
--
ALTER TABLE `order_status`
  ADD CONSTRAINT `fk_order_status_order1` FOREIGN KEY (`order`) REFERENCES `order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `partial_payment`
--
ALTER TABLE `partial_payment`
  ADD CONSTRAINT `fk_partial_payment_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `partial_payment_status`
--
ALTER TABLE `partial_payment_status`
  ADD CONSTRAINT `fk_partial_payment_status_partial_payment1` FOREIGN KEY (`partial_payment`) REFERENCES `partial_payment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payment_item`
--
ALTER TABLE `payment_item`
  ADD CONSTRAINT `fk_payment_item_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_payment_item_payment1` FOREIGN KEY (`payment`) REFERENCES `payment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payroll`
--
ALTER TABLE `payroll`
  ADD CONSTRAINT `fk_payroll_pegawai1` FOREIGN KEY (`pegawai`) REFERENCES `pegawai` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_payroll_periode1` FOREIGN KEY (`periode`) REFERENCES `periode` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payroll_item`
--
ALTER TABLE `payroll_item`
  ADD CONSTRAINT `fk_payroll_item_payroll1` FOREIGN KEY (`payroll`) REFERENCES `payroll` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_payroll_item_payroll_category1` FOREIGN KEY (`payroll_category`) REFERENCES `payroll_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payroll_workdays`
--
ALTER TABLE `payroll_workdays`
  ADD CONSTRAINT `fk_payroll_workdays_payroll1` FOREIGN KEY (`payroll`) REFERENCES `payroll` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pegawai_kontrak`
--
ALTER TABLE `pegawai_kontrak`
  ADD CONSTRAINT `fk_pegawai_kontrak_pegawai1` FOREIGN KEY (`pegawai`) REFERENCES `pegawai` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_has_angsuran`
--
ALTER TABLE `pembayaran_has_angsuran`
  ADD CONSTRAINT `fk_pembayaran_has_angsuran_pembayaran_product1` FOREIGN KEY (`pembayaran_product`) REFERENCES `pembayaran_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_has_cash`
--
ALTER TABLE `pembayaran_has_cash`
  ADD CONSTRAINT `fk_pembayaran_has_cash_pembayaran_rumah1` FOREIGN KEY (`pembayaran_product`) REFERENCES `pembayaran_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_has_product`
--
ALTER TABLE `pembayaran_has_product`
  ADD CONSTRAINT `fk_pembayaran_has_angsuran_pembayaran1` FOREIGN KEY (`pembayaran_product`) REFERENCES `pembayaran_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_lain_lain`
--
ALTER TABLE `pembayaran_lain_lain`
  ADD CONSTRAINT `fk_pembayaran_lain_lain_jenis_pembayaran1` FOREIGN KEY (`jenis_pembayaran`) REFERENCES `jenis_pembayaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_product`
--
ALTER TABLE `pembayaran_product`
  ADD CONSTRAINT `fk_pembayaran_pembeli_has_rumah1` FOREIGN KEY (`pembeli_has_product`) REFERENCES `pembeli_has_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembayaran_status_pembayaran1` FOREIGN KEY (`status_pembayaran`) REFERENCES `status_pembayaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_tagihan`
--
ALTER TABLE `pembayaran_tagihan`
  ADD CONSTRAINT `fk_pembayaran_tagihan_jenis_pembayaran1` FOREIGN KEY (`jenis_pembayaran`) REFERENCES `jenis_pembayaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembayaran_tagihan_tagihan1` FOREIGN KEY (`tagihan`) REFERENCES `tagihan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_vendor`
--
ALTER TABLE `pembayaran_vendor`
  ADD CONSTRAINT `fk_pembayaran_vendor_jenis_pembayaran1` FOREIGN KEY (`jenis_pembayaran`) REFERENCES `jenis_pembayaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembayaran_vendor_vendor1` FOREIGN KEY (`vendor`) REFERENCES `vendor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembeli`
--
ALTER TABLE `pembeli`
  ADD CONSTRAINT `fk_pembeli_pembeli_kategori1` FOREIGN KEY (`pembeli_kategori`) REFERENCES `pembeli_kategori` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembeli_has_angsuran`
--
ALTER TABLE `pembeli_has_angsuran`
  ADD CONSTRAINT `fk_pembeli_has_angsuran_pembeli_has_rumah1` FOREIGN KEY (`pembeli_has_product`) REFERENCES `pembeli_has_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembeli_has_angsuran_rumah_has_harga_angsuran1` FOREIGN KEY (`product_has_harga_angsuran`) REFERENCES `product_has_harga_angsuran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembeli_has_identitas`
--
ALTER TABLE `pembeli_has_identitas`
  ADD CONSTRAINT `fk_kreditur_has_identitas_indetitas1` FOREIGN KEY (`indetitas`) REFERENCES `indetitas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_kreditur_has_identitas_kreditur1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembeli_has_persyaratan`
--
ALTER TABLE `pembeli_has_persyaratan`
  ADD CONSTRAINT `fk_pembeli_has_persyaratan_pembelian1` FOREIGN KEY (`pembelian`) REFERENCES `pembelian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembeli_has_persyaratan_persyaratan_has_berkas1` FOREIGN KEY (`persyaratan_has_berkas`) REFERENCES `persyaratan_has_berkas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembeli_has_product`
--
ALTER TABLE `pembeli_has_product`
  ADD CONSTRAINT `fk_kreditur_has_rumah_kreditur1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_kreditur_has_rumah_rumah1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembeli_has_rumah_pembelian1` FOREIGN KEY (`pembelian`) REFERENCES `pembelian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembeli_has_rumah_status_pembelian1` FOREIGN KEY (`status_pembelian`) REFERENCES `status_pembelian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pengiriman_detail`
--
ALTER TABLE `pengiriman_detail`
  ADD CONSTRAINT `fk_pengiriman_detail_invoice_product1` FOREIGN KEY (`invoice_product`) REFERENCES `invoice_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pengiriman_detail_pengiriman1` FOREIGN KEY (`pengiriman`) REFERENCES `pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pengiriman_has_customer`
--
ALTER TABLE `pengiriman_has_customer`
  ADD CONSTRAINT `fk_pengiriman_has_customer_pembeli1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pengiriman_has_customer_pengiriman1` FOREIGN KEY (`pengiriman`) REFERENCES `pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pengiriman_has_sales`
--
ALTER TABLE `pengiriman_has_sales`
  ADD CONSTRAINT `fk_pengiriman_has_sales_pengiriman1` FOREIGN KEY (`pengiriman`) REFERENCES `pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pengiriman_has_sales_user1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pengiriman_status`
--
ALTER TABLE `pengiriman_status`
  ADD CONSTRAINT `fk_pengiriman_status_pengiriman1` FOREIGN KEY (`pengiriman`) REFERENCES `pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `persyaratan`
--
ALTER TABLE `persyaratan`
  ADD CONSTRAINT `fk_persyaratan_jenis_akad1` FOREIGN KEY (`jenis_akad`) REFERENCES `jenis_akad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_persyaratan_kategori_akad1` FOREIGN KEY (`kategori_akad`) REFERENCES `kategori_akad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `persyaratan_has_berkas`
--
ALTER TABLE `persyaratan_has_berkas`
  ADD CONSTRAINT `fk_persyaratan_has_berkas_persyaratan1` FOREIGN KEY (`persyaratan`) REFERENCES `persyaratan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `procurement`
--
ALTER TABLE `procurement`
  ADD CONSTRAINT `fk_procurement_vendor1` FOREIGN KEY (`vendor`) REFERENCES `vendor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `procurement_item`
--
ALTER TABLE `procurement_item`
  ADD CONSTRAINT `fk_procurement_item_procurement1` FOREIGN KEY (`procurement`) REFERENCES `procurement` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_procurement_item_product1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_procurement_item_satuan1` FOREIGN KEY (`satuan`) REFERENCES `satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `procurement_retur`
--
ALTER TABLE `procurement_retur`
  ADD CONSTRAINT `fk_procurement_retur_procurement1` FOREIGN KEY (`procurement`) REFERENCES `procurement` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `procurement_retur_item`
--
ALTER TABLE `procurement_retur_item`
  ADD CONSTRAINT `fk_procurement_return_item_procurement_item1` FOREIGN KEY (`procurement_item`) REFERENCES `procurement_item` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_procurement_return_item_procurement_retur1` FOREIGN KEY (`procurement_retur`) REFERENCES `procurement_retur` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `procurement_status`
--
ALTER TABLE `procurement_status`
  ADD CONSTRAINT `fk_procurement_status_procurement1` FOREIGN KEY (`procurement`) REFERENCES `procurement` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_rumah_kategori_rumah1` FOREIGN KEY (`kategori_product`) REFERENCES `kategori_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rumah_tipe_rumah1` FOREIGN KEY (`tipe_product`) REFERENCES `tipe_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_distriburst`
--
ALTER TABLE `product_distriburst`
  ADD CONSTRAINT `fk_product_distriburst_pembeli1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_distriburst_product1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_has_foto`
--
ALTER TABLE `product_has_foto`
  ADD CONSTRAINT `fk_rumah_has_foto_rumah1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_has_harga_angsuran`
--
ALTER TABLE `product_has_harga_angsuran`
  ADD CONSTRAINT `fk_rumah_has_harga_angsuran_ansuran1` FOREIGN KEY (`ansuran`) REFERENCES `ansuran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rumah_has_harga_angsuran_rumah1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_has_harga_jual_pokok`
--
ALTER TABLE `product_has_harga_jual_pokok`
  ADD CONSTRAINT `fk_rumah_has_harga_jual_rumah1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_has_harga_jual_tunai`
--
ALTER TABLE `product_has_harga_jual_tunai`
  ADD CONSTRAINT `fk_rumah_has_harga_jual_kredit_rumah1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_kirim`
--
ALTER TABLE `product_kirim`
  ADD CONSTRAINT `fk_product_kirim_document_pengiriman1` FOREIGN KEY (`document_pengiriman`) REFERENCES `document_pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_kirim_product1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_kirim_status`
--
ALTER TABLE `product_kirim_status`
  ADD CONSTRAINT `fk_product_kirim_status_product_kirim1` FOREIGN KEY (`product_kirim`) REFERENCES `product_kirim` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_log_stock`
--
ALTER TABLE `product_log_stock`
  ADD CONSTRAINT `fk_product_log_stock_product_satuan1` FOREIGN KEY (`product_satuan`) REFERENCES `product_satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_satuan`
--
ALTER TABLE `product_satuan`
  ADD CONSTRAINT `fk_product_satuan_product1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_satuan_satuan1` FOREIGN KEY (`satuan`) REFERENCES `satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_stock`
--
ALTER TABLE `product_stock`
  ADD CONSTRAINT `fk_product_stock_product_satuan1` FOREIGN KEY (`product_satuan`) REFERENCES `product_satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_stock_minus`
--
ALTER TABLE `product_stock_minus`
  ADD CONSTRAINT `fk_product_stock_minus_product_stock1` FOREIGN KEY (`product_stock`) REFERENCES `product_stock` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_surat_jalan`
--
ALTER TABLE `product_surat_jalan`
  ADD CONSTRAINT `fk_product_surat_jalan_document_pengiriman1` FOREIGN KEY (`document_pengiriman`) REFERENCES `document_pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_vendor`
--
ALTER TABLE `product_vendor`
  ADD CONSTRAINT `fk_product_vendor_product1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_vendor_vendor1` FOREIGN KEY (`vendor`) REFERENCES `vendor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `reimburse`
--
ALTER TABLE `reimburse`
  ADD CONSTRAINT `fk_reimburse_pegawai1` FOREIGN KEY (`pegawai`) REFERENCES `pegawai` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `return_penjualan`
--
ALTER TABLE `return_penjualan`
  ADD CONSTRAINT `fk_return_penjualan_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `return_penjualan_item`
--
ALTER TABLE `return_penjualan_item`
  ADD CONSTRAINT `fk_return_penjualan_item_invoice_product1` FOREIGN KEY (`invoice_product`) REFERENCES `invoice_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_return_penjualan_item_return_penjualan1` FOREIGN KEY (`return_penjualan`) REFERENCES `return_penjualan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `retur_barang`
--
ALTER TABLE `retur_barang`
  ADD CONSTRAINT `fk_retur_barang_pembeli1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `retur_barang_item`
--
ALTER TABLE `retur_barang_item`
  ADD CONSTRAINT `fk_retur_barang_item_product_satuan1` FOREIGN KEY (`product_satuan`) REFERENCES `product_satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_retur_barang_item_retur_barang1` FOREIGN KEY (`retur_barang`) REFERENCES `retur_barang` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_retur_barang_item_stok_kategori1` FOREIGN KEY (`stok_kategori`) REFERENCES `stok_kategori` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `retur_barang_status`
--
ALTER TABLE `retur_barang_status`
  ADD CONSTRAINT `fk_retur_barang_status_retur_barang1` FOREIGN KEY (`retur_barang`) REFERENCES `retur_barang` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `retur_order`
--
ALTER TABLE `retur_order`
  ADD CONSTRAINT `fk_retur_order_order1` FOREIGN KEY (`order`) REFERENCES `order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `retur_order_item`
--
ALTER TABLE `retur_order_item`
  ADD CONSTRAINT `fk_retur_order_item_order_product1` FOREIGN KEY (`order_product`) REFERENCES `order_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_retur_order_item_retur_order1` FOREIGN KEY (`retur_order`) REFERENCES `retur_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sales_rute`
--
ALTER TABLE `sales_rute`
  ADD CONSTRAINT `fk_sales_rute_minggu1` FOREIGN KEY (`minggu`) REFERENCES `minggu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sales_rute_customer`
--
ALTER TABLE `sales_rute_customer`
  ADD CONSTRAINT `fk_sales_rute_customer_pembeli1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sales_rute_customer_sales_rute1` FOREIGN KEY (`sales_rute`) REFERENCES `sales_rute` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sales_rute_hari`
--
ALTER TABLE `sales_rute_hari`
  ADD CONSTRAINT `fk_sales_rute_hari_hari1` FOREIGN KEY (`hari`) REFERENCES `hari` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sales_rute_hari_sales_rute1` FOREIGN KEY (`sales_rute`) REFERENCES `sales_rute` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sales_rute_user`
--
ALTER TABLE `sales_rute_user`
  ADD CONSTRAINT `fk_sales_rute_user_sales_rute1` FOREIGN KEY (`sales_rute`) REFERENCES `sales_rute` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sales_rute_user_user1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `surat_jalan`
--
ALTER TABLE `surat_jalan`
  ADD CONSTRAINT `fk_surat_jalan_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_priveledge` FOREIGN KEY (`priveledge`) REFERENCES `priveledge` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `vendor`
--
ALTER TABLE `vendor`
  ADD CONSTRAINT `fk_vendor_vendor_category1` FOREIGN KEY (`vendor_category`) REFERENCES `vendor_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
