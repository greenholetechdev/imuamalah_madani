<?php

class Investor extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'investor';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
  '<script src="' . base_url() . 'assets/js/controllers/investor.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'investor';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Investor";
  $data['title_content'] = 'Data Investor';
  $content = $this->getDataInvestor();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataInvestor($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('i.nama', $keyword),
   array('i.no_hp', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
  'table' => $this->getTableName() . ' i',
  'field' => array('i.*'),
  'like' => $like,
  'is_or_like' => true,
  'where' => "(i.deleted = 0 or i.deleted is null)"
  ));

  return $total;
 }

 public function getDataInvestor($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('i.nama', $keyword),
   array('i.no_hp', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' i',
  'field' => array('i.*'),
  'like' => $like,
  'is_or_like' => true,
  'limit' => $this->limit,
  'offset' => $this->last_no,
  'where' => "(i.deleted = 0 or i.deleted is null) and i.status = 1"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
  'data' => $result,
  'total_rows' => $this->getTotalDataInvestor($keyword)
  );
 }

 public function getDetailDataInvestor($id) {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' kr',
  'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Investor";
  $data['title_content'] = 'Tambah Investor';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataInvestor($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Investor";
  $data['title_content'] = 'Ubah Investor';
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataInvestor($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Investor";
  $data['title_content'] = 'Detail Investor';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['nama'] = $value->nama;
  $data['no_hp'] = $value->no_hp;
  $data['email'] = $value->email;
  $data['alamat'] = $value->alamat;
  $data['keterangan'] = $value->keterangan;
  $data['status'] = $value->status;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
  $tipe_rumah = $id;
  $this->db->trans_begin();
  try {
   $post_tipe_rumah = $this->getPostDataHeader($data);
   if ($id == '') {
    $tipe_rumah = Modules::run('database/_insert', $this->getTableName(), $post_tipe_rumah);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_tipe_rumah, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'tipe_rumah' => $tipe_rumah));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Investor";
  $data['title_content'] = 'Data Investor';
  $content = $this->getDataInvestor($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
