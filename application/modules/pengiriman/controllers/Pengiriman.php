<?php

class Pengiriman extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 10;
	}

	public function getModuleName()
	{
		return 'pengiriman';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/js/printjs.js"></script>',
			'<link rel="stylesheet" href="' . base_url() . 'assets/admin_lte/bower_components/bootstrap-daterangepicker/daterangepicker.css">',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/min/moment.min.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/pengiriman.js"></script>'
		);

		return $data;
	}

	public function getTableName()
	{
		return 'pengiriman';
	}

	public function index()
	{
		$this->segment = 3;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;

		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Pengiriman";
		$data['title_content'] = 'Data Pengiriman';
		$content = $this->getDataPengiriman();
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		echo Modules::run('template', $data);
	}

	public function getTotalDataPengiriman($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('p.no_pengiriman', $keyword),
				array('p.tanggal_pengiriman', $keyword),
				array('p.sopir', $keyword),
				array('p.no_polisi', $keyword),
				array('p.filter_by', $keyword),
			);
		}
		$total = Modules::run('database/count_all', array(
			'table' => $this->getTableName() . ' p',
			'field' => array('p.*'),
			'like' => $like,
			'is_or_like' => true,
			'where' => "p.deleted is null or p.deleted = 0"
		));

		return $total;
	}

	public function getDataPengiriman($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('p.no_pengiriman', $keyword),
				array('p.tanggal_pengiriman', $keyword),
				array('p.sopir', $keyword),
				array('p.no_polisi', $keyword),
				array('p.filter_by', $keyword),
			);
		}
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' p',
			'field' => array('p.*'),
			'like' => $like,
			'is_or_like' => true,
			'limit' => $this->limit,
			'offset' => $this->last_no,
			'where' => "p.deleted is null or p.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		return array(
			'data' => $result,
			'total_rows' => $this->getTotalDataPengiriman($keyword)
		);
	}

	public function getDetailDataPengiriman($id)
	{
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' p',
			'field' => array('p.*'),
			//              'join' => array(
			//                  array('invoice i', 'sr.invoice = i.id'),
			//                  array('pembeli p', 'i.pembeli = p.id'),
			//              ),
			'where' => "p.id = '" . $id . "'"
		));

		return $data->row_array();
	}

	public function getListInvoice()
	{
		$data = Modules::run('database/get', array(
			'table' => 'invoice i',
			'field' => array('i.*'),
			'join' => array(
				array('(select max(id) id, invoice from invoice_status group by invoice) iss', 'iss.invoice = i.id'),
				array('invoice_status ist', 'ist.id = iss.id')
			),
			'where' => "i.deleted = 0 and ist.status = 'DRAFT'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function add()
	{
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Tambah Pengiriman";
		$data['title_content'] = 'Tambah Pengiriman';
		$data['list_invoice'] = $this->getListInvoice();
		echo Modules::run('template', $data);
	}

	public function ubah($id)
	{
		$data = $this->getDetailDataPengiriman($id);
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Ubah Pengiriman";
		$data['title_content'] = 'Ubah Pengiriman';
		$data['list_invoice'] = $this->getListInvoice();
		echo Modules::run('template', $data);
	}

	public function getFilterData($filter_by, $pengiriman)
	{
		$result = array();
		switch ($filter_by) {
			case "sales":
				$data = Modules::run('database/get', array(
					'table' => 'pengiriman_has_sales phs',
					'field' => array('phs.*', 'pg.nama'),
					'join' => array(
						array('user u', 'u.id = phs.user'),
						array('pegawai pg', 'pg.id = u.pegawai')
					),
					'where' => "phs.deleted = 0 and phs.pengiriman = '" . $pengiriman . "'"
				));
				break;
			case "pelanggan":
				$data = Modules::run('database/get', array(
					'table' => 'pengiriman_has_customer phc',
					'field' => array('phc.*', 'p.nama'),
					'join' => array(
						array('pembeli p', 'p.id = phc.pembeli')
					),
					'where' => "phc.deleted = 0 and phc.pengiriman = '" . $pengiriman . "'"
				));
				break;

			default:
				break;
		}

		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value['nama']);
			}
		}


		return $result;
	}
	
	public function getFilterDataId($filter_by, $pengiriman)
	{
		$result = array();
		switch ($filter_by) {
			case "sales":
				$data = Modules::run('database/get', array(
					'table' => 'pengiriman_has_sales phs',
					'field' => array('phs.*', 'pg.nama'),
					'join' => array(
						array('user u', 'u.id = phs.user'),
						array('pegawai pg', 'pg.id = u.pegawai')
					),
					'where' => "phs.deleted = 0 and phs.pengiriman = '" . $pengiriman . "'"
				));
				break;
			case "pelanggan":
				$data = Modules::run('database/get', array(
					'table' => 'pengiriman_has_customer phc',
					'field' => array('phc.*', 'p.nama'),
					'join' => array(
						array('pembeli p', 'p.id = phc.pembeli')
					),
					'where' => "phc.deleted = 0 and phc.pengiriman = '" . $pengiriman . "'"
				));
				break;

			default:
				break;
		}

		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$id = $filter_by == 'pelanggan' ? $value['pembeli'] : $value['user'];
				array_push($result, $id);
			}
		}


		return $result;
	}

	public function getFilterDataById($filter_by, $pengiriman, $id = "")
	{
		$result = array();
		switch ($filter_by) {
			case "sales":
				$data = Modules::run('database/get', array(
					'table' => 'pengiriman_has_sales phs',
					'field' => array('phs.*', 'pg.nama', 'u.id as id_input'),
					'join' => array(
						array('user u', 'u.id = phs.user'),
						array('pegawai pg', 'pg.id = u.pegawai')
					),
					'where' => "phs.deleted = 0 and phs.pengiriman = '" . $pengiriman . "'"
				));
				break;
			case "pelanggan":
				$data = Modules::run('database/get', array(
					'table' => 'pengiriman_has_customer phc',
					'field' => array('phc.*', 'p.nama', 'p.id as id_input'),
					'join' => array(
						array('pembeli p', 'p.id = phc.pembeli')
					),
					'where' => "phc.deleted = 0 and phc.pengiriman = '" . $pengiriman . "'"
				));
				break;

			default:
				break;
		}

		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				if ($id != '') {
					array_push($result, $value['id_input']);
				}
			}
		}


		return $result;
	}

	public function detail($id)
	{
		$data = $this->getDetailDataPengiriman($id);
		$data['filter_data'] = $this->getFilterData($data['filter_by'], $data['id']);

		$data['view_file'] = 'detail_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Detail Pengiriman";
		$data['title_content'] = 'Detail Pengiriman';


		$tanggal_order = $data['tanggal_awal_order'].' - '.$data['tanggal_akhir_order'];
		$filter_by = $data['filter_by'];
		$filter_data = $this->getFilterDataId($data['filter_by'], $data['id']);
		$tgl_order = $tanggal_order;

		$filter_data = implode(',', $filter_data);
		$where = "";
		switch ($filter_by) {
			case 'sales':
				$where = "and (i.createdby in (" . $filter_data . ") or o.createdby in (" . $filter_data . "))";
				break;
			case 'pelanggan':
				$where = "and i.pembeli in (" . $filter_data . ")";
				break;
			default:
				break;
		}

		if ($tgl_order != '') {
			list($tgl_awal, $tgl_akhir) = explode(' - ', $tgl_order);

			$tgl_awal = trim($tgl_awal);
			$tgl_akhir = trim($tgl_akhir);
			$where .= " and ((o.tanggal_faktur >= '" . $tgl_awal . "' and o.tanggal_faktur <= '" . $tgl_akhir . "') or "
				. "(i.tanggal_faktur >= '" . $tgl_awal . "' and i.tanggal_faktur <= '" . $tgl_akhir . "'))";
		}
		 
		$data_item = $this->getListInvoiceItem($where);
		// $data_item = $this->getListPengirimanDetail($data['id']);
		//  echo '<pre>';
		//  print_r($data_item);die;
		// $data['invoice_item'] = $data_item['data'];
		$data['invoice_item'] = $data_item['data'];
		$data['total'] = $data_item['total'];
		echo Modules::run('template', $data);
	}

	public function getPostDataHeader($value)
	{
		$data['no_pengiriman'] = Modules::run('no_generator/generateNoPengirimanJalan');
		$data['tanggal_pengiriman'] = date('Y-m-d', strtotime($value->tanggal));
		$data['total'] = str_replace('.', '', $value->total);
		$data['sopir'] = $value->sopir;
		$data['no_polisi'] = $value->no_polisi;
		$data['filter_by'] = $value->filter_by;
		//  echo $value->tanggal_order;die;
		list($awal, $akhir) = explode(' - ', $value->tanggal_order);
		if ($value->tanggal_order != '') {
			$data['tanggal_awal_order'] = $awal;
			$data['tanggal_akhir_order'] = $akhir;
		}
		return $data;
	}

	public function simpan()
	{
		$data = json_decode($this->input->post('data'));
		//  echo '<pre>';
		//  print_r($data);die;
		$is_valid = false;

		$this->db->trans_begin();
		try {
			$post_data = $this->getPostDataHeader($data);
			//   echo '<pre>';
			//   print_r($post_data);die;
			$id = Modules::run('database/_insert', $this->getTableName(), $post_data);

			switch ($data->filter_by) {
				case 'sales':
					if (!empty($data->filter_data)) {
						foreach ($data->filter_data as $value) {
							$post_sales['user'] = $value;
							$post_sales['pengiriman'] = $id;

							Modules::run('database/_insert', 'pengiriman_has_sales', $post_sales);
						}
					}
					break;
				case 'pelanggan':
					if (!empty($data->filter_data)) {
						foreach ($data->filter_data as $value) {
							$post_cust['pembeli'] = $value;
							$post_cust['pengiriman'] = $id;

							Modules::run('database/_insert', 'pengiriman_has_customer', $post_cust);
						}
					}
					break;
				default:
					break;
			}

			//pengiriman detail
			if ($data->data_item) {
				foreach ($data->data_item as $value) {
					$post_item['pengiriman'] = $id;
					$post_item['invoice_product'] = $value->invoice_product;
					$post_item['product_satuan'] = $value->product_satuan;
					$post_item['qty'] = $value->qty;
					Modules::run('database/_insert', 'pengiriman_detail', $post_item);
				}
			}

			//pengiriman status

			$post_status['status'] = 'DRAFT';
			$post_status['pengiriman'] = $id;
			Modules::run('database/_insert', 'pengiriman_status', $post_status);
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
	}

	public function search($keyword)
	{
		$this->segment = 4;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;
		$keyword = urldecode($keyword);

		$data['keyword'] = $keyword;
		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Pengiriman";
		$data['title_content'] = 'Data Pengiriman';
		$content = $this->getDataPengiriman($keyword);
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		echo Modules::run('template', $data);
	}

	public function delete($id)
	{
		$is_valid = false;
		$this->db->trans_begin();
		try {
			Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}

	public function getListInvoiceItem($filter_by)
	{
		$data = Modules::run('database/get', array(
			'table' => 'invoice_product ip',
			'field' => array(' distinct `ip`.*, `s`.`nama_satuan`, `p`.`kode_product`, `p`.`product` as `nama_product`, `ps`.`harga`, `p`.`id` as `product_id`, `o`.`tanggal_faktur` as `tgl_order`, `pg`.`nama` as `sales`'),
			'join' => array(
				array('product_satuan ps', 'ps.id = ip.product_satuan'),
				array('product p', 'p.id = ps.product'),
				array('satuan s', 's.id = ps.satuan'),
				array('invoice i', 'i.id = ip.invoice'),
				array('order o', 'i.ref = o.id', 'left'),
				array('user usr', 'usr.id = o.createdby', 'left'),
				array('pegawai pg', 'pg.id = usr.pegawai', 'left'),
				array('pengiriman_detail pd', 'ip.id = pd.invoice_product', 'left'),
			),
			'where' => "ip.deleted = 0 " . $filter_by . ' and i.deleted = 0',
			'orderby' => 'p.product, s.parent'
		));

		//  echo "<pre>";
		//  echo $this->db->last_query();
		//  die;
		// echo '<pre>';
		// print_r($data->result_array());die;
		$result = array();
		$total = 0;
		if (!empty($data)) {
			$temp = array();
			foreach ($data->result_array() as $value) {
				$sub_total = 0;
				$product = $value['product_id'];
				$satuan = array();
				$satuan_dan_qty = array();
				$qty = array();
				if (!in_array($product, $temp)) {
					$detail = array();
					foreach ($data->result_array() as $v_child) {
						if ($product == $v_child['product_id']) {
							$sub_total += $v_child['sub_total'];
							array_push($satuan, $v_child['nama_satuan']);
							array_push($satuan_dan_qty, $v_child['nama_satuan'] . ";" . $v_child['qty'] . ';' . $v_child['harga']);
							array_push($qty, $v_child['qty']);
							array_push($detail, $v_child);
						}
					}

					$data_satuan = $this->getDataAllSatuanProduk($product);
					//  echo '<pre>';
					//  print_r($satuan_dan_qty);die;
					$satuan_fix = array();
					$temp_satuan = array();
					foreach ($satuan_dan_qty as $v_satu) {
						list($sat, $quantity, $harga) = explode(';', $v_satu);
						if (!in_array($sat, $temp_satuan)) {
							$qty_total = 0;
							$sub_total_child = 0;
							foreach ($satuan_dan_qty as $v_sat_child) {
								list($sat_child, $quantity_child, $harga_child) = explode(';', $v_sat_child);
								if ($sat_child == $sat) {
									$qty_total += $quantity_child;
									$sub_total_child += ($quantity_child * $harga_child);
								}
							}

							array_push($satuan_fix, array(
								'satuan' => $sat,
								'total_qty' => $qty_total,
								'harga' => $harga,
								'sub_total' => $sub_total_child
							));
							$temp_satuan[] = $sat;
						}
					}

					//  echo '<pre>';
					//  print_r($satuan_fix);die;
					$qty_data = array();
					$harga_data = array();
					$satuan = array();
					$sub_total_fix = 0;
					foreach ($satuan_fix as  $v_sat_fix) {
						$sub_total_fix += $v_sat_fix['harga'] * $v_sat_fix['total_qty'];
						array_push($qty_data, $v_sat_fix['total_qty']);
						array_push($satuan, $v_sat_fix['satuan']);
						array_push($harga_data, 'Rp. ' . number_format($v_sat_fix['harga'], 0, ',', '.'));
					}

					$qty_str = array();
					for($i = 0; $i < count($data_satuan['satuan']); $i++){
						$satuan_acuan = $data_satuan['satuan'][$i];
						$is_exist = 0;
						foreach ($satuan_fix as $v_sat_fix) {
							if($satuan_acuan == $v_sat_fix['satuan']){
								$is_exist = 1;								
								break;
							}
						}

						if($is_exist){
							array_push($qty_str, $v_sat_fix['total_qty']);
						}else{
							array_push($qty_str, 0);
						}
					}

					$data_harga = array();
					
					if(!empty($qty_str)){
						for($i = 0; $i < count($qty_str); $i++){
							$harga_sub_total = $data_satuan['harga_data'][$i]*$qty_str[$i];
							array_push($data_harga, number_format($harga_sub_total));
						}
					}

					// echo '<pre>';
					// print_r($v_child);die;
// 					if(trim($value['kode_product']) == '2814437'){
// echo '<pre>';
// 					print_r($data_harga);die;
// 					}
										
					$satuan = array_unique($satuan);
					$value['sub_total'] = $sub_total;
					$value['satuan_str'] = implode(' / ', $satuan);
					$value['harga_str'] = implode('/', $harga_data);
					$value['qty_str'] = implode('/', $qty_data);
					//  $value['satuan_str'] = $value['nama_satuan'];

					//  $value['harga_str'] = 'Rp. ' . number_format($value['harga'], 0, ',', '.');
					//  $value['qty_str'] = $total_qty;
					//  $value['sub_total_str'] = $total_qty * $value['harga'];

					$value['sub_total_str'] = $sub_total_fix;
					$value['satuan_all'] = implode('/', $data_satuan['satuan_all']);
					$value['harga_all'] = implode('/', $data_harga);
					$value['qty_all'] = implode('/', $qty_str);
					 // echo '<pre>';
					 // print_r($data_satuan);die;		 
					$total += $sub_total_fix;
					$value['total'] = $total;
					array_push($result, $value);
					$temp[] = $product;
				}
			}
		}

		 // echo '<pre>';
		 // print_r($result);
		 // die;

		return array(
			'data' => $result,
			'total' => $total
		);
	}

	public function getListPengirimanDetail($pengiriman)
	{
		$data = Modules::run('database/get', array(
			'table' => 'invoice_product ip',
			'field' => array(
				'ip.*', 's.nama_satuan',
				'p.product as nama_product', 'ps.harga',
				'p.id as product_id',
				'p.kode_product',
				'ord.tanggal_faktur as tgl_order',
				'pg.nama as sales', 'pd.qty as qty_fix'
			),
			'join' => array(
				array('product_satuan ps', 'ps.id = ip.product_satuan'),
				array('product p', 'p.id = ps.product'),
				array('satuan s', 's.id = ps.satuan'),
				array('invoice i', 'i.id = ip.invoice'),
				array('order ord', 'ord.id = i.ref', 'left'),
				array('user usr', 'usr.id = ord.createdby', 'left'),
				array('pegawai pg', 'pg.id = usr.pegawai', 'left'),
				array('pengiriman_detail pd', 'ip.id = pd.invoice_product'),
			),
			'where' => "pd.pengiriman = " . $pengiriman,
			'orderby' => 'p.product'
		));

		//  echo "<pre>";
		//  echo $this->db->last_query();
		//  die;
		$result = array();
		$total = 0;
		if (!empty($data)) {
			$temp = array();
			foreach ($data->result_array() as $value) {
				$sub_total = 0;
				$product = $value['product_id'];
				$satuan = array();
				$harga = array();
				$qty = array();
				if (!in_array($product, $temp)) {
					$detail = array();
					$total_qty = 0;
					foreach ($data->result_array() as $v_child) {
						if ($product == $v_child['product_id']) {
							$sub_total += $v_child['sub_total'];
							array_push($satuan, $v_child['nama_satuan']);
							array_push($qty, $v_child['qty']);
							array_push($harga, 'Rp, ' . number_format($v_child['harga']));
							array_push($detail, $v_child);
							$total_qty += $v_child['qty'];
						}
					}
					$data_satuan = $this->getDataAllSatuanProduk($product);
					//     echo '<pre>';
					//     print_r($qty);die;
					$value['detail'] = $detail;
					$value['sub_total'] = $sub_total;
					$value['satuan_str'] = implode(' / ', $satuan);
					$value['harga_str'] = implode('<br/>', $harga);
					//  $value['qty_str'] = implode('<br/>', $qty);
					$value['qty_str'] = $value['qty_fix'] == '' ? implode('<br/>', $qty) : $value['qty_fix'];
					$value['satuan_all'] = implode('/', $data_satuan['satuan_all']);


					$qty_str = array();
					foreach ($data_satuan['satuan'] as $v_s) {
						if ($value['nama_satuan'] == $v_s) {
							//       echo 'asdasd';die;
							//       echo $total_qty;die;
							$total_qty = $value['qty_fix'] == '' ? $total_qty : $value['qty_fix'];
							array_push($qty_str, $total_qty);
						} else {
							array_push($qty_str, 0);
						}
					}
					// echo '<pre>';
					// print_r($qty_str);die;
					$value['qty_all'] = implode('/', $qty_str);
					$value['sub_total_str'] = $total_qty * $value['harga'];
					$total += $value['sub_total_str'];
					$value['total'] = $total;
					array_push($result, $value);
					$temp[] = $product;
				}
			}
		}

		//  echo '<pre>';
		//  print_r($result);
		//  die;

		// echo $total;die;
		return array(
			'data' => $result,
			'total' => $total
		);
	}

	public function getDataAllSatuanProduk($product)
	{
		$sql = "select	
	p.product as nama_product
	, s.nama_satuan as satuan
	, p.id as product_id
 , ps.qty
	, s.parent
	, ps.harga
 	from product_satuan ps
	join satuan s
		on s.id = ps.satuan
	join product p
		on p.id = ps.product
	where ps.deleted = 0 and p.id = '" . $product . "' order by s.parent asc";

		$data = Modules::run('database/get_custom', $sql);
		$result = array();
		$satuan_data = array();
		$harga_data = array();
		if (!empty($data)) {
			for ($i = 0; $i < count($data->result_array()); $i++) {
				$value = $data->result_array();
				$qty = $i == 0 ? $value[count($value) - 1]['qty'] : $value[$i - 1]['qty'];
				array_push($satuan_data, $value[$i]['satuan']);
				array_push($harga_data, $value[$i]['harga']);
				array_push($result, $value[$i]['satuan'] . $qty);
			}
			//  foreach ($data->result_array() as $value) {

			//  }
		}


		return array(
			'satuan_all' => $result,
			'satuan' => $satuan_data,
			'harga_data'=> $harga_data
		);
	}

	public function printFakturOld($id)
	{
		$data['sj'] = $this->getDetailDataPengiriman($id);

		$data['invoice_item'] = $this->getListPengirimanDetail($data['sj']['id']);

		$data['self'] = Modules::run('general/getDetailDataGeneral', 1);
		//  echo '<pre>';
		//  print_r($data);die;
		$mpdf = Modules::run('mpdf/getInitPdfPaperA5');

		//  $pdf = new mPDF('A4');
		$view = $this->load->view('cetak', $data, true);
		$mpdf->WriteHTML($view);

		$mpdf->Output('Nota Pengiriman - ' . date('Y-m-d') . '.pdf', 'I');
	}

	public function printFakturTcpdf($id)
	{
		$data['sj'] = $this->getDetailDataPengiriman($id);

		$data['invoice_item'] = $this->getListPengirimanDetail($data['sj']['id']);

		$data['self'] = Modules::run('general/getDetailDataGeneral', 1);
		//  echo '<pre>';
		//  print_r($data);die;
		$pdf = Modules::run('mpdf/initTCPDF');
		$pdf->SetTitle("Faktur Permintaan Barang");
		$pdf->SetTopMargin(20);
		$pdf->setFooterMargin(20);
		$pdf->SetAutoPageBreak(true);
		$pdf->SetAuthor('Author');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->AddPage();
		$data['logo'] = base_url() . '/files/berkas/general/' . $data['self']['logo'];
		//  echo $img;die;
		$view = $this->load->view('cetak_tcpdf', $data, true);
		//  echo $view;die;
		$pdf->writeHTML($view, true, false, true, false, '');
		//  $pdf->Output('contoh1.pdf', 'I');
		//
		////  $pdf = new mPDF('A4');  
		//  $mpdf->WriteHTML($view);
		//  ob_start();
		$pdf->Output('Nota Pengiriman - ' . date('Y-m-d') . '.pdf', 'I');
		//  ob_end_flush();
	}

	public function printFaktur($id)
	{
		$data['sj'] = $this->getDetailDataPengiriman($id);
		
		$tanggal_order = $data['sj']['tanggal_awal_order'].' - '.$data['sj']['tanggal_akhir_order'];		
		$filter_by = $data['sj']['filter_by'];
		$filter_data = $this->getFilterDataId($data['sj']['filter_by'], $data['sj']['id']);		
		$tgl_order = $tanggal_order;

		$filter_data = implode(',', $filter_data);
		$where = "";
		switch ($filter_by) {
			case 'sales':
				$where = "and (i.createdby in (" . $filter_data . ") or o.createdby in (" . $filter_data . "))";
				break;
			case 'pelanggan':
				$where = "and i.pembeli in (" . $filter_data . ")";
				break;
			default:
				break;
		}

		if ($tgl_order != '') {
			list($tgl_awal, $tgl_akhir) = explode(' - ', $tgl_order);

			$tgl_awal = trim($tgl_awal);
			$tgl_akhir = trim($tgl_akhir);
			$where .= " and ((o.tanggal_faktur >= '" . $tgl_awal . "' and o.tanggal_faktur <= '" . $tgl_akhir . "') or "
				. "(i.tanggal_faktur >= '" . $tgl_awal . "' and i.tanggal_faktur <= '" . $tgl_akhir . "'))";
		}
		 
		$data_item = $this->getListInvoiceItem($where);
		// echo '<pre>';
		// print_r($data_item);die;
		// $data['invoice_item'] = $this->getListPengirimanDetail($data['sj']['id']);
		$data['invoice_item'] = $data_item;

		$data['self'] = Modules::run('general/getDetailDataGeneral', 1);
		//  echo '<pre>';
		//  print_r($data);die;
		// $data['view_file'] = 'cetak_html';
		// $data['header_data'] = $this->getHeaderJSandCSS();
		// $data['module'] = $this->getModuleName();
		// $data['title'] = "Cetak Pengiriman";
		// $data['title_content'] = 'Cetak Pengiriman';
		// echo Modules::run('template', $data);
		echo $this->load->view('cetak_html', $data, true);
	}

	public function getFakturDetail()
	{
		$filter_by = $_POST['filter_by'];
		//  echo '<pre>';
		//  print_r($filter_by);die;

		$filter = $filter_by == 'sales' ? 'ip.createdby = 1' : '';
		$data_item = $this->getListInvoiceItem($filter_by);
		//  echo '<pre>';
		//  print_r($data_item);die;
		$content['invoice_item'] = $data_item['data'];
		$view_item = $this->load->view('form_product', $content, true);
		echo json_encode(array('view_item' => $view_item, 'total' => $data_item['total']));
	}

	public function getFilterBy()
	{
		$filter_by = $_POST['filter_by'];
		$data = $filter_by == 'sales' ? $this->getDataSales() : $this->getDataCustomer();
		$content['list_filter'] = $data;

		$content['filter'] = $filter_by;
		echo $this->load->view('list_filter', $content, true);
	}

	public function getDataSales()
	{
		$data = Modules::run('database/get', array(
			'table' => 'pegawai p',
			'field' => array('u.id', 'p.nama', 'pv.hak_akses'),
			'join' => array(
				array('user u', 'u.pegawai = p.id and u.deleted = 0'),
				array('priveledge pv', 'u.priveledge = pv.id'),
			),
			'where' => "p.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getDataCustomer()
	{
		$data = Modules::run('database/get', array(
			'table' => 'pembeli p',
			'field' => array('p.*'),
			'where' => "p.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getDataListProduct()
	{
		$filter_by = $_POST['filter_by'];
		$filter_data = $_POST['filter_data'];
		$tgl_order = $_POST['tanggal_order'];
		$where = "";
		switch ($filter_by) {
			case 'sales':
				$where = "and (i.createdby in (" . $filter_data . ") or o.createdby in (" . $filter_data . "))";
				break;
			case 'pelanggan':
				$where = "and i.pembeli in (" . $filter_data . ")";
				break;
			default:
				break;
		}

		if ($tgl_order != '') {
			list($tgl_awal, $tgl_akhir) = explode(' - ', $tgl_order);

			$tgl_awal = trim($tgl_awal);
			$tgl_akhir = trim($tgl_akhir);
			$where .= "and ((o.tanggal_faktur >= '" . $tgl_awal . "' and o.tanggal_faktur <= '" . $tgl_akhir . "') or "
				. "(i.tanggal_faktur >= '" . $tgl_awal . "' and i.tanggal_faktur <= '" . $tgl_akhir . "'))";
		}

		//  echo $where;die;
		$data = $this->getListInvoiceItem($where);
		$content['invoice_item'] = $data['data'];
		$view_item = $this->load->view('form_product', $content, true);
		echo json_encode(array('view_item' => $view_item, 'total' => number_format($data['total'], 0, ',', '.')));
		//  echo '<pre>';
		//  print_r($data);die;
	}
}
