<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block"> 
    <div class="row">
     <div class='col-md-3 text-bold'>
      No Pengiriman
     </div>
     <div class='col-md-3'>
      <?php echo $no_pengiriman ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Berdasarkan
     </div>
     <div class='col-md-3'>
      <?php echo strtoupper($filter_by) ?>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      
     </div>
     <div class='col-md-3'>
      <?php echo implode(', ', $filter_data) ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal Order
     </div>
     <div class='col-md-3'>
      <?php echo $tanggal_awal_order.' - '.$tanggal_akhir_order ?>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal Kirim
     </div>
     <div class='col-md-3'>
      <?php echo $tanggal_pengiriman ?>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Sopir
     </div>
     <div class='col-md-3'>
      <?php echo $sopir ?>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      No Polisi
     </div>
     <div class='col-md-3'>
      <?php echo $no_polisi ?>
     </div>     
    </div>
    <br/>
    <br/>

    <div class="row">
     <div class="col-md-12">
      <u>Data Produk</u>
     </div>
    </div>
    <hr/>

    <div class="row">
     <div class="col-md-12">
      <div class="form-item">
       <?php echo $this->load->view('form_product'); ?>
      </div>      
     </div>
    </div>
    <br/>


    <div class="row">
     <div class="col-md-12 text-right">
      <h4>Total : Rp, <label id="total"><?php echo isset($total) ? number_format($total) : '0' ?></label></h4>
     </div>
    </div>
    <hr/>    
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger" onclick="Pengiriman.cetak('<?php echo isset($id) ? $id : '' ?>')">Cetak</button>
      &nbsp;
      <button id="" class="btn btn-danger-baru" onclick="Pengiriman.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
