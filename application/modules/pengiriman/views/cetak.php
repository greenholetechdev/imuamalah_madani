<!DOCTYPE html>
<html lang="en" dir="ltr">
 <head>
  <meta charset="utf-8">
  <title><?php echo 'Surat Jalan'; ?></title>

  <style>
   body {
    font-family: "arial";
    font-size: 11px;
   }
   table {
    width: 100%;
   }
   .text-center {
    text-align: center;
   }
   .text-right {
    text-align: right;
   }
   .text-left {
    text-align: left;
   }
   .font-bold {
    font-weight: bold;
   }
   .mb-32px {
    margin-bottom: 32px;
   }
   .mr-8px {
    margin-right: 8px;
   }
   .ml-8px {
    margin-left: 8px;
   }
   .table-logo {
    width: 100%;
   }
   table.table-logo > tbody > tr > td {
    padding: 16px;
   }
   table.table-logo td {
    vertical-align: top;
   }
   .table-item {
    width: 100%;
    margin-top: 16px;
    border-collapse: collapse;
    font-size: 10px;
   }
   table.table-item th,  table.table-item td {
    border: 1px solid #333;
    padding: 4px 8px;
    border-collapse: collapse;
    vertical-align: top;
   }
   .media {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: start;
    align-items: flex-start;
   }
   .media-body {
    -ms-flex: 1;
    flex: 1;
   }


  </style>
 </head>
 <body>
  <table class="table-logo">
   <tbody>
    <tr>
     <td style="width: 60%">
      <table style="margin-bottom: 8px">
       <tr>
        <td width="40"><img src="<?php echo base_url() ?>files/berkas/general/<?php echo $self['logo'] ?>" alt="" width="40"></td> 
        <td>
         <div class="font-bold">Permintaan Barang</div>
         <div class="font-bold"><?php echo $self['title'] ?></div>
         <div><?php echo $self['alamat'] ?></div>
        </td>
       </tr>
      </table>
     </td>
     <td style="width: 40%">
      <table>
       <tr>
        <td class="text-right">
         <div class="font-bold">ID Dokumen</div>
        </td>
        <td class="text-right" width="10">:</td>
        <td>
         <div><?php echo $sj['no_pengiriman'] ?></div>
        </td>
       </tr>
       <tr>
        <td class="text-right">
         <div class="font-bold">Tgl. Pengiriman</div>
        </td>
        <td class="text-right" width="10">:</td>
        <td>
         <div><?php echo date("d F Y", strtotime($sj['tanggal_pengiriman'])) ?></div>
        </td>
       </tr>
       <tr>
        <td class="text-right">
         <div class="font-bold">Tanggal Cetak</div>
        </td>
        <td class="text-right" width="10">:</td>
        <td>
         <div><?php echo date('d F Y') ?></div>
        </td>
       </tr>
       <tr>
        <td class="text-right">
         <div class="font-bold">Sopir</div>
        </td>
        <td class="text-right" width="10">:</td>
        <td>
         <div><?php echo $sj['sopir'] ?></div>
        </td>
       </tr>
       <tr>
        <td class="text-right">
         <div class="font-bold">Nopol</div>
        </td>
        <td class="text-right" width="10">:</td>
        <td>
         <div><?php echo $sj['no_polisi'] ?></div>
        </td>
       </tr>
      </table>
     </td>
    </tr>
   </tbody>
  </table>
  <table class="table-item">
   <thead>
    <tr>
     <th style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: 1px solid #ccc;">No</th>
     <th style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: 1px solid #ccc;">Kode Produk</th>
     <th style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: 1px solid #ccc;">Produk</th>
     <th style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: 1px solid #ccc;">Jumlah</th>
     <th style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: 1px solid #ccc;">Satuan</th>
     <!--<th>Harga</th>-->
     <!--<th>Sub Total</th>-->
    </tr>
   </thead>
   <tbody>
    <?php if (!empty($invoice_item['data'])) { ?>
     <?php $no = 1; ?>
     <?php foreach ($invoice_item['data'] as $value) { ?>
      <tr>
       <td style="border-bottom: none;border-left: none;border-right: none;border-top: none;"><?php echo $no++ ?></td>
       <td style="border-bottom: none;border-left: none;border-right: none;border-top: none;"><?php echo $value['kode_product'] ?></td>
       <td style="border-bottom: none;border-left: none;border-right: none;border-top: none;"><?php echo $value['nama_product'] ?></td>
       <td style="border-bottom: none;border-left: none;border-right: none;border-top: none;" class="text-center"><?php echo $value['qty_all'] ?></td>
       <td style="border-bottom: none;border-left: none;border-right: none;border-top: none;" class="text-center"><?php echo $value['satuan_all'] ?></td>
      </tr>
     <?php } ?>
    <?php } ?>
<!--    <tr>
 <td style="border: 0" class="text-right" colspan="4">Total</td>
 <td class="text-right font-bold"><?php echo 'Rp. ' . number_format($sj['total']) ?></td>
</tr>-->
   </tbody>
  </table>
  <table style="width: 100%; margin-top: 20px">
   <tbody>
    <tr>
     <td class="text-center">Dibuat,</td>
     <td class="text-center">Diperiksa,</td>
     <td class="text-center">Gudang,</td>
     <td class="text-center">Sopir,</td>
     <td class="text-center">Diterima oleh,</td>
     <td class="text-center">&nbsp;</td>
    </tr>
    <tr>
     <td style="height: 30px"></td>
    </tr>
    <tr>
     <td class="text-center">(------------------------------)</td>
     <td class="text-center">(------------------------------)</td>
     <td class="text-center">(------------------------------)</td>
     <td class="text-center">(------------------------------)</td>
     <td class="text-center">(------------------------------)</td>
    </tr>
   </tbody>
  </table>  
 </body>
</html>
