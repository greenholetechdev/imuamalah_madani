<aside id="left-panel" class="left-panel">
 <nav class="navbar navbar-expand-sm navbar-default">

  <div class="navbar-header">
   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
    <i class="fa fa-bars"></i>
   </button>
   <a class="navbar-brand" href="javascript:;">IMuamalah</a>
   <a class="navbar-brand hidden">&nbsp;</a>
   <a id="openToggle" class="menutoggle pull-left"><i style="margin-top: 12px;" class="fa fa-tasks"></i></a>
  </div>

  <br/>
  <div id="main-menu" class="main-menu collapse navbar-collapse">
   <ul class="nav navbar-nav">
    <li>
     <a href="<?php echo base_url() . 'dashboard/dashboard' ?>"> <i class="menu-icon mdi mdi-file-chart mdi-18px"></i>Dashboard </a>
    </li>        
    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon-fa icon-left fa fa-folder-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Penjualan
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="penjualan">
       <a href="<?php echo base_url() . 'pelanggan' ?>"> Pelanggan </a>
      </li>
      <li style="padding-left: 16px;" class="penjualan">
       <a href="<?php echo base_url() . 'produk' ?>"> Produk </a>
      </li>
      <li style="padding-left: 16px;" class="penjualan">
       <a href="<?php echo base_url() . 'surat_jalan' ?>"> Surat Jalan </a>
      </li>
      <li style="padding-left: 16px;" class="penjualan">
       <a href="<?php echo base_url() . 'retur' ?>"> Retur </a>
      </li>
      <li style="padding-left: 16px;" class="penjualan">
       <a href="<?php echo base_url() . 'rute_salesman' ?>"> Rute Salesman </a>
      </li>
      <li style="padding-left: 16px;" class="penjualan">
       <a href="<?php echo base_url() . 'lacak_salesman' ?>"> Lacak Salesman </a>
      </li>
     </ul>    
    </li>   
    
    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon-fa icon-left fa fa-folder-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Penagihan
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="penagihan">
       <a href="<?php echo base_url() . 'faktur' ?>"> Faktur bersyarat</a>
      </li>
      <li style="padding-left: 16px;" class="penagihan">
       <a href="<?php echo base_url() . 'faktur_pelanggan' ?>"> Faktur </a>
      </li>
      <li style="padding-left: 16px;" class="penagihan">
       <a href="<?php echo base_url() . 'persyaratan' ?>"> Syarat Pembayaran </a>
      </li>
      <li style="padding-left: 16px;" class="penagihan">
       <a href="<?php echo base_url() . 'metode_bayar' ?>"> Metode Pembayaran </a>
      </li>      
     </ul>    
    </li>   
    
    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon-fa icon-left fa fa-folder-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pembelian
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="pembelian">
       <a href="<?php echo base_url() . 'vendor' ?>"> Vendor </a>
      </li>
      <li style="padding-left: 16px;" class="pembelian">
       <a href="<?php echo base_url() . 'pengadaan' ?>"> Pengadaan </a>
      </li>
      <li style="padding-left: 16px;" class="pembelian">
       <a href="<?php echo base_url() . 'retur_pengadaan' ?>"> Retur </a>
      </li>
     </ul>    
    </li>   
    
    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon-fa icon-left fa fa-folder-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pembayaran
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="pembayaran">
       <a href="<?php echo base_url() . 'tagihan' ?>"> Tagihan </a>
      </li>
      <li style="padding-left: 16px;" class="pembayaran">
       <a href="<?php echo base_url() . 'pembayaran' ?>"> Biaya Umum </a>
      </li>
     </ul>    
    </li>   
    
    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon-fa icon-left fa fa-folder-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Inventori
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="inventori">
       <a href="<?php echo base_url() . 'gudang' ?>"> Gudang </a>
      </li>
      <li style="padding-left: 16px;" class="inventori">
       <a href="<?php echo base_url() . 'rak' ?>"> Rak </a>
      </li>
      <li style="padding-left: 16px;" class="inventori">
       <a href="<?php echo base_url() . 'product_stock' ?>"> Stok </a>
      </li>
     </ul>    
    </li>   
   
    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon-fa icon-left fa fa-folder-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Akuntansi
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="akuntansi">
       <a href="<?php echo base_url() . 'bank_akun' ?>"> Bagan Akun </a>
      </li>
      <li style="padding-left: 16px;" class="akuntansi">
       <a href="<?php echo base_url() . 'kas' ?>"> Kas & Bank</a>
      </li>
      <li style="padding-left: 16px;" class="akuntansi">
       <a href="<?php echo base_url() . 'mata_uang' ?>"> Mata Uang </a>
      </li>
     </ul>    
    </li>
    
    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon-fa icon-left fa fa-folder-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Penggajian
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="penggajian">
       <a href="<?php echo base_url() . 'payroll' ?>"> Gaji </a>
      </li>
      <li style="padding-left: 16px;" class="penggajian">
       <a href="<?php echo base_url() . 'payroll_category' ?>"> Kategori </a>
      </li>
      <li style="padding-left: 16px;" class="penggajian">
       <a href="<?php echo base_url() . 'periode' ?>"> Periode </a>
      </li>
      <li style="padding-left: 16px;" class="penggajian">
       <a href="<?php echo base_url() . 'kasbon' ?>"> Kasbon</a>
      </li>
     </ul>    
    </li>
    
    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon-fa icon-left fa fa-folder-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Personalia
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="personalia">
       <a href="<?php echo base_url() . 'pegawai' ?>"> Karyawan </a>
      </li>
      <li style="padding-left: 16px;" class="personalia">
       <a href="<?php echo base_url() . 'kontrak' ?>"> Kontrak</a>
      </li>
     </ul>    
    </li>
    
    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon-fa icon-left fa fa-folder-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Perpajakan
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="perpajakan">
       <a href="<?php echo base_url() . 'efaktur' ?>"> E-faktur </a>
      </li>
     </ul>    
    </li>
    
    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon-fa icon-left fa fa-folder-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Zakat
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="zakat">
       <a href="<?php echo base_url() . 'zakat_usaha' ?>"> Zakat Usaha </a>
      </li>
     </ul>    
    </li>
    
    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon-fa icon-left fa fa-folder-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Laporan
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="laporan">
       <a href="<?php echo base_url() . 'laba_rugi' ?>"> Laba Rugi </a>
      </li>
      <li style="padding-left: 16px;" class="laporan">
       <a href="<?php echo base_url() . 'bagi_hasil' ?>"> Bagi Hasil </a>
      </li>
      <li style="padding-left: 16px;" class="laporan">
       <a href="<?php echo base_url() . 'arus_kas' ?>"> Arus Kas </a>
      </li>
      <li style="padding-left: 16px;" class="laporan">
       <a href="<?php echo base_url() . 'ringkasan_pemasukan' ?>"> Ringkasan Pemasukan </a>
      </li>
      <li style="padding-left: 16px;" class="laporan">
       <a href="<?php echo base_url() . 'ringkasan_pengeluaran' ?>"> Ringkasan Pengeluaran </a>
      </li>
      <li style="padding-left: 16px;" class="laporan">
       <a href="<?php echo base_url() . 'pemasukan_pengeluaran' ?>"> Pemasukan vs Pengeluaran </a>
      </li>
     </ul>    
    </li>

    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon-fa icon-left fa fa-folder-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Musyarokah
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="musyarokah">
       <a href="<?php echo base_url() . 'kerja_sama_internal' ?>"> Internal </a>
      </li>
      <li style="padding-left: 16px;" class="musyarokah">
       <a href="<?php echo base_url() . 'kerja_sama_eksternal' ?>"> Eksternal </a>
      </li>
     </ul>    
    </li>
    
    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon-fa icon-left fa fa-folder-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Akad
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="musyarokah">
       <a href="<?php echo base_url() . 'kategori_akad' ?>"> kategori </a>
      </li>
      <li style="padding-left: 16px;" class="musyarokah">
       <a href="<?php echo base_url() . 'jenis_akad' ?>"> Jenis </a>
      </li>
     </ul>    
    </li>
    
    <li class="menu-item-has-children dropdown">
     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="menu-icon-fa icon-left fa fa-folder-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pengaturan
     </a>
     <ul class="sub-menu children dropdown-menu">
      <li style="padding-left: 16px;" class="musyarokah">
       <a href="<?php echo base_url() . 'general' ?>"> Umum </a>
      </li>
      <li style="padding-left: 16px;" class="musyarokah">
       <a href="<?php echo base_url() . 'pengguna' ?>"> Pengguna </a>
      </li>
      <li style="padding-left: 16px;" class="musyarokah">
       <a href="<?php echo base_url() . 'tanggal' ?>"> Tanggal </a>
      </li>
     </ul>    
    </li>
    
    <li>
     <a href="<?php echo base_url() . 'tentang' ?>"> <i class="menu-icon mdi mdi-bookmark-outline mdi-18px"></i>Tentang </a>
    </li>    
    <li>
     <a href="<?php echo base_url() . 'bantuan' ?>" onclick="Template.showUpdateSystem(this, event)"> <i class="menu-icon mdi mdi-help-circle mdi-18px"></i>Bantuan </a>
    </li>    
    <li>
     <a href="<?php echo base_url() . 'apps' ?>"> <i class="menu-icon mdi mdi-apps mdi-18px"></i>Apps </a>
    </li>    
   </ul>
  </div><!-- /.navbar-collapse -->
 </nav>
</aside><!-- /#left-panel -->
