<?php if ($this->session->userdata('hak_akses') == "manajemen hrd") { ?>
	<ul class="sidebar-menu" data-widget="tree">
		<li class="header">MAIN NAVIGATION</li>
		<li class="active"><a href="<?php echo base_url() . 'dashboard' ?>"><i class="fa fa-folder-o"></i> <span>Dashboard</span></a></li>		

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Penggajian</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'payroll' ?>"><i class="fa fa-file-text-o"></i> Gaji </a></li>
				<li><a href="<?php echo base_url() . 'payroll_category' ?>"><i class="fa fa-file-text-o"></i> Kategori </a></li>
				<li><a href="<?php echo base_url() . 'periode' ?>"><i class="fa fa-file-text-o"></i> Periode </a></li>
				<li><a href="<?php echo base_url() . 'reimburse' ?>"><i class="fa fa-file-text-o"></i> Reimburse </a></li>
				<li><a href="<?php echo base_url() . 'kasbon' ?>"><i class="fa fa-file-text-o"></i> Kasbon </a></li>
				<li><a href="<?php echo base_url() . 'kasbon_payment' ?>"><i class="fa fa-file-text-o"></i> Kasbon Bayar</a></li>
			</ul>
		</li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Personalia</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'pegawai' ?>"><i class="fa fa-file-text-o"></i> Karyawan </a></li>
				<li><a href="<?php echo base_url() . 'pegawai_kontrak' ?>"><i class="fa fa-file-text-o"></i> Kontrak </a></li>
			</ul>
		</li>

		<li><a href="<?php echo base_url() . 'login/sign_out' ?>"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
	</ul>

<?php } ?>
