<?php if ($this->session->userdata('hak_akses') == "superadmin") { ?>
 <ul class="sidebar-menu" data-widget="tree">
  <li class="header">MAIN NAVIGATION</li>
  <li class="active"><a href="<?php echo base_url() . 'dashboard' ?>"><i class="fa fa-folder-o"></i> <span>Dashboard</span></a></li>
  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Penjualan</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <li><a href="<?php echo base_url() . 'pelanggan' ?>"><i class="fa fa-file-text-o"></i> Pelanggan </a></li>
    <li><a href="<?php echo base_url() . 'produk' ?>"><i class="fa fa-file-text-o"></i> Produk </a></li>
    <li><a href="<?php echo base_url() . 'kategori_produk' ?>"><i class="fa fa-file-text-o"></i> Kategori Produk </a></li>
    <li><a href="<?php echo base_url() . 'tipe_produk' ?>"><i class="fa fa-file-text-o"></i> Tipe Produk </a></li>
    <li><a href="<?php echo base_url() . 'order' ?>"><i class="fa fa-file-text-o"></i> Order </a></li>
    <!--<li><a href="<?php echo base_url() . 'returorder' ?>"><i class="fa fa-file-text-o"></i> Retur Order </a></li>-->
    <!--<li><a href="<?php echo base_url() . 'surat_jalan' ?>"><i class="fa fa-file-text-o"></i> Surat Jalan </a></li>-->
    <!--<li><a href="<?php echo base_url() . 'retur' ?>"><i class="fa fa-file-text-o"></i> Retur </a></li>-->
    <li><a href="<?php echo base_url() . 'rute_salesman' ?>"><i class="fa fa-file-text-o"></i> Rute Salesman </a></li>
    <li><a href="<?php echo base_url() . 'lacak_salesman' ?>"><i class="fa fa-file-text-o"></i> Lacak Salesman </a></li>
   </ul>
  </li>

  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Penagihan</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <!--<li><a href="<?php echo base_url() . 'faktur' ?>"><i class="fa fa-file-text-o"></i> Faktur bersyarat </a></li>-->
    <li><a href="<?php echo base_url() . 'faktur_pelanggan' ?>"><i class="fa fa-file-text-o"></i> Faktur </a></li>
    <li><a href="<?php echo base_url() . 'retur_barang' ?>"><i class="fa fa-file-text-o"></i> Retur </a></li>
    <!--<li><a href="<?php echo base_url() . 'persyaratan' ?>"><i class="fa fa-file-text-o"></i> Syarat Pembayaran </a></li>-->
    <!--<li><a href="<?php echo base_url() . 'metode_bayar' ?>"><i class="fa fa-file-text-o"></i> Metode Pembayaran </a></li>-->
   </ul>
  </li>

  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Pembelian</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <li><a href="<?php echo base_url() . 'supplier' ?>"><i class="fa fa-file-text-o"></i> Vendor </a></li>
    <li><a href="<?php echo base_url() . 'pengadaan' ?>"><i class="fa fa-file-text-o"></i> Pengadaan </a></li>
    <li><a href="<?php echo base_url() . 'retur_pengadaan' ?>"><i class="fa fa-file-text-o"></i> Retur </a></li>
   </ul>
  </li>

  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Pembayaran</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <li><a href="<?php echo base_url() . 'tagihan' ?>"><i class="fa fa-file-text-o"></i> Tagihan </a></li>
    <li><a href="<?php echo base_url() . 'pembayaran' ?>"><i class="fa fa-file-text-o"></i> Biaya Umum </a></li>
    <li><a href="<?php echo base_url() . 'payment' ?>"><i class="fa fa-file-text-o"></i> Faktur </a></li>
   </ul>
  </li>

  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Pengiriman</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <li><a href="<?php echo base_url() . 'pengiriman' ?>"><i class="fa fa-file-text-o"></i> Surat Jalan </a></li>
   </ul>
  </li>

  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Inventori</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <li><a href="<?php echo base_url() . 'gudang' ?>"><i class="fa fa-file-text-o"></i> Gudang </a></li>
    <li><a href="<?php echo base_url() . 'rak' ?>"><i class="fa fa-file-text-o"></i> Rak </a></li>
    <li><a href="<?php echo base_url() . 'unit' ?>"><i class="fa fa-file-text-o"></i> Satuan </a></li>
    <li><a href="<?php echo base_url() . 'satuan' ?>"><i class="fa fa-file-text-o"></i> Produk Satuan </a></li>
    <li><a href="<?php echo base_url() . 'product_stock' ?>"><i class="fa fa-file-text-o"></i> Stok </a></li>
   </ul>
  </li>

  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Akuntansi</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <li><a href="<?php echo base_url() . 'coa' ?>"><i class="fa fa-file-text-o"></i> Bagan Akun </a></li>
    <li><a href="<?php echo base_url() . 'jurnal_struktur' ?>"><i class="fa fa-file-text-o"></i> Jurnal Struktur </a></li>
    <li><a href="<?php echo base_url() . 'bank_akun' ?>"><i class="fa fa-file-text-o"></i> Bank Akun </a></li>
    <li><a href="<?php echo base_url() . 'kas' ?>"><i class="fa fa-file-text-o"></i> Kas & Bank </a></li>
    <!--<li><a href="<?php echo base_url() . 'mata_uang' ?>"><i class="fa fa-file-text-o"></i> Mata Uang </a></li>-->
   </ul>
  </li>

  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Penggajian</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <li><a href="<?php echo base_url() . 'payroll' ?>"><i class="fa fa-file-text-o"></i> Gaji </a></li>
    <li><a href="<?php echo base_url() . 'payroll_category' ?>"><i class="fa fa-file-text-o"></i> Kategori </a></li>
    <li><a href="<?php echo base_url() . 'periode' ?>"><i class="fa fa-file-text-o"></i> Periode </a></li>
    <li><a href="<?php echo base_url() . 'reimburse' ?>"><i class="fa fa-file-text-o"></i> Reimburse </a></li>
    <li><a href="<?php echo base_url() . 'kasbon' ?>"><i class="fa fa-file-text-o"></i> Kasbon </a></li>
    <li><a href="<?php echo base_url() . 'kasbon_payment' ?>"><i class="fa fa-file-text-o"></i> Kasbon Bayar</a></li>
   </ul>
  </li>

  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Personalia</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <li><a href="<?php echo base_url() . 'pegawai' ?>"><i class="fa fa-file-text-o"></i> Karyawan </a></li>
    <li><a href="<?php echo base_url() . 'pegawai_kontrak' ?>"><i class="fa fa-file-text-o"></i> Kontrak </a></li>
   </ul>
  </li>

  <!--   <li class="treeview">
    <a href="#">
     <i class="fa fa-folder"></i>
     <span>Perpajakan</span>
     <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
     </span>
    </a>
    <ul class="treeview-menu">
     <li><a href="<?php echo base_url() . 'efaktur' ?>"><i class="fa fa-file-text-o"></i> E-faktur </a></li> 
    </ul>
   </li>-->

  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Zakat</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <li><a href="<?php echo base_url() . 'zakat_usaha' ?>"><i class="fa fa-file-text-o"></i> Zakat Usaha </a></li>
   </ul>
  </li>

  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Laporan</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <li><a href="<?php echo base_url() . 'laba_rugi' ?>"><i class="fa fa-file-text-o"></i> Laba Rugi </a></li>
    <li><a href="<?php echo base_url() . 'bagi_hasil' ?>"><i class="fa fa-file-text-o"></i> Bagi Hasil </a></li>
    <li><a href="<?php echo base_url() . 'arus_kas' ?>"><i class="fa fa-file-text-o"></i> Arus Kas </a></li>
    <li><a href="<?php echo base_url() . 'jurnal' ?>"><i class="fa fa-file-text-o"></i> Jurnal </a></li>
    <li><a href="<?php echo base_url() . 'lappenjualan' ?>"><i class="fa fa-file-text-o"></i> Penjualan </a></li>
    <li><a href="<?php echo base_url() . 'lapstock' ?>"><i class="fa fa-file-text-o"></i> Stok </a></li>
    <li><a href="<?php echo base_url() . 'lappelanggan' ?>"><i class="fa fa-file-text-o"></i> Pelanggan </a></li>
<!--    <li><a href="<?php echo base_url() . 'ringkasan_pemasukan' ?>"><i class="fa fa-file-text-o"></i> Ringkasan Pemasukan </a></li>
    <li><a href="<?php echo base_url() . 'ringkasan_pengeluaran' ?>"><i class="fa fa-file-text-o"></i> Ringkasan Pengeluaran </a></li>
    <li><a href="<?php echo base_url() . 'pemasukan_pengeluaran' ?>"><i class="fa fa-file-text-o"></i> Pemasukan vs Pengeluaran </a></li>-->
   </ul>
  </li>

  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Musyarokah</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <li><a href="<?php echo base_url() . 'kerja_sama_internal' ?>"><i class="fa fa-file-text-o"></i> Internal </a></li>
    <!--<li><a href="<?php echo base_url() . 'kerja_sama_eksternal' ?>"><i class="fa fa-file-text-o"></i> Eksternal </a></li>-->
   </ul>
  </li>

  <!--   <li class="treeview">
    <a href="#">
     <i class="fa fa-folder"></i>
     <span>Akad</span>
     <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
     </span>
    </a>
    <ul class="treeview-menu">
     <li><a href="<?php echo base_url() . 'kategori_akad' ?>"><i class="fa fa-file-text-o"></i> Kategori </a></li> 
     <li><a href="<?php echo base_url() . 'jenis_akad' ?>"><i class="fa fa-file-text-o"></i> Jenis </a></li> 
    </ul>
   </li>-->

  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Reminder</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <li><a href="<?php echo base_url() . 'notif_jatuh_tempo' ?>"><i class="fa fa-file-text-o"></i> Jatuh Tempo </a></li>
    <li><a href="<?php echo base_url() . 'send_notif' ?>"><i class="fa fa-file-text-o"></i> Kirim Reminder </a></li>
   </ul>
  </li>

  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Pengaturan</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <li><a href="<?php echo base_url() . 'general' ?>"><i class="fa fa-file-text-o"></i> Umum </a></li>
    <li><a href="<?php echo base_url() . 'pengguna' ?>"><i class="fa fa-file-text-o"></i> Pengguna </a></li>
    <!--<li><a href="<?php echo base_url() . 'tanggal' ?>"><i class="fa fa-file-text-o"></i> Tanggal </a></li>-->
   </ul>
  </li>
  <li><a href="<?php echo base_url() . 'login/sign_out' ?>"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
 	<li><a href="<?php echo base_url() . 'apps' ?>"><i class="fa fa-sign-out"></i> <span>Apps</span></a></li>
 </ul>

<?php } ?>
