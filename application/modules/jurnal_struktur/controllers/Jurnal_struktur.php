<?php

class Jurnal_struktur extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'jurnal_struktur';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/jurnal_struktur.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'jurnal_struktur';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Jurnal Struktur";
  $data['title_content'] = 'Data Jurnal Struktur';
  $content = $this->getDataStruktur();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataStruktur($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('js.nama', $keyword),
       array('c.code', $keyword),
       array('c.keterangan', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' js',
              'field' => array('js.*', 'c.code',
                  'c.keterangan as nama_akun',
                  'ct.jenis', 'f.nama as transaksi'),
              'join' => array(
                  array('coa c', 'c.id = js.coa'),
                  array('coa_type ct', 'ct.id = js.coa_type'),
                  array('feature f', 'f.id = js.feature'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "js.deleted is null or js.deleted = 0"
  ));

  return $total;
 }

 public function getDataStruktur($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('js.nama', $keyword),
       array('c.code', $keyword),
       array('c.keterangan', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' js',
              'field' => array('js.*', 'c.code',
                  'c.keterangan as nama_akun',
                  'ct.jenis', 'f.nama as transaksi'),
              'join' => array(
                  array('coa c', 'c.id = js.coa'),
                  array('coa_type ct', 'ct.id = js.coa_type'),
                  array('feature f', 'f.id = js.feature'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "js.deleted is null or js.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataStruktur($keyword)
  );
 }

 public function getDetailDataStruktur($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' js',
              'field' => array('js.*', 'c.code',
                  'c.keterangan as nama_akun',
                  'ct.jenis', 'f.nama as transaksi'),
              'join' => array(
                  array('coa c', 'c.id = js.coa'),
                  array('coa_type ct', 'ct.id = js.coa_type'),
                  array('feature f', 'f.id = js.feature'),
              ),
              'where' => "js.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListFeature() {
  $data = Modules::run('database/get', array(
              'table' => 'feature',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListCoa() {
  $data = Modules::run('database/get', array(
              'table' => 'coa',
              'where' => array('deleted' => 0)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListCoaTipe() {
  $data = Modules::run('database/get', array(
              'table' => 'coa_type',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Jurnal Struktur";
  $data['title_content'] = 'Tambah Jurnal Struktur';
  $data['list_feature'] = $this->getListFeature();
  $data['list_coa'] = $this->getListCoa();
  $data['list_coa_tipe'] = $this->getListCoaTipe();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataStruktur($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Jurnal Struktur";
  $data['title_content'] = 'Ubah Jurnal Struktur';
  $data['list_feature'] = $this->getListFeature();
  $data['list_coa'] = $this->getListCoa();
  $data['list_coa_tipe'] = $this->getListCoaTipe();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataStruktur($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Jurnal Struktur";
  $data['title_content'] = 'Detail Jurnal Struktur';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['feature'] = $value->feature;
  $data['coa'] = $value->coa;
  $data['coa_type'] = $value->coa_type;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post = $this->getPostDataHeader($data);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Coa";
  $data['title_content'] = 'Data Coa';
  $content = $this->getDataStruktur($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
