<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block"> 
    <div class="row">
     <div class='col-md-3 text-bold'>
      Feature
     </div>
     <div class='col-md-3'>
      <?php echo $transaksi ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Bagan Akun
     </div>
     <div class='col-md-3'>
      <?php echo $code.' - '.$nama_akun ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Tipe
     </div>
     <div class='col-md-3'>
      <?php echo $jenis ?>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger-baru" onclick="JurnalStruktur.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
