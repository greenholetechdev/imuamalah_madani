<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class='col-md-3 text-bold'>
      Feature
     </div>
     <div class='col-md-3'>
      <select class="form-control" id="feature" error="Feature">
       <option value="">Pilih Feature</option>
       <?php if (!empty($list_feature)) { ?>
        <?php foreach ($list_feature as $value) { ?>
         <?php $selected = '' ?>
         <?php if (isset($feature)) { ?>
          <?php $selected = $feature == $value['id'] ? 'selected' : '' ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Bagan Akun
     </div>
     <div class='col-md-3'>
      <select class="form-control" id="coa" error="Bagan Akun">
       <option value="">Pilih Bagan Akun</option>
       <?php if (!empty($list_coa)) { ?>
        <?php foreach ($list_coa as $value) { ?>
         <?php $selected = '' ?>
         <?php if (isset($coa)) { ?>
          <?php $selected = $coa == $value['id'] ? 'selected' : '' ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['code'] . ' - ' . $value['keterangan'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Tipe Bagan Akun
     </div>
     <div class='col-md-3'>
      <select class="form-control" id="coa_type" error="Tipe Bagan Akun">
       <option value="">Pilih Tipe</option>
       <?php if (!empty($list_coa_tipe)) { ?>
        <?php foreach ($list_coa_tipe as $value) { ?>
         <?php $selected = '' ?>
         <?php if (isset($coa_type)) { ?>
          <?php $selected = $coa_type == $value['id'] ? 'selected' : '' ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['jenis'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="JurnalStruktur.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="JurnalStruktur.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
