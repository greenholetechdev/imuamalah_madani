<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table table-striped table-bordered table-list-draft" id="tb_product">
    <thead>
     <tr class="bg-primary-light text-white">
      <th>Produk</th>
      <th>Satuan</th>
      <th>Harga</th>
      <th>Jumlah</th>
      <th>Sub Total</th>
      <th>Action</th>
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($invoice_item)) { ?>
      <?php $index = 0; ?>
      <?php foreach ($invoice_item as $value) { ?>
       <tr data_id="<?php echo $value['id'] ?>"> 
        <td>
         <select class="form-control select2 required" id="product<?php echo $index ?>" 
                 error="Produk" onchange="Pengadaan.hitungSubTotal(this, 'product')">
          <option value="" harga="0">Pilih Produk</option>
          <?php if (!empty($list_product)) { ?>
           <?php foreach ($list_product as $v_p) { ?>
            <?php $selected = $v_p['id'] == $value['product_satuan'] ? 'selected' : '' ?>
            <option harga="<?php echo $v_p['harga'] ?>" <?php echo $selected ?> value="<?php echo $v_p['id'] ?>"><?php echo $v_p['nama_product']?></option>
           <?php } ?>
          <?php } ?>
         </select>
        </td>
        <td>
         <select class="form-control select2 required" id="pajak<?php echo $index ?>" error="Pajak"
                 onchange="Pengadaan.hitungSubTotal(this, 'pajak')">
          <option value="" persentase="0">Pilih Pajak</option>
          <?php if (!empty($list_pajak)) { ?>
           <?php foreach ($list_pajak as $v_pj) { ?>
            <?php $selected = $v_pj['id'] == $value['pajak'] ? 'selected' : '' ?>
            <option persentase="<?php echo $v_pj['persentase'] ?>" <?php echo $selected ?> value="<?php echo $v_pj['id'] ?>"><?php echo $v_pj['jenis'] ?></option>
           <?php } ?>
          <?php } ?>
         </select>
        </td>
        <td>
         <select class="form-control select2 required" id="metode<?php echo $index ?>" 
                 error="Metode Bayar" onchange="Pengadaan.getMetodeBayar(this)">
          <option value="">Pilih Metode Bayar</option>
          <?php if (!empty($list_metode)) { ?>
           <?php foreach ($list_metode as $vm) { ?>
            <?php $selected = $vm['id'] == $value['metode_bayar'] ? 'selected' : '' ?>
            <option <?php echo $selected ?> value="<?php echo $vm['id'] ?>"><?php echo $vm['metode'] ?></option>
           <?php } ?>
          <?php } ?>
         </select>
        </td>
        <td>
         <input type="number" value="<?php echo $value['qty'] ?>" min="1" id="jumlah" 
                class="form-control text-right" 
                onkeyup="Pengadaan.hitungSubTotal(this, 'jumlah')" 
                onchange="Pengadaan.hitungSubTotal(this, 'jumlah')"/>
        </td>
        <td><?php echo number_format($value['sub_total']) ?></td>
        <td class="text-center">
         <i class="mdi mdi-delete mdi-18px" onclick="Pengadaan.deleteItem(this)"></i>
        </td>
       </tr>

       <?php if ($value['bank'] != '0' && $value['bank'] != '') { ?>
        <tr data_bank="<?php echo $value['bank'] ?>">
         <td colspan="7"><?php echo $value['nama_bank'] . '-' . $value['no_rekening'] . '-' . $value['akun'] ?></td>
        </tr>
       <?php } ?>

       <?php $index += 1; ?>
      <?php } ?>
     <?php } ?> 
     <tr data_id="">
      <td colspan="7">
       <label id="add_detail">
        <a href="#" onclick="Pengadaan.addItem(this, event)">Tambah Item</a>
       </label>
      </td>
     </tr>
    </tbody>
   </table>
  </div>
 </div>
</div>