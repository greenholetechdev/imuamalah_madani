<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block"> 

    <div class="row">
     <div class='col-md-3 text-bold'>
      Nama Bank
     </div>
     <div class='col-md-3'>
      <input type='text' name='' placeholder="" id='nama_bank' class='form-control required' 
             value='<?php echo isset($nama_bank) ? $nama_bank : '' ?>' error="Nama Bank"/>
     </div>     
    </div>
    <br/>
   
    <div class="row">
     <div class='col-md-3 text-bold'>
      No Rekening
     </div>
     <div class='col-md-3'>
      <input type='text' name='' placeholder="" id='no_rekening' class='form-control required' 
             value='<?php echo isset($no_rekening) ? $no_rekening : '' ?>' error="No Rekening"/>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Akun
     </div>
     <div class='col-md-3'>
      <input type='text' name='' placeholder="" id='akun' class='form-control required' 
             value='<?php echo isset($akun) ? $akun : '' ?>' error="Akun"/>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="BankAkun.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="BankAkun.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
