<?php

class Apps extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function getModuleName() {
  return 'apps';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
  );

  return $data;
 }

 public function index() {
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Apps Greenhole Tech";
  $data['title_content'] = 'Apps Greenhole Tech';
  echo Modules::run('template', $data);
 }

}
