<?php

class Biaya_umum extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 10;
	}

	public function getModuleName()
	{
		return 'Biaya_umum';
	}

	public function getTableName()
	{
		return 'pembayaran_tagihan';
	}

	public function index()
	{
		echo 'Biaya_umum';
	}

	public function getListTagihan()
	{
		$data = Modules::run('database/get', array(
			'table' => 'tagihan t',
			'field' => array('t.*'),
			'where' => "t.deleted is null or t.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		echo json_encode(array(
			'data' => $result
		));
	}

	public function prosesSimpan()
	{
		$is_valid = "0";
		$id = "";
		$this->db->trans_begin();
		try {
			$no_invoice = Modules::run('no_generator/generateInvoice', 'INVPEMTAG', 'pembayaran_tagihan');
			$post_tagihan['no_invoice'] = $no_invoice;
			$post_tagihan['total'] = $_POST['total'];
			$post_tagihan['tagihan'] = $_POST['tagihan'];
			$post_tagihan['tgl_bayar'] = date('Y-m-d', strtotime($_POST['tanggal_bayar']));
			$post_tagihan['jenis_pembayaran'] = "1";
			$id = Modules::run('database/_insert', 'pembayaran_tagihan', $post_tagihan);

			$this->createJurnal($post_tagihan['total'], $no_invoice);
			$this->db->trans_commit();
			$is_valid = "1";
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
	}

	public function createJurnal($total, $ref = "")
	{
		//create jurnal akuntan
		$jurnal = Modules::run('generate_jurnal/insertJurnal', $ref);
		$jurnal_struktur = Modules::run('generate_jurnal/getJurnalStruktur', 'Biaya Umum');
		//    echo '<pre>';
		//    print_r($jurnal_struktur);die;
		if (!empty($jurnal_struktur)) {
			foreach ($jurnal_struktur as $value) {
				$post_detail['jurnal'] = $jurnal;
				$post_detail['jurnal_struktur'] = $value['id'];
				$post_detail['jumlah'] = str_replace('.', '', $total);
				Modules::run('generate_jurnal/insertJurnalDetail', $post_detail);
			}
		}
	}
}
