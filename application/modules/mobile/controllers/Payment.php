<?php

class Payment extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		$this->limit = 10;
	}

	public function getModuleName()
	{
		return 'Payment';
	}

	public function getTableName()
	{
		return 'payment';
	}

	public function index()
	{
		echo 'Payment';
	}

	public function getPostDataHeader($value)
	{
		$data['no_faktur_bayar'] = Modules::run('no_generator/generateNoFakturBayar');
		$data['tanggal_faktur'] = date('Y-m-d H:i:s');
		$data['tanggal_bayar'] = date('Y-m-d H:i:s');
		$data['jumlah'] = str_replace('.', '', $value['jumlah']);
		$data['createdby'] = $_POST['user'];
		$data['createddate'] = date('Y-m-d');
		return $data;
	}

	public function simpan()
	{
		$data = $_POST;
		$data_item = json_decode($this->input->post('data_item'));
		$is_valid = "0";

		// echo '<pre>';
		// print_r($_POST);
		// die;
		$this->db->trans_begin();
		try {
			$jumlah_bayar = str_replace('.', '', $data['jumlah']);
			$post_data = $this->getPostDataHeader($data);
			$this->db->insert($this->getTableName(), $post_data);
			$id = $this->db->insert_id();

			//payment_item
			if (!empty($data_item)) {
				$sisa = $jumlah_bayar;
				foreach ($data_item as $value) {
					$sisa_hutang = str_replace(',', '', $value->sisa_hutang);
					if ($sisa < $sisa_hutang) {
						$total_sisa = $sisa_hutang - $sisa;
					} else {
						$total_sisa = $sisa_hutang;
					}

					$margin = $sisa - $total_sisa;

					$post_item['payment'] = $id;
					$post_item['invoice'] = $value->id;
					$post_item['jumlah_bayar'] = $margin > 0 ? $sisa_hutang : $sisa;
					Modules::run('database/_insert', 'payment_item', $post_item);

					if ($margin >= 0) {
						//paid
						$post_status['invoice'] = $value->id;
						$post_status['user'] = $_POST['user'];
						$post_status['status'] = 'PAID';
						Modules::run('database/_insert', 'invoice_status', $post_status);

						$post_sisa['jumlah'] = 0;
					} else {
						$post_sisa['jumlah'] = $total_sisa;
					}

					//invoice sisa     
					$post_sisa['invoice'] = $value->id;
					Modules::run('database/_insert', 'invoice_sisa', $post_sisa);
					$sisa -= $sisa_hutang;
				}
			}


			//create jurnal akuntan
			$jurnal = Modules::run('generate_jurnal/insertJurnal', $post_data['no_faktur_bayar']);
			$jurnal_struktur = Modules::run('generate_jurnal/getJurnalStruktur', 'Pembayaran');
			//    echo '<pre>';
			//    print_r($jurnal_struktur);die;
			if (!empty($jurnal_struktur)) {
				foreach ($jurnal_struktur as $value) {
					$post_detail['jurnal'] = $jurnal;
					$post_detail['jurnal_struktur'] = $value['id'];
					$post_detail['jumlah'] = $jumlah_bayar;
					Modules::run('generate_jurnal/insertJurnalDetail', $post_detail);
				}
			}
			$this->db->trans_commit();
			$is_valid = "1";
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
	}


	public function getListPaymentFakturPelanggan()
	{
		$sales = $_POST['user'];
		$date = date('Y-m-d');
		// $date = '2019-12-17';
		// $sales = 4;
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' p',
			'field' => array('p.*', 'pb.nama as nama_pembeli'),
			'join' => array(
				array('(select max(id) id, payment from payment_item group by payment) pss', 'pss.payment = p.id'),
				array('payment_item pit', 'pit.id = pss.id'),
				array('invoice iv', 'iv.id = pit.invoice'),
				array('pembeli pb', 'pb.id = iv.pembeli'),
			),
			'where' => "p.deleted is null or p.deleted = 0 and p.createdby = '" . $sales . "' and p.createddate = '".$date."'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		echo json_encode(array('data' => $result));
	}

	public function getDataPaymentItem($payment)
	{
		$data = Modules::run('database/get', array(
			'table' => 'payment_item pi',
			'field' => array(
				'pi.*', 'ist.status',
				'isa.jumlah as sisa_hutang',
				'i.no_faktur',
				'i.total as invoice_total', 'ist.status'
			),
			'join' => array(
				array('invoice i', 'pi.invoice = i.id'),
				array('(select max(id) id, invoice from invoice_status group by invoice) iss', 'iss.invoice = i.id'),
				array('invoice_status ist', 'ist.id = iss.id'),
				array('(select max(id) id, invoice from invoice_sisa group by invoice) ss', 'ss.invoice = i.id', 'left'),
				array('invoice_sisa isa', 'isa.id = ss.id', 'left'),
			),
			'where' => "pi.deleted = 0 and pi.payment = '" . $payment . "'",
			'orderby' => 'pi.id'
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				if ($value['sisa_hutang'] == '') {
					$value['sisa_hutang'] = $value['total'];
				}
				array_push($result, $value);
			}
		}

		echo json_encode(array('data' => $result));
	}
}
