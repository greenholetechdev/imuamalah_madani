<?php

class Pegawai_kontrak extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'pegawai_kontrak';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/pegawai_kontrak.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'pegawai_kontrak';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Kontrak";
  $data['title_content'] = 'Data Kontrak';
  $content = $this->getDataKontrak();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataKontrak($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('pk.tanggal_awal', $keyword),
       array('pk.tanggal_akhir', $keyword),
       array('p.nama', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' pk',
              'field' => array('pk.*', 'p.nama as nama_pegawai'),
              'join' => array(
                  array('pegawai p', 'pk.pegawai = p.id')
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "pk.deleted is null or pk.deleted = 0"
  ));

  return $total;
 }

 public function getDataKontrak($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('pk.tanggal_awal', $keyword),
       array('pk.tanggal_akhir', $keyword),
       array('p.nama', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' pk',
              'field' => array('pk.*', 'p.nama as nama_pegawai'),
              'join' => array(
                  array('pegawai p', 'pk.pegawai = p.id')
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "pk.deleted is null or pk.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataKontrak($keyword)
  );
 }

 public function getDetailDataKontrak($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' r',
              'field' => array('r.*', 'p.nama as nama_pegawai'),
              'join' => array(
                  array('pegawai p', 'r.pegawai = p.id')
              ),
              'where' => "r.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListPegawai() {
  $data = Modules::run('database/get', array(
              'table' => 'pegawai p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Kontrak";
  $data['title_content'] = 'Tambah Kontrak';
  $data['list_pegawai'] = $this->getListPegawai();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataKontrak($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Kontrak";
  $data['title_content'] = 'Ubah Kontrak';
  $data['list_pegawai'] = $this->getListPegawai();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataKontrak($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Kontrak";
  $data['title_content'] = 'Detail Kontrak';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['pegawai'] = $value->pegawai;
  $data['tanggal_awal'] = date('Y-m-d', strtotime($value->tanggal_awal));
  $data['tanggal_akhir'] = date('Y-m-d', strtotime($value->tanggal_akhir));
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));

  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post_data = $this->getPostDataHeader($data);

   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_data);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_data, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Kontrak";
  $data['title_content'] = 'Data Kontrak';
  $content = $this->getDataKontrak($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function printFaktur($id) {
  $data['reimburse'] = $this->getDetailDataKontrak($id);
  $mpdf = Modules::run('mpdf/getInitPdf');

//  $pdf = new mPDF('A4');
  $view = $this->load->view('cetak', $data, true);
  $mpdf->WriteHTML($view);
  $mpdf->Output('Nota Kontrak - ' . date('Y-m-d') . '.pdf', 'I');
 }

}
