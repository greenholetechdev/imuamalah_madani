<input type="hidden" value="<?php echo $threshold_id ?>" id="threshold_id" class="form-control" />
<div class="content">
 <div class="animated fadeIn">
   <div class="box padding-16">   
    <div class="box-body">   
      <div class="row">
        <div class="col-md-6">
          <div class="form-group row">
            <label for="filterUser" class="col-sm-2 col-form-label">Periode</label>
            <div class="col-sm-10">
              <select class="form-control required" id="thresholdmas" 
                      error="Zakat_usaha" 
                      onchange="ZakatUsaha.goThresholdMasZakat(this)"
                      >
                <option disabled selected></option>
                <!-- <option value="000">Semua salesman</option> -->
                <?php if (!empty($threshold_mas_list)) { ?>
                  <?php foreach ($threshold_mas_list as $value) { ?>
                  <?php $selected = '' ; ?>
                  <?php if (isset($threshold_id)) { ?>
                    <?php $selected = $threshold_mas['id'] == $value['id'] ? 'selected' : '' ?>
                  <?php } ?>
                  <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['period']['end_hijri'] ?></option>
                  <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
           <label for="" class="col-sm-2 col-form-label">Mulai</label>
           <div class="col-sm-5">
             <input disabled type class="form-control required" id="zakatMalPeriodeHijr" 
             value="<?php if($threshold_mas && $threshold_mas['period']) echo $threshold_mas['period']['start_hijri'] ?>" />
           </div>
           <div class="col-sm-5">
             <input type="date" data-date-format="dd/mm/yyyy" class="form-control required" id="zakat_mal_periode_mash" 
             <?php if($threshold_mas && !$first_year) echo 'disabled' ?>
             value="<?php if($threshold_mas && $threshold_mas['period']) echo $threshold_mas['period']['start_masehi'] ?>"
             />
           </div>
          </div>
          <div class="form-group row">
           <label for="" class="col-sm-2 col-form-label">Akhir</label>
           <div class="col-sm-5">
             <input disabled type class="form-control required"
             value="<?php if($threshold_mas && $threshold_mas['period']) echo $threshold_mas['period']['end_hijri'] ?>" />
           </div>
           <div class="col-sm-5">
             <input disabled type="date" class="form-control required"
             value="<?php if($threshold_mas && $threshold_mas['period']) echo $threshold_mas['period']['end_masehi'] ?>" />
           </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group row">
           <label for="" class="col-sm-4 col-form-label">Rp emas/gr</label>
           <div class="col-sm-8">
             <input class="form-control required" id="zakat_mal_harga_emas"
             <?php if($threshold_mas && !$threshold_mas['enabled_edit']) echo 'disabled' ?> 
             value="<?php if($threshold_mas && $threshold_mas['harga']) echo $threshold_mas['harga'] ?>"/>
           </div>
          </div>
        </div>
        <div class="col-md-2">
         <button type="submit" class="btn btn-primary mb-2" 
         onclick="ZakatUsaha.updateThresholdMas('<?php echo isset($threshold_mas['id']) ? $threshold_mas['id'] : '' ?>')"
         >Terapkan</button>
        </div>
      </div>
    </div>
    
  </div> 

  <div class="box padding-16"> 
    <div class="box-body">   
      <div class="row">
        <div class="col-md-12">  
          <div class="form-group row">
            <label for="" class="col-sm-4 col-form-label">1. Total Bank & Kas</label>
            <div class="col-sm-5">
              <input disabled class="form-control required"
              value="<?php if($threshold_mas && $threshold_mas['kas']) echo $threshold_mas['kas']['total_jumlah'] ?>" />
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-sm-4 col-form-label">2. Total Inventory</label>
            <div class="col-sm-5">
              <input disabled class="form-control required" 
              value="<?php if($threshold_mas && $threshold_mas['stok']) echo $threshold_mas['stok']['total_jumlah'] ?>" />
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-sm-4 col-form-label">3. Total Asset Bruto (poin 1 + poin 2)</label>
            <div class="col-sm-5">
              <input disabled class="form-control required" 
              value="<?php if($threshold_mas) echo $threshold_mas['asset_bruto'] ?>" />
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-sm-4 col-form-label">4. Total Hutang</label>
            <div class="col-sm-5">
              <input disabled class="form-control required"
              value="<?php if($threshold_mas ) echo $threshold_mas['hutang'] ?>" />
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-sm-4 col-form-label">5. Total Asset Net (poin 3 - poin 4)</label>
            <div class="col-sm-5">
              <input disabled class="form-control required" 
              value="<?php if($threshold_mas) echo $threshold_mas['asset'] ?>" />
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-sm-4 col-form-label">6. Threshold Zakat (Harga Emas/gr * 85gr)</label>
            <div class="col-sm-5">
              <input disabled class="form-control required" 
              value="<?php if($threshold_mas) echo $threshold_mas['zakat_threshold'] ?>" />
            </div>
          </div>
          <div class="form-group row">
            <label for="" class="col-sm-4 col-form-label">7. Zakat Usaha (25% * poin 4)</label>
            <div class="col-sm-5">
              <input disabled class="form-control required" 
              value="<?php if($threshold_mas) echo $threshold_mas['zakat'] ?>" />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  </div>
</div>
