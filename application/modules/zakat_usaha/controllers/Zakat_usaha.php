<?php

use GeniusTS\HijriDate\Date;
use \GeniusTS\HijriDate\Hijri;
Date::setToStringFormat('l d F o');

class Zakat_usaha extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'zakat_usaha';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
  '<script src="' . base_url() . 'assets/js/controllers/zakat_usaha.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'zakat_mal';
 }

 public function index($threshold_id = '0') {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Zakat Usaha";
  $data['title_content'] = 'Zakat Usaha';
  $threshold_list = $this->getThresholdMasList();
  $data['threshold_mas_list'] = $threshold_list;
  $data['first_year'] = (sizeof($threshold_list) == 1)? true : false; 
  $data['threshold_mas'] = $this->getThresholdMas($threshold_id);
  //$total_rows = $content['total_rows'];
  //$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  $data['threshold_id'] = isset($data['threshold_mas']['id']) ? $data['threshold_mas']['id'] : $threshold_id;
  echo Modules::run('template', $data);
 }

 public function insertThresholdMas($_harga, $_period_start, $_period_end, int $_jumlah = 85){
  $mas_data['jumlah'] = $_jumlah;
  $mas_data['harga'] = $_harga;
  $mas_data['period_start'] = $_period_start;
  $mas_data['period_end'] = $_period_end;
  $this->db->trans_begin();
  $data = false;
  try {
    $data = Modules::run('database/_insert', 'threshold_mas', $mas_data);
    $this->db->trans_commit();
   } catch (Exception $ex) {
    $this->db->trans_rollback();
   }
  //var_dump($data);
  return;
 }

 public function getThisDay(){
   $thisday = date("Y-m-d");
   return $thisday;
 }

 public function getThresholdMasList(){
  $data = Modules::run('database/get', array(
    'table' => 'threshold_mas p',
    'field' => array('p.*'),
    'where' => "p.deleted = 0 or p.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    
    $value['period'] = $this->getZakatPeriod($value['period_start']);
    array_push($result, $value);
   }
  }
  else {} 
  return $result; 
 }

 public function getThresholdMas($threshold_id){
  $thisday = $this->getThisDay();
  $data = Modules::run('database/get', array(
    'table' => 'threshold_mas p',
    'field' => array('p.*'),
    'where' => ($threshold_id == '0')? 
      "(p.deleted = 0 or p.deleted is null) and p.period_end >= '".$thisday. "'" : 
      "(p.deleted = 0 or p.deleted is null) and p.id = '".$threshold_id. "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    
    $value['period'] = $this->getZakatPeriod($value['period_start']);
    array_push($result, $value);
   }
  }
  else {
    if ($threshold_id == '0'){
      // $start = '2018-11-03';
      $start = $this->getMuharamThisyear();
      $mas = 695000;
      $period = $this->getZakatPeriod($start);
      $this->insertThresholdMas(
        $mas, 
        $period['start_masehi'], 
        $period['end_masehi']
      ); 
    }
  }

  if (sizeof($result) == 0) return $result;
  else {
    $data = $this->showZakat($result[0], $thisday);
    return $data;
  }
 }

 public function showZakat($data, $thisday){
  if ($data['period_end'] >= $thisday){
    $data['enabled_edit'] = true;
    $data['kas'] = $this->getDataKas($data['period']['end_masehi']);
    $data['stok'] = $this->getDataStok($data['period']['end_masehi']);
    $data['hutang'] = 0;

    $asset_bruto = $data['kas']['total_jumlah'] + $data['stok']['total_jumlah'];
    $asset = $asset_bruto  - $data['hutang'];
    $zakat = $this->calcZakat($data['jumlah'], $data['harga'], $asset);

    
    $data['zakat'] = $zakat;
    $data['asset_bruto'] = $asset_bruto;
    $data['zakat_threshold'] = ($data['jumlah'] && $data['harga']) ? ($data['jumlah'] * $data['harga']) : 0;
    $data['asset'] = $asset;

    $this->renewZakatMal($data['id'], $data);
  }
  else {
    $oldzakat = $this->getZakatMal($data['id']);
    $data['kas'] = array();
    $data['stok'] = array();
    $data['kas']['total_jumlah'] = $oldzakat[0]['tot_kas_bank'];
    $data['stok']['total_jumlah'] = $oldzakat[0]['tot_inventori'];
    $data['zakat'] = $oldzakat[0]['total'];
    $data['hutang'] = $oldzakat[0]['tot_hutang'];
    $data['asset_bruto'] = $oldzakat[0]['tot_bruto'];
    $data['zakat_threshold'] = $oldzakat[0]['zakat_threshold'];
    $data['asset'] = $oldzakat[0]['tot_net'];
    $data['enabled_edit'] = false;
    
  }
  return $data;
 }

 public function getPostDataHeader($period, $value) {
  $data['period_start'] = $period['start_masehi'];
  $data['period_end'] = $period['end_masehi'];
  $data['harga'] = $value->harga;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = true;

  $is_valid = false;
  $this->db->trans_begin();
  try {
    $period = $this->getZakatPeriod($data->start);
    $post_threshold = $this->getPostDataHeader($period, $data);
    Modules::run('database/_update', 'threshold_mas', $post_threshold, array('id' => $id));
    $this->db->trans_commit();
    $is_valid = true;
   } catch (Exception $ex) {
    $this->db->trans_rollback();
   }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function goThresholdMas($id){
   $result = $this->getThresholdMas($id);
    // echo '<pre>';
    // print_r($result);die;
   echo json_encode(array('is_valid' => true,));
 }

 public function renewZakatMal($id, $zakat){
  $oldzakat = $this->getZakatMal($id);

  if (empty($oldzakat)){

    return $this->insertZakatMal($id, $zakat);
  }
  else {
    return $this->updateZakatMal($zakat, $oldzakat[0]);
  }
 }

 public function updateZakatMal($data, $oldzakat){
  $zakat_data['total'] = $data['zakat'];
  $zakat_data['tot_kas_bank'] = $data['kas']['total_jumlah'];
  $zakat_data['tot_inventori'] = $data['stok']['total_jumlah'];
  $zakat_data['tot_hutang'] = $data['hutang'];
  $zakat_data['tot_bruto'] = $data['asset_bruto'];
  $zakat_data['tot_net'] = $data['asset'];
  $zakat_data['zakat_threshold'] = $data['zakat_threshold'];
  $this->db->trans_begin();
  $data = false;
  try {
   $data = Modules::run('database/_update', $this->getTableName(), $zakat_data, array('id' => $oldzakat['id']));
   $this->db->trans_commit();
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }
  //var_dump($data);
  return;
 }

 public function insertZakatMal($id, $data){
  $zakat_data['threshold_mas'] = $id;
  $zakat_data['total'] = $data['zakat'];
  $zakat_data['tot_kas_bank'] = $data['kas']['total_jumlah'];
  $zakat_data['tot_inventori'] = $data['stok']['total_jumlah'];
  $zakat_data['tot_hutang'] = $data['hutang'];
  $zakat_data['tot_bruto'] = $data['asset_bruto'];
  $zakat_data['tot_net'] = $data['asset'];
  $zakat_data['zakat_threshold'] = $data['zakat_threshold'];

  $this->db->trans_begin();
  $data = false;
  try {
    $data = Modules::run('database/_insert', $this->getTableName(), $zakat_data);
    $this->db->trans_commit();
   } catch (Exception $ex) {
    $this->db->trans_rollback();
   }
  //var_dump($data);
  return;
 }

 public function getZakatMal($id){
  $data = Modules::run('database/get', array(
    'table' => $this->getTableName() . ' p',
    'field' => array('p.*' , 'pr.jumlah' , 'pr.harga' , 'pr.period_start', 'pr.period_end'),
    'join' => array(
      array('threshold_mas pr', 'p.threshold_mas = pr.id')
    ),
    'where' => 'p.threshold_mas ='.$id
  ));

  $result = array();
  if (!empty($data)) {
    foreach ($data->result_array() as $value) {
    array_push($result, $value);
    }
  }
  return $result;
 }

 // format YYYY-MM-DD
 public function getMasehi($day, $month, $year){
  $masehi = Hijri::convertToGregorian($day, $month, $year);
  return str_replace(' ', '-', $masehi->format('o m d'));
 }

 // format YYYY-MM-DD
 public function getHijri($masehi){
  $hijri = Hijri::convertToHijri($masehi);
  return $hijri->format();
 }

 // format YYYY-MM-DD
 public function getHijriValue($masehi){
  $hijri = Hijri::convertToHijri($masehi);
  return $hijri->getValues();
 }

 public function getZakatPeriod($masehi){
  $start_hijri = $this->getHijriValue($masehi);
  $result['start_masehi'] = $masehi;
  $result['start_hijri'] =  $start_hijri['d'] .' '. $start_hijri['F'] .' '. $start_hijri['o'];
  $result['end_hijri'] =  $start_hijri['d'] .' '. $start_hijri['F'] .' '. ($start_hijri['o']+1);
  $result['end_masehi'] = $this->getMasehi($start_hijri['j'], $start_hijri['n'], $start_hijri['o']+1);
  //echo '<pre>';
  //print_r($start_hijri);die;
  return $result;
 }

 public function getMuharamThisyear(){
  $now = Date::now();
  $muharam_thisyear = $this->getMasehi(1, 1, $now->format('o'));

  return $muharam_thisyear;
 }

 public function getTotalJumlahDataKas($data = []) {
  $total = 0;
  foreach ($data as $value) {
    if ($value['jumlah']) $total = $total + $value['jumlah'];
  }
  return $total;
 }

 public function getDataKas($enddate, $keyword = '') {
  $data = Modules::run('database/get', array(
  'table' => 'kas k',
  'field' => array('k.*'),
  //'where' => "k.deleted is null or k.deleted = 0" 
  'where' => "(k.deleted is null or k.deleted = 0) AND ((k.updateddate IS NULL AND k.createddate <= '". $enddate ."') OR (k.updateddate IS NOT NULL AND k.updateddate <= '". $enddate ."'))"    
));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
  'data' => $result,
  'total_jumlah' => $this->getTotalJumlahDataKas($result)
  );
 }

 public function getTotalJumlahDataStok($data = []) {
  $total = 0;
  foreach ($data as $value) {
    if ($value['harga'] && $value['stock']) $total = $total + ($value['harga'] * $value['stock']);
  }
  return $total;
 }

 public function getDataStok($enddate, $keyword = '') {
  $data = Modules::run('database/get', array(
              'table' => 'product_stock k',
              'field' => array('k.*', 
                  'ps.harga' ),
              'join' => array(
                  array('product_satuan ps', 'k.product_satuan = ps.id'),
              ),
              //'where' => "k.deleted is null or k.deleted = 0"
              'where' => "(k.deleted is null or k.deleted = 0) AND ((k.updateddate IS NULL AND k.createddate <= '". $enddate ."') OR (k.updateddate IS NOT NULL AND k.updateddate <= '". $enddate ."'))"    
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_jumlah' => $this->getTotalJumlahDataStok($result)
  );
 }

 public function calcZakat($jumlah_mas, $harga_mas, $total){
  if ($jumlah_mas && $harga_mas && $total){
    $threshold = $jumlah_mas * $harga_mas;
    if ($total >= $threshold) return 2.5*$total/100;
    else return 0;
  }
  else return 0;
 }

}
