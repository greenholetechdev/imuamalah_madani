<!DOCTYPE html>
<html>
 <?php echo $this->load->view('template/head_content'); ?>
 <body class="hold-transition skin-blue sidebar-mini">
  <div class="loader">

  </div>
  <div class="wrapper">

   <!-- Left side column. contains the logo and sidebar -->
   <?php echo $this->load->view('template/sidebar_content') ?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">   
    <!-- <section class="content-header">
     <h1>
      <?php echo $title ?>
      <small><?php echo 'iMuamalah' ?></small>
     </h1>
     <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-file-text"></i> <?php echo 'Data' ?></a></li>
      <li><a href="#"><?php echo $title_content ?></a></li>
     </ol>
    </section> -->
    <!-- Main content -->
    <section class="content">
     <?php echo $this->load->view('dashboard/v_index_mobile'); ?>
    </section>
    <!-- /.content -->
   </div>

   <!-- /.content-wrapper -->
   <?php echo $this->load->view('template/footer_content') ?>

   <!-- Control Sidebar -->
   <?php // echo $this->load->view('right_content') ?>
   <!-- /.control-sidebar -->
   <!-- Add the sidebar's background. This div must be placed
        immediately after the control sidebar -->   
  </div>
  <!-- ./wrapper -->

  <?php echo $this->load->view('template/script_content') ?>

  <?php
  if (isset($header_data)) {
   foreach ($header_data as $v_head) {
    echo $v_head;
   }
  }
  ?>
 </body>
</html>
