<?php

class Reimburse extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'reimburse';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/reimburse.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'reimburse';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Reimburse";
  $data['title_content'] = 'Data Reimburse';
  $content = $this->getDataReimburse();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataReimburse($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('r.tanggal', $keyword),
       array('r.keterangan', $keyword),
       array('r.no_faktur', $keyword),
       array('r.jumlah', $keyword),
       array('p.nama', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' r',
              'field' => array('r.*', 'p.nama as nama_pegawai'),
              'join' => array(
                  array('pegawai p', 'r.pegawai = p.id')
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "r.deleted is null or r.deleted = 0"
  ));

  return $total;
 }

 public function getDataReimburse($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('r.tanggal', $keyword),
       array('r.keterangan', $keyword),
       array('r.no_faktur', $keyword),
       array('r.jumlah', $keyword),
       array('p.nama', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' r',
              'field' => array('r.*', 'p.nama as nama_pegawai'),
              'join' => array(
                  array('pegawai p', 'r.pegawai = p.id')
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "r.deleted is null or r.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataReimburse($keyword)
  );
 }

 public function getDetailDataReimburse($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' r',
              'field' => array('r.*', 'p.nama as nama_pegawai'),
              'join' => array(
                  array('pegawai p', 'r.pegawai = p.id')
              ),
              'where' => "r.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListPegawai() {
  $data = Modules::run('database/get', array(
              'table' => 'pegawai p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Reimburse";
  $data['title_content'] = 'Tambah Reimburse';
  $data['list_pegawai'] = $this->getListPegawai();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataReimburse($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Reimburse";
  $data['title_content'] = 'Ubah Reimburse';
  $data['list_pegawai'] = $this->getListPegawai();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataReimburse($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Reimburse";
  $data['title_content'] = 'Detail Reimburse';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['no_faktur'] = Modules::run('no_generator/generateNoFakturReimburse');
  $data['pegawai'] = $value->pegawai;
  $data['tanggal'] = date('Y-m-d', strtotime($value->tanggal));
  $data['jumlah'] = str_replace('.', '', $value->jumlah);
  $data['keterangan'] = $value->keterangan;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));

  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post_data = $this->getPostDataHeader($data);

   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_data);

    //create jurnal akuntan
    $jurnal = Modules::run('generate_jurnal/insertJurnal', $post_data['no_faktur']);
    $jurnal_struktur = Modules::run('generate_jurnal/getJurnalStruktur', 'Reimburse');
//    echo '<pre>';
//    print_r($jurnal_struktur);die;
    if (!empty($jurnal_struktur)) {
     foreach ($jurnal_struktur as $value) {
      $post_detail['jurnal'] = $jurnal;
      $post_detail['jurnal_struktur'] = $value['id'];
      $post_detail['jumlah'] = str_replace('.', '', $data->jumlah);
      Modules::run('generate_jurnal/insertJurnalDetail', $post_detail);
     }
    }
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_data, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Reimburse";
  $data['title_content'] = 'Data Reimburse';
  $content = $this->getDataReimburse($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function printFaktur($id) {
  $data['reimburse'] = $this->getDetailDataReimburse($id);
  $data['self'] = Modules::run('general/getDetailDataGeneral', 1);
  $mpdf = Modules::run('mpdf/getInitPdf');

//  $pdf = new mPDF('A4');
  $view = $this->load->view('cetak', $data, true);
  $mpdf->WriteHTML($view);
  $mpdf->Output('Nota Reimburse - ' . date('Y-m-d') . '.pdf', 'I');
 }

}
