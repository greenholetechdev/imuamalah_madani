<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data General</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Logo Perusahaan
     </div>
     <div class='col-md-3'>
      <div class="" id="detail_file">
       <div class="input-group">
        <input disabled type="text" id="file_str" class="form-control" value="<?php echo isset($logo) ? $logo : '' ?>">
        <span class="input-group-addon">
         <i class="fa fa-image hover-content" file="<?php echo isset($logo) ? $logo : '' ?>" 
            onclick="General.showLogo(this, event)"></i>         
        </span>
       </div>      
      </div>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Nama Perusahaan
     </div>
     <div class='col-md-3'>
      <?php echo $nama_perusahaan ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Alamat
     </div>
     <div class='col-md-3'>
      <?php echo $alamat ?>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger-baru" onclick="General.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
