<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">       
    <div class='row'>
     <div class='col-md-9'>
      <h4><u>Pilih Metode Pembayaran</u></h4>
     </div>
     <div class='col-md-3 text-right'>
      <i class="mdi mdi-chevron-double-right mdi-18px hover"></i>
      Lihat Semua Data
     </div>
    </div>
    <br/>
    <hr/>

    <div class='row'>
     <div class='col-md-6'>
      <select id="metode_bayar" class="form-control">
       <?php if (!empty($list_pembayaran)) { ?>
        <?php foreach ($list_pembayaran as $v_pem) { ?>
         <option value="<?php echo $v_pem['id'] ?>"><?php echo $v_pem['metode'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>
     <div class='col-md-6 text-left'>
      <button id="" class="btn btn-success" onclick="Pembayaran.getPembayaranForm()">Proses</button>
     </div>
    </div>
    <br/>
    
    <div class='form_pembayaran'>
     
    </div>
   </div>
  </div>
 </div>
</div>
