<div class='row'>
 <div class='col-md-12'>
  <table class="table table-striped table-bordered table-list-draft" id="list_ansuran">
   <thead>
    <tr>
     <th>No</th>
     <th>Harga Ansguran</th>
     <th>Tenor</th>
     <th>Harga Total</th>
     <th class="text-center">Action</th>
    </tr>
   </thead>
   <tbody>
    <?php if (!empty($data)) { ?>
     <?php $no = 1; ?>
     <?php foreach ($data as $value) { ?>
      <tr class="edit" product="<?php echo $value['product'] ?>" id="<?php echo $value['id'] ?>"> 
       <td><?php echo $no++ ?></td>
       <td>
        <?php echo 'Rp. ' . number_format($value['harga'], 2, ',', '.') ?>
       </td>
       <td class="text-center">
        <?php echo $value['periode_tahun'] ?>
       </td>
       <td class="text-center">
        <?php echo 'Rp. '.number_format($value['harga_total'], 2,',', '.') ?>
       </td>
       <td class="text-center">
        <button id="" class="btn btn-succes-baru" onclick="PembelianProduk.pilihAngsuran(this)">Pilih</button>
       </td>
      </tr>
     <?php } ?>
    <?php } ?>
   </tbody>
  </table>
 </div>
</div>