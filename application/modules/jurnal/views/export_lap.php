<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">
    <div class='row'>
     <div class='col-md-12'>
      <a class="btn btn-success" download="<?php echo 'Jurnal' ?>.xls" href="#" id="" onclick="return ExcellentExport.excel(this, 'tb_jurnal', 'Jurnal');">Export</a>
     </div>
    </div>
    <br/>
    <div class="row">
     <div class="col-md-12">
      <div class="table-responsive">
       <div class="sticky-table sticky-headers sticky-ltr-cells">
        <table class="table table-striped table-bordered table-list-draft" id="tb_jurnal">
         <thead>
          <tr class="sticky-row">
           <th class="">No</th>
           <th>No Jurnal</th>
           <th>No Ref</th>
           <th>Transaksi</th>
           <th>Kode Akun</th>
           <th>Akun</th>
           <th>Kredit</th>
           <th>Debit</th>
          </tr>
         </thead>
         <tbody>
          <?php if (!empty($content)) { ?>
           <?php $no = 1; ?>        
           <?php $temp = "" ?>        
           <?php foreach ($content as $value) { ?>        
            <tr>
             <td><?php echo $value['no_jurnal'] == $temp ? '' : $no++ ?></td>
             <td class="text-bold"><?php echo $value['no_jurnal'] == $temp ? '' : $value['no_jurnal'] ?></td>
             <td><?php echo $value['no_jurnal'] == $temp ? '' : $value['ref'] ?></td>
             <td><?php echo $value['transaksi'] ?></td>
             <td><?php echo $value['code'] ?></td>
             <td><?php echo $value['akun'] ?></td>
             <?php if ($value['jenis'] == 'KREDIT') { ?>
              <td class="text-bold text-danger"><?php echo 'Rp, ' . number_format($value['jumlah']) ?></td>
              <td></td>
             <?php } ?>
             <?php if ($value['jenis'] == 'DEBIT') { ?>
              <td></td>
              <td class="text-bold text-success"><?php echo 'Rp, ' . number_format($value['jumlah']) ?></td>
             <?php } ?>
            </tr>
            <?php $temp = $value['no_jurnal'] ?>
           <?php } ?>
          <?php } else { ?>
           <tr>
            <td colspan="6" class="text-center">Tidak ada data ditemukan</td>
           </tr>
          <?php } ?>

         </tbody>
        </table>
       </div>
      </div>      
     </div>          
    </div> 
   </div>
  </div>
 </div>
</div>
