<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
     <div class="row">
     <div class='col-md-3 text-bold'>
      Kode
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='kode' class='form-control required' 
             value='<?php echo isset($kode) ? $kode : '' ?>' error="Kode"/>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Nama Gudang
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='nama_gudang' class='form-control required' 
             value='<?php echo isset($nama_gudang) ? $nama_gudang : '' ?>' error="Nama Gudang"/>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Panjang (m)
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='panjang' class='form-control' 
             value='<?php echo isset($panjang) ? $panjang : '' ?>' error="Panjang"/>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Lebar (m)
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='lebar' class='form-control' 
             value='<?php echo isset($lebar) ? $lebar : '' ?>' error="Lebar"/>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Tinggi (m)
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='tinggi' class='form-control' 
             value='<?php echo isset($tinggi) ? $tinggi : '' ?>' error="Tinggi"/>
     </div>     
    </div>
    <br/>

    

    <div class="row">
     <div class='col-md-3 text-bold'>
      Maksimal Tonase (Kg)
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='max_tonase' class='form-control required' 
             value='<?php echo isset($max_tonase) ? $max_tonase : '' ?>' error="Maksimal Tonase"/>
     </div>     
    </div>
    <br/>
    <hr/>
    
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="Gudang.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Gudang.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
