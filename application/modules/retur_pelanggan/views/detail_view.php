<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block"> 
    <div class="row">
     <div class='col-md-3 text-bold'>
      No Faktur
     </div>
     <div class='col-md-3'>
      <?php echo $no_invoice ?>
     </div>     
    </div>
    <br/>
    <hr/>

    <div class="row">
     <div class="col-md-12">
      <u>Data Produk</u>
     </div>
    </div>
    <br/>

    <?php echo $this->load->view('detail_product') ?>

    <div class="row">
     <div class="col-md-10 text-right">
      <h4>Total : Rp, <label id="total"><?php echo isset($total) ? number_format($total) : '0' ?></label></h4>
     </div>
    </div>
    <hr/>

    <div class="row">
     <div class="col-md-12">
      <u>Data Retur</u>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal Retur
     </div>
     <div class='col-md-3'>
      <?php echo $tanggal_faktur ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Keterangan
     </div>
     <div class='col-md-3'>
      <?php echo $keterangan ?>
     </div>     
    </div>
    <br/>    
    <hr/>

    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger" onclick="ReturPelanggan.cetak('<?php echo isset($id) ? $id : '' ?>')">Cetak</button>
      &nbsp;
      <button id="" class="btn btn-danger-baru" onclick="ReturPelanggan.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
