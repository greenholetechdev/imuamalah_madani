<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">
    <div class="row">
     <div class='col-md-3 text-bold'>
      Kategori
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='jenis' class='form-control required' 
             value='<?php echo isset($jenis) ? $jenis : '' ?>' error="Kategori"/>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Aksi
     </div>
     <div class='col-md-3'>
      <select class="form-control required" id="action" error="Aksi">
       <option value="">Pilih Aksi</option>
       <?php if (!empty($list_action)) { ?>
        <?php foreach ($list_action as $key => $value) { ?>
         <?php $selected = '' ?>
         <?php if (isset($action)) { ?>
          <?php $selected = $action == $key ? 'selected' : '' ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $key ?>"><?php echo $value ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="PayrollCategory.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="PayrollCategory.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
