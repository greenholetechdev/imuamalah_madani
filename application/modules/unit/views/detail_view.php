<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block"> 
    <div class="row">
     <div class='col-md-3 text-bold'>
      Group Satuan
     </div>
     <div class='col-md-3'>
      <?php echo $nama_group?>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Nama Satuan
     </div>
     <div class='col-md-3'>
      <?php echo $nama_satuan?>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger-baru" onclick="Unit.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
