<?php

class Kasbon extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'kasbon';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/kasbon.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'kasbon';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Kasbon";
  $data['title_content'] = 'Data Kasbon';
  $content = $this->getDataKasbon();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataKasbon($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('k.keterangan', $keyword),
       array('k.total', $keyword),
       array('k.tanggal_faktur', $keyword),
       array('k.tanggal_bayar', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*'),
              'like' => $like,
              'is_or_like' => true,
              'where' => "k.deleted is null or k.deleted = 0"
  ));

  return $total;
 }

 public function getDataKasbon($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('k.keterangan', $keyword),
       array('k.total', $keyword),
       array('k.tanggal_faktur', $keyword),
       array('k.tanggal_bayar', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*'),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "k.deleted is null or k.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataKasbon($keyword)
  );
 }

 public function getDetailDataKasbon($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' kr',
              'field'=> array('kr.*', 'p.nama as nama_pegawai'),
              'join' => array(
                  array('pegawai p', 'kr.pegawai = p.id')
              ),
              'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListPegawai() {
  $data = Modules::run('database/get', array(
              'table' => 'pegawai p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Kasbon";
  $data['title_content'] = 'Tambah Kasbon';
  $data['list_pegawai'] = $this->getListPegawai();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataKasbon($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Kasbon";
  $data['title_content'] = 'Ubah Kasbon';
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataKasbon($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Kasbon";
  $data['title_content'] = 'Detail Kasbon';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['no_kasbon'] = Modules::run('no_generator/generateNoFakturKasbon');
  $data['keterangan'] = $value->keterangan;
  $data['tanggal_faktur'] = date('Y-m-d', strtotime($value->tanggal_faktur));
  $data['tanggal_bayar'] = date('Y-m-d', strtotime($value->tanggal_bayar));
  $data['pegawai'] = $value->pegawai;
  $data['total'] = str_replace('.', '', $value->total);
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post_data = $this->getPostDataHeader($data);

   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_data);

    //kasbon status
    $post_status['kasbon'] = $id;
    $post_status['status'] = 'DRAFT';
    Modules::run('database/_insert', 'kasbon_status', $post_status);


    //create jurnal akuntan
    $jurnal = Modules::run('generate_jurnal/insertJurnal', $post_data['no_kasbon']);
    $jurnal_struktur = Modules::run('generate_jurnal/getJurnalStruktur', 'Kasbon');

    if (!empty($jurnal_struktur)) {
     foreach ($jurnal_struktur as $value) {
      $post_detail['jurnal'] = $jurnal;
      $post_detail['jurnal_struktur'] = $value['id'];
      $post_detail['jumlah'] = str_replace('.', '', $data->total);
      Modules::run('generate_jurnal/insertJurnalDetail', $post_detail);
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Kasbon";
  $data['title_content'] = 'Data Kasbon';
  $content = $this->getDataKasbon($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
