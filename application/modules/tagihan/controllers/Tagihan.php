<?php

class Tagihan extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'tagihan';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
  '<script src="' . base_url() . 'assets/js/controllers/tagihan.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'tagihan';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Tagihan";
  $data['title_content'] = 'Data Tagihan';
  $content = $this->getDataTagihan();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataTagihan($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('t.tagihan', $keyword)
   );
  }
  $total = Modules::run('database/count_all', array(
  'table' => $this->getTableName() . ' t',
  'field' => array('t.*'),
  'like' => $like,
  'is_or_like' => true,
  'where' => "t.deleted is null or t.deleted = 0"
  ));

  return $total;
 }

 public function getDataTagihan($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('t.tagihan', $keyword)
   );
  }
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' t',
  'field' => array('t.*'),
  'like' => $like,
  'is_or_like' => true,
  'limit' => $this->limit,
  'offset' => $this->last_no,
  'where' => "t.deleted is null or t.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
  'data' => $result,
  'total_rows' => $this->getTotalDataTagihan($keyword)
  );
 }

 public function getDetailDataTagihan($id) {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' t',
  'where' => "t.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Tagihan";
  $data['title_content'] = 'Tambah Tagihan';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataTagihan($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Tagihan";
  $data['title_content'] = 'Ubah Tagihan';
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataTagihan($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Tagihan";
  $data['title_content'] = 'Detail Tagihan';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['tagihan'] = $value->tagihan;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);
//  die;
  $id = $this->input->post('id');
  $is_valid = false;
  $tagihan = $id;
  $this->db->trans_begin();
  try {
   $post_tagihan = $this->getPostDataHeader($data);
   if ($id == '') {
    $tagihan = Modules::run('database/_insert', $this->getTableName(), $post_tagihan);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_tagihan, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'tagihan' => $tagihan));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Tagihan";
  $data['title_content'] = 'Data Tagihan';
  $content = $this->getDataTagihan($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }
}
