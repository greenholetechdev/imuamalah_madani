<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Notif</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Nama Notif
     </div>
     <div class='col-md-3'>
      <?php echo $nama_notif ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Jadwal Notif
     </div>
     <div class='col-md-3'>
      <?php echo $jadwal_notif ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Jenis Notif
     </div>
     <div class='col-md-3'>
      <?php echo $jenis_notif ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-4'>
      <u>Jenis Pengiriman</u>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class='col-md-12'>
      <table class="table table-striped table-bordered table-list-draft">
       <thead>
        <tr class="bg-primary-light text-white">
         <th>Pengiriman</th>
         <th class="text-center">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php foreach ($jenis_pengiriman as $v_p) { ?>
         <tr>
          <td><?php echo $v_p['jenis'] ?></td>
          <td class="text-center">
           <?php $check = ""; ?>
           <?php $id = ""; ?>
           <?php if (isset($detail)) { ?>
            <?php foreach ($detail as $v_d) { ?>
             <?php if ($v_d['jenis_notif_pengiriman'] == $v_p['id']) { ?>
              <?php $check = "checked" ?>
              <?php $id = $v_d['id']; ?>
             <?php } ?>
            <?php } ?>
           <?php } ?>
           <input disabled <?php echo $check ?> id_jalur="<?php echo $id ?>" <?php echo $v_p['status'] == 1 ? '' : 'disabled' ?> type='checkbox' name='' id='check' class='' value='<?php echo $v_p['id'] ?>'/>
          </td>
         </tr>
        <?php } ?>
       </tbody>
      </table>

     </div>
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-baru" onclick="JatuhTempo.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
